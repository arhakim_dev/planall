<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UniversityReview extends Model
{

    protected $fillable = [
        'university_id', 'name', 'rate', 'description'
    ];

    public function university(){
        return $this->belongsTo('App\University');
    }

    public function user(){
        return $this->belongsTo('App\user');
    }

    public function scopeApproved($query){
        return $query->where('is_approved', 1);
    }
}
