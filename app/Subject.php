<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subject extends Model{
	use SoftDeletes;
	
	protected $casts = [
		'type' => 'array',
	];

	
	public function getUrlAttribute(){
		return 'search/universities?major='.$this->id.'&country=&degree=undergraduate&tuition=1500,500000&is_public=&sort=ranking';
	}

	
	public function scopeType($query, $type, $isUniversity = true){
		if($isUniversity)
			return $query->where('university_subjects.type', $type)->orderBy('name');
		else
			return $query->whereRaw('type LIKE "%'.$type.'%"')->orderBy('name');
	}

	public function universities(){
		return $this->belongsToMany('App\University', 'university_subjects');
	}
}
