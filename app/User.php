<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use App\Notifications\Auth\UserResetPasswordMail;

class User extends Authenticatable{
	
	use SoftDeletes, Notifiable;
	
	protected $hidden = [
		'password', 'remember_token',
	];
	
	protected $fillable = [
		'first_name', 'last_name', 'profession', 'email', 'birth_date', 'country_code', 'gender', 'number', 'interest_course', 'interest_location', 'dream_university', 'facebook', 'twitter', 'instagram', 'password', 'phone', 'about', 'activation_code',
	];
	
	public function sendPasswordResetNotification($token){
		$this->notify(new UserResetPasswordMail($token));
	}
	
	public function getFullNameAttribute(){
		return $this->first_name.' '.$this->last_name;
	}
	
	public function getAvatarAttribute($value){
		return empty($value)? asset('img/avatar.png') : $value;
	}
	
	public function getHeroImageAttribute($value){
		// return empty($value)? asset('img/hero-profile.png') : \Storage::url($this->hero_image);
		return empty($value)? asset('img/hero-profile.png') : asset($this->hero_image);
	}
	
	public function getUrlAttribute(){
		return 'user/'.$this->id.'.'.strtotime($this->created_at).'-'.str_slug($this->fullName);
	}
	
	public function roles(){
		return $this->belongsToMany('App\Role', 'user_roles');
	}
	
	public function friends(){
		return $this->hasMany('App\Friendship');
	}
	
	public function universities(){
		return $this->belongsToMany('App\University', 'shortlists', 'user_id', 'table_id')
			->wherePivot('table', 'universities');
	}
	
	public function services(){
		return $this->belongsToMany('App\Service', 'shortlists', 'user_id', 'table_id')
			->wherePivot('table', 'services');
	}
	
	public function posts(){
		return $this->hasMany('App\Post');
	}
	
	public function postComments(){
		return $this->hasMany('App\PostComment');
	}
	
	public function events(){
		return $this->belongsToMany('App\Event', 'user_events');
	}
	
	public function country(){
		return $this->belongsTo('App\Country', 'country_code', 'code');
	}

	public function socials(){
		return $this->hasMany('App\UserSocial');
	}

	public function access($module = ''){
		if($module){
			$module = strtolower(str_singular($module));
			if(!in_array($module, ['university', 'service']))
				return collect([]);
			
			$model = ($module == 'university')? 'App\University' : 'App\Service';
			
			return $this->belongsToMany($model, 'user_accesses', 'user_id', 'access_id')
				->wherePivot('module', $module);

		}else{
			return [
				'universities' => $this->belongsToMany('App\University', 'user_accesses', 'user_id', 'access_id')
					->wherePivot('module', 'university'),

				'services' => $this->belongsToMany('App\Service', 'user_accesses', 'user_id', 'access_id')
				->wherePivot('module', 'service'),
			];
		}

	}

}
