<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventMedia extends Model
{

	protected $fillable = [
		'event_id', 'media', 'type'
	];
	
  public function event(){
		return $this->belongsTo('App\Event');
	}
}
