<?php
namespace App\Support\Foundation;

use Validator;
use Illuminate\Http\Request;

trait Uploader{
	
	private $type;
	private $path;
	private $fileName;

	protected function uploadFile(Request $request, $type){
		$isSuccess = false;

		$this->type = $type;
		$this->setPath();

		$validator = $this->validateFile($request->all());
		if($validator !== true){
			return $validator;
		}

		$file = $request->file('file');
		$extension = $file->getClientOriginalExtension();
		$fileName = str_slug(
			str_limit(
				str_replace('.'.$extension, '', $file->getClientOriginalName())
			, 10)
		);		
		$this->fileName = time().'~'.$fileName.'~.'.$extension;

		// using jquery cropit. base64 format
		if($this->type == 'logo'){
			$image = imagecreatefrompng($file);
			imagejpeg($image, public_path($this->path.$this->fileName), 100);
			imagedestroy($image);
			$isSuccess = (boolean)\File::exists($this->path.$this->fileName);

		}else{
			$file->move(public_path($this->path), $this->fileName);
			$isSuccess = (boolean) $file;

		}

		if($isSuccess){
			return $this->successUploadResponse($request);
		}
		
		return response()->json([
			'status' => 'error',
			'message' => 'Failed uploading file'
		], 422);
	}

	protected function removeFile(Request $request, $type){
		$this->type = $type;

		@unlink($request->file);

		$response = [
			'status' => 'success',
			'message' => '',
		];

		if($this->type=='hero'){
			$response['fn'] = [
				'form-reset' => '#form-hero-image',
				'hide-element' => [
					'target' => '#form-hero-image'
				],
				'style-css' => [
					'target' => '.hero-area',
					'styles' => [[
						'key' => 'backgroundImage',
						'value' => 'url(../img/blank-image.jpg)'
					]]
				],
			];
		}

		return response()->json($response);
	}

	protected function setPath(){
		if($this->type == 'editor')
			$this->path = 'files/post/';
		else
			$this->path = 'files/'.$this->type.'/';
	}

	protected function validateFile($data){
		if($this->type == 'hero'){
			$validator = Validator::make($data, [
				'file' => 'required|image|mimes:jpeg,png,jpg|dimensions:min_width=900,min_height=400'
			],[
				'dimensions' => "The :attribute has invalid image dimensions.\n It should have 900x400 pixels for the minimum size"
			]);

		}elseif($this->type == 'logo' || $this->type == 'avatar'){
			$validator = Validator::make($data, [
				'file' => 'required|image|mimes:jpeg,png,jpg|dimensions:min_width=200,min_height=200,ratio=1/1'
			],[
				'dimensions' => "The :attribute has invalid image dimensions.\n It should have 200x200 pixels for the minimum size and 1:1 for the ration"
			]);

		}elseif($this->type == 'ppt'){
			$validator = Validator::make($data, [
					'file' => 'required|mimes:ppt,pptx'
			],[
					'mimes' => "The :attribute should be a power-point format. Or please try to re-save the file using .ppt extension"
			]);

		}elseif($this->type == 'excel'){
			$validator = Validator::make($data, [
					'file' => 'required|mimes:xls,xlsx'
			],[
					'mimes' => "The :attribute should be in a spreadsheet format. Or please try to re-save the file using .xls extension"
			]);
	}else{
			$validator = Validator::make($data, [
				'file' => 'required|image|mimes:jpeg,png,jpg|dimensions:min_width=200,min_height=200'//,max_width=500,max_height=500'
			]);
		}

		if($validator->fails()){
			return response()->json([
				'status' => 'error',
				'message' => 'Failed uploading file',
				'errors' => $validator->errors()
			], 422);
		}

		return true;
	}

	protected function successUploadResponse(Request $request){
		$fn = [];
		if($this->type == 'hero'){
			$fn = [
				'style-css' => [
					'target' => '.hero-area',
					'styles' => [[
						'key' => 'backgroundImage',
						'value' => 'url('.$this->path.$this->fileName.')'
					]]
				],
				'update-content' => [
					'target' => '#hero-image-file',
					'content' => $this->fileName
				],
				'set-attribute' => [
					'target' => 'input[name="hero_image"]',
					'attribute' => [
						'name' => 'value',
						'value' => $this->path.$this->fileName
					],
				],
				'set-data-attribute' => [
					'target' => '#form-hero-image .remove-file',
					'data' => [[
						'name' => 'file',
						'value' => $this->path.$this->fileName
					]]
				],
				'show-element' => [
					'target' => '#form-hero-image'
				]
			];

		}elseif($this->type == 'logo'){
			$fn = [					
				'set-attribute' => [
					[
						'target' => '#logo',
						'attribute' => [
							'name' => 'src',
							'value' => $this->path.$this->fileName
						]
					],
					[
						'target' => '#top-menu img#user-avatar',
						'attribute' => [
							'name' => 'value',
							'value' => $this->path.$this->fileName
						]
					]
				],
				'modal-close' => '#modal-logo',
			];

		}elseif($this->type == 'avatar'){
			$fn = [
				'set-attribute' => [[
					'target' => 'img.'.$request->photo,
					'attribute' => [
						'name' => 'src',
						'value' => $this->path.$this->fileName
					]
				],[
					'target' => 'input[type="hidden"].'.$request->photo,
					'attribute' => [
						'name' => 'value',
						'value' => $this->path.$this->fileName
					]
				]]
			];
		}

		return response()->json([
			'status' => 'success',
			'message' => '',
			'location' => $this->path.$this->fileName,
			'file' => $this->path.$this->fileName,
			'fn' => $fn
		]);
	}

}
