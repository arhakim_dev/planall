<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UniversityAfterCollege extends Model{
    
    protected $table = 'university_aftercolleges';
}
