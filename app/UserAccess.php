<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAccess extends Model{

	public function university(){
		return $this->belongsTo('App\University', 'access_id');
	}
	public function service(){
		return $this->belongsTo('App\Service', 'access_id');
	}
}
