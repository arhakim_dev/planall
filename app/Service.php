<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model{
	
	use SoftDeletes;

	protected $fillable = [
		'name', 'category_id', 'description', 'address', 'contact_phone', 'contact_name', 'founding_year', 'website', 'open_at', 'closed_at', 'is_delivery', 'logo', 'hero_image', 'facebook', 'twitter', 'youtube','linkedin','country_code', 'city', 'slug', 'longitude', 'latitude','is_featured'
	];

	public function getFacebookAttribute($value){
		return !empty($value)
			? 'https'.str_replace('http', '', str_replace('https', '', $value))
			: '';
	}
	public function getTwitterAttribute($value){
		return !empty($value)
			? 'https'.str_replace('http', '', str_replace('https', '', $value))
			: '';
	}
	public function getLinkedinAttribute($value){
		return !empty($value)
			? 'https'.str_replace('http', '', str_replace('https', '', $value))
			: '';
	}
	public function getYoutubeAttribute($value){
		return !empty($value)
			? 'https'.str_replace('http', '', str_replace('https', '', $value))
			: '';
	}
	public function getUrlAttribute(){
		return 'service/'.strtolower($this->country_code).'/'.$this->slug;
	}
	public function getLogoAttribute($value){
		return empty($value)? asset('img/blank-image.jpg') : $value;
	}
	public function getViewsAttribute(){
		return $this->insight()->where('type', 'views')->count();
	}
	public function getRateAttribute(){
		$totalRate = 0;
		$totalVote = 0;		
		for($i = 1; $i <= 5; $i++){
			$vote = $this->reviews()->where('rate', $i)->pluck('rate')->count();
			if($vote == 0) continue;
			$totalVote += $vote;
			$totalRate += $vote * $i;
		}

		if($totalRate > 0 && $totalVote > 0)
			return $totalRate / $totalVote;
		else
			return 0;
	}

	
	public function users(){
		return $this->belongsToMany('App\User', 'shortlists', 'table_id', 'user_id')
		->wherePivot('table', 'services');
	}
	public function reviews(){
		return $this->hasMany('App\ServiceReview');
	}
	public function category(){
		return $this->belongsTo('App\ServiceCategory');
	}
	public function businessHours(){
		return $this->hasMany('App\ServiceBusinessHour');
	}
	public function insight(){
		return $this->hasMany('App\Insight', 'table_id')->where('table', 'services');
	}
	public function country(){
		return $this->belongsTo('App\Country', 'country_code', 'code');
	}
	public function access(){
		return $this->belongsToMany('App\User', 'user_accesses', 'access_id', 'user_id')
		->wherePivot('module', 'service');
	}
}
