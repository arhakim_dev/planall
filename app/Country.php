<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = [
        'code', 'code3', 'name', 'region', 'image', 'currency_code', 'currency_symbol'
    ];
    
    public function weathers(){
        return $this->hasMany('App\Weather', 'country_code', 'code');
    }
    
	public function getUrlAttribute(){
		return 'search/universities?major=&country='.$this->code.'&degree=undergraduate&tuition=1500,500000&is_public=&sort=ranking';
	}
}
