<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UniversityProgram extends Model{
  
	public function university(){
		return $this->belongsTo('App\University');
	}	
	public function category(){
		return $this->belongsTo('App\UniversityProgramCategory');
	}
	
}
