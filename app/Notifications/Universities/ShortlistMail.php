<?php

namespace App\Notifications\Universities;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ShortlistMail extends Notification{
	use Queueable;

	private $user;
	private $university;

	public function __construct($user, $university){
		$this->user = $user;
		$this->university = $university;
	}

	public function via($user){
		return ['mail'];
	}

	public function toMail($user){
		$admin = $user;

		return (new MailMessage)
			->subject('['.config('app.name').'] University Shortlisted')
			->markdown('emails.universities.shortlist', [
				'admin' => $admin, 
				'user' => $this->user, 
				'university' => $this->university,
			]);
	}
}
