<?php

namespace App\Notifications\Universities;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ReviewCreatedNotifyAdminMail extends Notification{
	use Queueable;

	private $university;
	private $reviewer;

	public function __construct($university, $reviewer){
		$this->university = $university;
		$this->reviewer = $reviewer;
	}

	public function via($user){
		return ['mail'];
	}

	public function toMail($user){
		$admin = $user;

		return (new MailMessage)
			->subject('['.config('app.name').'] New Review On Univesity: '.$this->university->name)
			->markdown('emails.universities.review-created-notify-admin', [
				'admin' => $admin, 
				'university' => $this->university,
				'reviewer' => $this->reviewer
			]);
	}

}
