<?php

namespace App\Notifications\Posts;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CommentCreatedNotifyAdminMail extends Notification{
	use Queueable;

	private $comment;
	private $post;

	public function __construct($comment, $post){
		$this->comment = $comment;
		$this->post = $post;
	}

	public function via($user){
		return ['mail'];
	}

	public function toMail($user){
		$admin = $user;

		return (new MailMessage)
			->subject('['.config('app.name').'] New Comment On '.ucfirst($this->post->type))
			->markdown('emails.posts.comment-created-notify-admin', [
				'admin' => $admin, 
				'post' => $this->post, 
				'comment' => $this->comment
			]);
	}

}
