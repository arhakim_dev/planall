<?php

namespace App\Notifications\Posts;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PostCreatedNotifyAdminMail extends Notification{
	use Queueable;

	private $post;

	public function __construct($post){
		$this->post = $post;
	}

	public function via($user){
		return ['mail'];
	}

	public function toMail($user){
		$admin = $user;

		return (new MailMessage)
			->subject('['.config('app.name').'] New '.ucfirst($this->post->type).' Created')
			->markdown('emails.posts.post-created-notify-admin', ['admin' => $admin, 'post' => $this->post]);
	}
}
