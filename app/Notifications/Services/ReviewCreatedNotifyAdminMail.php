<?php

namespace App\Notifications\Services;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ReviewCreatedNotifyAdminMail extends Notification{
		use Queueable;
		
		private $service;
		private $reviewer;

		public function __construct($service, $reviewer){
			$this->service = $service;
			$this->reviewer = $reviewer;
		}

    public function via($user){
        return ['mail'];
    }

    public function toMail($user){
			$admin = $user;
	
			return (new MailMessage)
				->subject('['.config('app.name').'] New Review On Service Provider: '.$this->service->name)
				->markdown('emails.services.review-created-notify-admin', [
					'admin' => $admin, 
					'service' => $this->service,
					'reviewer' => $this->reviewer
				]);
    }
}
