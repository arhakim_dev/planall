<?php

namespace App\Notifications\Services;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ShortlistMail extends Notification{
	use Queueable;

	private $user;
	private $service;

	public function __construct($user, $service){
		$this->user = $user;
		$this->service = $service;
	}

	public function via($user){
		return ['mail'];
	}

	public function toMail($user){
		$admin = $user;

		return (new MailMessage)
			->subject('['.config('app.name').'] University Shortlisted')
			->markdown('emails.services.shortlist', [
				'admin' => $admin, 
				'user' => $this->user, 
				'service' => $this->service,
			]);
	}
}
