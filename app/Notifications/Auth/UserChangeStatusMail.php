<?php

namespace App\Notifications\Auth;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserChangeStatusMail extends Notification implements ShouldQueue{
	use Queueable;

	private $status;

	public function __construct($status){
		$this->status = $status;
	}

	public function via($user){
		return ['mail'];
	}

	public function toMail($user){
		return (new MailMessage)
			->subject('['.config('app.name').'] Account Changes')
			->markdown('emails.auth.user-change-status', ['user' => $user, 'status' => $this->status]);
	}

}
