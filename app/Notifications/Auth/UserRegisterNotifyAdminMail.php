<?php

namespace App\Notifications\Auth;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserRegisterNotifyAdminMail extends Notification implements ShouldQueue{
	use Queueable;

	private $user;

	public function __construct($user){
		$this->user = $user;
	}

	public function via($user){
		return ['mail'];
	}

	public function toMail($user){
		$admin = $user;

		return (new MailMessage)
			->subject('['.config('app.name').'] New User Registered')
			->markdown('emails.auth.register-notify-admin', ['admin' => $admin, 'user' => $this->user]);
	}

}
