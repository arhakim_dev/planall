<?php

namespace App\Notifications\Auth;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserRegisterMail extends Notification{
	use Queueable;

	public function __construct(){
	}

	public function via($user){
			return ['mail'];
	}

	public function toMail($user)	{
		return (new MailMessage)
			->subject('['.config('app.name').'] Registration - Activate Your Account')
			->markdown('emails.auth.register', ['user' => $user]);		
	}

}
