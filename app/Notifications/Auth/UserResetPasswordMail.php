<?php

namespace App\Notifications\Auth;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserResetPasswordMail extends Notification{
	use Queueable;
	
	private $token;

	public function __construct($token){
		$this->token = $token;
	}

	public function via($user){
		return ['mail'];
	}

	public function toMail($user) {
			return (new MailMessage)
				->subject('['.config('app.name').'] Reset Password')
				->markdown('emails.auth.reset-password', [
					'user' => $user,
					'token' => $this->token,
				]);	
	}
	
}
