<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NotifyWeb extends Notification{
	use Queueable;

	private $data;

	public function __construct(Array $data){
		$this->data = $data;
	}

	public function via($notifiable){
		return ['database'];
	}

	public function toArray($notifiable) {
		return $this->data;
	}
}
