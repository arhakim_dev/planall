<?php

namespace App\Http\Controllers;

use Validator;
use App\Subscriber;
use App\Country;
use App\University;
use App\Post;
use App\Event;
use App\ServiceCategory;
use App\Testimonial;
use App\Subject;
use App\Notification;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class HomeController extends Controller{

	const NEXT_SUBJECT_URL = 'home/majors/next';
	const NEXT_COUNTRY_URL = 'home/countries/next';
	const NEXT_UNIVERSITY_URL = 'home/universities/next';
	const NEXT_EVENT_URL = 'home/events/next';
	const NEXT_BLOG_URL = 'blog/latest';
	const NEXT_FORUM_URL = 'forum/latest';
	const NEXT_TESTIMONIAL_URL = 'home/testimonials/next';
	
	private $offset = 8;
	private $limit = 8;

	public function __construct(){
			// $this->middleware('auth');
	}

	public function index(){
		$services = ServiceCategory::where('id', '<>', 3)->get();
		
		$universities = University::featured()->orderBy('name')->limit($this->limit - 2);
		$nextUniversitiesURL = ($universities->count() > ($this->limit - 2))
		? self::NEXT_UNIVERSITY_URL.'?offset='.($this->offset - 2).'&limit='.$this->limit : '';
		$universities = $universities->get();

		$totalSubject = Subject::count();
		$_subjects = Subject::where('sort', '>', 0)->orderBy('sort')->get();
		$subjects = Subject::where('sort', 0)->orderBy('name')->get();
		$subjects = $_subjects->merge($subjects);
		$subjects = $subjects->take($this->limit - 2);
		$nextSubjectsURL = ($totalSubject > ($this->limit - 2))
		? self::NEXT_SUBJECT_URL.'?offset='.($this->offset - 2).'&limit='.($this->limit - 2) : '';
		
		$countryCodes = University::where('country_code', '!=', NULL)
			->groupBy('country_code')->pluck('country_code');
		$countries = Country::whereIn('code', $countryCodes)->orderBy('name')->limit($this->limit - 2);
		$nextCountriesURL = ($countries->count() > ($this->limit - 2))
		? self::NEXT_COUNTRY_URL.'?offset='.($this->offset - 2).'&limit='.$this->limit : '';
		$countries = $countries->get();

		$events = Event::active()->orderBy('start_date')->limit($this->limit - 4);
		$nextEventsURL = ($events->count() > ($this->limit - 4))
		? self::NEXT_EVENT_URL.'?offset='.($this->offset - 4).'&limit='.$this->limit : '';
		$events = $events->get();
		
		$forums = Post::forum()->whereStatus('approved')->latest()->limit($this->limit- 2);
		$nextForumURL = ($forums->count() > ($this->limit - 2))
		? self::NEXT_FORUM_URL.'?offset='.($this->offset - 2).'&limit='.$this->limit : '';
		$forums = $forums->get();
		$sortForumURL = 'forum/latest?ref=header&offset=0&limit='.($this->limit - 2);
		
		$blogs = Post::blog()->orderBy('updated_at', 'DESC')->limit($this->limit - 2);
		$nextBlogsURL = ($blogs->count() > ($this->limit - 2))
		? self::NEXT_BLOG_URL.'?offset='.($this->offset - 2).'&limit='.$this->limit : '';
		$blogs = $blogs->get();
		
		$testimonials = Testimonial::latest()->limit($this->limit - 4);
		$nextTestimonialsURL = ($blogs->count() > ($this->limit - 4))
		? self::NEXT_TESTIMONIAL_URL.'?offset='.($this->offset - 4).'&limit='.$this->limit : '';
		$testimonials = $testimonials->get();

		$userActivities = Notification::latest()->limit(6)->get();
		$userActivities = collect($this->_mapUserActivities($userActivities));
		$userActivitiesLatestTime = (@$userActivities->first()->time)
		? $userActivities->first()->time : strtotime(now());
		$userActivities = $userActivities->reverse();

		return view('index', [
			'META' => [
				'title'=> config('app.name'),
				'general' => [
					[
						'name' => 'description', 
						'content' => 'Permisit aliud locoque fuit dedit dissociata figuras pressa quam.'
					],
					[
						'name' => 'keywords', 
						'content' => 'Instabilis, quisque, phoebe, terra, duae'
					],
				],
				'og' => [
					['property' => 'og:title', 'content' => config('app.name')],
					['property' => 'og:image', 'content' => asset('img/logo-blue.png')],
					['property' => 'og:image:alt', 'content' => config('app.name')],
					['property' => 'og:site_name', 'content' => config('app.name')],
					['property' => 'og:description', 'content' => 'Permisit aliud locoque fuit dedit dissociata figuras pressa quam.'],
				]
			],
			'services' => $services,
			'universities' => $universities,
			'majors' => $subjects,
			'countries' => $countries,
			'userActivities' => $userActivities,
			'userActivitiesLatestTime' => $userActivitiesLatestTime,
			'events' => $events,
			'forums' => $forums,
			'blogs' => $blogs,
			'testimonials' => $testimonials,

			'nextMajorsURL' => $nextSubjectsURL,
			'nextCountriesURL' => $nextCountriesURL,
			'nextUniversitiesURL' => $nextUniversitiesURL,
			'nextEventsURL' => $nextEventsURL,
			'nextBlogsURL' => $nextBlogsURL,
			'nextForumURL' => $nextForumURL,
			'sortForumURL' => $sortForumURL,
			'nextTestimonialsURL' => $nextTestimonialsURL,
		]);
	}
	
	public function subscribe(Request $request){
		$validator = Validator::make($request->all(), [
			'email' => 'required|email'
		]);
		
		if($validator->fails()){
			return response()->json([
				'status' => 'error',
				'message' => 'Incorrect email address',
				'errors' => $validator->errors()
			], 422);
		}
		
		if(Subscriber::create($request->all())){
			return response()->json([
				'status' => 'success',
				'message' => 'Thank you',
				'fn' => [
					'form-reset' => '#form-subscriber'
				]
			]);
		}
		
		return response()->json([
			'status' => 'error',
			'message' => 'Something went wrong'
		], 422);
	}

	public function toc(){
		return view('terms-conditions', [
			'META' => [
				'title'=> config('app.name'),
				'general' => [
					[
						'name' => 'description', 
						'content' => 'Permisit aliud locoque fuit dedit dissociata figuras pressa quam.'
					],
					[
						'name' => 'keywords', 
						'content' => 'Instabilis, quisque, phoebe, terra, duae'
					],
				],
				'og' => [
					['property' => 'og:title', 'content' => config('app.name')],
					['property' => 'og:image', 'content' => asset('img/logo-blue.png')],
					['property' => 'og:image:alt', 'content' => config('app.name')],
					['property' => 'og:site_name', 'content' => config('app.name')],
					['property' => 'og:description', 'content' => 'Permisit aliud locoque fuit dedit dissociata figuras pressa quam.'],
				]
			],
			
		]);
	}
	
	public function policy(){
		return view('privacy-policy');
	}
	
	public function nextMajors(Request $request){		
		$totalSubject = Subject::count();
		$_subjects = Subject::where('sort', '>', 0)->orderBy('sort')->get();
		$subjects = Subject::where('sort', 0)->orderBy('name')->get();
		$subjects = $_subjects->merge($subjects);

		$majors = $subjects->slice($request->offset)->take($request->limit);
		$view = NULL;
		if($majors->count()){
			foreach($majors as $major){
				$view .= '<div class="col-md-4">';
				$view .= View::make('_partials/box-hover', ['title' => $major->name, 'url' => $major->url])->render();
				$view .= '</div>';
			}
		}
		
		if(!empty($view)){
			return response()->json([
				'status' => 'success',
				'fn' => [
					'append-element' => [
						'container' => '#major-container',
						'content' => $view
					],
					'set-attribute' => [
						'target' => '#next-majors',
						'attribute' => [
							'name' => 'href',
							'value' => self::NEXT_SUBJECT_URL.'?offset='.($request->offset + $majors->count()).'&limit='.$request->limit
						]
					],
					'add-class'=> [
						'target' => '#major-loader-container',
						'class' => 'd-flex justify-content-center'
					],
					'show-element' => [
						'target' => '#major-loader-container .collapse-to'
					]
				]
			]);
		}else{
			return response()->json([
				'status' => 'error',
				'message' => 'All data already displayed',
				'fn' => [
					'remove-class'=> [
						'target' => '#major-loader-container',
						'class' => 'd-flex justify-content-center'
					],
					'hide-element' => [
						'target' => '#major-loader-container'
					]
				]
			], 422);
		}
	}

	public function nextCountries(Request $request){
		$countryCodes = University::where('country_code', '!=', NULL)
			->groupBy('country_code')->pluck('country_code');
		$countries = Country::whereIn('code', $countryCodes)
			->orderBy('name')
			->offset($request->query('offset'))
			->limit($request->query('limit'))
			->get();
		
		$view = NULL;
		if($countries->count()){
			foreach($countries as $country){
				$view .= '<div class="col-md-4">';
				$view .= View::make('_partials/box-hover', ['title' => $country->name, 'url' => $country->url_universities])->render();
				$view .= '</div>';
			}
		}
		
		if(!empty($view)){
			return response()->json([
				'status' => 'success',
				'fn' => [
					'append-element' => [
						'container' => '#country-container',
						'content' => $view
					],
					'set-attribute' => [
						'target' => '#next-countries',
						'attribute' => [
							'name' => 'href',
							'value' => self::NEXT_COUNTRY_URL.'?offset='.($request->query('offset') + $countries->count()).'&limit='.$this->limit
						]
					],
					'add-class'=> [
						'target' => '#country-loader-container',
						'class' => 'd-flex justify-content-center'
					],
					'show-element' => [
						'target' => '#country-loader-container .collapse-to'
					]
				]
			]);
		}else{
			return response()->json([
				'status' => 'error',
				'message' => 'All data already displayed',
				'fn' => [
					'remove-class'=> [
						'target' => '#country-loader-container',
						'class' => 'd-flex justify-content-center'
					],
					'hide-element' => [
						'target' => '#country-loader-container'
					]
				]
			], 422);
		}
	}

	public function nextUniversities(Request $request){
		$universities = University::orderBy('name')
			->offset($request->query('offset'))
			->limit($request->query('limit'))
			->get();
		
		$view = NULL;
		if($universities->count()){
			foreach($universities as $university){
				$view .= '<div class="col-md-6 mb-5">';
				$view .= View::make('universities._box-info', ['university' => $university, 'style' => 'height:360px;'])->render();
				$view .= '</div>';
			}
		}
		
		if(!empty($view)){
			return response()->json([
				'status' => 'success',
				'fn' => [
					'append-element' => [
						'container' => '#university-container',
						'content' => $view
					],
					'set-attribute' => [
						'target' => '#next-universities',
						'attribute' => [
							'name' => 'href',
							'value' => self::NEXT_UNIVERSITY_URL.'?offset='.($request->query('offset') + $universities->count()).'&limit='.$this->limit
						]
					],
					/* 'show-element' => [
						'target' => '#major-loader-container .collapse-to'
					] */
				]
			]);
		}else{
			return response()->json([
				'status' => 'error',
				'message' => 'All data already displayed',
				'fn' => [
					'hide-element' => [
						'target' => '#university-loader-container'
					]
				]
			], 422);
		}
	}

	public function nextEvents(Request $request){
		$events = Event::active()->orderBy('start_date')
			->offset($request->query('offset'))
			->limit($request->query('limit'))
			->get();
		
		$view = NULL;
		if($events->count()){
			foreach($events as $event){
				$view .= View::make('_partials.list-event', ['event' => $event])->render();
			}
		}
		
		if(!empty($view)){
			return response()->json([
				'status' => 'success',
				'fn' => [
					'append-element' => [
						'container' => '#event-container',
						'content' => $view
					],
					'set-attribute' => [
						'target' => '#next-events',
						'attribute' => [
							'name' => 'href',
							'value' => self::NEXT_EVENT_URL.'?offset='.($request->query('offset') + $events->count()).'&limit='.$this->limit
						]
					],
					'add-class'=> [
						'target' => '#event-loader-container',
						'class' => 'd-flex justify-content-center'
					],
					'show-element' => [
						'target' => '#event-loader-container .collapse-to'
					]
				]
			]);
		}else{
			return response()->json([
				'status' => 'error',
				'message' => 'All data already displayed',
				'fn' => [
					'add-class'=> [
						'target' => '#event-loader-container',
						'class' => 'd-flex justify-content-center'
					],
					'hide-element' => [
						'target' => '#event-loader-container'
					]
				]
			], 422);
		}
	}

	public function nextTestimonials(Request $request){
		$testimonials = Testimonial::latest()
			->offset($request->query('offset'))
			->limit($request->query('limit'))
			->get();
		
		$view = NULL;
		if($testimonials->count()){
			foreach($testimonials as $testimonial){
				$view .= View::make('_partials/card-testimonial', ['testimonial' => $testimonial])->render();
			}
		}
		
		if(!empty($view)){
			return response()->json([
				'status' => 'success',
				'fn' => [
					'append-element' => [
						'container' => '#testimonial-container',
						'content' => $view
					],
					'set-attribute' => [
						'target' => '#next-testimonials',
						'attribute' => [
							'name' => 'href',
							'value' => self::NEXT_TESTIMONIAL_URL.'?offset='.($request->query('offset') + $testimonials->count()).'&limit='.$this->limit
						]
					],
					'add-class'=> [
						'target' => '#testimonial-loader-container',
						'class' => 'd-flex justify-content-center'
					],
					'show-element' => [
						'target' => '#testimonial-loader-container .collapse-to'
					]
				]
			]);
		}else{
			return response()->json([
				'status' => 'error',
				'message' => 'All data already displayed',
				'fn' => [
					'add-class'=> [
						'target' => '#testimonial-loader-container',
						'class' => 'd-flex justify-content-center'
					],
					'hide-element' => [
						'target' => '#testimonial-loader-container'
					]
				]
			], 422);
		}
	}
	
	private function _mapUserActivities($_notifications){
		$notifications = [];

		if($_notifications->count()){
			foreach($_notifications as $notification){
				$data = json_decode($notification->data);
				
				foreach($data as $i => $notify){
					if($notify->show == 'user-activities'){
						$_notify = $notify->data;
						$_notify->time = strtotime($notification->created_at);
						$_notify->timeHuman = $notification->created_at->diffForHumans();
						array_push($notifications, $_notify);
					}
				}
			}
		}

		return $notifications;
	}

	public function userActivities(Request $request){
		$userActivities = Notification::where('created_at', '>', date('Y-m-d H:i:s', $request->latest))
			->take(1)
			->get();
		$notifications = $this->_mapUserActivities($userActivities);
		
		if($notifications){
			$data = $notifications[0];
			$latestTime = $data->time;

			return response()->json([
				'status' => 'success',
				'message' => '',
				'fn' => [
					'append-element' => [
						'container' => '#user-activities-container',
						'content' => View::make('_partials.list-user-activity', [
							'avatar' => $data->avatar,
							'name' => $data->name,
							'message' => $data->message,
							'timeHuman' => $data->timeHuman,
						])->render()
					],
					'set-data-attribute' => [
							'target' => '#user-activities-container',
							'data' => [
								'name' => 'latest',
								'value' => $latestTime
							]
					]
				]
			]);
		}
		return [];
	}

}
