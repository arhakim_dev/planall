<?php

namespace App\Http\Controllers;

use Validator;
use App\Role;
use App\ServiceReview;
use Illuminate\Http\Request;
use App\Notifications\Services\ReviewCreatedNotifyAdminMail;

class ServiceReviewController extends Controller{

	const ROLE_ADMIN = 8;
	
	public function __construct(){}

	public function save(Request $request){
		$validator = Validator::make($request->all(), [
			'name' => 'required|string',
			'rate' => 'required|numeric',
			'description' => 'required|string'
		]);
		
		if($validator->fails()){
			return response()->json([
				'status' => 'error',
				'message' => 'These fields are required',
				'errors' => $validator->errors()
			], 422);
		}
		
		$review = new ServiceReview;
		$review->service_id = $request->service_id;
		$review->user_id = \Auth::user()->id;
		$review->name = empty($request->name)? 'Annonymous' : $request->name;
		$review->rate = $request->rate;
		$review->description = $request->description;
		$review->is_approved = 0;

		if($review->save()){
			/* $service = Service::find($review->service_id);

			$administrators = Role::find(self::ROLE_ADMIN)->users()->get();
			if($administrators->count()){
				$delay = $when = now()->addMinutes(5);
				foreach($administrators as $admin){
					$admin->notify((new ReviewCreatedNotifyAdminMail($service, $review->user))->delay($delay));
				}
			} */

			return response()->json([
				'status' => 'success',
				'message' => 'Thank you, your review will be displayed when its approved',
				'fn' => [
					'form-reset' => '#form-review',
					'modal-close' => '#modal-review'
				]
			]);
		}else{
			return response()->json([
				'status' => 'error',
				'message' => 'Failed to add review'
			], 422);
		}
	}
}
