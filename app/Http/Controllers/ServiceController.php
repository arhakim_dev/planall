<?php

namespace App\Http\Controllers;

use App\Service;
use App\ServiceCategory;
use App\ServiceBusinessHour;
use App\Country;
use App\Insight;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class ServiceController extends Controller{

	private $limit = 8;
	private $offset = 8;

	public function __construct(){}

	public function index(Request $request, $country, $slug){
		$userLoggedin = \Auth::check();

		$service = Service::whereRaw('LOWER(country_code) = "'. strtolower($country).'"')
			->whereSlug($slug)->first();

		if(empty($service)) abort(404);
		
		if($userLoggedin && $request->ref == 'user-notif'){
			$user = \Auth::user();
			$notification = \App\Notification::find($request->i);
			
			if(!empty($notification) && $notification->notifiable_id == $user->id){
				if(empty($notification->read_at)){
					$notification->read_at = now();
					$notification->save();
				}
			}
		}

		$isShortlisted = false;
		$userHasAccess = false;
		if($userLoggedin){
			$isShortlisted = (boolean) $user->services()
				->wherePivot('table', 'services')
				->wherePivot('table_id', $service->id)
				->count();

			$userHasAccess = (boolean) $service->access()->whereUserId($user->id)->count();
		}
		if(empty($service->longitude) && empty($service->latitude)){
			$geocode = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.str_replace(' ', '+', $service->address).'&key='.env('GOOGLE_MAP_KEY')));
			$geocode = @$geocode->results[0];
			
			$service->longitude = @$geocode->geometry->location->lng;
			$service->latitude = @$geocode->geometry->location->lat;
			$service->save();
		}
		$this->insight($request, $service->id, 'views');
		$categories = ServiceCategory::orderBy('name')->get();
		$countries = Country::orderBy('name')->get();
		
		return view('services.page', [
			'service' => $service,
			'userHasAccess' => $userHasAccess,
			'isShortlisted' => $isShortlisted,
			'categories' => $categories,
			'countries' => $countries,
		]);
	}

	public function search(Request $request, $slug = ''){
		$limit = ($request->limit != '')? $request->limit : $this->limit;
		$offset = ($request->offset != '')? $request->offset : 0;

		if($request->ajax()){
			$countryCode = $request->country;
			$city = $request->city;
			
			$category = ServiceCategory::whereSlug($request->category)->first();
			$services = Service::selectRaw('services.*')
				->where('category_id', $category->id)
				->when($countryCode, function($query) use ($countryCode){
					return $query->where('country_code', $countryCode);
				})
				->when($city, function($query) use ($city){
					$city = strtolower(trim(str_replace('-', ' ', $city)));
					return $query->whereRaw("LOWER(city) = '".$city."'");
				})
				->when($request->sort == 'ratings', function($query){
					return $query->addSelect(\DB::Raw('COUNT(service_reviews.id) AS c'))
						->leftJoin('service_reviews', 'services.id', '=', 'service_reviews.service_id')
						->groupBy('services.id')
						->orderBy('c', 'DESC');
				})
				->when($request->sort == 'viewed', function($query){
					return $query->addSelect(\DB::Raw('COUNT(insights.id) AS c'))
						->leftJoin('insights', 'services.id', '=', 'insights.table_id')
							->where('insights.table', 'services')
							->where('insights.type', 'views')
						->groupBy('services.id')
						->orderBy('c', 'DESC');
				})
				->orderBy('services.is_featured', 'DESC')
				->orderBy('services.name')
				->offset($offset)
				->limit($limit)
				->get();
			
			return $this->searchResponseAjax($request, $services);
		}else{
			$category = ServiceCategory::whereSlug($slug)->first();
			$diffCategories = ServiceCategory::whereNotIn('id', [$category->id, 3, 8]);
			// $countries = Country::all();
			$countries = Service::selectRaw('services.country_code AS code, countries.name')
				->join('countries', 'services.country_code', '=', 'countries.code')
				->where('services.category_id', $category->id)
				->groupBy('code')
				->get();
				
			return view('services.index', [
				'category' => $category,
				'diffCategories' => $diffCategories,
				'countries' => $countries
			]);
		}
	}
	
	public function cities(Request $request){
		$cities = Service::where('country_code', $request->country)
			->groupBy('city')
			->pluck('city');
		$data = [];
		if($cities->count()){
			foreach($cities as $i => $city){
				$data[$i]['id'] = $city;
				$data[$i]['value'] = $city;
				$data[$i]['text'] = $city;
				$i++;
			}
		}

		return response()->json($data);
	}

	public function uploadFile(Request $request, $type){
		if($type == 'hero'){
			$validator = Validator::make($request->all(), [
				'file' => 'required|image|mimes:jpeg,png,jpg|dimensions:min_width=900,min_height=400'
			],[
				'dimensions' => "The :attribute has invalid image dimensions.\n It should have 900x400 pixels for the minimum size"
			]);
		}
		
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json([
				'status' => 'error',
				'message' => 'Failed uploading file',
				'errors' => $errors
			], 422);
		}

		$file = $request->file('file');
		$isSuccess = false;
		if($type == 'hero'){
			$fileName = time().'~'.strtolower($file->getClientOriginalName());
			$file->move(public_path('files/hero/'), $fileName);
			if($file) $isSuccess = true;

		}
		
		if($isSuccess){
			if($type == 'hero'){
				$fn = [
					'style-css' => [
						'target' => '.hero-area',
						'styles' => [[
							'key' => 'backgroundImage',
							'value' => 'url(files/hero/'.$fileName.')'
						]]
					],
					'update-content' => [
						'target' => '#hero-image-file',
						'content' => $fileName
					],
					'set-attribute' => [
						'target' => 'input[name="hero_image"]',
						'attribute' => [
							'name' => 'value',
							'value' => 'files/hero/'.$fileName
						],
					],
					'set-data-attribute' => [
						'target' => '#form-hero-image .remove-file',
						'data' => [[
							'name' => 'file',
							'value' => 'files/hero/'.$fileName
						]]
					],
					'show-element' => [
						'target' => '#form-hero-image'
					]
				];

			}
			
			return response()->json([
				'status' => 'success',
				'message' => '',
				'fn' => $fn
			]);
		}
		
		return response()->json([
			'status' => 'error',
			'message' => 'Failed uploading file'
		], 422);
	}

	public function removeFile(Request $request){
		@unlink($request->file);

		$fn = [
			'form-reset' => '#form-hero-image',
			'hide-element' => [
				'target' => '#form-hero-image'
			],
			'style-css' => [
				'target' => '.hero-area',
				'styles' => [[
					'key' => 'backgroundImage',
					'value' => 'url(../img/blank-image.jpg)'
				]]
			],
		];

		return response()->json([
			'status' => 'success',
			'message' => '',
			'fn' => $fn
		]);
	}
	
	public function update(Request $request, $serviceId){
		$service = Service::find($serviceId);
		if(empty($service)) return [];
		$data = collect($request->all());
		$fn = [];

		if($data->has('hours')){
			return $this->updateServiceBusinessHours($service, $data);
		}else{
			return $this->updateService($request, $service, $data);
		}
	}
	private function updateService(Request $request, $service, $data){
		$useValidation = false;
		if($data->has('hero_image')){
			$useValidation = true;
			$validator = Validator::make($request->all(), ['hero_image' => 'required']);

		}
		if($data->has('name')){
			$useValidation = true;
			$validator = Validator::make($request->all(), [
				'name'  => 'required',
			]);
			// $data['slug'] = str_slug($data['name']);

		}
		if($data->has('address')){
			$useValidation = true;
			$validator = Validator::make($request->all(), ['address'  => 'required']);

		}
		if($data->has('founding_year')){
			$useValidation = true;
			$validator = Validator::make($request->all(), ['founding_year' => 'required|numeric|digits:4']);

		}
		if($data->has('website') || $data->has('facebook') || $data->has('twitter')){
			$useValidation = true;
			$validator = Validator::make($request->all(), [
				'website' => ['nullable', 'regex:'.$this->ctrl__regexUrlPattern],
				'facebook' => ['nullable', 'regex:'.$this->ctrl__regexUrlPattern],
				'twitter' => ['nullable', 'regex:'.$this->ctrl__regexUrlPattern],
			]);
		}
		if($data->has('city')){
			$data['city'] = trim(title_case($data['city']));
		}
		
		if($useValidation && $validator->fails()){
			return response()->json([
				'status' => 'error',
				'message' => 'These fields are required',
				'errors' => $validator->errors()
			], 422);
		}
		
		$response = [
			'status' => 'succes',
			'message' => '',
		];
		if($service->update($data->toArray())){
			if($data->has('hero_image')){
				$response['fn']['form-reset'] = '#form-hero-image';
				$response['fn']['hide-element'] = ['target' => '#form-hero-image'];

			}

			return response()->json($response);
		}
		return response()->jsnon([
			'status' => 'error',
			'message' => 'Failed to save data'
		], 422);
	}
	private function updateServiceBusinessHours($service, $data){
		$hours = [];
		$i = 0;
		foreach($data['hours'] as $day => $hour){
			$hours[$i]['service_id'] = $service->id;
			$hours[$i]['day'] = $day;
			$hours[$i]['open'] = $hour['open'];
			$hours[$i]['close'] = $hour['close'];
			$i++;
		}
		
		if(count($hours) == 7){
			$service->businessHours()->delete();
			
			if($service->businessHours()->insert($hours)){
				$content = '';
				foreach($hours as $hour){
					$content .= '<div class="col-6 col-md-3 mb-2"><div class="font-weight-bold">'.$hour['day'].'</div>';
					
					if($hour['open'] == '00:00:00' && $hour['close'] == '00:00:00')
						$content .= '<small><i>Closed</i></small>';
					elseif($hour['open'] == '00:00:00' && $hour['close'] == '23:59:59')
						$content .= '<small><i>Open 24 Hours</i></small>';
					else
						$content .= '<small><i>Open: '.$hour['open'].'</i></small><br><small><i>Close: '.$hour['close'].'</i></small><br>';
					
					$content .= '</div>';
				}
				return response()->json([
					'status' => 'success',
					'message' => '',
					'fn' => [
						'update-content' => [
							'target' => '#business-hours-container',
							'content' => $content
						],
						'modal-close' => '#modal-hours'
					]
				]);
			}
		}
	}
	/* public function map(Request $request, $country, $slug){
		$service = Service::where('country_code', $country)
			->whereSlug($slug)->first();
			
		if(empty($service)) abort(404);

		if(empty($service->longitude) && empty($service->latitude)){
			$geocode = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.str_replace(' ', '+', $service->address).'&key='.env('GOOGLE_MAP_KEY')));
			$geocode = @$geocode->results[0];
			
			$service->longitude = @$geocode->geometry->location->lng;
			$service->latitude = @$geocode->geometry->location->lat;
			$service->save();
		}

		return view('services.map', [
			'service' => $service
		]);
	} */

	private function searchResponseAjax(Request $request, $services){	
		$view = NULL;
		if($services->count()){
			foreach($services as $service){
				$view .= View::make('services/_box-info', ['service' => $service])->render();
			}
		}
		
		if(!empty($view)){
			$fn = [
				'set-attribute' => [
					'target' => '#next-services',
					'attribute' => [
						'name' => 'href',
						'value' => 'search/services?'.http_build_query([
							'country' => $request->country,
							'city' => $request->city,
							'category' => $request->category,
							'offset' => ($request->offset != '')? $request->offset + $this->limit : $this->offset,
							'limit' => $this->limit,
							'ref' => 'next'
						])
					]
				],
				'show-element' => [
					'target' => '#service-loader-container'
				]
			];

			if($request->offset != ''){
				$fn['append-element'] = [
						'container' => '#service-container',
						'content' => $view
				];
			}else{
				$fn['update-content'] = [
						'target' => '#service-container',
						'content' => $view
				];
			}
			return response()->json([
				'status' => 'success',
				'fn' => $fn,
			]);
		}else{
			$fn['hide-element'] = ['target' => '#service-loader-container'];
			
			if($request->ref == '')
				$fn['update-content'] = [
					'target' => '#service-container',
					'content' => '<p class="alert alert-info text-center border text-muted">No data found</p>'
				];

			return response()->json([
				'status' => 'success',
				'fn' => $fn
			]);
		}
	}

	private function insight(Request $request, $id, $type){
		$user = \Auth::user();
		$saveAble = true;
		// \DB::enableQueryLog();
		$insight = Insight::services();
		$insight =	$insight->where('table_id', $id)
			->where('type', 'views')
			->where(function($query) use($request, $user){
				$query->where('ip_address', $request->ip())
					->when($user, function($_query) use($user){
						return $_query->where('user_id', $user->id);
					});
			})->first();
			
		// var_dump(\DB::getQueryLog());	
		if($insight){
			$saveAble = false;
		}

		if($saveAble){
			if(empty($insight)) $insight = new Insight;
			$insight->user_id = @$user->id;
			$insight->table = 'services';
			$insight->table_id = $id;
			$insight->type = $type;
			$insight->ip_address = $request->ip();
			
			if($insight->save()){
				return true;
			}
		}

		return false;
	}
}
