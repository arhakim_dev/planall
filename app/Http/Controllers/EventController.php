<?php

namespace App\Http\Controllers;

use App\Event;
use App\UserEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class EventController extends Controller{
	
	private $offset = 8;
	private $limit = 8;

	public function index(Request $request){
		$limit = ($request->limit != '')? $request->limit : $this->limit;
		$offset = ($request->offset != '')? $request->offset : 0;
		$nextURL = 'event/latest/next';

		$month = ($request->month)? $request->month: date('m');
		
		$events = Event::whereRaw('MONTH(`start_date`) = '.$month)
			->whereRaw('`status`!= "pending"')
			->orderBy('start_date');
		$c = $events->count();

		if($offset){
			$total = $c;
			$events->offset($offset)->limit($this->limit);
		}else{
			$events->offset($offset)->limit($this->limit);
			$total = $events->count();
			$c = $events->get()->count();
		}

		if($request->ajax()){
			$view = '';
			if($c){
				foreach($events->get() as $event){
					$view .= View::make('events._box-info', ['event' => $event])->render();
				}
			}
				
			if($view){
				$fn['set-attribute'] = [
					'target' => '#next-latest-event',
					'attribute' => [
						'name' => 'href',
						'value' => $nextURL.'?offset='.($offset + $c).'&limit='.$this->limit.'&month='.$month
					]
				];

				if($request->sort){
					$fn['update-content'] = [
						'target' => '#events-container',
						'content' => $view
					];
				}else{
					$fn['append-element'] = [
						'container' => '#events-container',
						'content' => $view
					];
				}

				if($total > $c){
					$fn['show-element'] = [
						'target' => '#event-loader-container'
					];
				}
				return response()->json([
					'status' => 'succes',
					'message' => '',
					'fn' => $fn
				]);
			}else{
				if($request->sort){
					return response()->json([
						'status' => 'done',
						'message' => '',
						'fn' => [
							'update-content' => [
								'target' => '#events-container',
								'content' => '<div class="list-group-item text-center mb-2">No Data Available</div>'
							]
						]
					]);
				}else{
					return response()->json([
						'status' => 'success',
						'message' => '',
						'fn' => [
							'hide-element' => [
								'target' => '#event-loader-container'
							]
						]
					]);
				}
			}
		
			
		}else{
			return view('events.index', [
				'events' => $events->get(),
				'nextURL' => ($total > $c)
					? $nextURL.'?offset='.$this->offset.'&limit='.$this->limit
					: ''
			]);
		}
	}

	public function detail(Request $request, $slug){
		$event = Event::whereSlug($slug)->first();

		if(!$event) abort(404);

		$user = \Auth::user();
		$isUserRegistered = false;
		if($user){
			$isUserRegistered = (boolean) UserEvent::whereUserId($user->id)->whereEventId($event->id)->count();
		}

		$eventDate = date('d F', strtotime($event->start_date)).' - '.date('d F', strtotime($event->end_date));
		if(date('Y-m-d', strtotime($event->start_date)) == date('Y-m-d', strtotime($event->end_date))){
			$eventDate = date('d F', strtotime($event->start_date));
		}
		
		$isPastEvent = false;
		if(date('Y-m-d') > date('Y-m-d', strtotime($event->end_date))){
			$isPastEvent = true;
		}
		
		$users = $event->users()->wherePivot('is_waiting_list', 0)->limit(3);
		$media = $event->media()->limit(3);
		$attendees = $event->attendees()->whereIsWaitingList(0)->count();
		$waitingList = $event->attendees()->whereIsWaitingList(1)->count();

		$nextUserURL = ($users->count())
			? 'event/'.$event->id.'/users?offset=3&limit=3'
			: '';

		$nextMediaURL = ($media->count())
			? 'event/'.$event->id.'/media?offset=3&limit=3'
			: '';

		return view('events.page', [
			'event' => $event,
			'users' => $users,
			'media' => $media,
			'attendees' => $attendees,
			'waitingList' => $waitingList,
			'isUserRegistered' => $isUserRegistered,
			'isPastEvent' => $isPastEvent,
			'eventDate' => $eventDate,
			'nextUserURL' => $nextUserURL,
			'nextMediaURL' => $nextMediaURL,
		]);
	}

	public function users(Request $request, $eventId){
		$limit = ($request->limit != '')? $request->limit : $this->limit;
		$offset = ($request->offset != '')? $request->offset : 0;

		$event = Event::find($eventId);
		if(empty($event)){
			return [];
		}

		$total = $event->users()->wherePivot('is_waiting_list', 0)->count();
		$users = $event->users()->wherePivot('is_waiting_list', 0)->offset($offset)->limit($limit)->get();
		$c = $users->count();

		if($users->count()){
			$view = '';
			foreach($users as $user){
				$view .= '<div class="col-md-4">';
				$view .= View::make('events._card-user', ['user' => $user]);
				$view .= '</div>';
			}
			$fn['append-element'] = [
				'container' => '#event-user-container',
				'content' => $view
			];
			$fn['set-attribute'] = [
				'target' => '#next-latest-eventuser',
				'attribute' => [
					'name' => 'href',
					'value' => 'event/'.$event->id.'/users?offset='.($offset + $c).'&limit='.$this->limit
				]
			];

			return response()->json([
				'status' => 'success',
				'message' => '',
				'fn' => $fn
			]);
		}else{
			return response()->json([
				'status' => 'success',
				'message' => '',
				'fn' => [
					'hide-element' => [
						'target' => '#eventuser-loader-container'
					]
				]
			]);
		}
	}	

	public function media(Request $request, $eventId){
		$limit = ($request->limit != '')? $request->limit : $this->limit;
		$offset = ($request->offset != '')? $request->offset : 0;

		$event = Event::find($eventId);
		if(empty($event)){
			return [];
		}

		$total = $event->media()->count();
		$media = $event->media()->offset($offset)->limit($limit)->get();
		$c = $media->count();

		if($media->count()){
			$view = '';
			foreach($media as $_media){
				$view .= '<div class="col-md-4">';
				$view .= View::make('events._card-media', ['media' => $_media]);
				$view .= '</div>';
			}
			$fn['append-element'] = [
				'container' => '#event-media-container',
				'content' => $view
			];
			$fn['set-attribute'] = [
				'target' => '#next-latest-eventmedia',
				'attribute' => [
					'name' => 'href',
					'value' => 'event/'.$event->id.'/media?offset='.($offset + $c).'&limit='.$this->limit
				]
			];

			return response()->json([
				'status' => 'success',
				'message' => '',
				'fn' => $fn
			]);
		}else{
			return response()->json([
				'status' => 'success',
				'message' => '',
				'fn' => [
					'hide-element' => [
						'target' => '#eventmedia-loader-container'
					]
				]
			]);
		}
	}

	public function register(Request $request){
		$user = \Auth::user();
		
		$event = Event::find($request->event);
		if(empty($event)){
			return response()->json([
				'status' => 'error',
				'message' => 'Something went wrong. Please reload the browser and try again'
			], 422);
		}

		if(empty($user)){
			return response()->json([
				'status' => 'error',
				'message' => 'Please logged in first to register to this event.',
				'fn' => [
					'modal-open' => '#modal-login'
				]
			], 422);
		}
		
		if(date('Y-m-d') > date('Y-m-d', strtotime($event->end_date))){
			$event->update(['status' => 'done']);

			return response()->json([
				'status' => 'error',
				'message' => 'Failed to register. The event is already passed',
				'fn' => [
					'redirect' => $event->url
				]
			], 422);
		}
		
		if($event->seat <= $event->users()->count()){
			if($user->events()->sync([$event->id => ['is_waiting_list' => 1]])){
				// \Mail::to($user->email)->send(new RegisterEventMail($event, $user));
			}
			return response()->json([
				'status' => 'error',
				'message' => "Sorry, no seats left.\nBut we already added you to a waiting list for this event.\nPlease check your email address for more details",
				'fn' => [
					'redirect' => $event->url
				]
			], 422);
		}

		$message = '';
		if(empty($user->about)) $message .= "- About\n";
		if(empty($user->birth_date)) $message .= "- DateOf Birth\n";
		if(empty($user->country_code)) $message .= "- Country\n";
		if(empty($user->phone)) $message .= "- Contact Number\n";
		if(empty($user->interest_course)) $message .= "- Interested Course\n";
		if(empty($user->dream_university)) $message .= "- Dream University\n";

		if($message){
			return response()->json([
				'status' => 'error',
				'message' => "You are not able to register to this event before you complete this data below:\n"
					.$message."\nPlease go to your profile page and complete the required data."
			], 422);
		}

		if($user->events()->sync([$event->id])){
			// \Mail::to($user->email)->send(new RegisterEventMail($event, $user));
		}
		return response()->json([
			'status' => 'success',
			'message' => 'You have successfully registered for this event. Please check your email address for more details',
			'fn' => [
				'redirect' => $event->url
			]
		]);
	}

	public function unregister(Request $request){
		$user = \Auth::user();
		
		$event = Event::find($request->event);
		if(empty($event)){
			return response()->json([
				'status' => 'error',
				'message' => 'Something went wrong. Please reload the browser and try again'
			], 422);
		}

		$isWaitingList = (boolean)UserEvent::whereUserId($user->id)
			->whereEventId($event->id)
			->whereIsWaitingList(1)
			->count();

		if($event->users()->detach([$user->id])){
			// \Mail::to($user->email)->send(new UnregisterMail($event, $user));
		}
		return response()->json([
			'status' => 'success',
			'message' => 'You have unregistered from this event.',
			'fn' => [
				'redirect' => $event->url
			]
		]);
	}

}

