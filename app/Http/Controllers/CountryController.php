<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CountryController extends Controller{
		
	private $fileName = 'countries.json';

	public function __construct(){}

	public function index(Request $request){
		return Country::all();
		// return file_get_contents('http://battuta.medunes.net/api/country/all?key='.env('BATTUTA_KEY'));
	}

	public function cities(Request $request){
		// $cities = file_get_contents('http://battuta.medunes.net/api/city/'.$request->country.'/search/?city='.$request->city.'&key='.env('BATTUTA_KEY'));
		$cities = file_get_contents('http://gd.geobytes.com/AutoCompleteCity?filter='.$request->country.'&q='.$request->city);

		// dd($cities);

		$data = [];
		$temp = [];
		if(!empty($cities)){
			$ii = 0;
			foreach(json_decode($cities) as $i => $city){
				$city = explode(',', $city);
				
				if(in_array($city[0], $temp)) continue;				
				$temp[$i] = $city[0];

				$data[$ii]['id'] = $city[0];
				$data[$ii]['text'] = $city[0];
				$ii++;
			}
		}

		return ['results' => $data];
	} 
	public function fetch(){
			$countries = file_get_contents('https://restcountries.eu/rest/v2/all');
			file_put_contents(public_path($this->fileName), $countries);

			$countries = file_get_contents(public_path($this->fileName));
			return response()->json(json_decode($countries));
	}

	public function import(){
		Country::truncate();

		$countries = [];
		$_countries = json_decode(file_get_contents(public_path($this->fileName)));
		foreach($_countries as $i => $c){
				$countries[$i]['code'] = $c->alpha2Code;
				$countries[$i]['code3'] = $c->alpha3Code;
				$countries[$i]['name'] = $c->name;
				$countries[$i]['region'] = $c->region;
				$countries[$i]['image'] = $c->flag;
				$countries[$i]['currency_code'] = $c->currencies[0]->code;
				$countries[$i]['currency_symbol'] = $c->currencies[0]->symbol;
		}

		Country::insert($countries);
		$countries = Country::all();

		return response()->json([
			'status' => 'success',
			'message' => [
				'total' => $countries->count(),
				'data' => $countries
			]
		]);
	}
	/* public function regions(Request $request, $country){
			// return Country::all();
			return file_get_contents('http://battuta.medunes.net/api/region/'.$country.'/all?key='.env('BATTUTA_KEY'));
	}

	public function cities(Request $request, $country, $region){
			// return Country::all();
			return file_get_contents('http://battuta.medunes.net/api/city/'.$country.'/search?region='.$region.'&key='.env('BATTUTA_KEY'));
	}

	public function cities(Request $request){
		// return Country::all();
		$cities = file_get_contents('http://battuta.medunes.net/api/city/'.$request->country.'/search/?city='.$request->city.'&key='.env('BATTUTA_KEY'));

		$data = [];
		if(!empty($cities)){
			foreach(json_decode($cities) as $i => $city){
				$data['results'][$i]['id'] = $city->city;
				$data['results'][$i]['text'] = $city->city;
			}
		}

		return $data;
	}  */


}
