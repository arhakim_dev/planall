<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	protected $ctrl__regexUrlPattern = '/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/';

	protected function ctrl__sqlSlug($col){
		return <<<STR
REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( REPLACE( LOWER(TRIM({$col})), ':', ''), ')', ''), '(', ''), ',', ''), '/', ''), '"', ''), '?', ''), "'", ''), '&', ''), '!', ''), '.', ''), ' ', '-'), '--', '-'), '--', '-'), 'ù','u'), 'ú','u'), 'û','u'), 'ü','u'), 'ý','y'), 'ë','e'), 'à','a'), 'á','a'), 'â','a'), 'ã','a'), 'ä','a'), 'å','a'), 'æ','a'), 'ç','c'), 'è','e'), 'é','e'), 'ê','e'), 'ë','e'), 'ì','i'), 'í','i'), 'ě','e'), 'š','s'), 'č','c'), 'ř','r'), 'ž','z'), 'î','i'), 'ï','i'), 'ð','o'), 'ñ','n'), 'ò','o'), 'ó','o'), 'ô','o'), 'õ','o'), 'ö','o'), 'ø','o'),'%', ''), 'ç', 'c'), 'ü', 'u'), 'ğ', 'g'), 'ş', 's'), 'ı', 'i'), '.', ''), 'ö', 'ö'), 'ç', 'c'),'#x27;', ''), '&', '')
STR;
	}
}
