<?php

namespace App\Http\Controllers;

use App\Friendship;
use Illuminate\Http\Request;

class FriendshipController extends Controller
{
  
	public function __construct(){
		$this->middleware('auth');
	}
	
	public function remove($friend){
		$user = \Auth::user();
		
		$data1 = \DB::table('friendships')
			->select('id')
			->where('from', $user->id)
			->where('to', $friend)
			->where('is_approved', 1);
			
		$data2 = \DB::table('friendships')
			->select('id')
			->where('from', $friend)
			->where('to', $user->id)
			->where('is_approved', 1)
			->union($data1)->get();
			
		$friendships = $data2->all();
		
		$ids = collect([]);
		if(count($friendships)){
			foreach($friendships as $friendship){
				$ids->push($friendship->id);
			}
		}
		if(Friendship::where('id', $ids->all())->delete()){
			return response()->json([
				'status' => 'success',
				'message' => '',
				'fn' => [
					'remove-element' => '#friend-'.$friend
				]
			]);
		}
		
		return response()->json([
			'status' => 'error',
			'message' => 'Something went wrong'
		], 422);
	}
	
}
