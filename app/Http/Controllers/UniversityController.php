<?php

namespace App\Http\Controllers;

use App\Country;
use App\Weather;
use App\Subject;
use App\Program;
use App\University;
use App\UniversitySubject;
use App\UniversityProgram;
use App\UniversityAlumni;
use App\Insight;
use Validator;
use DB;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class UniversityController extends Controller{

	private $offset = 8;
	private $limit = 8;

	public function __construct(){}

	public function index(Request $request, $country, $slug){
		$userLoggedin = \Auth::check();

		$university = University::whereRaw('LOWER(country_code) = "'. strtolower($country).'"')
			->whereSlug($slug)->first();
		
		if(empty($university)) abort(404);
		
		if($userLoggedin && $request->ref == 'user-notif'){
			$notification = \App\Notification::find($request->i);
			
			if(!empty($notification) && $notification->notifiable_id == \Auth::user()->id){
				if(empty($notification->read_at)){
					$notification->read_at = now();
					$notification->save();
				}
			}
		}
		$this->insight($request, $university->id);

		$weathers = $this->weathers($university->country_code, $university->zip);

		$shortList = $university->users()->count();
		$rating  = $university->rate;

		$isShortlisted = false;
		$userHasAccess = false;
		$showDescription = true;
		if($userLoggedin){
			$user = \Auth::user();
			$isShortlisted = (boolean) $user->universities()
				->wherePivot('table', 'universities')
				->wherePivot('table_id', $university->id)
				->count();

			$userHasAccess = (boolean) $university->access()->whereUserId($user->id)->count();
		}
		
		if(($university->show_desc == 0 && $userHasAccess == false)
		|| (empty($university->description) && $userHasAccess == false)){
			$showDescription = false;
		}
		
		$fameAlumnis= [];
		if($university->alumni){
			for($i = 1; $i <= 5; $i++){
				if(!empty($university->alumni->{'famous_name'.$i}) 
				&& !empty($university->alumni->{'famous_photo'.$i})){
					$fameAlumnis[$i] = [
						'id' => $university->alumni->id,
						'name' => $university->alumni->{'famous_name'.$i},
						'photo'=> $university->alumni->{'famous_photo'.$i}
					];
				}
			}
		}
		$subjects['undergraduate'] = Subject::type('undergraduate', false)->get();
		$subjects['ms'] = Subject::type('ms', false)->get();
		$subjects['mba'] = Subject::type('mba', false)->get();
		$subject['undergraduate'] = (Array)$university->subjects()->type('undergraduate')->pluck('subject_id')->toArray();
		$subject['ms'] = $university->subjects()->type('ms')->pluck('subject_id')->toArray();
		$subject['mba'] = $university->subjects()->type('mba')->pluck('subject_id')->toArray();

		return view('universities.page', [
			'university' => $university,
			'afterCollege' => $university->afterCollege,
			'weathers' => $weathers,
			'shortList' => $shortList,
			'rating' => $rating,
			'isShortlisted' => $isShortlisted,
			'fameAlumnis' => $fameAlumnis,
			'userHasAccess' => $userHasAccess,
			'showDescription' => $showDescription,
			'subjects' => $subjects,
			'subject' => $subject,
		]);
	}

	public function search(Request $request){
		$tuition['start'] = 1500;
		$tuition['end'] = 500000;

		if($request->ajax()){
			$sort = empty($request->sort)? 'ranking' : $request->sort;
			$keyword = $request->q;

			/* if($request->country || $request->major || $request->degree || ($request->is_public !== NULL) 
				|| ($request->tuition != $tuition['start'].','.$tuition['end'] 
				&& $request->tuition != $tuition['start'].','.$tuition['start'])){
					$keyword = '';
			} */
			$limit = ($request->limit != '')? $request->limit : $this->limit;
			$offset = ($request->offset != '')? $request->offset : 0;

			DB::enableQueryLog();
			$universities = University::selectRaw('universities.*')
				->join('university_programs AS up1', 'universities.id', '=', 'up1.university_id')				

				->when($sort, function($query) use($request, $sort, $tuition){
					if($sort == 'ranking'){
						$programId = 17;
						$select = 'FORMAT((SELECT AVG(CAST(_up.'.$request->degree.' AS SIGNED)) FROM university_programs _up WHERE _up.university_id = universities.id AND _up.program_id IN (16, 17, 18)), 0)';

					}elseif($sort == 'tuition'){
						$programId = 12;
						$select = 'CAST(digits(up1.'.$request->degree.') AS SIGNED)';
						
					}else{
						$programId = 2;
						$select = 'CAST(up1.'.$request->degree.' AS DECIMAL(4,2))';
					}
					
					return $query->addSelect(DB::raw($select.' AS sort'))
						->where('up1.program_id', $programId);
				})
				->when($keyword, function($query) use($keyword){
					$subjectsIds = Subject::whereRaw('LOWER(name) LIKE LOWER("%'.$keyword.'%")')
							->orWhereRaw('LOWER(synonyms) LIKE LOWER("%'.$keyword.'%")')->pluck('id');
					
					return $query->whereRaw('LOWER(universities.name) LIKE LOWER("%'.$keyword.'%")')
						->orWhereRaw('LOWER(universities.address) LIKE LOWER("%'.$keyword.'%")')
						->orWhereRaw('LOWER(universities.zip) LIKE LOWER("%'.$keyword.'%")')
						->orWhereRaw('LOWER(universities.website) LIKE LOWER("%'.$keyword.'%")')
						->orWhereRaw('LOWER(universities.founding_year) LIKE LOWER("%'.$keyword.'%")')
						->orWhereRaw('LOWER(universities.description) LIKE LOWER("%'.$keyword.'%")')
						->orWhereRaw('LOWER(universities.facebook) LIKE LOWER("%'.$keyword.'%")')
						->orWhereRaw('LOWER(universities.twitter) LIKE LOWER("%'.$keyword.'%")')
						->orWhereRaw('LOWER(universities.youtube) LIKE LOWER("%'.$keyword.'%")')
						->orWhereRaw('LOWER(universities.linkedin) LIKE LOWER("%'.$keyword.'%")')
												
						->when(count($subjectsIds), function($__query) use($subjectsIds){
							return $__query->join('university_subjects', 'universities.id', '=', 'university_subjects.university_id')
								->orWhereIn('university_subjects.subject_id', $subjectsIds);
						});
						// ->orderBy('universities.name');
			
				})
				->when($request->country, function($_query) use($request){
					return $_query->where('country_code', $request->country);
				})
				->when($request->major, function($_query) use($request){
					return $_query->join('university_subjects', 'universities.id', '=', 'university_subjects.university_id')
						->where('university_subjects.subject_id', $request->major);
						
						/* ->when($request->degree, function($__query) use($request){
							return $__query->where('university_subjects.type', strtolower($request->degree));
						}); */
				})
				->when(($request->is_public !== NULL), function($_query) use($request){
						return $_query->where('is_public', $request->is_public);
				})
				->when($request->tuition, function($_query) use($request, $sort, $tuition){
					if($request->tuition != $tuition['start'].','.$tuition['end'] 
						|| $request->tuition != $tuition['start'].','.$tuition['start']){

						$tuition = explode(',', $request->tuition);
						if($sort == 'ranking'){
							$programId = 17;
							$select = 'digits(up2.'.$request->degree.') AS tuition';

						}elseif($sort == 'tuition'){
							$programId = 12;
							$select = 'CAST(digits(up2.'.$request->degree.') AS SIGNED) AS tuition';
							
						}else{
							$programId = 2;
							$select = 'CAST(up1.'.$request->degree.' AS DECIMAL(4,2)) AS tuition';
						}
						return $_query->addSelect(DB::raw($select))
							->join('university_programs AS up2', 'universities.id', '=', 'up2.university_id')
							->where('up2.program_id', $programId)
							->when($programId, function($_query) use($programId, $tuition){
								if($programId != 2)
									return $_query->havingRaw("tuition BETWEEN ".$tuition[0]." AND ".$tuition[1]);
							});
					}
				})
			->groupBy('universities.id')
			->orderBy('sort', 'DESC')
			->offset($offset)
			->limit($limit)
			->get();

			// var_dump(DB::getQueryLog());
			return $this->searchResponseAjax($request, $universities, $keyword);

		}else{
			$majors = Subject::groupBy('name')->orderBy('name');					
			$countryCodes = University::where('country_code', '!=', NULL)
				->groupBy('country_code')->pluck('country_code');
			$countries = Country::whereIn('code', $countryCodes)->orderBy('name');

			$_tuition = explode(',', $request->tuition);

			return view('universities.index', [
				'majors' => $majors,
				'countries' => $countries,
				'tuition_start' => ($_tuition[0])? intval($_tuition[0]) : $tuition['start'],
				'tuition_end' => isset($_tuition[1])? intval($_tuition[1]): $tuition['end'],
				'q' => $request->q
			]);
		}
	}

	private function searchResponseAjax(Request $request, $universities, $keyword){
		$universities = $this->_mapDataUniversities($request, $universities);
		$total = $universities->count();

		$view = NULL;
		if($total){
			foreach($universities as $university){
				$view .= View::make('universities._box-statistic', ['university' => $university, 'compareable' => true])->render();
			}
		}
		
		if(!empty($view)){
			$params = [
				'country' => $request->country,
				'major' => $request->major,
				'degree' => $request->degree,
				'tuition' =>$request->tuition,
				'is_public' => $request->is_public,
				'sort' => $request->sort,
				'offset' => ($request->offset != '')? $request->offset + $total : $total,
				'limit' => $this->limit,
				'ref' => 'next'
			];

			if($keyword)
				$params['q'] = $keyword;

			$fn = [
				'set-attribute' => [
					'target' => '#next-universities',
					'attribute' => [
						'name' => 'href',
						'value' => 'search/universities?'.http_build_query($params)
					]
				],
				'show-element' => [
					'target' => '#university-loader-container'
				]
			];

			if(request('offset') != ''){
				$fn['append-element'] = [
						'container' => '#university-container',
						'content' => $view
				];
			}else{
				$fn['update-content'] = [
						'target' => '#university-container',
						'content' => $view
				];
			}
			return response()->json([
				'status' => 'success',
				'fn' => $fn,
			]);
		}else{
			$fn['hide-element'] = ['target' => '#university-loader-container'];
			
			if(request('ref') == '')
				$fn['update-content'] = [
					'target' => '#university-container',
					'content' => '<p class="alert alert-info text-center border text-muted">No data found</p>'
				];

			return response()->json([
				'status' => 'success',
				'fn' => $fn
			]);
		}
	}

	private function _mapDataUniversities(Request $request, $_universities){
		$universities = [];
		$ucompare = (array)session('ucompare');
		if($_universities->count()){
			$i = 0;
			foreach($_universities as $university){
				$ranking = ($request->sort == 'ranking')
					? $university->sort 
					: $university->programRankingsAvg($request->degree);
				$tuition = $university->programTuition()->pivot->{$request->degree};
				$arate = $university->programAcceptanceRate()->pivot->{$request->degree};
				$programs = $university->subjects()
					->wherePivot('type', $request->degree)->count();
				$adminTest = $university->programAdminTest($request->degree);				
				$englishTest = $university->programEnglishTest($request->degree);				
				$deadline = $university->programDeadline()->pivot->{$request->degree};
				$earning = ($university->afterCollege)? $university->afterCollege->earning : 'Data Not Available';

				/* $wheaters = Weather::where('country_code', $university->country_code)
					->where('zip', $university->zip)
					->where(DB::raw('MONTH(updated_at)'), date('n'))
					->where(DB::raw('YEAR(updated_at)'), date('Y'))
					->first(); */
				$rating  = $university->rate;
				
				$universities[$i]['id'] = $university->id;
				$universities[$i]['logo'] = $university->logo;
				$universities[$i]['name'] = $university->name;
				$universities[$i]['address'] = $university->fullAddress;
				$universities[$i]['viewed'] = $university->viewed;
				$universities[$i]['url'] = $university->url;				
				$universities[$i]['ranking'] = $ranking;
				$universities[$i]['tuitionAVG'] = $tuition;
				$universities[$i]['acceptanceRate'] = $arate;
				$universities[$i]['programs'] = $programs;
				$universities[$i]['testingRequirements'] = $adminTest;
				$universities[$i]['englishRequirements'] = $englishTest;
				$universities[$i]['applicationDeadline'] = $deadline;
				$universities[$i]['earningAVG'] = $earning;
				// $universities[$i]['weathers'] = !empty($wheaters)? $wheaters->temp : '0';
				$universities[$i]['rating'] = $rating;
				$universities[$i]['isCompared'] = key_exists($university->id, $ucompare);
				$i++;
			}
		}
		
		return collect($universities);
	}

	public function weathers($countryCode, $zip){
		$weathers = [];
		$year = date('Y');
		$weathers['year'] = $year;
		
		$label = collect([]);
		$tempMin = collect([]);
		$tempMax = collect([]);
		for($m = 1; $m <= 12; $m++){
			$_weather = Weather::select(
				// DB::raw(date('M', strtotime($m)).' AS month'), 
				DB::raw($year.' AS year'), DB::raw('MIN(temp) as temp_min'), 
				DB::raw('MAX(temp) as temp_max')
				)
				->where('country_code', $countryCode)
				->where('zip', $zip)
				->where(DB::raw('MONTH(updated_at)'), $m)
				->where(DB::raw('YEAR(updated_at)'), $year)
				->first();
			
			$label->push(date('M', strtotime($year.'-'.$m)));
			$tempMin->push(intval($_weather->temp_min));
			$tempMax->push(intval($_weather->temp_max));
				
		}
		$weathers['label'] = $label;
		$weathers['tempMin'] = $tempMin;
		$weathers['tempMax'] = $tempMax;

		return $weathers;
	}

	public function addCompare(Request $request){
		if($request->university_id == '') return response()->json([]);
		
		$university = University::find($request->university_id);
		if($request->opt == ''){
			$universityIds = session('ucompare');
			unset($universityIds[$university->id]);
			session(['ucompare' => $universityIds]);

			$fn['remove-element'] = 'li#ucompare-list-'.$university->id;
			$fn['prop-element'][0] = [
				 'target' => '#univ-compare-'.$university->id,
				 'status'=> 'checked',
				 'prop' => false
			 ];
			
			if(count(session('ucompare')) < 2){
				$fn['prop-element'][1] = [
					'target' => '#ucompare-container form button[type="submit"]',
					'status'=> 'disabled',
					'prop' => true
				];
			}
			return response()->json([
				'status' => 'success',
				'fn' => $fn
			]);
		}else{
			if(session('ucompare') == ''){
				session(['ucompare' => [$university->id=> $university->name]]);
			}else{
				$universityIds = session('ucompare');

				if(count($universityIds) > 2){
					return response()->json([
						'status' => 'error',
						'message' => 'Maximum University to compare is three.',
						'fn' => [
							'prop-element' => [
								'target' => '#univ-compare-'.$university->id,
								'status'=> 'checked',
								'prop' => false
							]
						]
					]);
				}

				$universityIds[$university->id] = $university->name;
				session(['ucompare' => $universityIds]);
			}
		}

		$btn = '<button type="button" class="btn-sm btn btn-outline-danger remove-compare float-right" value="'.$university->id.'"> x </button>';
		$text = '<div class="text-truncate-ellipsis-90">'.$university->name.'</div>';

		$fn['append-element'] = [
			'container' => '#ucompare-container ul.list-group',
			'content' => '<li class=" d-flex list-group-item justify-content-between align-items-center" id="ucompare-list-'.$university->id.'">'.$text.' '.$btn.'</li>'
		];

		if(count(session('ucompare')) > 1){
			$fn['prop-element'] = [
				'target' => '#ucompare-container form button[type="submit"]',
				'status'=> 'disabled',
				'prop' => false
			];
		}

		return response()
			->json([
				'status' => 'success',
				'fn' => $fn
			]);
	}

	public function compare(Request $request){
		$universities = [];
		foreach(session('ucompare') as $id => $name){
			$universities[$id] = University::find($id);
		}

		$view = View::make('universities.compare', [
			'universities' => $universities, 
			'degree' => $request->degree	
		])->render();

		return response()->json([
			'status' => 'success',
			'fn' => [
				'update-content' => [
					'target' => '#university-compared',
					'content' => $view
				],
				'modal-open' => '#modal-ucompare'
			]
		]);
	}

	public function uploadFile(Request $request, $type){
		if($type == 'hero'){
			$validator = Validator::make($request->all(), [
				'file' => 'required|image|mimes:jpeg,png,jpg|dimensions:min_width=900,min_height=400'
			],[
				'dimensions' => "The :attribute has invalid image dimensions.\n It should have 900x400 pixels for the minimum size"
			]);
		}elseif(in_array($type, ['alumni-1', 'alumni-2', 'alumni-3'])){
			$validator = Validator::make($request->all(), [
				'file' => 'required|image|mimes:jpeg,png,jpg|dimensions:min_width=200,min_height=200,ratio=1/1'
			],[
				'dimensions' => "The :attribute has invalid image dimensions.\n It should have 200x200 pixels for the minimum size and 1:1 for the ration"
			]);
		}
		
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json([
				'status' => 'error',
				'message' => 'Failed uploading file',
				'errors' => $errors
			], 422);
		}

		$file = $request->file('file');
		$isSuccess = false;
		if($type == 'hero'){
			$fileName = time().'~'.strtolower($file->getClientOriginalName());
			$file->move(public_path('files/hero/'), $fileName);
			if($file) $isSuccess = true;

		}elseif($type == 'logo'){
			$image = imagecreatefrompng($file);
			$fileName = 'files/logo/'.time().'~'.str_random(10).'.jpg';
			imagejpeg($image, public_path($fileName), 100);
			imagedestroy($image);
			if(\File::exists($fileName)) $isSuccess = true;
		}elseif(in_array($type, ['alumni-1', 'alumni-2', 'alumni-3'])){
			$fileName = time().'~'.strtolower($file->getClientOriginalName());
			$file->move(public_path('files/avatar/'), $fileName);
			if($file) $isSuccess = true;
		}
		if($isSuccess){
			if($type == 'hero'){
				$fn = [
					'style-css' => [
						'target' => '.hero-area',
						'styles' => [[
							'key' => 'backgroundImage',
							'value' => 'url(files/hero/'.$fileName.')'
						]]
					],
					'update-content' => [
						'target' => '#hero-image-file',
						'content' => $fileName
					],
					'set-attribute' => [
						'target' => 'input[name="hero_image"]',
						'attribute' => [
							'name' => 'value',
							'value' => 'files/hero/'.$fileName
						],
					],
					'set-data-attribute' => [
						'target' => '#form-hero-image .remove-file',
						'data' => [[
							'name' => 'file',
							'value' => 'files/hero/'.$fileName
						]]
					],
					'show-element' => [
						'target' => '#form-hero-image'
					]
				];

			}elseif($type == 'logo'){
				$fn = [					
					'set-attribute' => [
						[
							'target' => '#logo',
							'attribute' => [
								'name' => 'src',
								'value' => $fileName
							]
						],
						[
							'target' => '#top-menu img#user-avatar',
							'attribute' => [
								'name' => 'value',
								'value' => $fileName
							]
						]
					],
					'modal-close' => '#modal-logo',
				];

			}elseif(in_array($type, ['alumni-1', 'alumni-2', 'alumni-3'])){
				$fn = [
					'set-attribute' => [[
						'target' => 'img.'.$type,
						'attribute' => [
							'name' => 'src',
							'value' => 'files/avatar/'.$fileName
						]
					],[
						'target' => 'input[type="hidden"].'.$type,
						'attribute' => [
							'name' => 'value',
							'value' => 'files/avatar/'.$fileName
						]
					]]
				];
			}
			return response()->json([
				'status' => 'success',
				'message' => '',
				'fn' => $fn
			]);
		}
		
		return response()->json([
			'status' => 'error',
			'message' => 'Failed uploading file'
		], 422);
	}

	public function removeFile(Request $request){
		$type = str_contains($request->file, 'hero')? 'hero' : 'logo';

		@unlink($request->file);

		if($type=='hero'){
			$fn = [
				'form-reset' => '#form-hero-image',
				'hide-element' => [
					'target' => '#form-hero-image'
				],
				'style-css' => [
					'target' => '.hero-area',
					'styles' => [[
						'key' => 'backgroundImage',
						'value' => 'url(../img/blank-image.jpg)'
					]]
				],
			];
		}

		return response()->json([
			'status' => 'success',
			'message' => '',
			'fn' => $fn
		]);
	}

	public function removeAlumni(Request $request, $universityId, $index){
		$alumni = UniversityAlumni::whereUniversityId($universityId)->first();
		
		if($alumni){
			@unlink(public_path($alumni->famous_photo.$index));
			$alumni->{'famous_name'.$index} = '';
			$alumni->{'famous_photo'.$index} = '';

			if($alumni->save()){
				return response()->json([
					'status' => 'success',
					'message' => '',
					'fn' => [
						'set-attribute' => [[
								'target' => '#collapse-famous img.alumni-'.$index,
								'attribute' => [
									'name' => 'src',
									'value' => 'img/blank-image.jpg'
								]
							],[
								'target' => '#collapse-famous input.alumni-'.$index,
								'attribute' => [
									'name' => 'value',
									'value' => ''
							]]
						],
						'remove-class' => [
							'target' => '#collapse-famous .btn.alumni-'.$index,
							'class' => 'd-block'
						],
						'set-data-attribute' => [
							'target' => '#collapse-famous .btn-remove-alumni.alumni-'.$index,
							'data' => [
								'name' => 'class-enabled',
								'value'=> ''
							]
						]
					]
				]);
			}

			return response()->json([
				'status' => 'error',
				'message' => 'Cannot remove data alumni'
			], 422);
		}
	}

	public function update(Request $request, $universityId){
		$university = University::find($universityId);
		if(empty($university)) return [];
		$data = collect($request->all());
		$fn = [];

		if($data->has('subjects')){
			return $this->updateUniversitySubjects($university, $data);
		}elseif($data->has('programs')){
			return $this->updateUniversityPrograms($university, $data);
		}elseif($data->has('after_college')){
			return $this->updateUniversityAfterCollege($university, $data);
		}elseif($data->has('alumni')){
				return $this->updateUniversityAlumni($university, $data);
		}elseif($data->has('facility')){
				return $this->updateUniversityFacility($university, $data);
		}else{
			return $this->updateUniversity($request, $university, $data);
		}

	}

	private function updateUniversity(Request $request, $university, $data){
		$useValidation = false;
		if($data->has('hero_image')){
			$useValidation = true;
			$validator = Validator::make($request->all(), ['hero_image' => 'required']);

		}
		if($data->has('logo')){
			$useValidation = true;
			$validator = Validator::make($request->all(), ['logo' => 'required']);
			$image = imagecreatefrompng($request->logo);
			$fileName = 'files/logo/'.time().'~'.str_random(10).'.jpg';
			imagejpeg($image, public_path($fileName), 100);
			imagedestroy($image);
			
			if(\File::exists($fileName)){
				$data['logo'] = $fileName;
			}else{
				return response()->jsnon([
					'status' => 'error',
					'message' => 'File not saved'
				], 422);
			}
			
		}
		if($data->has('name')){
			$useValidation = true;
			$validator = Validator::make($request->all(), [
				'name'  => 'required',
				'address' => 'required'
			]);
			// $data['slug'] = str_slug($data['name']);

		}
		if($data->has('founding_year')){
			$useValidation = true;
			$validator = Validator::make($request->all(), ['founding_year' => 'required|numeric|digits:4']);

		}
		if($data->has('website') || $data->has('facebook') || $data->has('twitter') || $data->has('youtube') || $data->has('linkedin')){
			$useValidation = true;
			$validator = Validator::make($request->all(), [
				'website' => ['nullable', 'regex:'.$this->ctrl__regexUrlPattern],
				'facebook' => ['nullable', 'regex:'.$this->ctrl__regexUrlPattern],
				'twitter' => ['nullable', 'regex:'.$this->ctrl__regexUrlPattern],
				'youtube' => ['nullable', 'regex:'.$this->ctrl__regexUrlPattern],
				'linkedin' => ['nullable', 'regex:'.$this->ctrl__regexUrlPattern],
			]);
		}

		if($useValidation && $validator->fails()){
			return response()->json([
				'status' => 'error',
				'message' => 'These fields are required',
				'errors' => $validator->errors()
			], 422);
		}
		
		$response = [
			'status' => 'succes',
			'message' => '',
		];
		if($university->update($data->toArray())){
			if($data->has('hero_image')){
				$response['fn']['form-reset'] = '#form-hero-image';
				$response['fn']['hide-element'] = ['target' => '#form-hero-image'];

			}elseif($data->has('logo')){
				$response['fn'] = [
					'set-attribute' => [
						[
							'target' => '#logo',
							'attribute' => [
								'name' => 'src',
								'value' => $university->logo
							]
						],[
							'target' => '.cropit-preview-image',
							'attribute' => [
								'name' => 'src',
								'value' => ''
							]
						],[
							'target' => '.cropit-preview-background',
							'attribute' => [
								'name' => 'src',
								'value' => asset('img/blank-image.jpg')
							]
						]
					],
					'modal-close' => '#modal-logo',
					'form-reset' => '#form-logo'
				];
			}

			return response()->json($response);
		}
		return response()->jsnon([
			'status' => 'error',
			'message' => 'Failed to save data'
		], 422);
	}
	private function updateUniversitySubjects($university, $data){
		$subjects = [];
		$type= '';

		foreach($data['subjects'] as $_type => $id){
			$type = $_type;
			$subjects = $id;
		}
		
		$university->subjects()->wherePivot('type', $type)->sync(array_fill_keys($subjects, ['type' => $type]));
		
		return response()->json([
			'status' => 'success',
			'message' => ''
		]);
		
	}
	private function updateUniversityPrograms($university, $data){
		$programs = [];
		$type= '';

		$i = 0;
		foreach($data['programs'] as $id => $program){
			$update['undergraduate'] = $program['undergraduate'];
			if(isset($program['ms']))
				$update['ms'] = $program['ms'];
			if(isset($program['mba']))
				$update['mba'] = $program['mba'];

			UniversityProgram::whereUniversityId($university->id)
				->whereProgramId($id)
				->update($update);
		
		}

		return response()->json([
			'status' => 'success',
			'message' => ''
		]);
		
	}
	private function updateUniversityAfterCollege($university, $data){
		$fn = [];
		$data = $data['after_college'];
		if(collect($data)->has('top_employees')){
			$data['top_employees'] = implode(', ', $data['top_employees']);
		}
		
		if($university->afterCollege()->update($data)){
			if(collect($data)->has('placement_rate')){
				if(intval($data['placement_rate'])){
					$fn['add-class'] = [[
						'target' => '#collapse-placement .box-placeholder',
						'class' => 'd-none'
					]];
					$fn['remove-class'] = [[
						'target' => '#collapse-placement .pie-chart',
						'class' => 'd-none'
					],[
						'target' => '#collapse-placement .box-placeholder',
						'class' => 'd-block'
					]];

				}else{
					$fn['add-class'] = [[
							'target' => '#collapse-placement .box-placeholder',
							'class' => 'd-none'
						]];

					$fn['set-data-attribute'] = [
						'target' => '#collapse-placement .pie-chart',
						'data' => [
							'name' => 'class-enabled',
							'value'=> 'd-block'
						]];					
				}
				$fn['update-piechart'] = [
					'target' => '.pie-chart',
					'data' => [
						'value' => intval($data['placement_rate'])
					]
				];

			}
			return response()->json([
				'status' => 'success',
				'message' => '',
				'fn' => $fn
			]);			
		}
		
		return response()->json([
			'status' => 'error',
			'message' => 'Failed update data'
		], 422);
	}
	private function updateUniversityAlumni($university, $data){
		$alumnis = [];
		$fn = [];
		
		if(isset($data['alumni']['website'])){
			$alumnis['website'] = $data['alumni']['website'];
		}else{
			$ii = 0;
			foreach($data['alumni'] as $i => $alumni){
				if(empty($alumni['photo'])) continue;
				$alumnis['famous_photo'.$i] = $alumni['photo'];
				$alumnis['famous_name'.$i] = $alumni['name'];

				$fn['set-data-attribute'][$ii] = [
					'target' => '#collapse-famous .btn-remove-alumni.alumni-'.$i,
					'data' => [
						'name' => 'class-enabled',
						'value'=> 'd-block'
					]
				];
				$fn['add-class'][$ii] = [
					'target' => '#collapse-famous img.alumni-'.$i.', #collapse-famous input[type="text"].alumni-'.$i,
					'class' => 'd-block'
				];
				$ii++;
			}
		}

		if($university->alumni()->update($alumnis)){
			return response()->json([
				'status' => 'success',
				'message' => '',
				'fn' => $fn
			]);
		}
		return response()->json([
			'status' => 'error',
			'message' => 'Failed to update data'
		], 422);
	}
	private function updateUniversityFacility($university, $data){
		if($university->facility()->update($data['facility'])){
			return response()->json([
				'status' => 'success',
				'message' => ''
			]);
		}
		return response()->json([
			'status' => 'error',
			'message' => 'Failed to update data'
		], 422);
	}
	
	private function insight(Request $request, $id){
		$user = \Auth::user();
		$saveAble = true;
		// \DB::enableQueryLog();
		$insight = Insight::universities();
		$insight =	$insight->where('table_id', $id)
			->where('type', 'views')
			->where(function($query) use($request, $user){
				$query->where('ip_address', $request->ip())
					->when($user, function($_query) use($user){
						return $_query->where('user_id', $user->id);
					});
			})->first();
			
		// var_dump(\DB::getQueryLog());	
		if(empty($insight)){
			$insight = new Insight;
			$insight->user_id = @$user->id;
			$insight->table = 'universities';
			$insight->table_id = $id;
			$insight->type = 'views';
			$insight->ip_address = $request->ip();
			
			if($insight->save()){
				return true;
			}
		}

		return false;
	}
}