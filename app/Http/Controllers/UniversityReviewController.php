<?php

namespace App\Http\Controllers;

use Validator;
use App\Role;
use App\UniversityReview;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use App\Notifications\Universities\ReviewCreatedNotifyAdminMail;

class UniversityReviewController extends Controller{

	const ROLE_ADMIN = 8;

	public function __contruct(){}

	public function next(Request $request, $university){
		$reviews = UniversityReview::where('university_id', $university)
			->limit($request->limit)
			->offset($request->offset)->get();
		
		$view = NULL;
		if($reviews->count()){
			foreach($reviews as $review){
				$view .= View::make('_partials/card-review', ['review' => $review])->render();
			}
		}
		
		if(!empty($view)){
			return response()->json([
				'status' => 'success',
				'fn' => [
					'append-element' => [
						'container' => '#review-container',
						'content' => $view
					],
					'set-attribute' => [
						'target' => '#next-reviews',
						'attribute' => [
							'name' => 'href',
							'value' => 'university/'.$university.'/review/next?offset='.($request->offset + $reviews->count()).'&limit=8'
						]
					]
				]
			]);
		}else{
			return response()->json([
				'status' => 'error',
				'message' => 'All data already displayed',
				'fn' => [
					'hide-element' => [
						'target' => '#review-loader-container'
					]
				]
			], 422);
		}
	}

	public function save(Request $request){
		$validator = Validator::make($request->all(), [
			'name' => 'required|string',
			'rate' => 'required|numeric',
			'description' => 'required|string'
		]);
		
		if($validator->fails()){
			return response()->json([
				'status' => 'error',
				'message' => 'These fields are required',
				'errors' => $validator->errors()
			], 422);
		}
		
		$review = new UniversityReview;
		$review->university_id = $request->university_id;
		$review->user_id = \Auth::user()->id;
		$review->name = empty($request->name)? 'Annonymous' : $request->name;
		$review->rate = $request->rate;
		$review->description = $request->description;
		$review->is_approved = 0;

		if($review->save()){
			/* $university = University::find($review->university_id);

			$administrators = Role::find(self::ROLE_ADMIN)->users()->get();
			if($administrators->count()){
				$delay = $when = now()->addMinutes(5);
				foreach($administrators as $admin){
					$admin->notify((new ReviewCreatedNotifyAdminMail($university, $review->user))->delay($delay));
				}
			} */

			return response()->json([
				'status' => 'success',
				'message' => 'Thank you, your review will be displayed when its approved',
				'fn' => [
					'form-reset' => '#form-review',
					'modal-close' => '#modal-review'
				]
			]);
		}else{
			return response()->json([
				'status' => 'error',
				'message' => 'Failed to add review'
			], 422);
		}
	}

}
