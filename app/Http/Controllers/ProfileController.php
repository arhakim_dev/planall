<?php

namespace App\Http\Controllers;

use App\User;
use App\Country;
use App\UniversitySubject;
use App\University;
use App\Friendship;
use App\Post;
use App\PostCategory;
use App\Service;
use App\Weather;
use App\Role;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Storage;
use App\Notifications\NotifyWeb;

class ProfileController extends Controller{
	
	const NEXT_FRIEND_URL = 'profile/friends/next';
	const NEXT_UNIVERSITY_URL = 'profile/universities/next';
	const NEXT_SERVICE_URL = 'profile/services/next';
	const NEXT_BLOG_URL = 'blog/latest';
	const NEXT_BLOGCOMMENT_URL = 'profile/blog/comment/next';
	const NEXT_FORUM_URL = 'forum/latest';
	const NEXT_FORUMCOMMENT_URL = 'profile/forum/comment/next';
	const NEXT_EVENT_URL = 'profile/events/next';
	const ROLE_ADMIN = 8;
  
	public function __construct(){
		$this->middleware('auth')->except('uploadPhoto');
	}
	
	public function index($id = ''){
		$user = ($id)? User::findOrFail($id) : \Auth::user();
		$countries = Country::all();
		$majors = UniversitySubject::all();
		$allUniversity = University::all();
		
		$offset = 8;
		$limit = 8;
		
		// Data friendships
		/* $friends = $user->friends()->limit($limit);
		$nextFriendsURL = ($user->friends()->count() > $limit)
			? self::NEXT_FRIEND_URL.'?offset='.$offset.'&limit='.$limit : ''; */
				
		// Data university
		$_universities = $user->universities()->limit($limit)->get();
		$universities = $this->_mapDataUniversities($_universities);
		$nextUniversitiesURL = ($user->universities()->count() > $limit)
			? self::NEXT_UNIVERSITY_URL.'?offset='.$offset.'&limit='.$limit : '';
					
		// Data service
		$services = $user->services()->limit($limit)->get();
		$nextServicesURL = ($user->services()->count() > $limit)
			? self::NEXT_SERVICE_URL.'?offset='.$offset.'&limit='.$limit : '';

		// Data blog
		$blogs = $user->posts()->blog()->latest()->limit($limit)->get();
		$nextBlogsURL = ($user->posts()->blog()->count() > $limit)
			? self::NEXT_BLOG_URL.'?offset='.$offset.'&limit='.$limit : '';
		
		// Data blog comment
		$blogComments = $user->postComments()->blog()->limit($limit)->get();
		$nextBologCommentsURL = ($user->postComments()->blog()->count() > $limit)
			? self::NEXT_BLOGCOMMENT_URL.'?offset='.$offset.'&limit='.$limit : '';
		
		// Data forum
		$forums = $user->posts()->forum()->limit($limit)->get();
		$nextForumURL = ($user->posts()->forum()->count() > $limit)
			? self::NEXT_FORUM_URL.'?offset='.$offset.'&limit='.$limit : '';
		
		// Data forum comment
		$foroumComments = $user->postComments()->forum()->limit($limit)->get();
		$nextForumCommentsURL = ($user->postComments()->forum()->count() > $limit)
			? self::NEXT_FORUMCOMMENT_URL.'?offset='.$offset.'&limit='.$limit : '';
		
		// Data event
		$events = $user->events()->limit($limit)->get();
		$nextEventsURL = ($blogComments->count() > $limit)
			? self::NEXT_EVENT_URL.'?offset='.$offset.'&limit='.$limit : '';
		
		// Data Post Category
		$blogCategories = PostCategory::blog();
		
		return view('profile.index', [
			'user' => $user,
			'countries' => $countries,
			'majors' => $majors,
			
			// 'friends' => $friends,
			'allUniversity' => $allUniversity,
			'universities' => $universities,
			'services' => $services,
			'blogs' => $blogs,
			'blogComments' => $blogComments,
			'blogCategories' => $blogCategories,
			'forums' => $forums,
			'forumComments' => $foroumComments,
			'events' => $events,
			
			// 'nextFriendsURL' => $nextFriendsURL,
			'nextUniversitiesURL' => $nextUniversitiesURL,
			'nextServicesURL' => $nextServicesURL,
			'nextBlogsURL' => $nextBlogsURL,
			'nextBologCommentsURL' => $nextBologCommentsURL,
			'nextForumURL' => $nextForumURL,
			'nextForumCommentsURL' => $nextForumCommentsURL,
			'nextEventsURL' => $nextEventsURL,
		]);
	}
	
	public function uploadPhoto(Request $request){
		$image = $request->avatar;
		$image = imagecreatefrompng($image);
		$file = 'files/avatar/'.$request->uid.'~'.str_random(24).'.jpg';
		imagejpeg($image, public_path($file), 100);
		imagedestroy($image);

		if(\File::exists($file)){
			$user = User::find($request->uid);
			$user->avatar = $file;
			$user->save();
			return response()->json([
				'status' => 'success',
				'message' => '',
				'fn' => [					
					'set-attribute' => [
						[
							'target' => '#profile-photo',
							'attribute' => [
								'name' => 'src',
								'value' => $file
							]
						],
						[
							'target' => '#form-signup2 #photo',
							'attribute' => [
								'name' => 'value',
								'value' => $file
							]
						],
						[
							'target' => '#top-menu img#user-avatar',
							'attribute' => [
								'name' => 'value',
								'value' => $file
							]
						]
					],
					'modal-close' => '#modal-cavatar',
				]
			]);
		}
		
		return response()->json([
			'status' => 'error',
			'message' => 'Failed uploading file'
		]);
	}

	public function update(Request $request){
		$data = collect($request->all());
		
		if($data->has('facebook') || $data->has('twitter') || $data->has('instagram')){			
			$validator = Validator::make($request->all(), [
				'facebook' => 'nullable|url',
				'twitter' => 'nullable|url',
				'instagram' => 'nullable|url',
			]);
			
			if($validator->fails()){
				$errors = $validator->errors();
				return response()->json([
					'status' => 'error',
					'message' => 'Invalid URL format',
					'errors' => $errors
				], 422);
			}
		}
		
		if(\Auth::user()->update($request->all())){
			$response = [
				'status' => 'success',
				'message' => '',
				'fn' => [
					'disable-element' => 'all',	
				]
			];

			if($data->has('country_code')){
				$response += $response['fn']['update-content'] = [
					'target' => '#user-country',
					'content' => \Auth::user()->country->name
				];
			}

			return response()->json($response);
		}
		
		return response()->json([
			'status' => 'error',
			'message' => 'Failed to update profile info'
		]);
	}

	public function shotlistUniversity(Request $request){
		$university = University::find($request->university_id);

		$user = \Auth::user();
		if($university->users()->sync([$user->id => ['table' => 'universities']])){

			// $this->shortlistSendNotication($user, $university);

			return response()->json([
				'status' => 'success',
				'message' => $university->name.' has been added to your shortlist',
				'fn' => [
					'remove-element' => '#form-university-shortlist'
				]
			]);
		}else{
			return response()->json([
				'status' => 'error',
				'message' => 'Failed to add '.$university->name.' to your shortlist'
			]);
		}
	}	

	public function shotlistService(Request $request){
		$service = Service::find($request->service_id);

		$user = \Auth::user();
		if($service->users()->sync([$user->id => ['table' => 'services']])){
			// $this->shortlistSendNotication($user, $service);

			return response()->json([
				'status' => 'success',
				'message' => $service->name.' has been added to your shortlist',
				'fn' => [
					'remove-element' => '#form-service-shortlist'
				]
			]);
		}else{
			return response()->json([
				'status' => 'error',
				'message' => 'Failed to add '.$service->name.' to your shortlist'
			]);
		}
	}	

	private function shortlistSendNotication($user, $data){
		$administrators = Role::find(self::ROLE_ADMIN)->users()->get();
		if($administrators->count()){
			$delay = $when = now()->addMinutes(5);
			foreach($administrators as $admin){
				$admin->notify(
					(new \App\Notifications\Services\ShortlistNotifyAdminMail($user, $data))
						->delay($delay)
				);
			}
		}

		$administrators = $data->access()->get();
		if($administrators->count()){
			$delay = $when = now()->addMinutes(5);
			foreach($administrators as $admin){
				$admin->notify(
					(new \App\Notifications\Services\ShortlistMail($user, $data))
						->delay($delay)
				);
			}
		}


		$dataURL = '<a href="'.$data->url.'">'.str_limit($data->name).'</a>';
		$data = [[
			'show' => 'user-activities',
			'data' => [
				'avatar' => $user->avatar,
				'name' => $user->fullName,
				'message' => ' shortlisted "'.$dataURL
			]
		]];
		$user->notify(new NotifyWeb($data));
	}

	/* public function removeFriend($friend){
		if(Friendship::findOrFail($friend)->delete()){
			return response()->json([
				'status' => 'success',
				'message' => '',
				'fn' => [
					'remove-element' => '#friend-'.$friend
				]
			]);
		}
		
		return response()->json([
			'status' => 'error',
			'message' => 'Something went wrong'
		], 422);
	} */
	
	public function removeUniversity($university){
		$user = \Auth::user();
		$user->universities()->detach($university);
		return response()->json([
			'status' => 'success',
			'message' => '',
			'fn' => [
				'remove-element' => '#university-'.$university
			]
		]);
		
	}

	public function removeService($service){
		$user = \Auth::user();
		$user->services()->detach($service);
		return response()->json([
			'status' => 'success',
			'message' => '',
			'fn' => [
				'remove-element' => '#service-'.$service
			]
		]);
	}
	
	public function removePost($post){
		if(Post::findOrFail($post)->delete()){
			return response()->json([
				'status' => 'success',
				'message' => '',
				'fn' => [
					'remove-parent-element' => [
						'target' => '#post-'.$post,
						'parent' => ''
					]
				]
			]);
		}
		
		return response()->json([
			'status' => 'error',
			'message' => 'Something went wrong'
		], 422);
	}
	
	public function removeComment($comment){
		$user = \Auth::user();
		
		$deleted = $user->postComments()->where('id', $comment)->delete();
		
		if($deleted){
			return response()->json([
				'status' => 'success',
				'message' => '',
				'fn' => [
					'remove-element' => '#comment-'.$comment
				]
			]);
		}
		
		return response()->json([
			'status' => 'error',
			'message' => 'Something went wrong'
		], 422);
	}
	
	public function removeEvent($event){
		$user = \Auth::user();
		
		$deleted = \DB::table('user_events')
			->where('user_id', $user->id)
			->where('event_id', $event)->delete();
			
		if($deleted){
			return response()->json([
				'status' => 'success',
				'message' => '',
				'fn' => [
					'remove-element' => '#event-'.$event
				]
			]);
		}
		
		return response()->json([
			'status' => 'error',
			'message' => 'Something went wrong'
		], 422);
	}
	
	public function nextFriends(Request $request){
		$user = \Auth::user();
		
		$friends = $user->friends()
			->offset($request->query('offset'))
			->limit($request->query('limit'))
			->get();
		
		$view = NULL;
		if($friends->count()){
			foreach($friends as $friend){
				$view .= View::make('_partials.list-friend', ['friend' => $friend])->render();
			}
		}
		
		if(!empty($view)){
			return response()->json([
				'status' => 'success',
				'fn' => [
					'append-element' => [
						'container' => '#friends-container',
						'content' => $view
					],
					'set-attribute' => [
						'target' => '#next-friends',
						'attribute' => [
							'name' => 'href',
							'value' => self::NEXT_FRIEND_URL.'?offset='.($request->query('offset') + $friends->count()).'&limit=5'
						]
					]
				]
			]);
		}else{
			return response()->json([
				'status' => 'error',
				'message' => 'All data already displayed',
				'fn' => [
					'hide-element' => [
						'target' => '#friend-loader-container'
					]
				]
			], 422);
		}
		
	}
	
	public function nextUniversities(Request $request){
		$user = \Auth::user();
		
		$_universities = $user->universities()
			->offset($request->query('offset'))
			->limit($request->query('limit'))
			->get();
		$universities = $this->_mapDataUniversities($_universities);
		
		$view = NULL;
		if($universities->count()){
			foreach($universities as $university){
				$view .= View::make('universities._box-statistic', ['university' => $university, 'removable' => true, 'user' => $user])->render();
			}
		}
		
		if(!empty($view)){
			return response()->json([
				'status' => 'success',
				'fn' => [
					'append-element' => [
						'container' => '#university-container',
						'content' => $view
					],
					'set-attribute' => [
						'target' => '#next-university',
						'attribute' => [
							'name' => 'href',
							'value' => self::NEXT_UNIVERSITY_URL.'?offset='.($request->query('offset') + $universities->count()).'&limit=5'
						]
					]
				]
			]);
		}else{
			return response()->json([
				'status' => 'error',
				'message' => 'All data already displayed',
				'fn' => [
					'hide-element' => [
						'target' => '#university-loader-container'
					]
				]
			], 422);
		}
		
	}

	public function nextServices(Request $request){
		$user = \Auth::user();
		$services = $user->services()
		->offset($request->query('offset'))
		->limit($request->query('limit'))
		->get();
		
		$view = NULL;
		if($services->count()){
			foreach($services as $service){
				$view .= View::make('services._box-info', ['service' => $service, 'removable' => true, 'user' => $user])->render();
			}
		}
		
		if(!empty($view)){
			return response()->json([
				'status' => 'success',
				'fn' => [
					'append-element' => [
						'container' => '#service-container',
						'content' => $view
					],
					'set-attribute' => [
						'target' => '#next-service',
						'attribute' => [
							'name' => 'href',
							'value' => self::NEXT_SERVICE_URL.'?offset='.($request->query('offset') + $services->count()).'&limit=5'
						]
					]
				]
			]);
		}else{
			return response()->json([
				'status' => 'error',
				'message' => 'All data already displayed',
				'fn' => [
					'hide-element' => [
						'target' => '#service-loader-container'
					]
				]
			], 422);
		}
		
	}
	
	public function nextEvents(Request $request){
		$user = \Auth::user();
		
		$events = $user->events()
			->offset($request->query('offset'))
			->limit($request->query('limit'))
			->get();
		
		$view = NULL;
		if($events->count()){
			foreach($events as $comment){
				$view .= View::make('_partials.card-event', ['events' => $events, 'removable' =>true])->render();
			}
		}
		
		if(!empty($view)){
			return response()->json([
				'status' => 'success',
				'fn' => [
					'append-element' => [
						'container' => '#events-container',
						'content' => $view
					],
					'set-attribute' => [
						'target' => '#next-events',
						'attribute' => [
							'name' => 'href',
							'value' => self::NEXT_EVENT_URL.'?offset='.($request->query('offset') + $comments->count()).'&limit=5'
						]
					]
				]
			]);
		}else{
			return response()->json([
				'status' => 'error',
				'message' => 'All data already displayed',
				'fn' => [
					'hide-element' => [
						'target' => '#event-loader-container'
					]
				]
			], 422);
		}
		
	}
	
	
	private function _mapDataUniversities($_universities){
		$universities = [];
		if($_universities->count()){
			$i = 0;
			foreach($_universities as $university){
				$ranking = $university->programRankingsAvg('undergraduate');
				$tuition = $university->programTuition()->pivot->{'undergraduate'};
				$arate = $university->programAcceptanceRate()->pivot->{'undergraduate'};
				$programs = $university->subjects()
					->wherePivot('type', 'undergraduate')->count();
				$adminTest = $university->programAdminTest('undergraduate');				
				$englishTest = $university->programEnglishTest('undergraduate');				
				$deadline = $university->programDeadline()->pivot->{'undergraduate'};
				$earning = ($university->afterCollege)? $university->afterCollege->earning : 'Data Not Available';

			/* 	$wheaters = Weather::where('country_code', $university->country_code)
					->where('zip', $university->zip)
					->where(\DB::raw('MONTH(updated_at)'), date('n'))
					->where(\DB::raw('YEAR(updated_at)'), date('Y'))
					->first(); */
				$rating  = $this->getRating($university);

				$universities[$i]['id'] = $university->id;
				$universities[$i]['logo'] = $university->logo;
				$universities[$i]['name'] = $university->name;
				$universities[$i]['address'] = $university->fullAddress;
				$universities[$i]['viewed'] = $university->viewed;
				$universities[$i]['url'] = $university->url;				
				$universities[$i]['ranking'] = $ranking;
				$universities[$i]['tuitionAVG'] = $tuition;
				$universities[$i]['acceptanceRate'] = $arate;
				$universities[$i]['programs'] = $programs;
				$universities[$i]['testingRequirements'] = $adminTest;
				$universities[$i]['englishRequirements'] = $englishTest;
				$universities[$i]['applicationDeadline'] = $deadline;
				$universities[$i]['earningAVG'] = $earning;
				// $universities[$i]['weathers'] = !empty($wheaters)? $wheaters->temp : '0';
				$universities[$i]['rating'] = $rating;
				
				$i++;
			}
		}
		
		return collect($universities);
	}
	
	private function getRating($university){
		$totalRate = 0;
		$totalVote = 0;		
		for($i = 1; $i <= 5; $i++){
			$vote = $university->reviews()->where('rate', $i)->pluck('rate')->count();
			if($vote == 0) continue;
			$totalVote += $vote;
			$totalRate += $vote * $i;
		}

		if($totalRate > 0 && $totalVote > 0)
			return $totalRate / $totalVote;
		else
			return 0;

	}
}
