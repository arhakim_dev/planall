<?php

namespace App\Http\Controllers;

use App\Role;
use App\Post;
use App\PostCategory;
use App\PostComment;
use App\Insight;
use App\Notifications\Posts\PostCreatedNotifyAdminMail;
use App\Notifications\Posts\CommentCreatedNotifyAdminMail;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class PostController extends Controller{
	
	const ROLE_ADMIN = 8;
	private $limit = 8;
	private $offset = 8;
	private $comments = [];

	public function __construct(){}
	
	public function index($categorySlug = ''){
		$isBlog = (str_contains(url()->current(), 'blog'));
		$type = ($isBlog)? 'blog' : 'forum';
		$nextURL = (Post::whereType($type)->count() > $this->limit)? $type.'/latest?offset='.$this->offset.'&limit='.$this->limit : '';

		if($type == 'blog'){
			$data = $this->fetchDataIndex('blog', $categorySlug);
			return view('posts.index-blog', [
				'latest' => $data['latest'],
				'nextURL' => $nextURL,
				'popular' => $data['popular'],
				'categories' => $data['categories'],
			]);
		}else{
			return view('posts.index-forum', [
				'categories' => PostCategory::whereType('forum')->get(),
				'nextURL' => $nextURL
			]);
		}
	}
	
	public function post(Request $request, $slug){
		$viewComments = '';
		$post = Post::whereSlug($slug)->first();
		
		if(empty($post)) abort(404);
		
		if($request->ref == 'user-notif'){
			$notification = \App\Notification::find($request->i);
			
			if(empty($notification->read_at)){
				$notification->read_at = now();
				$notification->save();
			}
		}

		if($post->status == 'approved')
			$this->insight($request, $post->id, 'views');
		
		$related = Post::whereType($post->type)->where('category_id', $post->category_id)
			->where('id', '!=', $post->id)
			->orderBy('created_at', 'desc')
			->limit($this->limit);
		
		$nextRelatedsURL = ($related->count() > $this->limit)
			? $post->type.'/category/'.$post->category_id.'/next?offset='.$this->offset.'&limit='.$this->limit 
			: '';

		$comments = $post->comments()->whereStatus('approved')
			->whereRepliedTo(0)
			->latest()
			->offset(0)
			->limit($this->limit)
			->get();
		$total = $comments->count();

		if($total){
			foreach($comments as $comment){
				$viewComments .= View::make('posts._'.$post->type.'-comment', ['comment' => $comment]);

				if($comment->replies()->count()){
					$loaderId = str_random(5).'-loader';
					$containerId = $comment->id.strtotime($comment->created_at);
					$params = http_build_query([
						'l' => 1,
						'c' => $containerId,
						'offset' => 0,
						'limit'=> 3
					]);
					$url = $post->type.'/comment/'.$comment->id.'/replies/next?'.$params;
	
					$viewComments .= '<div class="pl-4" id="'.$containerId.'">'
						.'<p class="text-center border-top border-bottom my-2 loader">'
							.'<a href="'.$url.'" class="btn-action" data-loader="#'.$loaderId.'">Load Replies '
								.'<i class="fa fa-spinner fa-spin" id="'.$loaderId.'" style="display:none;"></i>'
							.'</a>'
						.'</p>'
					.'</div>';
				}
			}
		}

		$nextCommentsURL = ($post->comments->count() > $this->limit)
			? $post->type.'/'.$post->id.'/comment/next?offset='.$total.'&limit='.$this->limit 
			: '';
		
		return view('posts.post-'.$post->type, [
			'post' => $post,
			'related' => $related,
			'comments' => $viewComments,
			'nextCommentsURL' => $nextCommentsURL,
			'nextRelatedsURL' => $nextRelatedsURL,
		]);
	}

	public function search(Request $request){
		$type = (str_contains(url()->current(), 'blog'))? 'blog' : 'forum';
		$limit = ($request->limit != '')? $request->limit : $this->limit;
		$offset = ($request->offset != '')? $request->offset : 0;

		if($request->ajax()){
			$sort = $request->sort;
			$categorySlug = $request->category;
			
			$category = PostCategory::whereType($type)->whereSlug($categorySlug)->first();
			$select = 'posts.*';
			$posts = Post::where('posts.status', 'approved')
				->where('posts.type', $type)
				->when($category, function($query) use($category){
					$query->where('category_id', $category->id);
				});

			if($sort == 'popular'){
				$select .= ', COUNT(posts.id) AS popular';
				$posts = $posts->leftJoin('post_comments', 'posts.id', '=', 'post_comments.post_id')
					->groupBy('posts.id')
					->orderByRaw('COUNT(posts.id) DESC');

			}elseif($sort == 'recent'){
				$posts = $posts->orderBy('posts.updated_at', 'ASC');

			}elseif($sort == 'viewed'){
				$select .= ', COUNT(posts.id) AS viewed';
				$posts = $posts->leftJoin('insights', 'posts.id', '=', 'insights.table_id')
					->where('insights.table', 'posts')
					->where('insights.type', 'views')
					->groupBy('posts.id')
					->orderByRaw('COUNT(posts.id) DESC');

			}elseif($sort == 'new'){
				$posts = $posts->latest();
			}

			$posts = $posts->selectRaw($select);
			/* \DB::enableQueryLog();
			$posts->get();
			var_dump(\DB::getQueryLog()); */
			
			return $this->searchResponseAjax($request, $posts, $type);
			/* 	}else{
					$category = ServiceCategory::whereSlug($slug)->first();
					$diffCategories = ServiceCategory::whereNotIn('id', [$category->id, 3, 8]);
					// $countries = Country::all();
					$countries = Service::selectRaw('services.country_code AS code, countries.name')
						->join('countries', 'services.country_code', '=', 'countries.code')
						->where('services.category_id', $category->id)
						->groupBy('code')
						->get();
						
					return view('services.index', [
						'category' => $category,
						'diffCategories' => $diffCategories,
						'countries' => $countries
					]); 
			*/
		}
	}
	
	public function latest(Request $request, $categorySlug = ''){
		$isBlog = (str_contains(url()->current(), 'blog'));
		$isProfile = str_contains(url()->previous(), 'profile');
		$type = ($isBlog)? 'blog' : 'forum';
		$select = 'posts.*';

		if($isProfile){
			$user = \Auth::user();
			$latest = $user->posts()->where('posts.type', $type)
				->where('posts.status', 'approved');
			
		}else{
			$latest = Post::where('posts.type', $type)
				->where('posts.status', 'approved');
		}

		$limit = ($request->limit != '')? $request->limit : $this->limit;
		$offset = ($request->offset != '')? $request->offset : 0;
		$sort = ($request->sort != '')? strtolower($request->sort) : 'posted-desc';
		$_sort = explode('-', $sort);
		
		$category = ($categorySlug)? PostCategory::whereSlug($categorySlug)->first() : '';
		
		$latest = $latest->when($category, function($query) use($category){
				return $query->where('category_id', $category->id);
			})
			->when($sort, function($query) use($sort, $_sort){				
				if($sort == 'posted-desc'){
					return $query->latest();

				}elseif($sort == 'posted-asc'){
					return $query->oldest();

				}elseif($_sort[0] == 'topic'){
					return $query->orderBy('title', $_sort[1]);

				}elseif($_sort[0] == 'answered'){
					return $query->leftJoin('post_comments', 'post_comments.post_id', '=', 'posts.id')
						->groupBy('posts.id')
						->orderBy('answered', $_sort[1])
						->orderBy('posts.created_at', 'DESC');

				}elseif($_sort[0] == 'views'){
					return $query->orderBy('views', $_sort[1])
						->orderBy('posts.created_at', 'DESC');
				}

			})
			->offset($offset)
			->limit($limit);

		if($_sort[0] == 'answered')
			$select .= ', (SELECT created_at	FROM post_comments cc	WHERE cc.post_id = posts.id	ORDER BY created_at DESC LIMIT 1) AS answered';

		elseif($_sort[0] == 'views')
			$select .= ', (SELECT COUNT(*) FROM insights WHERE `table` = "posts" AND `type` = "views" AND table_id = posts.id) AS views';


		$data = $latest->selectRaw($select)->get();
		// var_dump(\DB::getQueryLog());
		$view = NULL;
		if($data->count()){
			foreach($data as $post){
				if($isBlog){
					// $view .= '<div class="col-md-4 mb-3 p-3">';
					$view .= View::make('_partials.card-post', ['post' => $post, 'removable' => $isProfile])->render();
					// $view .= '</div>';
				}else{
					$view .= View::make('posts._forum-table-row', ['post' => $post, 'removable' => $isProfile])->render();
				}
			}
		}
		
		if(!empty($view)){
			$nextURL = ($categorySlug)? $type.'/'.$categorySlug : $type;
			$nextURL .= '/latest?offset='.($offset + $data->count()).'&limit='.$this->limit;
			$nextURL .= ($request->sort == '')? '' : '&sort='.$_sort[0].'-'.$_sort[1];

			$fn['set-attribute'] = [
				'target' => '#next-latest-'.$type,
				'attribute' => [
					'name' => 'href',
					'value' => $nextURL
				]				
			];

			if($offset && $request->header == ''){
				$fn['append-element'] = [
					'container' => '#latest-'.$type.'-container',
					'content' => $view
				];
				
				$fn['add-class']= [
					'target' => '#'.$type.'-loader-container',
					'class' => 'd-flex justify-content-center'
				];
				$fn['show-element'] = [
					'target' => '#'.$type.'-loader-container .collapse-to'
				];
			}else{
				$fn['update-content'] = [
					'target' => '#latest-'.$type.'-container',
					'content' => $view
				];
				$fn['add-class']= [
					'target' => '#'.$type.'-loader-container',
					'class' => 'd-flex justify-content-center'
				];
				$fn['show-element'] = [
					'target' => '#'.$type.'-loader-container'
				];
			}
			return response()->json([
				'status' => 'success',
				'fn' => $fn
			]);
		}else{
			$fn['remove-class']= [
				'target' => '#'.$type.'-loader-container',
				'class' => 'd-flex justify-content-center'
			];
			$fn['hide-element'] = [
				'target' => '#'.$type.'-loader-container'
			];

			if($request->clear){
				$fn['update-content'] = [
					'target' => '#latest-'.$type.'-container',
					'content' => '<div class="text-center">Data not available</div>'
				];
			}
			return response()->json([
				'status' => 'success',
				'message' => 'All data already displayed',
				'fn' => $fn
			]);
		}
	}
	
	public function addComment(Request $request){
		$validator = Validator::make($request->except('post_id'), [
			'name' => 'string|required',
			'email' => 'email|required',
			'description' => 'string|required'
		]);
		
		if($validator->fails()){
			return response()->json([
				'status' => 'error',
				'message' => '',
				'errors' => $validator->errors()
			], 422);
		}

		$post = Post::find($request->post_id);
		
		$comment = new PostComment;
		$comment->post_id = $post->id;
		$comment->replied_to = intval($request->replied_to);
		$comment->user_id = (\Auth::check())? \Auth::user()->id : 0;
		$comment->name = $request->name;
		$comment->email = $request->email;
		$comment->description = $request->description;
		$comment->type = $post->type;
		
		if($comment->save()){
			$fn['set-attribute'] = [
				[
					'target' => '#form-comment #replied_to',
					'attribute' => [
						'name' => 'value',
						'value' => ''
					]
				],[
					'target' => '#form-comment #description',
					'attribute' => [
						'name' => 'value',
						'value' => ''
					]
				]
			];

			if($post->type == 'forum'){
				$fn['modal-close'] = '#modal-comment';
			}
			
			/* $administrators = Role::find(self::ROLE_ADMIN)->users()->get();
			if($administrators->count()){
				$delay = $when = now()->addMinutes(5);
				foreach($administrators as $admin){
					$admin->notify((new CommentCreatedNotifyAdminMail($comment, $post))->delay($delay));
				}
			} */
			return response()->json([
				'status' => 'success',
				'message' => 'Thank you, your comment will be displayed when its approved',
				'fn' => $fn
			]);
		}
		
		return response()->json([
			'status' => 'error',
			'message' => 'Something went wrong'
		], 422);
		
	}
	
	public function nextComments(Request $request, $id = ''){
		$isProfile = str_contains(url()->previous(), 'profile');
		$isBlog = str_contains(url()->current(), 'blog');
		$type = ($isBlog)? 'blog' : 'forum';
		
		if($isProfile){
			$user = \Auth::user();
			$comments = $user->postComments()->where('type', $type);
				
		}else{
			$comments = PostComment::where('post_id', $id)
				->whereRepliedTo(0)
				->whereStatus('approved');
		}		
		
		$total = $comments->count();
		$comments = $comments->latest()
			->offset($request->offset)
			->limit($request->limit)
			->get();
		
		$offset = $request->offset + $comments->count();
		
		$view = NULL;
		if($comments->count()){
			foreach($comments as $comment){
				if($comment->replied_to){
					$comment->replied_to_user = PostComment::withTrashed()->where('id', $comment->replied_to)->first()->name;
				}
				$view .= View::make('posts._'.$comment->type.'-comment', ['comment' => $comment, 'removable' => $isProfile])->render();

				if($comment->replies()->count()){
					$loaderId = str_random(5).'-loader';
					$containerId = $comment->id.strtotime($comment->created_at);
					$params = http_build_query([
						'l' => 1,
						'c' => $containerId,
						'offset' => 0,
						'limit'=> 3
					]);
					$url = $comment->type.'/comment/'.$comment->id.'/replies/next?'.$params;
	
					$view .= '<div class="pl-4" id="'.$containerId.'">'
						.'<p class="text-center border-top border-bottom my-2 loader">'
							.'<a href="'.$url.'" class="btn-action" data-loader="#'.$loaderId.'">Load Replies '
								.'<i class="fa fa-spinner fa-spin" id="'.$loaderId.'" style="display:none;"></i>'
							.'</a>'
						.'</p>'
					.'</div>';
				}
			}
		}
		// dd(url()->previous());
		if(!empty($view)){
			$nextURL = ($isProfile)? 'profile/'.$type : $type.'/'.$id;
			
			return response()->json([
				'status' => 'success',
				'fn' => [
					'append-element' => [
						'container' => '#'.$type.'comment-container',
						'content' => $view
					],
					'set-attribute' => [
						'target' => '#next-'.$type.'-comment',
						'attribute' => [
							'name' => 'href',
							'value' => $nextURL . '/comment/next?offset='.$offset.'&limit='.$this->limit
						]
					]
				]
			]);
		}else{
			return response()->json([
				'status' => 'success',
				'message' => '',
				'fn' => [
					'hide-element' => [
						'target' => '#'.$type.'cmt-loader-container'
					]
				]
			]);
		}
		
	}
	private function commentReplies(PostComment $comment, $level, $offset = 0, $limit = 3){
		$view = '';
		$replies =  $comment->replies()->oldest()->offset($offset)->limit($limit);		
		$total = ($offset)? ($comment->replies()->count() - $offset) : $replies->count();

		if($total){
			$replies = $replies->get();
			$offset += $replies->count();

			foreach($replies as $reply){
				$disableReply = ($level >= 3)? true: false;
				if($reply->replied_to){
					$reply->replied_to_user = PostComment::withTrashed()->where('id', $reply->replied_to)->first()->name;
				}
				$view .= View::make('posts._'.$reply->type.'-comment', ['comment' => $reply, 'disableReply'=> $disableReply]);
				/* $view .= '<div class="pl-4" id="'.strtotime($reply->created_at).'">'
					.$this->commentReplies($reply, ($level+1))
				.'</div>'; */
				if($reply->replies()->count()){
					$loaderId = str_random(5).'-loader';
					$params = http_build_query([
						'l' => $level + 1,
						'c' => strtotime($reply->created_at),
						'offset' => 0,
						'limit'=> $limit
					]);
					$url = 'blog/comment/'.$reply->id.'/replies/next?'.$params;
	
					$view .= '<div class="pl-4" id="'.strtotime($reply->created_at).'">'
						.'<p class="text-center border-top border-bottom my-2 loader">'
							.'<a href="'.$url.'" class="btn-action" data-loader="#'.$loaderId.'">Load Replies '
								.'<i class="fa fa-spinner fa-spin" id="'.$loaderId.'" style="display:none;"></i>'
							.'</a>'
						.'</p>'
					.'</div>';
				}
			}
			if($total > $limit){
				$loaderId = str_random(5).'-loader';
				$containerId = $comment->id.strtotime($comment->created_at);
				$params = http_build_query([
					'l' => $level,
					'c' => $containerId,
					'offset' => $offset,
					'limit'=> $limit
				]);
				$url = 'blog/comment/'.$reply->replied_to.'/replies/next?'.$params;

				$view .= '<p class="text-center border-top border-bottom my-2 loader">'
					.'<a href="'.$url.'" class="btn-action" data-loader="#'.$loaderId.'">Load More '
						.'<i class="fa fa-spinner fa-spin" id="'.$loaderId.'" style="display:none;"></i>'
					.'</a>'
				.'</p>';
			}
		}
		return $view;
	}
	public function nextReplies(Request $request, $commentId){
		$comment = PostComment::find($commentId);
		$comments = $this->commentReplies($comment, $request->l, $request->offset, $request->limit);

		if($comments){
			return response()->json([
				'status' => 'success',
				'message' => '',
				'fn' => [
					'remove-element' => '#'.$request->c.' > .loader',
					'append-element' => [
						'container' => '#'.$request->c,
						'content' => $comments
					],
				]
			]);
		}
	}
	
	public function editorUpload(Request $request){
		$isHero = str_contains(url()->current(), 'hero');
		$validator = Validator::make($request->all(), [
			'file' => 'required|image|mimes:jpeg,png,jpg|dimensions:min_width=200,min_height=200'//,max_width=500,max_height=500'
		]);
			
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json([
				'status' => 'error',
				'message' => 'File not supported',
				'errors' => $errors
			], 400);
		}
		
		$file = $request->file('file');
		$fileName = str_random(64).'.'.$file->getClientOriginalExtension();
		$file->move(public_path('files/post/'), $fileName);
		
		if($file){
			$file = 'files/post/'.$fileName;
			$res = collect([
				'status' => 'success',
				'message' => '',
				'location' => $file,
				'file' => $file
			]);
			
			if($isHero){
				$res->put('fn', [
					'set-attribute' => [
						[
							'target' => '#form-blog input[name="hero_image"]',
							'attribute' => [
								'name' => 'value',
								'value' => $file
							]
						],
						[						
							'target' => '#form-blog #post-hero-image',
							'attribute' => [
								'name' => 'src',
								'value' => $file
							]
						]
					],
					'show-element' => [
						'target' => '#btn-remove-hero-image'
					]
				]);
			}
			return response()->json($res->all());
		}
		
		return response()->json([
			'status' => 'error',
			'message' => 'Failed uploading file'
		], 400);
	}
	
	public function store(Request $request){
		$isProfile = str_contains(url()->previous(), 'profile');
		$isBlog = str_contains(url()->current(), 'blog');
		$type = ($isBlog)? 'blog' : 'forum';
		
		if(!$isBlog)
			$request->title = preg_replace('/\r\n|\r|\n/', ' ', $request->title);

		$rules = [
			'title' => 'required|string',
			'category' => 'required|integer',
		];
		if($isBlog){
			$rules['description'] =  'required|string';
		}
		$validator = Validator::make($request->all(), $rules);
			
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json([
				'status' => 'error',
				'message' => 'All fields are required',
				'errors' => $errors
			], 422);
		}
		
		$slug = str_slug($request->title);
		$post = Post::whereSlug($slug);
		if($post->count()){
			$slug .= '-'.time();
		}

		$post = new Post;
		$post->user_id = \Auth::user()->id;
		$post->category_id = $request->category;
		$post->title = $request->title;
		$post->slug = $slug;
		$post->description = $request->description;
		$post->type = $type;
		$post->hero_image = $request->hero_image;
		
		if($post->save()){
			$view = '';			
			if($isBlog){
				$view .= '<div class="col-md-4 mb-3 p-3">';
				$view .= View::make('_partials.card-post', ['post' => $post, 'removable' => $isProfile])->render();
				$view .= '</div>';
			}else{
				$view .= View::make('posts._forum-table-row', ['post' => $post, 'removable' => $isProfile])->render();
			}

			/* $administrators = Role::find(self::ROLE_ADMIN)->users()->get();
			if($administrators->count()){
				$delay = $when = now()->addMinutes(5);
				foreach($administrators as $admin){
					$admin->notify((new PostCreatedNotifyAdminMail($post))->delay($delay));
				}
			} */

			return response()->json([
				'status' => 'success',
				'message' => 'Thank you, your '.(($type == 'blog')?'post':'question').' will be displayed when its approved',
				'fn' => [
					/* 'append-element' => [
						'container' => '#latest-'.$type.'-container',
						'content' => $view
					], */
					'modal-close' => '#modal-add-'.$type,
					'form-reset' => '#modal-add-'.$type.' form',
					'set-attribute' => [
						'target' => '#post-hero-image',
						'attribute' => [
							'name' => 'src',
							'value' => ''
						]
					],
					'hide-element' => '#btn-remove-hero-image'
				]
			]);
		}
		
		return response()->json([
			'status' => 'error',
			'message' => 'Something went wrong'
		], 422);
	}

	public function react(Request $request, $id, $type){
		if($this->insight($request, $id, $type)){
			return response()->json([
				'status' => 'success',
				'message' => '',
				'fn' => [
					'update-content' => [[
							'target' => '.'.(($request->comment)?'comment':'post').'-'.$id.'-total-likes',
							'content' => ($request->comment)
								? Insight::postComments()->where('table_id', $id)->likes()->count()
								: Insight::posts()->where('table_id', $id)->likes()->count()
						],[
							'target' => '.'.(($request->comment)?'comment':'post').'-'.$id.'-total-dislikes',
							'content' => ($request->comment)
								? Insight::postComments()->where('table_id', $id)->dislikes()->count()
								: Insight::posts()->where('table_id', $id)->dislikes()->count()
					]]
				]
			]);
		}

		return response()->json([
			'status' => 'failed',
			'message' => ''
		]);
	}

	private function insight(Request $request, $id, $type){
		$user = \Auth::user();
		$saveAble = true;
		// \DB::enableQueryLog();
		$insight = isset($request->comment)? Insight::postComments() : Insight::posts();
		$insight =	$insight->where('table_id', $id)
			->where(function($query) use($type){
				if($type == 'views')
					$query->where('type', $type);
				else
					$query->where('type', 'likes')
						->orWhere('type', 'dislikes');
			})
			->where(function($query) use($request, $user){
				$query->where('ip_address', $request->ip())
					->when($user, function($_query) use($user){
						return $_query->where('user_id', $user->id);
					});
			})->first();
			
		// var_dump(\DB::getQueryLog());	
		if($insight){
			if($type == 'views'){
				$saveAble = false;
			}else{
				$insight->delete();
			}
		}

		if($saveAble){
			if(empty($insight)) $insight = new Insight;
			$insight->user_id = @$user->id;
			$insight->table = ($request->comment)? 'post_comments' : 'posts';
			$insight->table_id = $id;
			$insight->type = $type;
			$insight->ip_address = $request->ip();
			
			if($insight->save()){
				return true;
			}
		}

		return false;
	}

	private function fetchDataIndex($type, $categorySlug = ''){
		$data = [];
		if($categorySlug){
			$data['latest'] = Post::select('posts.*')
				->join('post_categories', 'posts.category_id', '=', 'post_categories.id')
				->where('posts.type', $type)
				->where('posts.status', 'approved')
				->when($categorySlug, function($query) use($categorySlug){
					return $query->where('post_categories.slug',  $categorySlug);
				})
				->orderBy('posts.updated_at', 'DESC')
				->limit($this->limit);
				
		}else{		
			$data['latest'] = Post::whereType($type)
			->where('posts.status', 'approved')
			->orderBy('posts.updated_at', 'DESC')
			->limit($this->limit);	
		}

		$data['popular'] = Post::whereType($type)
			->where('viewed', '>=', 10)
			->where('posts.status', 'approved')
			->orderBy('viewed', 'desc')
			->limit($this->limit);

		$data['categories'] = PostCategory::whereType($type)->get();

		return $data;
	}

	private function searchResponseAjax(Request $request, $posts, $type){
		$offset = ($request->offset == '')? 0 : $request->offset;
		$offset = ($request->offset != '')? $offset + $this->limit : $offset;
		
		$nextURL = 'search/'.$type.'?sort='.request('sort').'&category='.request('category').'&ref=next';

		$view = NULL;
		if($posts->count()){
			$posts = $posts->offset($offset)
				->limit($this->limit);

			/* \DB::enableQueryLog();
			$posts->get();
			var_dump(\DB::getQueryLog()); */
			foreach($posts->get() as $i => $post){
				$view .= View::make('posts/_forum-table-row', ['post' => $post])->render();
			}
		}
		
		if(!empty($view)){
			$fn = [
				'set-attribute' => [
					'target' => '#next-post',
					'attribute' => [
						'name' => 'href',
						'value' => $nextURL.'&offset='.($offset).'&limit='.$this->limit
					]
				],
				'show-element' => [
					'target' => '#post-'.$type.'-container'
				]
			];

			if(request('offset') != ''){
				$fn['append-element'] = [
						'container' => '#post-'.$type.'-container',
						'content' => $view
				];
			}else{
				$fn['update-content'] = [
						'target' => '#post-'.$type.'-container',
						'content' => $view
				];
			}
			return response()->json([
				'status' => 'success',
				'fn' => $fn,
			]);
		}else{
			$fn['hide-element'] = ['target' => '#post-loader-container'];
			
			if(request('ref') == '')
				$fn['update-content'] = [
					'target' => '#post-'.$type.'-container',
					'content' => '<tr class="spacer"><td colspan="5">&nbsp;</td></tr><tr><td colspan="5"><p class="text-center border text-muted">No data found</p></td></tr>'
				];

			return response()->json([
				'status' => 'success',
				'fn' => $fn
			]);
		}
	}
}
