<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class NotificationController extends Controller{
	
	public function __construct(){
		$this->middleware('auth');
	}

	public function index(){
		$user = \Auth::user();
		$notifications = [];

		$_notifications = $user->notifications()->latest()->limit(25)->get();
		if($_notifications->count()){
			foreach($_notifications as $notification){
				
				foreach($notification->data as $notify){
					if($notify['show'] == 'user-notifications'){
						$notify['data']['created_at'] = $notification->created_at->diffForHumans();
						$notify['data']['is_read'] = ($notification->read_at != NULL);
						$notify['data']['url'] .= '&i='.$notification->id;

						array_push($notifications, $notify['data']);
					}
				}
			}
		}

		return view('notifications', ['notifications' => $notifications]);
	}

	public function counter(){
		$user = \Auth::user();
		$counter = 0;

		$_notifications = $user->unreadNotifications()->get();
		if($_notifications->count()){
			foreach($_notifications as $notification){				
				foreach($notification->data as $notify){

					if($notify['show'] == 'user-notifications'){
						$counter++;
					}
				}
			}
		}

		$fn['update-content'] = [
			'target' => '#user-notif-counter',
			'content' => $counter
		];
		if($counter){
			$fn['show-element'] = ['target' => '#user-notif-counter'];
		}else{
			$fn['hide-element'] = ['target' => '#user-notif-counter'];
		}
		return response()->json([
			'status' => 'success',
			'message' => '',
			'fn' => $fn
		]);
	}

	public function latest(){
		$user = \Auth::user();
		$notifications = '';

		$_notifications = $user->unreadNotifications()->latest()->get();
		if($_notifications->count()){
			foreach($_notifications as $notification){				
				foreach($notification->data as $notify){

					if($notify['show'] == 'user-notifications'){
						$notifications .= View::make('_partials.list-user-notification', [
							'url' => $notify['data']['url'].'&i='.$notification->id,
							'thumbnail' => $notify['data']['thumbnail'],
							'title' => $notify['data']['title'],
							'message' => $notify['data']['message'],
							'time' => $notification->created_at->diffForHumans(),
						])->render();
					}
				}
			}
		}

		if($notifications)
			return response()->json([
				'status' => 'success',
				'message' => $notifications
			]);

			
		return response()->json([
			'status' => 'success',
			'message' => '<div class="text-muted text-center">All is good!</div>'
		]);
	}

}
