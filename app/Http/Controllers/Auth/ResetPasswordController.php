<?php

namespace App\Http\Controllers\Auth;

use Validator;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Hash;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;
		
		const ROLE_SUPERADMIN = 8;
		const ROLE_SUBADMIN = 9;
    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('change');
    }
		
		public function reset(Request $request){
			$this->validate($request, $this->rules(), $this->validationErrorMessages());
			$response = $this->broker()->reset(
				$this->credentials($request), function ($user, $password) {
					$this->resetPassword($user, $password);
				}
			);
				
			$user = User::where('email', $request->email)->first();
			$userRoleIds = $user->roles->pluck('id')->toArray();
			
			if(in_array(SELF::ROLE_SUPERADMIN, $userRoleIds) 
				|| in_array(SELF::ROLE_SUBADMIN, $userRoleIds)){
				$this->redirectTo = '/dashboard';
			}
			
			return $response == Password::PASSWORD_RESET
				? $this->sendResetResponse($response)
				: $this->sendResetFailedResponse($request, $response);
		}
		
		public function change(Request $request){
			$validator = Validator::make($request->all(), [
				'password' => 'required|string|min:6|confirmed',
			]);
			
			if($validator->fails()){
				return response()->json([
					'status' => 'error',
					'message' => 'Incorrect password',
					'errors' => $validator->errors()
				], 422);
			}
		
		
			$user = \Auth::user();
			if(Hash::check($request->old_password, $user->password)){
				$user->password = Hash::make($request->password);
				
				if($user->save()){
					return response()->json([
						'status' => 'success',
						'message' => 'Password changed successfully',
						'fn' => [
							'modal-close' => '#modal-cpassword',
							'form-reset' => '#form-cpassword'
						]
					]);
				}else{
					return response()->json([
						'status' => 'error',
						'message' => 'Failed to change your password'
					], 422);
				}
			}else{
				return response()->json([
					'status' => 'error',
					'message' => 'Incorrect current password'
				], 422);
			}
		}
}
