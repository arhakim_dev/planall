<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller{
	use SendsPasswordResetEmails;

	public function __construct(){
		$this->middleware('guest');
	}

	public function sendResetLinkEmail(Request $request){
		$this->validateEmail($request);
		$response = $this->broker()->sendResetLink(
			$request->only('email')
		);

		if($request->ajax()){
			if($response == Password::RESET_LINK_SENT){
				return response()->json([
					'status' => 'success',
					'message' => trans($response)
				]);
			}

			return response()->json([
				'status' => 'success',
				'message' => trans($response)
			], 422);
		}else{
			return $response == Password::RESET_LINK_SENT
				? $this->sendResetLinkResponse($response)
				: $this->sendResetLinkFailedResponse($request, $response);
		}
	}
}
