<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\UserSocial;
use Socialite;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Contracts\Auth\Authenticatable;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SocialiteController extends Controller
{
	use AuthenticatesUsers;
	public function __construct(){
		$this->middleware('guest');
	}
  
	public function redirectToProvider($provider){
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider){
            $_user = Socialite::driver($provider)->user();
            
            /* $guzzle = new GuzzleClient();
            $guzzleResponse = $guzzle->get($_user->avatar);

            echo($guzzleResponse->getBody()->getContents()); */
            $user = User::where('email', $_user->email)->first();
            if(empty($user)){
                $name = explode(' ', $_user->name);

                $user = new User;
                $user->first_name = $name[0];
                $user->last_name = $name[1];
                $user->email = $_user->email;

                if($user->save()){
                    $user->roles()->sync([1]);
                }
                $social = new UserSocial;

            }else{
                $social = UserSocial::where('id', $_user->id)->where('provider', $provider)->first();
                $social = empty($social)? new UserSocial : $social;
            }
            
            $social->id = $_user->id;
            $social->user_id = $user->id;
            $social->token = $_user->token;
            $social->provider = $provider;
            $social->save();
            // dd($user);
			// $user = User::find(1);
			// \Auth::login($user);
			// dd(\Auth::user());
            // if(\Auth::login($user)){
            //     $message = 'User authenticated. Redirecting...';
            // }else{
            //     $message = 'Failed to authenticate user';
            // }

            sleep(2);
            echo '
            <html>
                <head>
                    <meta charset="utf-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <title>'.config('app.name').' - '.$provider.' authenticating</title>
                    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
                    <link rel="stylesheet" href="'.asset('css/app.css').'" type="text/css">
                </head>
                <body>
                    <div class="mt-5 text-center">
                        <p id="status-social-auth" style="text-align:center;"></p>
                        <button class="btn bg-white rounded shadow" onclick="window.close();">Close</button>
                    </div>
                    <script src="'.asset('js/app.js').'"></script>
                    <script>window.opener.checkSocialLogin('.$user->id.');</script>
                </body>
            </html>';
    }

    public function login(Request $request){
        \Auth::loginUsingId($request->id);
        if(\Auth::check()) 
            return response()->json([
                'status' => 'success',
                'message' => '',
                'fn' => [
                    'redirect' => '/'
                ]
            ]);
        else{
            return response()->json([
                'status' => 'error',
                'message' => 'Login failed',
            ], 422);
        }
    }

}
