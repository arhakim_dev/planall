<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\UserSocial;
use GuzzleHttp\Client as GuzzleClient;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Validation\ValidationException;
use Socialite;


class LoginController extends Controller
{
	use AuthenticatesUsers;

	const ROLE_STUDENT = 1;
	protected $redirectTo = '/';

	public function __construct(){
		$this->middleware('guest')->except('logout');
	}
	
	protected function authenticated(Request $request, $user){
		if($user->status == 'active') return true;
		return false;
	}

	protected function sendLoginResponse(Request $request){
		$this->clearLoginAttempts($request);
		$user = $this->guard()->user();
		
		if($this->authenticated($request, $user)){
			$user->api_token = str_random(64);
			$user->save();
			if ($request->ajax() || $request->expectsJson()) {

				return response()->json([
					'status' => 'success',
					'fn' => [
						'redirect' => str_replace(url('/').'/', '', url()->previous())
					]
				]);
			}else{
				if(in_array(self::ROLE_STUDENT, (array)$user->roles))
					return redirect($this->redirectPath());
				else
					return redirect('dashboard');	
			}
		}else{
			$this->guard()->logout();
			$request->session()->flush();
			$request->session()->regenerate();
			
			if ($request->ajax() || $request->expectsJson()) {
				return response()->json([
					'status' => 'error',
					'message' => ($user->status == 'inactive')
						? 'Login failed,  your account is not active. Please check your email and click the activation button'
						: 'Login denied, we noticed your account has been blocked'
				], 422);
			}else{
				throw ValidationException::withMessages([
					$this->username() => [($user->status == 'inactive')
						? 'Login failed,  your account is not active. Please check your email and click the activation button'
						: 'Login denied, we noticed your account has been blocked'],
				], 422);
			}
		}
	}
	
	protected function sendFailedLoginResponse(Request $request){
		if($request->ajax() || $request->expectsJson()){
			return response()->json([
				'status' => 'error',
				'message' => 'Invalid email address or password'
			], 422);
			
		}else{
			throw ValidationException::withMessages([
					$this->username() => [trans('auth.failed')],
			]);
		}				
	}
		
	public function logout(Request $request){
			$user = \Auth::user();
			$user->api_token = NULL;
			$user->save();

			$isDashboard = str_contains(url()->previous(), 'dashboard');

			$this->guard()->logout();

			$request->session()->invalidate();

			if(str_contains(url()->previous(), 'dashboard'))
				return redirect('/dashboard');
			else
				return redirect('/');
	}
}
