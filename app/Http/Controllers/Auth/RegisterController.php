<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Role;
use App\Notifications\Auth\UserRegisterMail;
use App\Notifications\Auth\UserRegisterNotifyAdminMail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller{
	use RegistersUsers;

	const ROLE_STUDENT = 1;
	const ROLE_ADMIN = 8;

	protected $redirectTo = '/';

	
	public function __construct(){
		$this->middleware('guest')->except('register2', 'activate');
	}

	protected function validator(array $data){
		return Validator::make($data, [
			'first_name' => 'required|string|max:255',
			'last_name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:users',
			'password' => 'required|string|min:6|confirmed',
			'birth_date' => 'required|date',
			'country' => 'required|string|max:3',
			'phone' => 'nullable|max:15',
		]);
	}

	protected function create(array $data){
		return User::create([
			'first_name' => $data['first_name'],
			'last_name' => $data['last_name'],
			'email' => $data['email'],
			'birth_date' => $data['birth_date'],
			'country_code' => $data['country'],
			'phone' => $data['phone'],
			'gender' => $data['gender'],
			'password' => Hash::make($data['password']),
			'activation_code' => str_random(100),
		]);
	}
	
	protected function registered(Request $request, $user){
			\Auth::logout();
			if($user){
				$user->roles()->sync([self::ROLE_STUDENT]);

				// $this->sendMailNotification($user);

				$request->session()->flash(
					'flash_message', 
					'Welcome '.$user->first_name.', you are now registered to '.config('app.name').', please check your email to activate your account.');

				return response()->json([
					'status' => 'succes',
					'message' => '',
					'fn' => [
						'show-tab' => '#tab-signup li:last-child a',
						'set-attribute' => [
							'target' => 'form input.uid',
							'attribute' => [
								'name' => 'value',
								'value' => $user->id
							],
						]
					]
				]);
			}
	}
	private function sendMailNotification($user){
		$user->notify(new UserRegisterMail());

		$administrators = Role::find(self::ROLE_ADMIN)->users()->get();
		if($administrators->count()){
			$delay = $when = now()->addMinutes(5);
			foreach($administrators as $admin){
				$admin->notify((new UserRegisterNotifyAdminMail($user))->delay($delay));
			}
		}

	}
	
	public function register2(Request $request){
		$user = User::find($request->uid);
		if($user){
			$user->interest_course = $request->interest_course;
			$user->interest_location = $request->interest_location;
			$user->dream_university = $request->dream_university;
			
			if(!empty($request->photo)){
				$photo = str_replace('/tmp', '', $request->photo);
				// \Storage::move($request->photo, $photo);
				copy($request->photo, $photo);
				unlink($request->photo);
				
				$user->avatar = $photo;
			}
				
			if($user->save()){
				$request->session()->flash(
					'flash_message', 
					'Welcome '.$user->first_name.', you are now registered to '.config('app.name').', please check your email to activate your account.');
				return response()->json([
					'status' => 'succes',
					'fn' => [
							'redirect' => ''
					]
				]);
			}
		}

		return response()->json([
			'status' => 'error',
			'message' => 'Something went wrong. You can try again later',
				'fn' => [
					'redirect' => $this->redirectTo
				]
		]);
	}

	public function activate(Request $request){
		$user = User::where('activation_code', $request->query('code'))->first();
		if($user){
			$user->status = 'active';
			$user->activation_code = NULL;
			if($user->save()){
				\Auth::login($user);
											
				return redirect('profile')->with(
					'flash_message', 
					'Hi '.$user->first_name.', your account has been activated!');
			}
		}

		return redirect('')->with(
			'flash_message', 
			'Invalid activation code!'
		);
	}

}
