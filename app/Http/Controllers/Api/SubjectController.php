<?php

namespace App\Http\Controllers\Api;

use Validator;
use App\Subject;
use App\UniversitySubject;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class SubjectController extends Controller{

	public function __construct(){
		$this->middleware('auth:api');
	}

	public function index(){
		$subjects = [];
		$_subjects = Subject::withTrashed()
			->where('sort', '>', 0)
			->orderBy('sort')
			->get();

		$__subjects = Subject::withTrashed()
		->where('sort', 0)
			->orderBy('name')
			->get();
		
		$_subjects = $_subjects->merge($__subjects);
		
		if($_subjects->count()){
			foreach($_subjects as $i => $subject){
				$subjects[$i] = $subject;
				$subjects[$i]['total_universities'] = $subject->universities->count();
			}
		}

		return $subjects;
	}

	public function save(Request $request, $id = ''){
		$validator = Validator::make($request->except('id'), [
				'name' => 'required|string',
				'image' => 'required|string',
				'type' => 'required|array'
		]);
			
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json([
				'status' => 'error',
				'message' => 'The input are requred',
				'errors' => $errors
			], 422);
		}
		$subject = empty($id)? new Subject : Subject::find($id);
		
		$subject->name = $request->name;
		$subject->synonyms = $request->synonyms;
		$subject->description = $request->description;
		$subject->image = $request->image;
		$subject->type = $request->type;
		$subject->is_featured = $request->is_featured;

		if($subject->save()){
			return ['status' => 'success'];
		}

		return ['status' => 'failed'];
	}
	
	public function trash($id){
		if(Subject::findOrFail($id)->delete()){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
    }
    
	public function restore($id){
		if(Subject::withTrashed()->where('id', $id)->restore()){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
	}
	
	public function destroy($id){
		/* $subjects = UniversitySubject::where('subject_id', $id)->groupBy('university_id');
		if($subjects->count()){
				return response()->json([
						'status' => 'denied',
						'message' => 'There are '.$subjects->count().' universities using this subject. Remove permanently, denied'
				], 422);
		} */
		UniversitySubject::where('subject_id', $id)->delete();
		if(Subject::withTrashed()->where('id', $id)->forceDelete()){
				return ['status' => 'success'];
			}
		
		return ['status' => 'failed'];
	}
    	
	public function uploadFile(Request $request){
		$validator = Validator::make($request->all(), [
				'file' => 'required|image|mimes:jpeg,png,jpg|dimensions:min_width=400,min_height=200'
		]);
			
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json([
				'status' => 'error',
				'message' => 'Failed uploading file',
				'errors' => $errors
			], 422);
		}
	
		$file = $request->file('file');
		$fileName = str_random(64).'.'.$file->getClientOriginalExtension();
		$file->move(public_path('files/subject/'), $fileName);
		
		if($file){
			$file = 'files/subject/'.$fileName;
			return response()->json([
				'status' => 'success',
				'message' => '',
				'location' => $file,
				'file' => $file
			]);
		}
	
		return response()->json([
			'status' => 'error',
			'message' => 'Failed uploading file'
		], 400);
	}

	public function removeFile(Request $request){
		@unlink( public_path($request->file) );
		return ['status' => 'success'];
	}

	public function updateSortNumber(Request $request, $id, $sort){
		$validator = Validator::make(['sort' => $sort], [
			'sort' => [
				'required',
				Rule::unique('subjects')->where(function($query){
					return $query->where('sort', '<>', 0);
				})
			]
		]);
		
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json([
				'status' => 'error',
				'message' => 'Duplicate sort number.',
				'errors' => $errors
			], 422);
		}

		$subject = Subject::withTrashed()->whereId($id)->first();
		if($subject){
			$subject->sort = $sort;
			$subject->save();

			return ['status' => 'success', 'subject' => $subject];
		}

		return ['status' => 'failed'];
	}

}
