<?php

namespace App\Http\Controllers\Api;

use App\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CountryController extends Controller{
    
    public function __construct(){}

    public function index(Request $request){
        return Country::all();
        // return file_get_contents('http://battuta.medunes.net/api/country/all?key='.env('BATTUTA_KEY'));
    }

    /* public function regions(Request $request, $country){
        // return Country::all();
        return file_get_contents('http://battuta.medunes.net/api/region/'.$country.'/all?key='.env('BATTUTA_KEY'));
    }

    public function cities(Request $request, $country, $region){
        // return Country::all();
        return file_get_contents('http://battuta.medunes.net/api/city/'.$country.'/search?region='.$region.'&key='.env('BATTUTA_KEY'));
    } */

    public function fetch(){
        $countries = [];
        $_countries = json_decode(file_get_contents('https://restcountries.eu/rest/v2/all'));
        foreach($_countries as $i => $c){
            $countries[$i]['code'] = $c->alpha2Code;
            $countries[$i]['code3'] = $c->alpha3Code;
            $countries[$i]['name'] = $c->name;
            $countries[$i]['region'] = $c->region;
        }

        Country::insert($countries);
    }

}
