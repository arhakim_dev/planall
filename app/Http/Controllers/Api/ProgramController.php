<?php

namespace App\Http\Controllers\Api;

use App\Program;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProgramController extends Controller{

	public function __construct(){
		$this->middleware('auth:api');
	}
    
	public function save(Request $request, $id = ''){
		$program = empty($id)? new Program : Program::find($id);
		
		$program->parent_id = $request->parent_id;
		$program->name = $request->name;

		if($program->save()){
				return ['status' => 'success'];
		}

		return ['status' => 'failed'];
	}

	public function trash($id){
		if(Program::findOrFail($id)->delete()){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
		}
		
	public function restore($id){
		if(Program::withTrashed()->where('id', $id)->restore()){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
	}

	public function destroy($id){
		$universities = UniversityProgram::where('program_id', $id);
		if($universities->count()){
				return response()->json([
						'status' => 'denied',
						'message' => 'There are '.$universities->count().' universities using this program. Remove permanently, denied'
				], 422);
		}
		if(Program::withTrashed()->where('id', $id)->forceDelete()){
				return ['status' => 'success'];
			}
		
		return ['status' => 'failed'];
	}
		
}
