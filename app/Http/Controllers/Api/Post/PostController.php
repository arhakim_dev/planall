<?php

namespace App\Http\Controllers\Api\Post;

use App\Post;
use App\PostCategory;
use App\PostComment;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Notifications\NotifyWeb;
use Illuminate\Support\Facades\View;

class PostController extends Controller{

	public function __construct(){
		$this->middleware('auth:api');
	}

	public function index($type){
		$isBlog = ($type == 'blog');
		
		$posts = Post::withTrashed()->whereType($type)->orderBy('updated_at', 'DESC');
		$data = [];
		if($posts->count()){
			foreach($posts->get() as $i => $post){
				$data[$i] = $post;
				$data[$i]['category_name'] = @$post->category->name;
				$data[$i]['author'] = @$post->user->fullName;
				$data[$i]['posted_at'] = @$post->created_at->format('F d, Y - H:ia');
				$data[$i]['url'] = $post->url;
			}
		}
		return $data;
	}

	public function save(Request $request, $type, $id = '') {	
		$rules = [
			'title' => 'required',
			'slug' => 'required|unique:posts',
			'category' => 'required|integer',
			'description' => 'nullable|string',
		];

		if(!empty($id)){
			$post = Post::withTrashed()->where('id', $id)->whereType($type)->first();
			$rules['slug'] = [
				'required',
				Rule::unique('posts')->where(function($query) use($type){
					return $query->whereType($type);
				})
				->ignore($post->id)
			];
		}else{
			$rules['slug'] = [
				'required',
				Rule::unique('posts')->where(function($query){
					return $query->whereType($type);
				})
			];
		}
		$validator = Validator::make($request->all(), $rules);
			
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json([
				'status' => 'error',
				'message' => 'All fields are required',
				'errors' => $errors
			], 422);
		}
		
		$post = ($id)? Post::withTrashed()->where('id', $id)->first() : new Post;
		$post->user_id = \Auth::user()->id;
		$post->category_id = $request->category;
		$post->title = $request->title;
		$post->slug = $request->slug;
		$post->description = $request->description;
		$post->type = $type;
		$post->hero_image = $request->hero_image;
		
		if($post->save()){
			return ['status' => 'success'];
		}
		
		return response()->json(['status' => 'failed'], 422);
	}

	public function trash($type, $id){
		if(Post::find($id)->delete()){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
	}
    
	public function restore($type, $id){
		if(Post::withTrashed()->where('id', $id)->restore()){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
	}
	
	public function approve($type, $id){
		$post = Post::withTrashed()->where('id', $id);
		if($post->update(['status' => 'approved'])){
			$this->approvedSendWebNotication($post);

			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
	}
	
	private function approvedSendWebNotication($post){
		$post = $post->first();
		$user = $post->user;

		$postURL = '<a href="'.$post->url.'">'.str_limit($post->title, 80).'</a>';

		$data = [[
			'show' => 'user-activities',
			'data' => [
				'avatar' => $user->avatar,
				'name' => $user->fullName,
				'message' => ($post->type == 'blog')
					? 'posted a blog on '.$postURL
					: 'ask a question on '.$postURL
			]
		], [
			'show' => 'user-notifications',
			'data' => [
				'url'=> $post->url.'?ref=user-notif',
				'thumbnail' => '',
				'title' => $post->title,
				'message' => ($post->type == 'blog')
					? 'your post has been approved'
					: 'your question has been approved',
			]
		
		]];
		$user->notify(new NotifyWeb($data));
	}

	public function block($type, $id){
		if(Post::withTrashed()->where('id', $id)->update(['status' => 'block'])){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
	}

	public function unblock($type, $id){
		if(Post::withTrashed()->where('id', $id)->update(['status' => 'approved'])){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
	}
	
	public function destroy($type, $id){
		if(Post::withTrashed()->where('id', $id)->forceDelete()){
			return ['status' => 'success'];
		}
		
		return ['status' => 'failed'];
	}
     
	public function comments($type){	
		$comments = PostComment::withTrashed()->whereType($type)->latest();
		
		$data = [];
		$i = 0;
		if($comments->count()){
			foreach($comments->get() as $comment){
				if(empty($comment->post)) continue;
				$data[$i] = [
					'id' => $comment->id,
					'user' => $comment->name,
					'user_url' => !empty($comment->user)? $comment->user->url : 'javascript:void(0);',
					'description' => $comment->description,
					'status' => $comment->status,
					'type' => $comment->type,
					'post' => $comment->post->title,
					'post_url' => $comment->post->url,
					'comment_at' => $comment->created_at->format('F d, Y - H:ia')
				];
				$i++;
			}
		}

		return $data;
	}

	public function commentUpdateStatus(Request $request, $type, $id){
		$status = (str_contains(url()->current(), 'approve'))? 'approved' : 'block';

		$comment = PostComment::withTrashed()->where('id', $id)->first();
		$comment->status = $status;
		if($comment->save()){

			if($comment->user)
				$this->commentApprovedSendWebNotication($comment);

			return ['status' => 'success'];
		}

		return ['status' => 'failed'];
	}
	
	private function commentApprovedSendWebNotication($comment){
		$post = $comment->post;
		$poster = $post->user;
		$commenter = $comment->user;

		$postURL = '<a href="'.$post->url.'">'.str_limit($post->title, 80).'</a>';
		$posterURL = '<a href="'.$poster->url.'">'.$poster->fullName.'</a>';
		$commenterURL = '<a href="'.$commenter->url.'">'.$commenter->fullName.'</a>';

		$data = [[
			'show' => 'user-activities',
			'data' => [
				'avatar' => $commenter->avatar,
				'name' => $commenter->fullName,
				'message' => ($comment->type == 'blog')
				? ' commented on the blog '.$postURL
				: ' responded to the question posted by <a href="'.$post->url.'">'.$poster->fullName.'</a>'
			]
		], [
			'show' => 'user-notifications',
			'data' => [
				'url'=> $post->url.'?ref=user-notif',
				'thumbnail' => '',
				'title' => $post->title,
				'message' => ($comment->type == 'blog')
					? ' your comment has been approved'
					: ' your response has been approved',
			]
		
		]];
		$commenter->notify(new NotifyWeb($data));
	}
}
