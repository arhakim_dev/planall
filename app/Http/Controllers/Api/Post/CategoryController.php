<?php

namespace App\Http\Controllers\Api\Post;

use Validator;
use App\Post;
use App\PostCategory;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class CategoryController extends Controller{

	public function __construct(){
		$this->middleware('auth:api');
	}

	public function index(Request $request, $type){
		$isActiveOnly = ($request->active)? true : false;
		
		if($isActiveOnly)
			$categories = PostCategory::whereType($type)->get();
		else
			$categories = PostCategory::withTrashed()->whereType($type)->get();

		$data = [];
		if($categories->count()){
			foreach($categories as $i => $category){
				$data[$i] = $category;
				$data[$i]['url'] = $category->url;
			}
		}

		return $data;
	}
	
	public function save(Request $request, $type, $id = ''){
		$rules= ['name' => 'required'];

		if(!empty($id)){
			$category = PostCategory::withTrashed()->where('id', $id)->first();
			$rules['slug'] = [
				'required',
				Rule::unique('post_categories')->ignore($category->id)
			];
		}else{
			$rules['slug'] = 'required|unique:post_categories,slug';
		}
		$validator = Validator::make($request->except('id'), $rules);
			
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json([
				'status' => 'error',
				'message' => 'The input are requred',
				'errors' => $errors
			], 422);
		}
		$category = ($id)? PostCategory::withTrashed()->where('id', $id)->first() : new PostCategory;
		$category->name = $request->name;
		$category->type = $type;
		$category->slug = $request->slug;

		if($category->save()){
			return ['status' => 'success'];
		}

		return ['status' => 'failed'];
	}
    
	public function trash($type, $id){
		if(PostCategory::findOrFail($id)->delete()){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
	}
    
	public function restore($type, $id){
		if(PostCategory::withTrashed()->where('id', $id)->restore()){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
	}
	
	public function destroy($type, $id){
		$totalPost = Post::where('category_id', $id)->count();
		if($totalPost){
			return response()->json([
				'status' => 'denied',
				'message' => 'This category has '.$totalPost.' '.$type.' posts. Remove category permanently, denied'
			], 422); 
		}
		if(PostCategory::withTrashed()->where('id', $id)->forceDelete()){
			return ['status' => 'success'];
		}
		
		return ['status' => 'failed'];
	}
   
}
