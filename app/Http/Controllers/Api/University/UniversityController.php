<?php

namespace App\Http\Controllers\Api\University;

use Validator;
use App\University;
use App\UniversitySubject;
use App\UniversityFacility;
use App\UniversityAfterCollege;
use App\UniversityAlumni;
use App\UniversityProgram;
use App\Program;
use App\UniversityReview;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class UniversityController extends Controller{

	private $messages = [];
	private $maxUniversityFeatured = 6;

	private $excelEmptyValues = [NULL, 'na', 'n/a', 'no'];
	private $excelNotAvailableValues = ['not available', 'no information available', 'not available on the website'];

	public function __construct(){
		$this->middleware('auth:api');
	}
	
	public function index(){
		$universities = [];
		$i = 0;
		$_universities = University::withTrashed()->orderBy('name');
		if($_universities->count()){
				foreach($_universities->get() as $university){
						$universities[$i] = $university;
						$universities[$i]['url'] = $university->url;
						$universities[$i]['country'] = ($university->country)? $university->country : '';
						$i++;
				}
		}

		return $universities;
	}

	public function save(Request $request, $id = ''){
		$university = '';

		$rules = [
			'name'  => 'required',
			'address' => 'required',
			'city' => 'required',
			'zip' => 'required|alpha_num|max:10',
			'website' => ['nullable', 'regex:'.$this->ctrl__regexUrlPattern],
			'facebook' => ['nullable', 'regex:'.$this->ctrl__regexUrlPattern],
			'twitter' => ['nullable', 'regex:'.$this->ctrl__regexUrlPattern],
			'youtube' => ['nullable', 'regex:'.$this->ctrl__regexUrlPattern],
			'linkedin' => ['nullable', 'regex:'.$this->ctrl__regexUrlPattern],
			'founding_year' => 'required|numeric|digits:4',
			'logo' => 'required',
			// 'hero_image' => 'required',
			'country_code' => 'required'
		];

		if(!empty($id)){
			$university = University::find($id);
			$rules['slug'] = [
				'required',
				Rule::unique('universities')->ignore($university->id)
			];
		}else{
			$rules['slug'] = 'required|unique:universities,slug';
		}
		$validator = Validator::make($request->all(), $rules);

		if($validator->fails()){
				return response()->json([
						'status' => 'error',
						'message' => 'These fields are required',
						'errors' => $validator->errors()
				], 422);
		}

		if($request->is_featured == 1){
			if(University::where('is_featured', 1)->count() > $this->maxUniversityFeatured){
				return response()->json([
					'status' => 'error',
					'message' => 'only '.$this->maxUniversityFeatured.' active universities can be shown in the featured list'
				], 422);
			}
		}

		if($id){
				if(University::withTrashed()->where('id', $id)->update($request->except('id', 'action', 'method'))){
						return ['status' => 'success'];
				}            
		}else{
				$university = University::create($request->except('id', 'action', 'method'));
				if($university){
						$university->action = 'api/university/'.$university->id;
						$university->method = 'put';
						return ['status' => 'success', 'university' => $university];
				}
		}

		return ['status' => 'failed'];
	}

	public function uploadFile(Request $request, $type){
		if($type == 'hero'){
				$validator = Validator::make($request->all(), [
						'file' => 'required|image|mimes:jpeg,png,jpg|dimensions:min_width=900,min_height=400'
				],[
						'dimensions' => "The :attribute has invalid image dimensions.\n It should have 900x400 pixels for the minimum size"
				]);
		}elseif($type == 'logo' || $type == 'avatar'){
				$validator = Validator::make($request->all(), [
						'file' => 'required|image|mimes:jpeg,png,jpg|dimensions:min_width=200,min_height=200,ratio=1/1'
				],[
						'dimensions' => "The :attribute has invalid image dimensions.\n It should have 200x200 pixels for the minimum size and 1:1 for the ration"
				]);
		}elseif($type == 'excel'){
				$validator = Validator::make($request->all(), [
						'file' => 'required|mimes:xls,xlsx'
				],[
						'mimes' => "The :attribute should be in a spreadsheet format. Or please try to re-save the file using .xls extension"
				]);
		}
			
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json([
				'status' => 'error',
				'message' => 'Failed uploading file',
				'errors' => $errors
			], 422);
		}
	
		$file = $request->file('file');
		if(($type == 'excel')){
				$fileName = $file->getClientOriginalName();
		}else{
				$fileName = str_random(64).'.'.$file->getClientOriginalExtension();
		}
		$file->move(public_path('files/'.$type.'/'), $fileName);
		
		if($file){
			$file = 'files/'.$type.'/'.$fileName;
			return response()->json([
				'status' => 'success',
				'message' => '',
				'location' => $file,
				'file' => $file
			]);
		}
	
		return response()->json([
			'status' => 'error',
			'message' => 'Failed uploading file'
		], 400);
	}

	public function removeFile(Request $request, $type){
		@unlink( public_path($request->file) );
		return ['status' => 'success'];
	}

	public function trash($id){
		if(University::findOrFail($id)->delete()){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
	}
		
	public function restore($id){
		if(University::withTrashed()->where('id', $id)->restore()){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
	}

	public function destroy($id){
		UniversitySubject::where('university_id', $id)->delete();
		UniversityFacility::where('university_id', $id)->delete();
		UniversityAfterCollege::where('university_id', $id)->delete();
		UniversityAlumni::where('university_id', $id)->delete();
		UniversityProgram::where('university_id', $id)->delete();
		UniversityReview::where('university_id', $id)->delete();

		if(University::withTrashed()->where('id', $id)->forceDelete()){
			return ['status' => 'success'];
		}
		
		return ['status' => 'failed'];
	}
			
	public function destroyReview($id){
		if(UniversityReview::find($id)->delete()){
			return ['status' => 'success'];
		}
		
		return ['status' => 'failed'];
	}
	
	public function bulkTrash(Request $request){
		$universityIds = $request->universityIds;
		$messages = '';
		$universities = University::whereIn('id', $universityIds)->get();
		if($universities->count()){
			foreach($universities as $university){
				$_university = $university;
				
				if($university->delete()){
					$messages .= "\n- ".$_university->name.' removed';
				}else{
					$messages .= "\n- ".$_university->name.' not removed';
				}
			}
		}

		$messages = empty($messages)? 'No data should be removed' : $messages;
		return response()->json([
			'status' => 'success',
			'message' => $messages
		]);
	}
    	
	public function bulkRestore(Request $request){
		$universityIds = $request->universityIds;
		$messages = '';
		$university = University::onlyTrashed()
			->whereIn('id', $universityIds)
			->get();

		if($university->count()){
			foreach($university as $university){
				$_university = $university;
				
				if($university->restore()){
					$messages .= "\n- ".$_university->name.' restored';
				}else{
					$messages .= "\n- ".$_university->name.' not restored';
				}
			}
		}

		$messages = empty($messages)? 'No data should be restored' : $messages;
		return response()->json([
			'status' => 'success',
			'message' => $messages
		]);
	}
    	
	public function bulkDestroy(Request $request){
		$universityIds = $request->universityIds;
		$messages = '';
		$university = University::onlyTrashed()
			->whereIn('id', $universityIds)
			->get();

		if($university->count()){
			foreach($university as $university){
				$_university = $university;
				
				if($university->forceDelete()){
					$messages .= "\n- ".$_university->name.' destroyed';
				}else{
					$messages .= "\n- ".$_university->name.' not destroyed';
				}
			}
		}

		$messages = empty($messages)? 'No data should be destroyed' : $messages;
		return response()->json([
			'status' => 'success',
			'message' => $messages
		]);
	}

	public function import(Request $request){
		$files = isset($request->all()['files'])? $request->all()['files'] : [];
		if(count($files)){
			foreach($files as $file){
				try{
						$sheet = \PhpOffice\PhpSpreadsheet\IOFactory::load( public_path($file) );
						$worksheet = $sheet->getSheetByName('Data');
						$data = $worksheet->toArray(null, true, true, true);
						if($this->isImportDuplicate($file, $data)){
							continue;
						}
						
						$university = $this->excelToUniversity($data);
						if($university){
							$this->excelToUniversityFacility($university, $data);
							$this->excelToUniveresityAfterCollage($university, $data);               
							$this->excelToUniveresityAlumni($university, $data);               
							$this->excelToUniveresityReview($university, $data);               
							$this->excelToUniveresityProgram($university, $worksheet);

							// $data = $sheet->getSheetByName('Majors')->toArray(null, true, true, true);
							// $this->excelToUniversityMajor($university, $data);

							$data = $sheet->getSheetByName('Images')->getDrawingCollection();
							$this->excelImagesToUniversity($university, $data);

							@unlink( public_path($file) );
						}

				}catch(\PhpOffice\PhpSpreadsheet\Reader\Exception $e){
						$this->messages .= $e;
				}
			}
				
			if(empty($this->messages))
				return ['status' => 'success'];
			else
				return [
					'status' => 'success', 
					'message' => 'Something went wrong',
					'errors' => $this->messages
				];

		}else{
			return response()->json([
				'status' => 'error',
				'message' => 'No file selected'
			], 422);
		}
	}

	private function isImportDuplicate($file, $data){
		$universityName = $this->excelGetValue($data[3]['E']);

		if($universityName){
			$university = University::withTrashed()
				->where(\DB::raw('UPPER(name)'), strtoupper($universityName))
				->count();
			
			if($university){
				array_push($this->messages, [
					"Duplicate university ".$universityName
				]);
				return true;
			}

			return false;
		}else{
			array_push($this->messages, [
				"Cannot process file ".basename(public_path($file)).' because cell "Data!3E" is empty']);
			return true;
		}
	}

	private function excelToUniversity($data){
		$university = University::create([
			'name' => $this->excelGetValue($data[3]['E']),
			'slug' => str_slug($this->excelGetValue($data[3]['E'])),
			'address' => $this->excelGetValue($data[4]['E']),
			'website' => $this->excelGetValue($data[5]['E']),
			'is_public' => (strtoupper($this->excelGetValue($data[6]['E'])) == 'PUBLIC')? 1 : 0,
			'founding_year' => intval($this->excelGetValue($data[7]['E'])),
			'description' => $this->excelGetValue($data[8]['E']),
			'facebook' => $this->excelGetValue($data[27]['E']),
			'twitter' => $this->excelGetValue($data[28]['E']),
			'linkedin' => $this->excelGetValue($data[29]['E']),
			'youtubze' => $this->excelGetValue($data[30]['E']),
			'show_desc' => 1,
		]);
		return $university;
	}

	private function excelToUniversityFacility($university, $data){
		$general = $this->excelGetValue($data[14]['E']);
		$places = $this->excelGetValue($data[16]['E']);

		$facility = new UniversityFacility;
		$facility->university_id = $university->id;
		$facility->general = $general;
		$facility->accomodation = $this->excelGetValue($data[15]['E']);
		$facility->nearby_places = $places;
		$facility->save();
	}

	private function excelToUniveresityAfterCollage($university, $data){
		$afterCollage = new UniversityAfterCollege;
		$afterCollage->university_id = $university->id;
		$afterCollage->earning = $this->excelGetValue($data[18]['E']);
		$afterCollage->placement_rate = $this->excelGetValue($data[19]['E']);
		$afterCollage->top_employees = $this->excelGetValue($data[20]['E']);
		$afterCollage->save();
	}

	private function excelToUniveresityAlumni($university, $data){
		$alumni = new UniversityAlumni;
		$alumni->university_id = $university->id;

		$_alumnis = explode(',', $this->excelGetValue($data[22]['E']));
		if(count($_alumnis)){
			foreach($_alumnis as $i => $name){
				$i += 1;
				if($i <= 5)
					$alumni->famous_name.$i = $name;
				else
					break;
			}
		}
		$alumni->website = $this->excelGetValue($data[23]['E']);
		$alumni->save();
	}

	private function excelToUniveresityReview($university, $data){
		$review = new UniversityReview;
		$review->university_id = $university->id;

		$descriptions = [];
		if(!empty($this->excelGetValue($data[25]['E']))){
			$reviews = explode('###', $this->excelGetValue($data[25]['E']));
			foreach($reviews as $i => $review){
				$descriptions[$i]['university_id'] = $university->id;
				$descriptions[$i]['description'] = $review;
			}
		}
		if(count($descriptions)) UniversityReview::insert($descriptions);
	}
	
	private function excelToUniveresityProgram($university, $worksheet){
		$programs = [];
		$categoryId = 2;
		$data = $worksheet->toArray(null, true, true, true);
		
		for($row = 36; $row <= 56; $row++){
			if($row == 45 || $row == 49 || $row == 53) continue; // skip empty rows

			$mergedCells = NULL;
			$cell = $worksheet->getCell('D'.$row);
			foreach($worksheet->getMergeCells() as $mergeRange){
				if ($cell->isInRange($mergeRange)) {
					$mergedCells = $mergeRange;
					break;
				}
			}

			$programs[$row]['university_id'] = $university->id;
			$programs[$row]['program_id'] = $categoryId;
			if($mergedCells){
				$value = $this->excelGetValue($data[$row]['D']);
				if($row == 56){
					$gender = $this->excelGetGenderRatio($value);
					$programs[$row]['undergraduate'] = $gender->male;
					$programs[$row]['ms'] = $gender->male;
					$programs[$row]['mba'] = $gender->male;

					$programs[57]['university_id'] = $university->id;
					$programs[57]['program_id'] = 24;
					$programs[57]['undergraduate'] = $gender->female;
					$programs[57]['ms'] = $gender->female;
					$programs[57]['mba'] = $gender->female;

				}else{
					$programs[$row]['undergraduate'] = $value;
					$programs[$row]['ms'] = $value;
					$programs[$row]['mba'] = $value;
				}
			}else{
				
				$mergedCells = NULL;
				$cell = $worksheet->getCell('E'.$row);
				foreach($worksheet->getMergeCells() as $mergeRange){
					if ($cell->isInRange($mergeRange)) {
						$mergedCells = $mergeRange;
						break;
					}
				}
				if($mergedCells){
					if($row == 56){
						$gender = $this->excelGetGenderRatio($this->excelGetValue($data[$row]['D']));
						$programs[$row]['undergraduate'] = $gender->male;
	
						$programs[57]['university_id'] = $university->id;
						$programs[57]['program_id'] = 24;
						$programs[57]['undergraduate'] = $gender->female;

						$gender = $this->excelGetGenderRatio($this->excelGetValue($data[$row]['E']));
						$programs[$row]['ms'] = $gender->male;
						$programs[$row]['mba'] = $gender->male;
	
						$programs[57]['ms'] = $gender->female;
						$programs[57]['mba'] = $gender->female;

					}else{
						$programs[$row]['undergraduate'] = $this->excelGetValue($data[$row]['D']);

						$value = $this->excelGetValue($data[$row]['E']);				
						$programs[$row]['ms'] = $value;
						$programs[$row]['mba'] = $value;
					}

				}else{
					if($row == 56){
						$gender = $this->excelGetGenderRatio($this->excelGetValue($data[$row]['D']));
						$programs[$row]['undergraduate'] = $gender->male;
	
						$programs[57]['university_id'] = $university->id;
						$programs[57]['program_id'] = 24;
						$programs[57]['undergraduate'] = $gender->female;

						$gender = $this->excelGetGenderRatio($this->excelGetValue($data[$row]['E']));
						$programs[$row]['ms'] = $gender->male;
						$programs[$row]['mba'] = $gender->male;
	
						$programs[57]['ms'] = $gender->female;
						$programs[57]['mba'] = $gender->female;

					}else{
						$programs[$row]['undergraduate'] = $this->excelGetValue($data[$row]['D']);
						$programs[$row]['ms'] = $this->excelGetValue($data[$row]['E']);
						$programs[$row]['mba'] = $this->excelGetValue($data[$row]['F']);
					}
				}
			}

			$categoryId++;
			if($categoryId ==  11 || $categoryId ==  15 || $categoryId ==  19 || $categoryId ==  23) 
				$categoryId++;
		}

		UniversityProgram::insert($programs);
	}

	/* private function excelToUniversityMajor($university, $data){
		$majors = [];
		$ii = 0;
		foreach($data as $i => $major){
			$i++;
			if($i >= 5){
				if(!empty($major['C'])){
					$majors[$ii]['university_id'] = $university->id;
					$majors[$ii]['name'] = trim(str_replace('•', '', $major['C']));
					$majors[$ii]['type'] = 'undergraduate';
					$ii++;
				}
				
				if(!empty($major['D'])){
					$majors[$ii]['university_id'] = $university->id;
					$majors[$ii]['name'] = trim(str_replace('•', '', $major['D']));
					$majors[$ii]['type'] = 'ms';
					$ii++;
				}
				
				if(!empty($major['E'])){
					$majors[$ii]['university_id'] = $university->id;
					$majors[$ii]['name'] = trim(str_replace('•', '', $major['E']));
					$majors[$ii]['type'] = 'mba';
					$ii++;
				}
			}
		}
		UniversityMajor::insert($majors);
	} */

	private function excelImagesToUniversity($university, $data){
		$files = [];
		foreach($data as $i => $drawing){
			if($drawing instanceof \PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing){
				ob_start();
				call_user_func(
					$drawing->getRenderingFunction(),
					$drawing->getImageResource()
				);
				$imageContents = ob_get_contents();
				ob_end_clean();
				switch ($drawing->getMimeType()) {
					case \PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing::MIMETYPE_PNG :
						$extension = 'png';
						break;
					case \PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing::MIMETYPE_GIF:
						$extension = 'gif';
						break;
					case \PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing::MIMETYPE_JPEG :
						$extension = 'jpg';
						break;
				}
			}else{
				$zipReader = fopen($drawing->getPath(),'r');
				$imageContents = '';
				while (!feof($zipReader)) {
					$imageContents .= fread($zipReader,2048);
				}
				fclose($zipReader);
				$extension = $drawing->getExtension();
			}

			$c = $drawing->getCoordinates();
			preg_match_all('!\d+!', $c, $matches);
			$column = $matches[0][0];
			$row = strstr($c,$column,true);

			$path = 'files/tmp/';
			$myFileName = public_path($path.'00_Image_'.++$i.'.'.$extension);
			file_put_contents($myFileName, $imageContents);

			$imageInfo = list($height, $width) = getimagesize($myFileName);
			$files[$i]['file'] = $myFileName;
			$files[$i]['extension'] = $extension;
			$files[$i]['size'] = filesize($myFileName);
			$files[$i]['height'] = $imageInfo[0];
			$files[$i]['width'] = $imageInfo[1];
		}
		
		$files = collect($files);
		$logoFile = $files->where('size', $files->min('size'));
		$heroFile = $files->where('size', $files->max('size'));

		$logo = '';
		$hero = '';
		if($logoFile->count()){
			foreach($logoFile as $file){
				if($file['height'] > 200 && $file['width'] > 200){
					if(round($file['height'] / $file['width']) == 1){
						$logo = 'files/logo/'.str_random(64).'.'.$file['extension'];
						rename($file['file'], $logo);
					}else{
						array_push($this->messages, [
							$university->name.': Invalid logo image ratio. It should have 1:1 image ratio'
						]);
					}
				}else{
					array_push($this->messages, [
						$university->name.': Invalid logo image dimension. It should have 200x200 pixels for the minimum size'
					]);
				}
			}
		}
		if($heroFile->count()){
			foreach($heroFile as $file){
				if($file['height'] > 400 && $file['width'] > 900){
					$hero = 'files/hero/'.str_random(64).'.'.$file['exstension'];
					rename($file['file'], $hero);
				}else{
					array_push($this->messages, [
						$university->name.': Invalid hero image dimension. It should have 900x400 pixels for the minimum size'
					]);
				}
			}
		}

		$university->logo = $logo;
		$university->hero_image = $hero;
		$university->save();
	}
	
	private function excelGetValue($cell){
		$value = $this->excelValidateValue($cell);

		return $value;
	}

	private function excelValidateValue($cell){
		if(in_array(strtolower($cell), $this->excelEmptyValues)){
			return NULL;
		}

		return trim($cell);
	}

	private function excelGetGenderRatio($value){
		$gender = new \stdClass();
		$gender->male = 0;
		$gender->female = 0;
		
		$value = in_array(strtolower($value), $this->excelNotAvailableValues)? 0 : $value;
		$value = explode(',', $value);
		if(isset($value[1])){
			if(str_contains(strtolower('female'), $value[0])){
				$value[0] = trim(str_replace('female', '', $value[0]));
				$gender->female = $value[0];
				
			}elseif(str_contains(strtolower('male'), $value[0])){
				$value[0] = trim(str_replace('male', '', $value[0]));
				$gender->male = $value[0];
			}
			
			if(str_contains(strtolower('female'), $value[1])){
				$value[1] = trim(str_replace('female', '', $value[1]));
				$gender->female = $value[1];
				
			}elseif(str_contains(strtolower('male'), $value[1])){
				$value[1] = trim(str_replace('male', '', $value[1]));
				$gender->male = $value[1];
			}
			
		}else{
			$value = explode(':', $value[0]);
			if(isset($value[1])){
				$gender->male = $value[0];
				$gender->female = $value[1];
			}
		}
		
		return $gender;
	}

}
