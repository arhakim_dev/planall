<?php

namespace App\Http\Controllers\Api\University;

use App\UniversityMajor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MajorController extends Controller{

    public function __construct(){
        $this->middleware('auth:api');
    }

    public function index($universityId){
        $u = UniversityMajor::where('university_id', $universityId)->where('type', 'undergraduate')->pluck('name');
        $u = $u->count() ? implode(PHP_EOL, $u->toArray()) : '';

        $ms = UniversityMajor::where('university_id', $universityId)->where('type', 'ms')->pluck('name');
        $ms = $ms->count() ? implode(PHP_EOL, $ms->toArray()) : '';

        $mba = UniversityMajor::where('university_id', $universityId)->where('type', 'mba')->pluck('name');
        $mba = $mba->count() ? implode(PHP_EOL, $mba->toArray()) : '';

        return [
            'undergraduate' => $u,
            'ms' => $ms,
            'mba' => $mba
        ];
    }

    public function save(Request $request){

        $majors = $this->formatData($request->undergraduate, $request->university_id,'undergraduate');        
        if(count($majors)){
            UniversityMajor::where('university_id', $request->university_id)
                ->where('type', 'undergraduate')
                ->delete();
            
            UniversityMajor::insert($majors);
        }

        $majors = $this->formatData($request->ms, $request->university_id,'MS');        
        if(count($majors)){
            UniversityMajor::where('university_id', $request->university_id)
                ->where('type', 'MS')
                ->delete();
            
            UniversityMajor::insert($majors);
        }

        $majors = $this->formatData($request->mba, $request->university_id,'MBA');        
        if(count($majors)){
            UniversityMajor::where('university_id', $request->university_id)
                ->where('type', 'MBA')
                ->delete();
            
            UniversityMajor::insert($majors);
        }

        return ['status' => 'success'];

    }

    private function formatData($data, $universityId, $type){
        $_majors = collect(preg_split('/\r\n|\r|\n/', $data));
        $__majors = $_majors->reject(function($value, $key){
            return empty($value);
        });
        
        $majors = [];
        if($__majors->count()){
            foreach($__majors as $i => $name){
                $majors[$i]['university_id'] = $universityId;
                $majors[$i]['name'] = $name;
                $majors[$i]['type'] = $type;
            }
        }

        return $majors;
    }

}
