<?php

namespace App\Http\Controllers\Api\University;

use App\UniversityAfterCollege;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AfterCollageController extends Controller{

    public function __construct(){
		$this->middleware('auth:api');
    }
    
    public function index($universityId){
        return UniversityAfterCollege::where('university_id', $universityId)->first();
         
    }
    
    public function save(Request $request){
        $data = UniversityAfterCollege::where('university_id', $request->university_id)->first();
        $data = empty($data)? new UniversityAfterCollege : $data;
        
        $data->university_id = $request->university_id;
        $data->earning = $request->earning;
        $data->placement_rate = $request->placement_rate;
        $data->top_employees = $request->top_employees;

        if($data->save()){
            return ['status' => 'success'];
        }

        return ['status' => 'failed'];
    }

}
