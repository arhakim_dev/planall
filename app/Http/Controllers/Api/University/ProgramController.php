<?php

namespace App\Http\Controllers\Api\University;

use App\UniversityProgram;
use App\Program;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProgramController extends Controller{

	public function __construct(){
		$this->middleware('auth:api');
	}
    
	public function index($universityId, $parentId){
		$programs = $this->programs($parentId);
		return $this->formatData($universityId, $programs);
	}

	public function activePrograms($universityId, $parentId){
		$programs = $this->programs($parentId, false);
		return $this->formatData($universityId, $programs);
	}
    
	public function programs($parentId = '', $withTrashed = true){
		$withChild = !is_numeric($parentId);
		
		$programs = [];
		if($withTrashed){
			$_programs = Program::withTrashed()
				->where('parent_id', intval($parentId))
				->orderBy('name');
		}else{
			$_programs = Program::where('parent_id', intval($parentId))
				->orderBy('name');
		}
		if($withChild){
			if($_programs->count()){
				foreach($_programs->get() as $program){
					array_push($programs, $program->toArray());

					if($withTrashed){
						$childs = Program::withTrashed()
							->where('parent_id', $program->id)
							->orderBy('name');
					}else{
						$childs = Program::where('parent_id', $program->id)
							->orderBy('name');
					}

					if($childs->count()){
					foreach($childs->get() as $child){
						array_push($programs, $child->toArray());
					}
					}
				}
			}
		}else{
			if($_programs->count()){
				foreach($_programs->get() as $_program){
					array_push($programs, $_program->toArray());
				}
			}
		}

		return $programs;
	}

	public function save(Request $request, $parent){
		$_programs = collect($request->programs);
		if($_programs->count()){
			$programs = [];
			foreach($_programs as $i => $program){
				$program = collect($program)->forget('program_name');
				$program = collect($program)->forget('isEmpty');
				$program = collect($program)->forget('isTrashed');
				$programs[$i] = $program->put('university_id', $request->university_id)->all();
			}
			if(count($programs)){
				$programIds = array_column($this->programs($parent), 'id');
				
				UniversityProgram::where('university_id', $request->university_id)
						->whereIn('program_id', $programIds)
						->delete();
				
				if(UniversityProgram::insert($programs)){
						return ['status' => 'success'];
				}
			}
		}
		return ['status' => 'failed'];
		
	}

	private function formatData($universityId, $_programs){
		$programs = [];
		if(count($_programs)){
				foreach($_programs as $i => $program){
						$programs[$i]['program_id'] = $program['id'];
						$programs[$i]['program_name'] = $program['name'];

						$program = UniversityProgram::where('university_id', $universityId)
								->where('program_id', $program['id'])->first();
					 
						if($program){
								$programs[$i]['undergraduate'] = $program->undergraduate;
								$programs[$i]['ms'] = $program->ms;
								$programs[$i]['mba'] = $program->mba;
						}else{
								$programs[$i]['undergraduate'] = '';
								$programs[$i]['ms'] = '';
								$programs[$i]['mba'] = '';
						}

						$programs[$i]['isEmpty'] = true;
						if($programs[$i]['undergraduate'] || $programs[$i]['ms'] || $programs[$i]['mba'])
								$programs[$i]['isEmpty'] = false;
						
						$programs[$i]['isTrashed'] = !empty($program['deleted_at']);
				}
		}    
		return $programs;
	}
}
