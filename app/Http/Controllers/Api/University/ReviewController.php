<?php

namespace App\Http\Controllers\Api\University;

use App\UniversityReview;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\NotifyWeb;
use Illuminate\Support\Facades\View;

class ReviewController extends Controller{

	public function __construct(){
		$this->middleware('auth:api');
	}
	
	public function index(){
		$reviews = [];
		$i = 0;
		$_reviews = UniversityReview::latest()->get();
		if($_reviews->count()){
			foreach($_reviews as $review){
				if(empty($review->university)) continue;

				$reviews[$i] = $review;
				$reviews[$i]['university_name'] = $review->university->name;
				$reviews[$i]['university_url'] = $review->university->url;
				$reviews[$i]['review_at'] = $review->created_at->format('Y-m-d, H:i');
				$i++;
			}
		}

		return $reviews;
	}

	public function save(Request $request, $id = ''){
		if($id){
			if(UniversityReview::find($id)->update($request->except('id'))){
				return ['status' => 'success'];
			}
		}else{
			if(UniversityReview::create($request->except('id'))){
				return ['status' => 'success'];
			}
		}

		return response()->json(['status' => 'failed'], 422);
	}

	public function approved(Request $request, $id){
		$review = UniversityReview::find($id);
		$review->is_approved = 1;

		if($review->save()){
			if($review->user)
				$this->approvedSendWebNotication($review);

			return ['status' => 'success'];
		}
		return response()->json(['status' => 'failed'], 422);
	}

	private function approvedSendWebNotication($review){
		$user = $review->user;
		$university = $review->university;
		$universityURL = '<a href="'.$university->url.'">'.str_limit($university->name).'</a>';

		$data = [[
			'show' => 'user-activities',
			'data' => [
				'avatar' => $user->avatar,
				'name' => $user->fullName,
				'message' => ' reviewed "'.$universityURL.'"'
			]
		], [
			'show' => 'user-notifications',
			'data' => [
				'url'=> $university->url.'?ref=user-notif',
				'thumbnail' => $university->logo,
				'title' => $university->name,
				'message' => 'your review has been approved',
			]
		
		]];
		$user->notify(new NotifyWeb($data));
	}

	public function destroy(Request $request, $id){
		if(UniversityReview::find($id)->delete()){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
	}
}
