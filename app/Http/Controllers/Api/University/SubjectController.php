<?php

namespace App\Http\Controllers\Api\University;

use App\Subject;
use App\UniversitySubject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubjectController extends Controller{

	public function __construct(){
		$this->middleware('auth:api');
	}
	
	public function index($university){
		$data['undergraduate'] = UniversitySubject::where('university_id', $university)
			->where('type', 'undergraduate')->pluck('subject_id');
			
		$data['ms'] = UniversitySubject::where('university_id', $university)
		->where('type', 'ms')->pluck('subject_id');
		
		$data['mba'] = UniversitySubject::where('university_id', $university)
			->where('type', 'mba')->pluck('subject_id');
		
		return $data;
	}

	public function save(Request $request){
		UniversitySubject::where('university_id', $request->university_id)->delete();
		
		$subjects = [];
		$i = 0;
		if(count($request->undergraduate)){
			foreach($request->undergraduate as $undergraduate){
				$subjects[$i]['university_id'] = $request->university_id;
				$subjects[$i]['subject_id'] = $undergraduate;
				$subjects[$i]['type'] ='undergraduate';
				$i++;
			}
		}
		if(count($request->ms)){
			foreach($request->ms as $ms){
				$subjects[$i]['university_id'] = $request->university_id;
				$subjects[$i]['subject_id'] = $ms;
				$subjects[$i]['type'] ='ms';
				$i++;
			}
		}
		if(count($request->mba)){
			foreach($request->mba as $mba){
				$subjects[$i]['university_id'] = $request->university_id;
				$subjects[$i]['subject_id'] = $mba;
				$subjects[$i]['type'] ='mba';
				$i++;
			}
		}

		if(UniversitySubject::insert($subjects)){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
	}

}
