<?php

namespace App\Http\Controllers\Api\University;

use Validator;
use App\UniversityAlumni;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AlumniController extends Controller{

    public function __construct(){
		$this->middleware('auth:api');
    }
    
    public function index($universityId){
        return UniversityAlumni::where('university_id', $universityId)->first();
    }
    
    public function save(Request $request){
        $validator = Validator::make($request->only('website'), [
            'website' => 'nullable|url'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => 'These fields are required',
                'errors' => $validator->errors()
            ], 422);
        }
        
        $data = collect($request->except('university_id', 'action', 'method'))->filter(function($value, $key){
          return !empty($value);  
        });

        if($data->isEmpty()){
            if(UniversityAlumni::where('university_id', $request->university_id)->delete()){
                return ['status' => 'success'];
            }
        }else{
            $data = UniversityAlumni::where('university_id', $request->university_id)->first();
            $data = empty($data)? new UniversityAlumni : $data;
            
            $data->university_id = $request->university_id;
            $data->website = $request->website;
            $data->famous_name1 = $request->famous_name1;
            $data->famous_photo1 = $request->famous_photo1;
            $data->famous_name2 = $request->famous_name2;
            $data->famous_photo2 = $request->famous_photo2;
            $data->famous_name3 = $request->famous_name3;
            $data->famous_photo3 = $request->famous_photo3;
            $data->famous_name4 = $request->famous_name4;
            $data->famous_photo4 = $request->famous_photo4;
            $data->famous_name5 = $request->famous_name5;
            $data->famous_photo5 = $request->famous_photo5;

            if($data->save()){
                return ['status' => 'success'];
            }
        }
        return ['status' => 'failed'];
    }

}
