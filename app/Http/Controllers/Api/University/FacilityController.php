<?php

namespace App\Http\Controllers\Api\University;

use App\UniversityFacility;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FacilityController extends Controller{
    
    public function __construct(){
		$this->middleware('auth:api');
    }

    public function index($universityId){
        return UniversityFacility::where('university_id', $universityId)->first();
    }

    public function save(Request $request){
        $facility = UniversityFacility::where('university_id', $request->university_id)->first();
        $facility = empty($facility)? new UniversityFacility : $facility;
        
        $facility->university_id = $request->university_id;
        $facility->general = $request->general;
        $facility->nearby_places = $request->nearby_places;
        $facility->accomodation = $request->accomodation;

        if($facility->save()){
            return ['status' => 'success'];
        }

        return ['status' => 'failed'];
    }

}
