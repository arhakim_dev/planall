<?php

namespace App\Http\Controllers\Api\Configurations;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationController extends Controller{
	
	private $table = 'app_notifications';

	public function __construct(){
		$this->middleware('auth:api');
	}

	public function getInterval(){
		$notification = DB::table($this->table)->first();

		return [
			'interval' => empty($notification)? 3 : $notification->interval
		];
	}

	public function setInterval(Request $request){
		$interval = DB::table($this->table)->update([
			'interval' => $request->interval
		]);

		return ['status' => 'success'];
	}
}
