<?php

namespace App\Http\Controllers\Api\Service;

use Validator;
use App\Service;
use App\ServiceCategory;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class CategoryController extends Controller{
  
	public function __construct(){
		$this->middleware('auth:api');
	}

	public function index(){
		$data = [];
		$categories = ServiceCategory::withTrashed()->orderBy('name');
		if($categories->count()){
			foreach($categories->get() as $i => $category){
				$data[$i] = $category;
				$data[$i]['url'] = $category->url;
			}
		}

		return $data;
	}
	
	public function save(Request $request, $id = ''){
		$rules= [
			'name' => 'required',
			'icon' => 'required'
		];

		if(!empty($id)){
			$category = ServiceCategory::find($id);
			$rules['slug'] = [
				'required',
				Rule::unique('service_categories')->ignore($category->id)
			];
		}else{
			$rules['slug'] = 'required|unique:service_categories,slug';
		}
		$validator = Validator::make($request->except('id'), $rules);
			
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json([
				'status' => 'error',
				'message' => 'The input are requred',
				'errors' => $errors
			], 422);
		}
		$category = empty($id)? new ServiceCategory : ServiceCategory::withTrashed()->where('id', $id)->first();
		
		$category->name = $request->name;
		$category->slug = $request->slug;
		$category->icon = $request->icon;

		if($category->save()){
			return ['status' => 'success'];
		}

		return ['status' => 'failed'];
	}
	
	public function trash($id){
		if(ServiceCategory::findOrFail($id)->delete()){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
    }
    
	
		public function restore($id){
		if(ServiceCategory::withTrashed()->where('id', $id)->restore()){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
	}
	
	public function destroy($id){
		$service = Service::where('category_id', $id);
		if($service->count()){
				return response()->json([
						'status' => 'denied',
						'message' => 'There are '.$service->count().' data using this service. Remove permanently, denied'
				], 422);
		}
		if(ServiceCategory::withTrashed()->where('id', $id)->forceDelete()){
				return ['status' => 'success'];
			}
		
		return ['status' => 'failed'];
	}
    	
	public function uploadFile(Request $request){
		$validator = Validator::make($request->all(), [
				'file' => 'required|image|mimes:jpeg,png,jpg|dimensions:min_width=100,min_height=100'
		]);
			
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json([
				'status' => 'error',
				'message' => 'Failed uploading file',
				'errors' => $errors
			], 422);
		}
	
		$file = $request->file('file');
		$fileName = str_random(64).'.'.$file->getClientOriginalExtension();
		$file->move(public_path('files/logo/'), $fileName);
		
		if($file){
			$file = 'files/logo/'.$fileName;
			return response()->json([
				'status' => 'success',
				'message' => '',
				'location' => $file,
				'file' => $file
			]);
		}
	
		return response()->json([
			'status' => 'error',
			'message' => 'Failed uploading file'
		], 400);
	}

	public function removeFile(Request $request){
		@unlink( public_path($request->file) );
		return ['status' => 'success'];
	}

}
