<?php

namespace App\Http\Controllers\Api\Service;

use App\Service;
use App\ServiceBusinessHour;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BusinessHourController extends Controller{
	
	public function __construct(){
		$this->middleware('auth:api');
	}

	public function index($serviceId){
		return ServiceBusinessHour::where('service_id', $serviceId)->get();
	}

	public function save(Request $request, $id){
		$data = [];
		// if($id){
			ServiceBusinessHour::where('service_id', $id)->delete();
		// }
		
		for($i = 0; $i <= 6; $i++){
			$data[$i] = [
				'service_id'=> $id,
				'day' => $request->days[$i],
				'open' => @$request->open[$i],
				'close' => @$request->close[$i],
			];
		}

		if(ServiceBusinessHour::insert($data)){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
	}

}
