<?php

namespace App\Http\Controllers\Api\Service;

use App\ServiceReview;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\NotifyWeb;
use Illuminate\Support\Facades\View;

class ReviewController extends Controller{

	public function __construct(){
		$this->middleware('auth:api');
	}
    
	public function index(){
		$reviews = [];
		$i = 0;
		$_reviews = ServiceReview::latest()->get();
		if($_reviews->count()){
			foreach($_reviews as $review){
				if(empty($review->service)) continue;
				$reviews[$i] = $review;
				$reviews[$i]['service_name'] = $review->service->name;
				$reviews[$i]['service_url'] = $review->service->url;
				$reviews[$i]['review_at'] = $review->created_at->format('Y-m-d, H:i');
				$i++;
			}
		}

		return $reviews;
	}

	public function save(Request $request, $id = ''){
		if($id){
			if(ServiceReview::find($id)->update($request->except('id'))){
				return ['status' => 'success'];
			}
		}else{
			if(ServiceReview::create($request->except('id'))){
				return ['status' => 'success'];
			}
		}

		return response()->json(['status' => 'failed'], 422);
	}

	public function destroy(Request $request, $id){
		if(ServiceReview::find($id)->delete()){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
	}

	public function approved(Request $request, $id){
		$review = ServiceReview::find($id);
		$review->is_approved = 1;

		if($review->save()){
			if($review->user)
				$this->approvedSendWebNotication($review);

			return ['status' => 'success'];
		}
		return response()->json(['status' => 'failed'], 422);
	}

	private function approvedSendWebNotication($review){
		$user = $review->user;
		$service = $review->service;
		$serviceURL = '<a href="'.$service->url.'">'.str_limit($service->name).'</a>';

		$data = [[
			'show' => 'user-activities',
			'data' => [
				'avatar' => $user->avatar,
				'name' => $user->fullName,
				'message' => ' reviewed '.$serviceURL
			]
		], [
			'show' => 'user-notifications',
			'data' => [
				'url'=> $service->url.'?ref=user-notif',
				'thumbnail' => $service->logo,
				'title' => $service->name,
				'message' => 'your review has been approved',
			]
		
		]];
		$user->notify(new NotifyWeb($data));
	}
}
