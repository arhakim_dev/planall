<?php

namespace App\Http\Controllers\Api\Service;

use Validator;
use App\Country;
use App\Service;
use App\ServiceBusinessHour;
use App\ServiceReview;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class ServiceController extends Controller{

	private $messages = [];
	private $excelEmptyValues = [NULL, 'na', 'n/a', 'no'];
	private $excelNotAvailableValues = ['not available', 'no information available', 'not available on the website'];

	const FOREX = 1;
	const TRAVEL_AGENCY = 2;
	const INSURANCE_AGENCY = 4;
	const TEST_PREPARATION = 5;
	const CONCULTANCY = 6;
	const FINCANCIAL_ASSISTANCE = 7;

	public function __construct(){
		$this->middleware('auth:api');
	}

	public function index(){
		$services= [];
		$_services = Service::withTrashed()->orderBy('name')->get();

		if($_services->count()){
			foreach($_services as $i => $service){
				$services[$i] = $service;
				$services[$i]['url'] = $service->url;
				$services[$i]['country'] = @$service->country;
				$services[$i]['category_name'] = @$service->category->name;
			}
		}

		return $services;
	}

	public function save(Request $request, $id = ''){
		$rules = [
			'name'  => 'required|max:100',
			'slug'  => 'required',
			// 'logo' => 'required',
			// 'address' => 'required',
			'country_code' => 'required',
			// 'city' => 'required',
			// 'contact_name' => 'required',
			// 'contact_phone' => 'required',
			// 'founding_year' => 'required|numeric|digits:4',
			// 'website' => ['required', 'regex:'.$this->ctrl__regexUrlPattern],
			// 'facebook' => ['nullable', 'regex:'.$this->ctrl__regexUrlPattern],
			// 'twitter' => ['nullable', 'regex:'.$this->ctrl__regexUrlPattern],
			// 'youtube' => ['nullable', 'regex:'.$this->ctrl__regexUrlPattern],
			// 'linkedin' => ['nullable', 'regex:'.$this->ctrl__regexUrlPattern],
			// 'description' => 'required',
			// 'hero_image' => 'required',
			// 'open_at' => 'required|date_format:H:i:s',
			// 'closed_at' => 'required|date_format:H:i:s|after:open_at',
		];
		
		if(!empty($id)){
			$university = Service::find($id);
			$rules['slug'] = [
				'required',
				Rule::unique('services')->ignore($university->id)
			];
		}else{
			$rules['slug'] = 'required|unique:services,slug';
		}
		$validator = Validator::make($request->except('id'), $rules);
			
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json([
				'status' => 'error',
				'message' => 'The input are requred',
				'errors' => $errors
			], 422);
		}

		/* if(strtotime($request->open_at) > strtotime($request->closed_at)){
			return response()->json([
				'status' => 'error',
				'message' => 'The input are invalid',
				'errors' => [
					'Business Hours' => ['Incorrect Business Hours value. The "closed at" hour should be greater than "open at" hour']
				]
				], 422);
		} */

		if(empty($id)){
			$service = Service::create($request->except('id'));
			if($service){
				return ['status' => 'success', 'service' => $service];
			}
		}else{
			if(Service::find($id)->update($request->except('id'))){
				return ['status' => 'success'];
			}
		}

		return ['status' => 'failed'];
	}
	
	public function trash($id){
		if(Service::findOrFail($id)->delete()){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
    }
    
	
		public function restore($id){
		if(Service::withTrashed()->where('id', $id)->restore()){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
	}
	
	public function destroy($id){
		ServiceBusinessHour::where('service_id', $id)->delete();
		ServiceReview::where('service_id', $id)->delete();
		if(Service::withTrashed()->where('id', $id)->forceDelete()){
				return ['status' => 'success'];
			}
		
		return ['status' => 'failed'];
	}

	public function bulkTrash(Request $request){
		$serviceIds = $request->serviceIds;
		$messages = '';
		$services = Service::whereIn('id', $serviceIds)->get();

		if($services->count()){
			foreach($services as $service){
				$_service = $service;
				
				if($service->delete()){
					$messages .= "\n- ".$_service->name.' removed';
				}else{
					$messages .= "\n- ".$_service->name.' not removed';
				}
			}
		}

		$messages = empty($messages)? 'No data should be removed' : $messages;
		return response()->json([
			'status' => 'success',
			'message' => $messages
		]);
	}
    	
	public function bulkRestore(Request $request){
		$serviceIds = $request->serviceIds;
		$messages = '';
		$services = Service::onlyTrashed()
			->whereIn('id', $serviceIds)
			->get();

		if($services->count()){
			foreach($services as $service){
				$_service = $service;
				
				if($service->restore()){
					$messages .= "\n- ".$_service->name.' restored';
				}else{
					$messages .= "\n- ".$_service->name.' not restored';
				}
			}
		}

		$messages = empty($messages)? 'No data should be restored' : $messages;
		return response()->json([
			'status' => 'success',
			'message' => $messages
		]);
	}
    	
	public function bulkDestroy(Request $request){
		$serviceIds = $request->serviceIds;
		$messages = '';
		$services = Service::onlyTrashed()
			->whereIn('id', $serviceIds)
			->get();

		if($services->count()){
			foreach($services as $service){
				$_service = $service;
				ServiceBusinessHour::where('service_id', $_service->id)->delete();
				ServiceReview::where('service_id', $_service->id)->delete();
				
				if($service->forceDelete()){
					$messages .= "\n- ".$_service->name.' destroyed';
				}else{
					$messages .= "\n- ".$_service->name.' not destroyed';
				}
			}
		}
		
		$messages = empty($messages)? 'No data should be destroyed' : $messages;
		return response()->json([
			'status' => 'success',
			'message' => $messages
		]);
	}

	public function uploadFile(Request $request, $type){
		if($type == 'hero'){
				$validator = Validator::make($request->all(), [
						'file' => 'required|image|mimes:jpeg,png,jpg|dimensions:min_width=900,min_height=400'
				],[
						'dimensions' => "The :attribute has invalid image dimensions.\n It should have 900x400 pixels for the minimum size"
				]);
		}elseif($type == 'logo'){
				$validator = Validator::make($request->all(), [
						'file' => 'required|image|mimes:jpeg,png,jpg|dimensions:min_width=200,min_height=200,ratio=1/1'
				],[
						'dimensions' => "The :attribute has invalid image dimensions.\n It should have 200x200 pixels for the minimum size and 1:1 for the ration"
				]);
		}elseif($type == 'excel'){
			$validator = Validator::make([
				'file' => $request->file,
				'extension' => strtolower($request->file->getClientOriginalExtension())
			],[
				'file'          => 'required',
      	'extension'      => 'required|in:xlsx,xls',
			]/* ,[
				'mimes' => "The :attribute should be in a spreadsheet format. Or please try to re-save the file using .xls extension. ".$request->file->getClientOriginalExtension().' file extenstion detected'
			] */);
		}
			
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json([
				'status' => 'error',
				'message' => 'Failed uploading file',
				'errors' => $errors
			], 422);
		}
	
		$file = $request->file('file');
		if(($type == 'excel')){
				$fileName = $file->getClientOriginalName();
		}else{
				$fileName = str_random(64).'.'.$file->getClientOriginalExtension();
		}
		$file->move(public_path('files/'.$type.'/'), $fileName);
		
		if($file){
			$file = 'files/'.$type.'/'.$fileName;
			return response()->json([
				'status' => 'success',
				'message' => '',
				'location' => $file,
				'file' => $file
			]);
		}
	
		return response()->json([
			'status' => 'error',
			'message' => 'Failed uploading file'
		], 400);
	}

	public function removeFile(Request $request){
		@unlink( public_path($request->file) );
		return ['status' => 'success'];
	}

	public function import(Request $request){
		$files = isset($request->all()['files'])? $request->all()['files'] : [];
		if(count($files)){
			foreach($files as $file){
				$_file = explode('/', $file);
				$cc = isset($_file[count($_file) - 1])
					? explode(',', str_replace('.xls', '', str_replace('.xlsx', '', $_file[count($_file) - 1]))) 
					: '';
				
				$city = trim($cc[0]);
				$country = strtolower(trim(@$cc[1]));
				$countryCode = '';
				if($country){
					$countryCode = Country::whereRaw('LOWER(name) = "'.$country.'"')->value('code');
				}
				
				if(empty($countryCode)){
					array_push($this->messages, ['Incorrect format file name or cannot find the country code for '.$file]);
					continue;
				}

				try{
					$sheet = \PhpOffice\PhpSpreadsheet\IOFactory::load( public_path($file) );

					$worksheet = $sheet->getSheetByName('Travel Agency');
					$data = $worksheet->toArray(null, true, true, true);
					$this->excelToServices(self::TRAVEL_AGENCY, $data, $city, $countryCode);
					
					$worksheet = $sheet->getSheetByName('Insurance Agency');
					$data = $worksheet->toArray(null, true, true, true);
					$this->excelToServices(self::INSURANCE_AGENCY, $data, $city, $countryCode);
					
					$worksheet = $sheet->getSheetByName('Forex');
					$data = $worksheet->toArray(null, true, true, true);
					$this->excelToServices(self::FOREX, $data, $city, $countryCode);
					
					$worksheet = $sheet->getSheetByName('Loans');
					$data = $worksheet->toArray(null, true, true, true);
					$this->excelToServices(self::FINCANCIAL_ASSISTANCE, $data, $city, $countryCode);
					
					$worksheet = $sheet->getSheetByName('Test Preparation');
					$data = $worksheet->toArray(null, true, true, true);
					$this->excelToServices(self::TEST_PREPARATION, $data, $city, $countryCode);
					
					$worksheet = $sheet->getSheetByName('Application Consultant');
					$data = $worksheet->toArray(null, true, true, true);
					$this->excelToServices(self::CONCULTANCY, $data, $city, $countryCode);
					
					@unlink( public_path($file) );

				}catch(\PhpOffice\PhpSpreadsheet\Reader\Exception $e){
						$this->messages .= $e;
				}
			}
				
			if(empty($this->messages))
				return ['status' => 'success'];
			else
				return [
					'status' => 'success', 
					'message' => 'Something went wrong',
					'errors' => $this->messages
				];

		}else{
			return response()->json([
				'status' => 'error',
				'message' => 'No file selected'
			], 422);
		}
	}

	private function excelToServices($categoryId, $data, $city, $countryCode){
		$travels = [];
		if(count($data)){
			$row = 1;
			for($i = 0; $i <= count($data); $i++){
				$row++;
				if($row > count($data)) continue;

				$name = $this->excelGetValue($data[$row]['B']);
				if(empty($name)) continue;

				$slug = str_slug($name);
				if($this->isServiceSlugExists($categoryId, $slug)){
					$slug .= '-'.time();
				}

				$travel = Service::create([
					'category_id' => $categoryId,
					'name' => $name,
					'slug' => $slug,
					'city' => $city,
					'country_code' => $countryCode,
					'description' => $this->excelGetValue($data[$row]['M']),
					'address' => $this->excelGetValue($data[$row]['C']),
					'contact_phone' => $this->excelGetValue($data[$row]['D']),
					'contact_name' => $this->excelGetValue($data[$row]['K']),
					'founding_year' => $this->excelGetValue($data[$row]['L']),
					'website' => $this->excelGetValue($data[$row]['E']),
					'facebook' => $this->excelGetValue($data[$row]['I']),
					'twitter' => $this->excelGetValue($data[$row]['J']),
					'rating' => $this->excelGetValue($data[$row]['H']),
					'is_delivery' => strtolower($this->excelGetValue($data[$row]['N']))? 1 : 0,
				]);
				
				if($travel){
					$_businessHours = $this->excelExtractBusinessHours($data[$row]['F']);				
					if(count($_businessHours)){
						$businessHours = [];
						$i = 0;
						foreach($_businessHours as $day => $hour){
							$businessHours[$i] = [
								'service_id' => $travel->id,
								'day' => $day,
								'open' => date('H:i:s', strtotime($hour['open'])),
								'close' => date('H:i:s', strtotime($hour['close'])),
							];
							$i++;
						}
						
						if($businessHours){
							ServiceBusinessHour::insert($businessHours);
						}
					}

					$reviews = [];
					$_reviews = explode('###', $this->excelGetValue($data[$row]['G']));
					foreach($_reviews as $i => $review){
						if(empty($review)) continue;
						$reviews[$i] = [
							'service_id' => $travel->id,
							'name' => 'Annonymous',
							'rate' => 3,
							'is_delivery' => 1,
							'description' => $review,
						];
					}

					if($reviews){
						ServiceReview::insert($reviews);
					}
				}
			}
		}		
	}

	private function excelGetValue($cell){
		$value = $this->excelValidateValue($cell);

		return $value;
	}

	private function excelValidateValue($cell){
		if(in_array(strtolower($cell), $this->excelEmptyValues)){
			return NULL;
		}

		return trim($cell);
	}

	private function excelExplodeNewLine($data){
		$data = $this->excelGetValue($data);
		if(empty($data)) return [];

		return preg_split('/\r\n|\r|\n/', $data);
	}

	private function excelExtractBusinessHours($data){
		$_data = $this->excelExplodeNewLine($data);
		$data = [];
		if(count($_data)){
			foreach($_data as $d){
				$_d = explode(':', $d);
				$day = trim($_d[0]);
				$hours = trim(strtolower(str_replace($day.':', '', $d)));

				if(trim(strtolower($day)) == 'no')
					continue;

				// default closed
				$open = '00:00:00';
				$close = '00:00:00';

				if($hours){
					if($hours == 'open 24 hours'){
						$open = '00:00:00';
						$close = '23:59:59';

					}elseif($hours == 'closed'){
						// default closed
					}else{
						$hours = str_replace('–', '-', $hours);
						$_hours = explode(',', $hours); // 10:00 AM – 3:00 PM, 4:30 – 8:00 PM
						if(count($_hours) > 1){
							$open = explode('-', $_hours[0])[0];
							$close = @explode('-', $_hours[count($_hours) - 1])[1];

						}else{
							$_hours = explode('-', $hours);
							$open = $_hours[0];
							$close = @$_hours[1];
						}

						$close = empty($close)? '23:59:59' : $close;
					}
				}

				$data[$day]['open'] = trim($open);
				$data[$day]['close'] = trim($close);
			}
		}

		return $data;
	}

	private function isServiceSlugExists($categoryId, $slug){
		$service = Service::where('category_id', $categoryId)->whereSlug($slug)->count();
		if($service) return true;

		return false;
	}
}
