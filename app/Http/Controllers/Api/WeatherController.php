<?php

namespace App\Http\Controllers\Api;

use App\Weather;
use App\University;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WeatherController extends Controller{

    public function __construct(){
		$this->middleware('auth:api');
    }

    public function index($countryCode, $zip){
        $weathers = [];
        $year = date('Y');
        $weathers['year'] = $year;
        
        $label = collect([]);
        $tempMin = collect([]);
        $tempMax = collect([]);
        for($m = 1; $m <= 12; $m++){
            $_weather = Weather::select(
                // \DB::raw(date('M', strtotime($m)).' AS month'), 
                \DB::raw($year.' AS year'), \DB::raw('MIN(temp) as temp_min'), 
                \DB::raw('MAX(temp) as temp_max')
                )
                ->where('country_code', $countryCode)
                ->where('zip', $zip)
                ->where(\DB::raw('MONTH(updated_at)'), $m)
                ->where(\DB::raw('YEAR(updated_at)'), $year)
                ->first();
            
            $label->push(date('M', strtotime($year.'-'.$m)));
            $tempMin->push(intval($_weather->temp_min));
            $tempMax->push(intval($_weather->temp_max));
            
        }
        $weathers['label'] = $label;
        $weathers['tempMin'] = $tempMin;
        $weathers['tempMax'] = $tempMax;

        return $weathers;
    }

    public function update(){
        $universities = University::select('country_code', 'zip')
            ->where('zip', '!=', NULL)
            ->where('country_code', '!=', NULL)
            ->groupBy('zip');
        
        $weathers = [];
        if($universities->count()){
            foreach($universities->get() as $i => $university){
                echo 'http://api.openweathermap.org/data/2.5/weather?zip='.$university->zip.', '.$university->country_code.'&units=metric&appid='.env('OPENWEATHER_KEY').PHP_EOL;

                $weather = file_get_contents('http://api.openweathermap.org/data/2.5/weather?zip='.$university->zip.', '.$university->country_code.'&units=metric&appid='.env('OPENWEATHER_KEY'));

                $weather = json_decode($weather);

                $weathers[$i]['country_code'] = $university->country_code;
                $weathers[$i]['zip'] = $university->zip;
                $weathers[$i]['temp'] = $weather->main->temp;
            }
        }

        if(count($weathers)){
            Weather::insert($weathers);
        }
    }
}
