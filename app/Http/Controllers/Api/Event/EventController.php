<?php

namespace App\Http\Controllers\Api\Event;

use App\Event;
use App\EventMedia;
use App\UserEvent;
use Carbon\Carbon;
use Validator;
use App\Support\Foundation\Uploader;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class EventController extends Controller{
	use Uploader;

	public function __construct(){
		$this->middleware('auth:api');
	}

	public function index(){
		$events = [];
		$_events = Event::withTrashed()
			->orderBy('start_date', 'DESC')
			->orderBy('start_time', 'DESC')
			->get();
		if($_events->count()){
			foreach($_events as $i => $event){
				$events[$i] = $event;
				$events[$i]['start_date_formated'] = date('d F, Y', strtotime($event->start_date));
				$events[$i]['end_date_formated'] = date('d F, Y', strtotime($event->end_date));
				$events[$i]['start_time_formated'] = date('h:i a', strtotime($event->start_time));
				$events[$i]['end_time_formated'] = date('h:i a', strtotime($event->end_time));
				$events[$i]['url'] = $event->url;
				$events[$i]['totalAttendance'] = $event->users()->count();
			}
		}

		return $events;
	}

	public function attendance($eventId){
		$attendance = [];
		$attendees = UserEvent::whereEventId($eventId)
			->oldest()
			->get();

		if($attendees->count()){
			foreach($attendees as $i => $_attendance){
				$attendance[$i] = $_attendance;
				$attendance[$i]['register_at'] = date('d F, Y - H:i a', strtotime($_attendance->created_at));
				$attendance[$i]['user']['fullName'] = $_attendance->user->fullName;
				$attendance[$i]['user']['country'] = $_attendance->user->country->name;
				$attendance[$i]['user']['birthDate'] = date('d F, Y', strtotime($_attendance->user->birth_date));
				$attendance[$i]['user']['email'] = $_attendance->user->email;
				$attendance[$i]['user']['phone'] = $_attendance->user->phone;
				$attendance[$i]['user']['gender'] = $_attendance->user->gender;
				$attendance[$i]['user']['url'] = $_attendance->user->url;
			}
		}
		return $attendance;
	}

	public function save(Request $request, $id = ''){
		$rules = [
			'title' => 'required',
			'description' => 'required',
			'address' => 'required',
			'seat' => 'required|numeric',
			'start_date' => 'required|date',
			'end_date' => 'required|date|after_or_equal:start_date',
			'start_time' => 'required|date_format:H:i:s',
			'end_time' => 'required|date_format:H:i:s|after:start_time',
		];

		if(!empty($id)){
			$event = Event::find($id);
			$rules['slug'] = [
				'required',
				Rule::unique('events')->ignore($event->id)
			];
		}else{
			$rules['slug'] = 'required|unique:events,slug';
		}
		$validator = Validator::make($request->all(), $rules);

		if($validator->fails()){
			return response()->json([
				'status' => 'error',
				'message' => 'These fields are required',
				'errors' => $validator->errors()
			], 422);
		}
		/* $message = "";
		$startDate = Carbon::parse($request->start_date);
		$endDate = Carbon::parse($request->end_date);
		
		if($startDate > $endDate){
			$message .= "- Invalid date range. End date should be greater than start date\n";
		}

		$startTime = Carbon::parse($request->start_time);
		$endTime = Carbon::parse($request->end_time);
		if($startTime > $endTime){
			$message .= "- Invalid hour range. End time should be greater than start time.\n";
		}

		if($message){
			return response()->json([
				'status' => 'error',
				'message' => $message
			], 422);
		} */

		$isSuccess = false;
		if($id){
			if(Event::withTrashed()->where('id', $id)->update($request->except('id', 'action', 'method'))){
				$isSuccess = true;
			}
		}else{
			if(Event::withTrashed()->insert($request->except('id', 'action', 'method'))){
				$isSuccess = true;
			}
		}

		if($isSuccess){
			return response()->json([
				'status' => 'success', 
				'message' => 'Data has been saved'
			]);
		}

		return response()->json([
			'status' => 'error', 
			'message' => 'Failed to save data event'
		], 422);

	}
		
	public function trash($id){
		if(Event::findOrFail($id)->delete()){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
    }
    
	
		public function restore($id){
		if(Event::withTrashed()->where('id', $id)->restore()){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
	}
	
	public function destroy($id){
		if(Event::withTrashed()->where('id', $id)->forceDelete()){
				return ['status' => 'success'];
			}
		
		return ['status' => 'failed'];
	}
/* 
	public function uploadFile(Request $request){
		$validator = Validator::make($request->all(), [
				'file' => 'required|image|mimes:jpeg,png,jpg|dimensions:min_width=900,min_height=400'
		],[
				'dimensions' => "The :attribute has invalid image dimensions.\n It should have 900x400 pixels for the minimum size"
		]);
			
		if($validator->fails()){
			$errors = $validator->errors();
			return response()->json([
				'status' => 'error',
				'message' => 'Failed uploading file',
				'errors' => $errors
			], 422);
		}
	
		$file = $request->file('file');
		$fileName = str_random(64).'.'.$file->getClientOriginalExtension();
		$file->move(public_path('files/hero/'), $fileName);
		
		if($file){
			$file = 'files/hero/'.$fileName;
			return response()->json([
				'status' => 'success',
				'message' => '',
				'location' => $file,
				'file' => $file
			]);
		}
	
		return response()->json([
			'status' => 'error',
			'message' => 'Failed uploading file'
		], 400);
	}

	public function removeFile(Request $request){
		@unlink( public_path($request->file) );
		return ['status' => 'success'];
	} */

}
