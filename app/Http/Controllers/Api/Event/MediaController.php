<?php

namespace App\Http\Controllers\Api\Event;

use App\EventMedia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MediaController extends Controller{
  
	public function __construct(){
		$this->middleware('auth:api');
	}
	
	public function index($eventId){
		return EventMedia::whereEventId($eventId)->get();
	}

	public function save(Request $request){
		EventMedia::whereEventId($request->event_id)->delete();
		$_message = '';
		
		$i = 0;
		$data = [];
		foreach(preg_split("/\r\n|\r|\n/", $request->url) as $url){
			if(empty($url)) continue;
			if(!filter_var($url, FILTER_VALIDATE_URL)){
				$_message .= "- [".$url."] invalid url format\n";
				continue;
			}
			if(!str_contains($url, 'youtube')){
				$_message .= "- [".$url."] not Youtube URL\n";
				continue;
			}
			if(!str_contains($url, 'embed')){
				$_message .= "- [".$url."] invalid embed url\n";
				continue;
			}
			$data[$i]['event_id'] = $request->event_id;
			$data[$i]['media'] = $url;
			$data[$i]['type'] = 'url';
			$i++;
		}

		$files = isset($request->all()['files'])? $request->all()['files'] : [];
		if(count($files)){
			foreach($files as $file){
				$data[$i]['event_id'] = $request->event_id;
				$data[$i]['media'] = $file;
				$data[$i]['type'] = 'ppt';
				$i++;

			}
		}

		if(EventMedia::insert($data)){
			$message = '';
			if(!empty($_message)){
				$message = "Success with error.cannot save this data:\n";
				$message .= $_message;
			}
			return response()->json([
				'status' => 'success',
				'message' => $message
			]);
		}

		return ['status' => 'fail'];
	}

}
