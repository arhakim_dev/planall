<?php

namespace App\Http\Controllers\Api\User;

use App\User;
use App\Role;
use Validator;
use App\Notifications\Auth\UserChangeStatusMail;
use App\Notifications\NotifyWeb;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class UserController extends Controller{
	
	public function __construct(){
		$this->middleware('auth:api');
	}

	public function index(){
		$i = 0;
		$data = [];
		$users =  User::withTrashed()->orderBy('updated_at', 'DESC')->get();
		
		foreach($users as $user){
			$data[$i] = [
				'id' => $user->id,
				'first_name' => $user->first_name,
				'last_name' => $user->last_name,
				'profession' => $user->profession,
				'gender' => $user->gender,
				'country_code' => ($user->country)? $user->country->code : '',
				'country' => ($user->country)? $user->country->name : '',
				'avatar' => $user->avatar,
				'email' => $user->email,
				'roles' => $user->roles->pluck('name'),
				'roleIds' => $user->roles->pluck('id'),
				'status' => !empty($user->deleted_at)?'trashed' : $user->status,
				'url' => 'user/'.$user->id.'.'.strtotime($user->created_at).'-'.str_slug($user->fullName)
			];
			$i++;
		}
		
		return $data;
	}
	
  public function roles(){
    return Role::withTrashed()->get();
	}
	
	public function save(Request $request, $id = ''){
		$rules = collect([
			'first_name' => 'required|string|max:255',
			'last_name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:users',
			'password' => 'required|string|min:6|confirmed',
			'country' => 'required|string|max:3',
			'roles' => 'required|array',
		]);

		if($id){
			$user = User::withTrashed()->where('id', $id)->first();
			
			if($user->email == $request->email){
				$rules->pull('email');
				
			}
			if(empty($request->password)){
				$rules->pull('password');
			}
		}
		
		$validator = Validator::make($request->all(), $rules->all());

		if($validator->fails()){
			return response()->json([
				'status' => 'error',
				'message' => 'All fields are required',
				'errors' => $validator->errors()
			], 422);
		}

		$password = !empty($password)? Hash::make($request->password) : $request->password;

		$user = ($id)? User::withTrashed()->where('id', $id)->first() : new User;
		$user->first_name = $request->first_name;
		$user->last_name = $request->last_name;
		$user->email = $request->email;
		if(!empty($password)){
			$user->password = bcrypt($password);
		}
		$user->country_code = $request->country;
		$user->profession = $request->profession;

		if($user->save()){
			if($user->roles()->sync($request->roles)){
				return ['status' => 'success'];
			}
		}
		return ['status' => 'failed'];
	}
	
	public function changeStatus(Request $request, $status, $id){
		$status = ($status == 'activate' || $status == 'unblock')? 'active' : 'block';
		$user = User::withTrashed()->where('id', $id);

		if($user->update(['status' => $status])){
			$user = $user->first();
			$this->sendNotication($status, $user);
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
	}
	private function sendNotication($status, $user){
			
		$_status = ($status == 'active')? 'Activated' : 'Blocked';			
		// $user->notify(new UserChangeStatusMail($_status));

		if($status == 'active'){
			$data = [[
				'show' => 'user-activities', 
				'data' => [
					'avatar' => $user->avatar,
					'name' => $user->fullName,
					'message' => ' joined'
				]
			]];
			$user->notify(new NotifyWeb($data));
		}

	}
	
	/*  public function unblock($id){
		if(User::withTrashed()->where('id', $id)->first()->update(['status' => 'active'])){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
	} */
	
	public function trash($id){
		if(User::findOrFail($id)->delete()){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
	}
	
	public function restore($id){
		if(User::withTrashed()->where('id', $id)->restore()){
			return ['status' => 'success'];
		}
		return ['status' => 'failed'];
	}
	
	public function destroy($id){
		$user = User::withTrashed()->where('id', $id)->first();
		$user->roles()->detach();
		
		if($user->forceDelete()){
				return ['status' => 'success'];
			}
		
		return ['status' => 'failed'];
	}
	
}
