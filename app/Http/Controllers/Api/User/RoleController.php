<?php

namespace App\Http\Controllers\Api\User;

use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{	
	public function __construct(){
		$this->middleware('auth:api');
	}

  public function index(){
    return Role::withTrashed()->get();
  }
/* 
  public function store(Request $request) {
    $data = $request->except(['id']);
    if(Role::create($data)){
      return ['status' => 'success'];
    }
    return ['status' => 'failed'];
  }

  public function update(Request $request, $id){
    $data = $request->except(['id']);
    if(Role::withTrashed()->where('id', $id)->update($data)){
      return ['status' => 'success'];
    }
    return ['status' => 'failed'];
  }

  public function trash($id){
    if(Role::findOrFail($id)->delete()){
      return ['status' => 'success'];
    }
    return ['status' => 'failed'];
  }
  
  public function restore($id){
    if(Role::withTrashed()->where('id', $id)->restore()){
      return ['status' => 'success'];
    }
    return ['status' => 'failed'];
    
  }
  
  public function destroy($id){
    if(Role::withTrashed()->where('id', $id)->forceDelete()){
      return ['status' => 'success'];
    }
    return ['status' => 'failed'];
    
  } */
}
