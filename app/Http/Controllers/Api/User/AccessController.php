<?php

namespace App\Http\Controllers\Api\User;

use App\User;
use App\UserAccess;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccessController extends Controller{
	
	public function __construct(){
		$this->middleware('auth:api');
	}

	public function index(Request $request, $id){
		$accesses = UserAccess::whereUserId($id)->get();
		$universities = [];
		$services = [];

		if($accesses->count()){
			$i = 0;
			$ii = 0;
			foreach($accesses as $access){
				// dump($access);
				if($access->module == 'university'){
					$university = $access->university;
					if(empty($university)) continue;
					// $universities[$university->id] = $university;
					$universities[$i] = $university->id;
					$i++;

				}elseif($access->module == 'service'){
					$service = $access->service;
					if(empty($service)) continue;
					// $services[$service->id] = $service;
					$services[$ii] = $service->id;
					$ii++;
				}
			}
		}

		return ['universities' => $universities, 'services' => $services];
	}

	public function save(Request $request){
		$user = User::find($request->user_id);

		$user->access('university')->sync(array_fill_keys($request->universities, ['module' => 'university']));
		$user->access('service')->sync(array_fill_keys($request->services, ['module' => 'service']));

		return ['status' => 'success'];
	}

}
