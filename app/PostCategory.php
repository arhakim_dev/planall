<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostCategory extends Model
{
   use SoftDeletes;
	 
	public function user(){
		return $this->belongsTo('App\User');
	}
	
	public function scopeBlog($query){
		return $query->where('type', 'blog');
	}
	
	public function scopeForum($query){
		return $query->where('type', 'forum');
	}
	
	
	public function getUrlAttribute(){
		return $this->type.'/category/'.$this->slug;
	}

}
