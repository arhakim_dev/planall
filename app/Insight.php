<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Insight extends Model{

  public function scopePosts($query){
		return $query->where('table', 'posts');
	}
  public function scopePostComments($query){
		return $query->where('table', 'post_comments');
	}
  public function scopeServices($query){
		return $query->where('table', 'services');
	}
  public function scopeUniversities($query){
		return $query->where('table', 'universities');
	}
	public function scopeLikes($query){
		return $query->where('type', 'likes');
	}
	public function scopeDislikes($query){
		return $query->where('type', 'dislikes');
	}
	public function scopeViews($query){
		return $query->where('type', 'views');
	}
}
