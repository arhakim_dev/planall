<?php

namespace App\Mail\Event;

use App\User;
use App\Event;
use App\UserEvent;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterMail extends Mailable{
	use Queueable, SerializesModels;

	public $event;
	public $user;
	public $userEvent;
	
	public function __construct(Event $event, User $user){
		$this->event = $event;
		$this->user = $user;
		$this->userEvent = UserEvent::whereUserId($user->id)
			->whereEventId($event->id)
			->first();
	}

	public function build(){
		return $this->subject('['.config('app.name').'] Event Registration - '.$this->event->title)
				->markdown('emails.events.register');
	}
}
