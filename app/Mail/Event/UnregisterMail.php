<?php

namespace App\Mail\Event;

use App\User;
use App\Event;
use App\UserEvent;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UnregisterMail extends Mailable{
	use Queueable, SerializesModels;

	public $event;
	public $user;
	
	public function __construct(Event $event, User $user){
		$this->event = $event;
		$this->user = $user;
	}

	public function build(){
		return $this->subject('['.config('app.name').']  Unregister Event - '.$this->event->title)
			->markdown('emails.events.unregister');
	}
}