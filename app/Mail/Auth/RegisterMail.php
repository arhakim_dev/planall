<?php

namespace App\Mail\Auth;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterMail extends Mailable{
    use Queueable, SerializesModels;

    public $user;

    public function __construct(User $user){
        $this->user = $user;
    }

    public function build(){
        return $this->subject('['.config('app.name').'] Registration - Activate Your Account')
            ->markdown('emails.auth.register');
    }
}
