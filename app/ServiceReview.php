<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceReview extends Model
{
    protected $fillable = [
        'service_id', 'name', 'rate', 'description'
    ];

    
    public function service(){
        return $this->belongsTo('App\Service');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
    
    public function scopeApproved($query){
        return $query->where('is_approved', 1);
    }
}
