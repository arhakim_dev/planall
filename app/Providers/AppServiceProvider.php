<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider{
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot(){
		View::composer('*', function($view){
			$view->with('_SERVICES_', \App\ServiceCategory::where('id', '<>', 3)->get());

			$appNotif = \DB::table('app_notifications')->first();
			$view->with('_NOTIFICATION_INTERVAL_', @$appNotif->interval);
			
			if(\Auth::check() == false){
				$view->with('_COUNTRIES_', \App\Country::all());
				$view->with('_UNIVERSITIES_', \App\University::all());
				$view->with('_MAJORS_', \App\Subject::all());

			}else{
				$view->with('_ACCESS_', \Auth::user()->access());
			}
		});
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
			//
	}
}
