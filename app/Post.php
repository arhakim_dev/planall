<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model{
	
	use SoftDeletes;
	
	protected $fillable = ['user_id', 'category_id', 'title', 'description', 'hero_image', 'type'];
	
	public function category(){
		return $this->belongsTo('App\PostCategory');
	}	
	public function user(){
		return $this->belongsTo('App\User');
	}	
	public function comments(){
		return $this->hasMany('App\PostComment');
	}
	public function insight(){
		// return $this->belongsToMany('App\User', 'user_insights', 'table_id', 'user_id')
		// 	->wherePivot('table', 'posts');
		return $this->hasMany('App\Insight', 'table_id')->where('table', 'posts');
	}


	public function getUrlAttribute(){
		// return $this->type.'/'.$this->id.'.'.strtotime($this->created_at).'-'.str_slug($this->title);
		return $this->type.'/'.$this->slug;
	}
	public function getHeroImageAttribute($value){
		// return empty($value)? asset('img/hero-profile.png') : \Storage::url($this->hero_image);
		if($this->type == 'forum')
			return empty($value)? asset('img/hero-image-forum.png') : asset($value);
		else
			return empty($value)? asset('img/hero.jpg') : asset($value);
	}
	public function getLikesAttribute(){
		return $this->insight()->where('type', 'likes')->count();
	}
	public function getDislikesAttribute(){
		return $this->insight()->where('type', 'dislikes')->count();
	}
	public function getViewsAttribute(){
		return $this->insight()->where('type', 'views')->count();
	}
	
	public function scopeBlog($query){
		return $query->where('type', 'blog');
	}	
	public function scopeForum($query){
		return $query->where('type', 'forum');
	}
	
}
