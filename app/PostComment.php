<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostComment extends Model
{
   use SoftDeletes;
	
	public function user(){
		return $this->belongsTo('App\User');
	}	
	public function post(){
		return $this->belongsTo('App\Post');
	}
	public function insight(){
		return $this->hasMany('App\Insight', 'table_id')->where('table', 'post_comments');
	}

	public function scopeBlog($query){
		return $query->where('type', 'blog');
	}	
	public function scopeForum($query){
		return $query->where('type', 'forum');
	}
	public function scopeReplies($query){
		return $query->whereRepliedTo($this->id)->whereStatus('approved');
	}
	public function scopeRepliedTo(){
		return \App\PostComment::where('id', $this->replied_to)->whereStatus('approved')->first();
	}
		
	public function getLikesAttribute(){
		return $this->insight()->where('type', 'likes')->count();
	}
	public function getDislikesAttribute(){
		return $this->insight()->where('type', 'dislikes')->count();
	}
}
