<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class University extends Model
{
	use SoftDeletes;

	protected $fillable = [
		'name', 'address', 'website', 'map_address', 'is_public', 'description', 'founding_year', 'logo', 'hero_image', 'facebook', 'twitter', 'youtube', 'linkedin', 'country_code', 'is_featured', 'slug', 'city',
	];
	

	public function getLogoAttribute($value){
		if(!empty($value) && file_exists($value))
			return $value;

		return asset('img/blank-image.jpg');
	}
	public function getUrlAttribute(){
		// return 'university/'.$this->id.'.'.strtotime($this->created_at).'-'.str_slug($this->name);
		return 'university/'.strtolower($this->country_code).'/'.$this->slug;
	}
	public function getFullAddressAttribute(){
		return $this->address
			.(!empty($this->zip)? ', '.$this->zip : '')
			.(!empty($this->country_code)?', '.$this->country->name : '');
	}
	public function getWebsiteAttribute($value){
		return !empty($value)
			? 'https://'.str_replace('https', '', str_replace('https', '', str_replace('://', '', $value)))
			: '';
	}
	public function getFacebookAttribute($value){
		return !empty($value)
			? 'https://'.str_replace('http', '', str_replace('https', '', str_replace('://', '', $value)))
			: '';
	}
	public function getTwitterAttribute($value){
		return !empty($value)
			? 'https://'.str_replace('http', '', str_replace('https', '', str_replace('://', '', $value)))
			: '';
	}
	public function getLinkedinAttribute($value){
		return !empty($value)
			? 'https://'.str_replace('http', '', str_replace('https', '', str_replace('://', '', $value)))
			: '';
	}
	public function getYoutubeAttribute($value){
		return !empty($value)
			? 'https://'.str_replace('http', '', str_replace('https', '', str_replace('://', '', $value)))
			: '';
	}
	public function getRateAttribute(){
		$totalRate = 0;
		$totalVote = 0;		
		for($i = 1; $i <= 5; $i++){
			$vote = $this->reviews()->where('rate', $i)->pluck('rate')->count();
			if($vote == 0) continue;
			$totalVote += $vote;
			$totalRate += $vote * $i;
		}

		if($totalRate > 0 && $totalVote > 0)
			return $totalRate / $totalVote;
		else
			return 0;
	}
	public function getViewsAttribute(){
		return $this->insight()->where('type', 'views')->count();
	}


	public function users(){
		return $this->belongsToMany('App\User', 'shortlists', 'table_id', 'user_id')
		->wherePivot('table', 'universities');
	}
	public function country(){
		return $this->belongsTo('App\Country', 'country_code', 'code');
	}
	public function majors(){
		return $this->hasMany('App\UniversityMajor');
	}
	public function programs(){
		return $this->belongsToMany('App\Program', 'university_programs')
			->withPivot('undergraduate', 'ms', 'mba');
	}
	public function reviews(){
		return $this->hasMany('App\UniversityReview');
	}
	public function afterCollege(){
		return $this->hasOne('App\UniversityAfterCollege');
	}
	public function facility(){
		return $this->hasOne('App\UniversityFacility');
	}
	public function alumni(){
		return $this->hasOne('App\UniversityAlumni');
	}
	public function subjects(){
		return $this->belongsToMany('App\Subject','university_subjects');
	}
	public function access(){
		return $this->belongsToMany('App\User', 'user_accesses', 'access_id', 'user_id')
		->wherePivot('module', 'university');
	}
	public function insight(){
		return $this->hasMany('App\Insight', 'table_id')->where('table', 'universities');
	}


	public function scopeProgramAdmissions(){
		$ids = \App\Program::where('parent_id', 1)->pluck('id');
		return $this->programs()->whereIn('programs.id', $ids)->oldest();//->orderBy('program_id', 'ASC');
	}
	public function scopeProgramCosts(){
		$ids = \App\Program::where('parent_id', 11)->pluck('id');
		return $this->programs()->whereIn('programs.id', $ids)->oldest();//->orderBy('program_id', 'DESC');
	}
	public function scopeProgramRankings(){
		$ids = \App\Program::where('parent_id', 15)->pluck('id');
		return $this->programs()->whereIn('programs.id', $ids);
	}
	public function scopeProgramRankingsAvg($query, $degree){
		$ids = \App\Program::where('parent_id', 15)->pluck('id');
		$rankings = $this->programs()->whereIn('programs.id', $ids)->get();
		
		/* $ug = collect([]);
		$ms = collect([]);
		$mba = collect([]); */
		$avg = collect([]);
		if($rankings){
			foreach($rankings as $ranking){
				/* $ug->push(intval(str_replace('#', '', $ranking->pivot->undergraduate)));
				$ms->push(intval(str_replace('#', '', $ranking->pivot->ms)));
				$mba->push(intval(str_replace('#', '', $ranking->pivot->mba))); */
				$avg->push(intval(str_replace('#', '', $ranking->pivot->{$degree})));
			}
		}
		
		/* return [
			'Undergraduate' => intval($ug->avg()),
			'MS' => intval($ms->avg()),
			'MBA' => intval($mba->avg()),
		]; */
		return intval($avg->avg());
	}
	public function scopeProgramStudents(){
		$ids = \App\Program::where('parent_id', 19)->pluck('id');
		return $this->programs()->whereIn('programs.id', $ids);
	}
	public function scopeProgramAcceptanceRate(){
		return $this->programs()->where('program_id', 2)->first();
	}
	public function scopeProgramDeadline(){
		return $this->programs()->where('program_id', 3)->first();
	}
	public function scopeProgramTuition(){
		return $this->programs()->where('program_id', 12)->first();
	}
	public function scopeProgramRanking(){
		$ranking = collect([]);
		$_ranking = $this->programs()->where('program_id', 17)->first();
		if($_ranking){
			$ranking->push(intval($_ranking->pivot->undergraduate));
			$ranking->push(intval($_ranking->pivot->ms));
			$ranking->push(intval($_ranking->pivot->mba));
			return $ranking->min();
		}
		return '-';
	}
	public function scopeProgramNumberStudent(){
		return $this->programs()->where('program_id', 20)->first();
	}
	public function scopeProgramNumberInternationalStudent(){
		return $this->programs()->where('program_id', 21)->first();
	}
	public function scopeProgramMaleRatio(){
		return $this->programs()->where('program_id', 22)->first();
	}
	public function scopeProgramAdminTest($query, $degree){
		$adminTest = [];
		if($degree == 'undergraduate') $ids = [5, 4];
		elseif($degree == 'ms') $ids = [6];
		else $ids = [6, 7];
		
		$tests = \App\Program::whereIn('programs.id', $ids);
		if($tests->count()){
			foreach($tests->get() as $test){
				$value = $this->programs()->where('program_id', $test->id)->first();
				if(empty($value)) continue;
				$adminTest[$test->name] = $value->pivot->{$degree};
			}
		}

		return $adminTest;
	}
	public function scopeProgramEnglishTest($query, $degree){
		$englishTest = [];
		$tests = \App\Program::whereIn('programs.id', [8,9,10])->oldest()->get();
		if($tests->count()){
			foreach($tests as $test){
				$value = $this->programs()->where('program_id', $test->id)->first();
				if(empty($value)) continue;
				$englishTest[$test->name] = $value->pivot->{$degree};
			}
		}
		
		return $englishTest;
	}
	public function scopeProgramFemaleRatio(){
		return $this->programs()->where('program_id', 24)->first();
	}
	public function scopeFeatured(){
		return $this->where('is_featured', 1);
	}
}
