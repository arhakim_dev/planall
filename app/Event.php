<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model{
	use SoftDeletes;
	
	protected $fillable = ['title', 'description', 'address', 'start_date', 'end_date', 'start_time', 'end_time', 'status', 'media', 'hero_image', 'seat'];

	public function media(){
		return $this->hasMany('App\EventMedia');
	}
	public function users(){
		return $this->belongsToMany('App\User', 'user_events');
	}
	public function attendees(){
		return $this->hasMany('App\UserEvent');
	}

	public function scopeActive($query){
		return $query->where('status', 'active');
    }
    
    
	public function getUrlAttribute(){
		return 'event/'.$this->slug;
	}
}
