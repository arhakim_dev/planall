<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyServicesColumnNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			Schema::table('services', function($table) {
				$table->text('map_address')->nullable()->change();
				$table->string('website', 255)->nullable()->change();
				$table->string('facebook', 255)->nullable()->change();
				$table->string('twitter', 255)->nullable()->change();
				$table->string('youtube', 255)->nullable()->change();
				$table->string('linkedin', 255)->nullable()->change();
				$table->integer('viewed')->default(0)->change();
			});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
