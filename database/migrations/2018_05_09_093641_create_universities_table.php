<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUniversitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('universities', function (Blueprint $table) {
            $table->increments('id');
						$table->string('name', 100);
						$table->string('address', 255);
						$table->string('website', 255);
						$table->text('map_address');
						$table->tinyInteger('is_public')->default(1);
						$table->year('since');
						$table->text('description');
						$table->string('logo', 255);
						$table->string('hero_image', 255);
						$table->string('facebook', 100);
						$table->string('twitter', 100);
						$table->string('youtube', 100);
						$table->string('linkedin', 100);
						$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('universities');
    }
}
