<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUniversityAlumnisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('university_alumnis', function (Blueprint $table) {
            $table->increments('id');
						$table->integer('university_id');
						$table->string('website', 255);
						$table->string('famous_name1');
						$table->string('famous_photo1');
						$table->string('famous_name2');
						$table->string('famous_photo2');
						$table->string('famous_name3');
						$table->string('famous_photo3');
						$table->string('famous_name4');
						$table->string('famous_photo4');
						$table->string('famous_name5');
						$table->string('famous_photo5');
						$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('university_alumnis');
    }
}
