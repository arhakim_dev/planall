let baseURL = $('base').attr('href');
$(function(){	

	$.ajaxSetup({ 
		dataType:'json', 
		headers: {
			'X-CSRF-TOKEN': $laravel.csrfToken, 
			'Access-Control-Allow-Headers': '*'
		} 
	});

	$(document).find('[data-toggle="popover"]').popover();

	$('#dropdownUserNotifications').parent().on('show.bs.dropdown', function (e) {
		let $container = $('#user-notifications-container')

		$container.html('<i class="fa fa-spinner fa-spin mx-auto py-2"></i>')
		$.get('user-notifications')
			.done(function(response){
				$container.html(response.message)
			})
			.fail(function(){
				$container.html('<div class="text-danger text-center fs-1">Oops, something went wrong.<br>Please try again.</div>')
			})

	});

/* 
	$('select').not('.no-select2').css('width', '100%').select2();
		$(document).on('select2:select', 'select', function(e){
			let data = e.params.data;
			let $el = $(e.target);
			let $parent = $el.parent();

			$el.val(data.id);
			$parent.removeClass('has-error').addClass('has-success');
			$parent.find('span.form-error').remove();
			
		}); 
*/
	$.each($('select').not('.no-selectize'), function(i, el){
		let $el = $(el)
		let options = ($el.data('selectize-options')!= undefined)
			? ($el.data('selectize-options')) : {};
		$el.selectize(options);
	})

	$(document).on('keyup input', 'textarea', function() { 
		$(this).css('height', 'auto').css('height', this.scrollHeight + (this.offsetHeight - this.clientHeight));
	});
	$("textarea").each(function () {
		if($(this).val() != '')
			this.style.height = (this.scrollHeight+10)+'px';
	});

	$(window).scroll(function (event) {
		var scroll = $(window).scrollTop();
		if(scroll > 100){
			$('#site-header').addClass('float-header');
			$('#scroll2-top').show();
		}else{
			$('#site-header').removeClass('float-header');
			$('#scroll2-top').hide();
		}				
	});
	
	$('[data-toggle="collapse"]').click(function(){
		if($(this).hasClass('collapsed')){
			$(this).find('.fa.fa-chevron-down')
				.addClass('fa-chevron-up')
				.removeClass('fa-chevron-down');
				
		}else{
			$(this).find('.fa.fa-chevron-up')
				.addClass('fa-chevron-down')
				.removeClass('fa-chevron-up');
		}
	});
	
	$('button.navbar-toggler').click(function(){
		if($(this).hasClass('collapsed')){
			$('#site-header').removeClass('bg-primary');
				
		}else{
			$('#site-header').addClass('bg-primary');
		}
	})
	
	// this is to handle modal issue between tinymce and bootstrap
	$(document).on('focusin', function(e) {
    if ($(e.target).closest(".mce-window").length) {
        e.stopImmediatePropagation();
    }
	});
	
	$(document).on('submit', 'form', function(event){
		var $form = $(this);
		var errorMessage = '';
		var $spinner = $('<i class="fa fa-circle-o-notch fa-spin ml-1"></i>');
		
		if($form.hasClass('noajax')) return;
		event.preventDefault();		
		
		if($form.is('[data-confirm]')){
			if(confirm($form.data('confirm')) == false) return false; 
		}
		
		$form.find('button[type="submit"]').append($spinner);
		$form.find('button[type="submit"]').prop('disabled', true);
		// NProgress.start();
		
		$.ajax({
			url: $form.attr('action'),
			method: $form.attr('method'),
			data: $form.serialize()
			
		}).done(function(response){
			if(response.fn != undefined){
				callback(response);
			}
			
			if(response.message){
				let message = response.message;
				if(response.errors != undefined){
					$.each(response.errors, function(i, el){
						$.each(el, function(i, msg){
							message += "\n"+msg;
						});
					})
				}
				alert(message);
			}
			
		}).always(function(){
			// NProgress.done();
			$form.find('button[type="submit"]').prop('disabled', false);
			$form.find('button[type="submit"] > i').remove();
			
		}).fail(function(xhr){
			var errorMessage = getErrorMessage(xhr);
			alert(errorMessage);
		});		
	});
	$(document).on('click', '.btn-action', function(event){
		event.preventDefault();
		let $btn = $(this);
		let action = $btn.attr('href') || $btn.data('action');
		let method = $btn.data('method') || 'get';
		
		if($btn.data('loader')){
			$($btn.data('loader')).show();
		}
		$.ajax({
			url: $btn.attr('href') || $btn.data('action'),
			method: $btn.data('method') || 'get',
			data: $btn.data('dataset') || {}
			
		}).done(function(response){		
			if(response.fn != undefined){
				callback(response);
			}
		}).always(function(){
			if($btn.data('loader')){
				$($btn.data('loader')).hide();
			}
		}).fail(function(xhr){
			var errorMessage = getErrorMessage(xhr);
			alert(errorMessage);
		});
	});
	
	$('table.sortable th.sort').click(function(){
		let $header = $(this)
		let $table = $header.parents('table').eq(0)
		let action = $table.data('action')
		let sort = ($header.data('sort') == 'desc')? 'asc' : 'desc'
		
		if($header.hasClass('active')) return
		action += '&sort='+$header.text()+'-'+$header.data('sort')

		$('.progress-overlay').show();
		$table.find('th').removeClass('sorted asc desc')
		$.get(action)
			.done(function(response){
				if(response.fn != undefined){
					callback(response);
				}
			}).always(function(){
				$header.addClass('sorted '+sort)
				$header.data('sort', sort)
				$('.progress-overlay').hide();

			}).fail(function(error){
				alert(getErrorMessage(error))
			})

	})

	$(document).on('click', '.collapse-to > .btn', function(e){
		let $el = $(e.target);
		if($el.not('.btn')){
			$el = $el.parents('.collapse-to');
		}
		
		let $btn = $el.find('.btn');
		let collapsTo = $btn.data('collapse-to');
		let $seemore = $el.prev();

		$($btn.data('items')+':gt('+(collapsTo - 1)+')').fadeOut('slow');
		setTimeout(function(){
			$($btn.data('items')+':gt('+(collapsTo - 1)+')').remove();
		}, 1000)
		if($seemore.length){
			let $seemoreBtn = $seemore.find('a');
			let href = $seemoreBtn.attr('href');
			$seemoreBtn.attr('href', href+'&offset='+collapsTo);
		}

		$el.hide();
	})
	
	$(document).on('keyup', 'input[type="search"]', function(e){
		e.preventDefault();
		
		if(e.keyCode == 13){
			$(e.target).parents('form').submit();
		}
	})
		
	$(document).on('click', '#scroll2-top', (e) => {
		e.preventDefault();
		let $a = $(e.target);

		$('html, body').animate({
			scrollTop: $(document).find('body').offset().top - 150
		}, 700)
	});

});
