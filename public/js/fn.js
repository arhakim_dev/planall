if(typeof baseURL == 'undefined')
	baseURL = $('base').attr('href');

function _fileUpload(selector){
	$.getScript(baseURL+'vendor/blueimp-file-upload/js/vendor/jquery.ui.widget.js')
	.done(function(){
		$.getScript(baseURL+'vendor/blueimp-file-upload/js/jquery.fileupload.js')
			.done(function()  {

				$(selector).fileupload({
					add: function (e, data) {
						var $el = $(e.target);
						
						data.url = $el.data('action');
						
						if($el.is('[data-filetype]'))
							data.acceptFileTypes = $el.data('filetype');
						
						if($el.is('[data-maxsize]'))
							data.maxFileSize = $el.data('maxsize');
									
						if($el.is('[data-dataset]')){
							var dataset = {};
							$.each($el.data('dataset').split('|'), function(i, dt){
								var dt_ = dt.split(':');
								dataset[dt_[0]] = dt_[1];
							});
							data.formData = { dataset: JSON.stringify(dataset) };
						}
						
						data.submit();
								
					},
					start: function (e) {
						var $el = $(e.target);
						var $container = $($el.parents('.file-container'));
						
						$container.addClass('uploading');
						$container.find('.upload-progress').show();
						// NProgress.start();
						
					},
					progressall: function (e, data) {
						var $el = $(e.target);
						var $container = $($el.parents('.file-container'));
						var progress = parseInt(data.loaded / data.total * 100, 10);
						
						$container.find('.upload-progress')
							.html('<i class="fa fa-circle-o-notch fa-spin"></i> '+progress +'%');
						
					},
					always: function (e) {
						var $el = $(e.target);
						var $container = $($el.parents('.file-container'));
						$container.find('.upload-progress').empty().hide();
						// NProgress.done();
						
					},
					done: function (e, response) {
						var $el = $(e.target);
						var $container = $($el.parents('.file-container'));
						
						setTimeout(function(){		
							if(response.result.fn != undefined){	
								callback(response.result)
							}
							
							$($container).removeClass('uploading');
							$($container).find('.upload-progress').hide();
						}, 500);
						
					},
					fail: function (e, response) {
						alert(getErrorMessage(response.jqXHR));
					}
				})
				.prop('disabled', !$.support.fileInput)
						.parent().addClass($.support.fileInput ? undefined : 'disabled');
			})
			.fail(function(error) {
				console.log('Failed to load "blueimp-file-upload" plugin');
				console.log(error)
			})
	})
	.fail(function(error) {
		console.log('Failed to load "jquery.ui.widget" plugin');
		console.log(error)
	})
}
function _removeFileUploaded(selector){
	let $el = $(selector)

	$.ajax({
		url: $el.data('action'),
		method: 'delete',
		data: {file: $el.data('file')}
	})
	.done(function(response){
		callback(response)
	})
	.fail(function(error){
		alert(getErrorMessage(error))
	})
}

function _tinymce(selector, pathUpload = '/post/editor/upload'){
	let path = baseURL+'vendor/tinymce';
	let $el = $(selector);
	
	let minHeight = 300;
	if($el.is('[rows]')){
		minHeight = $el.attr('rows') * 100;
	}

	$.getScript(path+'/tinymce.min.js')
		.done(function() {
			tinymce.init({
				selector: selector,
				menubar: false,
				branding: false,
				min_height: minHeight,
				theme_url: path+'/themes/modern/theme.min.js',
				external_plugins: {
					image: path+'/plugins/image/plugin.js',
					imagetools: path+'/plugins/imagetools/plugin.js',
					media: path+'/plugins/media/plugin.js',
					code: path+'/plugins/code/plugin.js',
					autolink: path+'/plugins/image/plugin.js',
					link: path+'/plugins/link/plugin.js',
					lists: path+'/plugins/lists/plugin.js',
					paste: path+'/plugins/paste/plugin.js',
					table: path+'/plugins/table/plugin.js',
					hr: path+'/plugins/hr/plugin.js',
					contextmenu: path+'/plugins/contextmenu/plugin.js',
					searchreplace: path+'/plugins/searchreplace/plugin.js',
					fullscreen: path+'/plugins/fullscreen/plugin.js',
				},
				skin_url: path+'/skins/lightgray',
				toolbar: 'undo redo | bold italic strikethrough alignleft aligncenter alignright | bullist numlist | outdent indent | table hr  image media code link | removeformat fullscreen',
				images_upload_handler: function (blobInfo, success, failure) {
					let xhr = new XMLHttpRequest();
					let csrfToken = document.head.querySelector('meta[name="csrf-token"]').content;
					let ApiToken = (pathUpload.indexOf('api') >= 0)
						? document.head.querySelector('meta[name="api-token"]').content
						: '';
					
					xhr.open('POST', pathUpload);
					
					// manually set header
					xhr.setRequestHeader('X-CSRF-TOKEN', csrfToken); 
					if(ApiToken)
						xhr.setRequestHeader('Authorization', 'Bearer '+ApiToken); // manually set header

					xhr.onload = function() {
							if (xhr.status !== 200) {
									failure('HTTP Error: ' + xhr.status);
									return;
							}

							let json = JSON.parse(xhr.responseText);

							if (!json || typeof json.location !== 'string') {
									failure('Invalid JSON: ' + xhr.responseText);
									return;
							}

							success(json.location);
					};

					let formData = new FormData();
					formData.append('file', blobInfo.blob(), blobInfo.filename());

					xhr.send(formData);
				},
			});
		})
		.fail(function(error) {
			console.log('Failed to load "tinymce" plugin');
			console.log(error)
		})
}

function callback(response){
	$.each(response.fn, function(fn, target){
		// console.log(fn);
		// console.log(target);
		
		if(fn == 'redirect'){
			setTimeout(function(){
				location.href = baseURL + target;
			}, 500);
			
		}else if(fn == 'show-tab'){
			$(target).removeClass('disabled');
			$(target).tab('show');
			$(target).addClass('disabled');
			
		}else if(fn == 'modal-close'){
			$(target).modal('hide');
			
		}else if(fn == 'modal-open'){
			$(target).modal('show');
			
		}else if(fn == 'form-reset'){
			$(target).find('input[type="hidden"][name!="_token"]').val('');
			$(target).find('input[type="text"], input[type="url"], input[type="email"], input[type="password"], textarea').val('');
			$(target).find('select option:first-child').prop('selected', true);
			
			if(typeof tinyMCE != 'undefined' && tinyMCE.activeEditor != null){
				tinyMCE.activeEditor.setContent('')
				tinyMCE.activeEditor.undoManager.clear()
			}
			
		}else if(fn == 'remove-element'){
			$(target).slideUp();
			$(target).remove();
						
		}else if(fn == 'remove-parent-element'){
			if(target.parent == ''){
				$(target.target).parent().slideUp();
				$(target.target).parent().remove();
			}else{
				$(target.target).parents(target.parent).slideUp();
				$(target.target).parents(target.parent).remove();
			}

		}else if(fn == 'append-element'){
			$(target.container).append(target.content);
			
		}else if(fn == 'set-attribute'){
			if(target.length == undefined){
				$(target.target).attr(target.attribute.name, target.attribute.value);
				
			}else{
				$.each(target, function(i, _target){
					if($(_target.target).is('textarea'))
						$(_target.target).val(_target.attribute.value)
					else
						$(_target.target).attr(_target.attribute.name, _target.attribute.value);
				});
			}
		
		}else if(fn == 'set-data-attribute'){
			if(target.length){
				$.each(target, function(i, _target){
					if(_target.data.length == undefined){
						console.log(_target)
						console.log('$('+_target.target+').data('+_target.data.name+', '+_target.data.value+')')
						$(_target.target).data(_target.data.name, _target.data.value);
					}else{
						$.each(_target.data, function(i, data){
							$(_target).data(data.name, data.value);
						});
					}
				});
			}else{
				if(target.data.length == undefined){
					$(target.target).data(target.data.name, target.data.value);
				}else{
					$.each(target.data, function(i, data){
						$(target.target).data(data.name, data.value);
					});
				}
			}
		}else if(fn == 'hide-element'){
			$(target.target).slideUp();
		
		}else if(fn == 'show-element'){
			$(target.target).slideDown('slow');
		
		}else if(fn == 'update-content'){
			if(target.length == undefined){
				$(target.target).html(target.content)
			}else{
				$.each(target, function(i, _target){
					$(_target.target).html(_target.content);
				});
			}
		}else if(fn == 'prop-element'){
			if(target.length == undefined){
				$(target.target).prop(target.status, target.prop)
			}else{
				$.each(target, function(i, _target){
					$(_target.target).attr(_target.status, _target.prop);
				});
			}

		}else if(fn == 'style-css'){
			$.each(target.styles, function(i, style){
				$(target.target).css(style.key, style.value)
			})

		}else if(fn == 'update-piechart'){
			$(target.target).data('easyPieChart').update(target.data.value);

		}else if(fn == 'remove-class'){
			if(target.length){
				$.each(target, function(i, _target){
					console.log(_target.target)
					console.log('$('+_target.target+').removeClass('+_target.class+')')
					$(_target.target).removeClass(_target.class)
				})
			}else
				$(target.target).removeClass(target.class)
		}else if(fn == 'add-class'){
			if(target.length){
				$.each(target, function(i, _target){
					$(_target.target).addClass(_target.class)
				})
			}else
				$(target.target).addClass(target.class)
		}


	});
}

function getErrorMessage(response){
	console.log(response)
	
	let code = (response.status != undefined)? response.status : ''
	let status = (response.statusText != undefined)? response.statusText : ''
	let message = '';

	if(code == 419){
		message = 'Token has been expired, please reload your page and try again.';
	
	}else if(code == 401 || response.message == 'Unauthenticated.'){
		message = 'Token has been expired, please logout and try to re-login';
		
	}else if(code == 200){
		message = response;
	}else{
		if(typeof response.responseJSON != 'undefined'){
			response = response.responseJSON;

			if(typeof response.exception != 'undefined'){
				message = status+' '+code+"\n"
				message += "\nException: "+response.exception+"\n"
			}
		}

		if(response.message){
			message += response.message;
		}	
		
		if(response.errors != 'undefined'){
			$.each(response.errors, function(i, el){
				$.each(el, function(i, msg){
					message += "\n- "+msg;
				});
			})
		}

		if(response.fn){
			callback(response)
		}
	}
	
	if(message == '')
		message += status +' '+ code
		
	return message;
}

function generateSlug(text) {
	if(text == null) return text;

	var slug = "";
	var titleLower = text.toLowerCase();
	slug = titleLower.replace(/e|é|è|ẽ|ẻ|ẹ|ê|ế|ề|ễ|ể|ệ/gi, 'e');
	slug = slug.replace(/a|á|à|ã|ả|ạ|ă|ắ|ằ|ẵ|ẳ|ặ|â|ấ|ầ|ẫ|ẩ|ậ/gi, 'a');
	slug = slug.replace(/o|ó|ò|õ|ỏ|ọ|ô|ố|ồ|ỗ|ổ|ộ|ơ|ớ|ờ|ỡ|ở|ợ/gi, 'o');
	slug = slug.replace(/u|ú|ù|ũ|ủ|ụ|ư|ứ|ừ|ữ|ử|ự/gi, 'u');
	slug = slug.replace(/đ/gi, 'd');
	slug = slug.replace(/\s*$/g, '');
	slug = slug.replace(/\s+/g, '-');
	
	return slug;
}