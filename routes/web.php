<?php

Route::get('/', 'HomeController@index');

Auth::routes();
Route::post('register2', 'Auth\RegisterController@register2');
Route::post('password/change', 'Auth\ResetPasswordController@change');
Route::get('account/activate', 'Auth\RegisterController@activate');

Route::get('dashboard', 'DashboardController@index')->name('dashboard');
Route::post('dashboard/login', 'Auth\LoginController@login');

Route::get('home', 'HomeController@index');
Route::get('home/majors/next', 'HomeController@nextMajors');
Route::get('home/countries/next', 'HomeController@nextCountries');
Route::get('home/universities/next', 'HomeController@nextUniversities');
Route::get('home/events/next', 'HomeController@nextEvents');
Route::get('home/testimonials/next', 'HomeController@nextTestimonials');

Route::get('privacy-policy', 'HomeController@policy');
Route::get('terms-conditions', 'HomeController@toc');
Route::post('mailing-list', 'HomeController@subscribe');

Route::get('profile', 'ProfileController@index')->name('profile');
Route::get('user/{user}', 'ProfileController@index');
Route::put('profile/info', 'ProfileController@update');
Route::post('profile/photo', 'ProfileController@uploadPhoto');
Route::post('user/university/shortlist', 'ProfileController@shotlistUniversity');
Route::post('user/service/shortlist', 'ProfileController@shotlistService');

Route::get('blog', 'PostController@index');
Route::get('blog/category/{categorySlug}', 'PostController@index');
Route::get('blog/latest', 'PostController@latest');
Route::get('blog/{category}/latest', 'PostController@latest');
Route::get('blog/{blog}/comment/next', 'PostController@nextComments');
Route::get('blog/comment/{comment}/replies/next', 'PostController@nextReplies');
Route::get('blog/{slug}', 'PostController@post');
Route::post('blog/post/create', 'PostController@store');

Route::get('search/forum',  'PostController@search');
Route::get('forum/latest', 'PostController@latest');
Route::get('forum', 'PostController@index');
Route::get('forum/{forum}/comment/next', 'PostController@nextComments');
Route::get('forum/comment/{comment}/replies/next', 'PostController@nextReplies');
Route::get('forum/category/{categorySlug}', 'PostController@index');
Route::get('forum/{slug}', 'PostController@post');
Route::post('forum/post/create', 'PostController@store');

// Route::put('forum/{table}/{id}/{action}', 'PostController@react');
Route::post('post/comment', 'PostController@addComment');
Route::post('post/editor/upload', 'PostController@editorUpload');
Route::post('post/hero/upload', 'PostController@editorUpload');
Route::post('post/{id}/{insight}', 'PostController@react');

// Route::get('profile/friends/next', 'ProfileController@nextFriends');
Route::get('profile/universities/next', 'ProfileController@nextUniversities');
Route::get('profile/services/next', 'ProfileController@nextServices');
// Route::get('profile/blogs/next', 'ProfileController@nextBlogs');
Route::get('profile/{type}/comment/next', 'PostController@nextComments');
// Route::get('profile/forums/next', 'ProfileController@nextForums');
// Route::get('profile/forumcomments/next', 'ProfileController@nextForumComments');
Route::get('profile/events/next', 'ProfileController@nextEvents');

// Route::delete('profile/friend/remove/{user}', 'ProfileController@removeFriend');
Route::delete('profile/post/remove/{post}', 'ProfileController@removePost');
Route::delete('profile/comment/remove/{comment}', 'ProfileController@removeComment');
Route::delete('profile/university/remove/{university}', 'ProfileController@removeUniversity');
Route::delete('profile/service/remove/{service}', 'ProfileController@removeService');
Route::delete('profile/event/remove/{event}', 'ProfileController@removeEvent');


Route::get('auth/{provider}', 'Auth\SocialiteController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\SocialiteController@handleProviderCallback');
Route::post('auth/social/login', 'Auth\SocialiteController@login');

Route::get('search/universities',  'UniversityController@search');
Route::get('university/{university}/review/next', 'UniversityReviewController@next');
Route::get('university/{country}/{slug}', 'UniversityController@index');
Route::post('university/compare/add', 'UniversityController@addCompare');
Route::post('university/compare', 'UniversityController@compare');
Route::post('university/review', 'UniversityReviewController@save');
Route::put('university/{university}/update', 'UniversityController@update');
Route::post('university/upload/{type}', 'UniversityController@uploadFile');
Route::delete('university/file/remove', 'UniversityController@removeFile');
Route::delete('university/{university}/alumni/{id}', 'UniversityController@removeAlumni');

Route::get('search/services',  'ServiceController@search');
Route::get('service/category/{slug}', 'ServiceController@search');
Route::get('service/{country}/{slug}', 'ServiceController@index');
Route::get('service/{country}/{slug}/map', 'ServiceController@map');
Route::get('service/cities', 'ServiceController@cities');
Route::put('service/{service}/update', 'ServiceController@update');
Route::post('service/review', 'ServiceReviewController@save');
Route::post('service/upload/{type}', 'ServiceController@uploadFile');
Route::delete('service/file/remove', 'ServiceController@removeFile');

Route::get('events', 'EventController@index');
Route::get('event/latest/next', 'EventController@index');
Route::get('event/{id}/users', 'EventController@users');
Route::get('event/{id}/media', 'EventController@media');
Route::get('event/{slug}', 'EventController@detail');
Route::post('event/register', 'EventController@register');
Route::delete('event/cancel/registration', 'EventController@unregister');

Route::get('country/fetch', 'CountryController@fetch');
Route::get('country/import', 'CountryController@import');

Route::get('user-activities', 'HomeController@userActivities');
Route::get('user-notif-counter', 'NotificationController@counter');
Route::get('user-notifications', 'NotificationController@latest');
Route::get('notifications', 'NotificationController@index');

Route::get('/cache:clear', function() {
    $exitCode = Artisan::call('cache:clear');
    dd($exitCode);
});

Route::get('/view:clear', function() {
    $exitCode = Artisan::call('view:clear');
    dd($exitCode);
});

Route::get('/config:cache', function() {
    $exitCode = Artisan::call('config:cache');
    dd($exitCode);
});

Route::get('/config:clear', function() {
    $exitCode = Artisan::call('config:clear');
    dd($exitCode);
});

Route::get('/storage:link', function() {
	echo 'storage_path : '.storage_path('app').'<br>';
	echo 'public_path : '.public_path();

    $exitCode = exec('ln -s '.storage_path('app').' '.public_path('storage'));
	//$exitCode = Artisan::call('storage:link');

    dd($exitCode);
});

Route::get('storage:url', function(){
	echo Storage::url('post/hero-slider-1.png').'<br>';
	var_dump( file_exists(public_path('/storage/post/hero-slider-1.png')) ).'<br>';
	var_dump( file_exists(Storage::url('/storage/post/hero-slider-1.png')) ).'<br>';
	var_dump( file_exists(asset('/storage/post/hero-slider-1.png')) ).'<br>';
	echo '<img src="'.Storage::url('post/hero-slider-1.png').'" width="100">';

});