<?php

use Illuminate\Http\Request;

Route::get('countries', 'Api\CountryController@index');
Route::get('country/{country}/regions', 'Api\CountryController@regions');
Route::get('country/{country}/region/{region}/cities', 'Api\CountryController@cities');

Route::get('weathers/update', 'Api\WeatherController@update');
Route::get('weathers/{countryCode}/{zip}', 'Api\WeatherController@index');

/* User */
Route::get('user', 'Api\User\UserController@index');
Route::put('user/{id}', 'Api\User\UserController@save');
Route::put('user/trash/{id}', 'Api\User\UserController@trash');
Route::put('user/restore/{id}', 'Api\User\UserController@restore');
Route::put('user/{status}/{user}', 'Api\User\UserController@changeStatus');
Route::delete('user/{id}', 'Api\User\UserController@destroy');
Route::post('user', 'Api\User\UserController@save');
Route::get('role', 'Api\User\UserController@roles');

Route::get('user/{id}/access', 'Api\User\AccessController@index');
Route::post('user/access', 'Api\User\AccessController@save');

/* Route::resource('role', 'Api\User\RoleController', ['except' => ['edit']]);
Route::put('role/trash/{role}', 'Api\User\RoleController@trash');
Route::put('role/restore/{role}', 'Api\User\RoleController@restore'); */

/* Post */
Route::get('{type}/posts', 'Api\Post\PostController@index');
Route::post('{type}/post', 'Api\Post\PostController@save');
Route::put('{type}/post/{id}', 'Api\Post\PostController@save');
Route::put('{type}/post/trash/{id}', 'Api\Post\PostController@trash');
Route::put('{type}/post/restore/{id}', 'Api\Post\PostController@restore');
Route::put('{type}/post/approve/{id}', 'Api\Post\PostController@approve');
Route::put('{type}/post/block/{id}', 'Api\Post\PostController@block');
Route::put('{type}/post/unblock/{id}', 'Api\Post\PostController@unblock');
Route::delete('{type}/post/{id}', 'Api\Post\PostController@destroy');

Route::get('{type}/post/comments', 'Api\Post\PostController@comments');
Route::put('{type}/post/comment/approve/{id}', 'Api\Post\PostController@commentUpdateStatus');
Route::put('{type}/post/comment/block/{id}', 'Api\Post\PostController@commentUpdateStatus');

Route::get('{type}/post/categories', 'Api\Post\CategoryController@index');
Route::post('{type}/post/category', 'Api\Post\CategoryController@save');
Route::put('{type}/post/category/{id}', 'Api\Post\CategoryController@save');
Route::put('{type}/post/category/trash/{id}', 'Api\Post\CategoryController@trash');
Route::put('{type}/post/category/restore/{id}', 'Api\Post\CategoryController@restore');
Route::delete('{type}/post/category/{id}', 'Api\Post\CategoryController@destroy');

/* University */
Route::get('universities', 'Api\University\UniversityController@index');
Route::get('university/{university}/majors', 'Api\University\MajorController@index');
Route::get('university/{university}/facilities', 'Api\University\FacilityController@index');
Route::get('university/{university}/after-collages', 'Api\University\AfterCollageController@index');
Route::get('university/{university}/alumnis', 'Api\University\AlumniController@index');
Route::get('university/{university}/program/{categoryId}', 'Api\University\ProgramController@activePrograms');
Route::get('university/{university}/subjects', 'Api\University\SubjectController@index');

Route::get('university/programs/{parentId?}', 'Api\University\ProgramController@programs');
Route::get('university/reviews', 'Api\University\ReviewController@index');

Route::put('university/{id}', 'Api\University\UniversityController@save')->where('id', '[0-9]+');
Route::put('university/trash/{id}', 'Api\University\UniversityController@trash');
Route::put('university/restore/{id}', 'Api\University\UniversityController@restore');
Route::post('university/upload/{type}', 'Api\University\UniversityController@uploadFile');
Route::post('university', 'Api\University\UniversityController@save');
Route::post('university/import', 'Api\University\UniversityController@import');
Route::post('universities/remove', 'Api\University\UniversityController@bulkTrash');
Route::post('universities/restore', 'Api\University\UniversityController@bulkRestore');
Route::post('universities/destroy', 'Api\University\UniversityController@bulkDestroy');
Route::delete('university/{id}', 'Api\University\UniversityController@destroy');
Route::delete('university/remove/{type}', 'Api\University\UniversityController@removeFile');

// Route::get('university/majors', 'Api\UniversityMajorController@index');
Route::post('university/majors', 'Api\University\MajorController@save');
Route::post('university/facilities', 'Api\University\FacilityController@save');
Route::post('university/after-collages', 'Api\University\AfterCollageController@save');
Route::post('university/alumnis', 'Api\University\AlumniController@save');
Route::post('university/programs/{parent}', 'Api\University\ProgramController@save');
Route::post('university/subject', 'Api\University\SubjectController@save');

Route::post('university/program/category', 'Api\ProgramController@save');
Route::put('university/program/category/{id}', 'Api\ProgramController@save');
Route::put('university/program/category/trash/{id}', 'Api\ProgramController@trash');
Route::put('university/program/category/restore/{id}', 'Api\ProgramController@restore');
Route::delete('university/program/category/{id}', 'Api\ProgramController@destroy');

Route::post('university/review', 'Api\University\ReviewController@save');
Route::put('university/review/{id}', 'Api\University\ReviewController@save');
Route::put('university/review/{id}/approved', 'Api\University\ReviewController@approved');
Route::delete('university/review/{id}', 'Api\University\ReviewController@destroy');

/* Subject */
Route::get('subjects', 'Api\SubjectController@index');
Route::put('subject/{id}', 'Api\SubjectController@save');
Route::put('subject/{id}/sort/{sort}', 'Api\SubjectController@updateSortNumber');
Route::put('subject/trash/{id}', 'Api\SubjectController@trash');
Route::put('subject/restore/{id}', 'Api\SubjectController@restore');
Route::post('subject', 'Api\SubjectController@save');
Route::post('subject/upload', 'Api\SubjectController@uploadFile');
Route::delete('subject/{id}', 'Api\SubjectController@destroy');
Route::delete('subject/remove/image', 'Api\SubjectController@removeFile');

/* Service */
Route::get('service/categories', 'Api\Service\CategoryController@index');
Route::put('service/category/{id}', 'Api\Service\CategoryController@save');
Route::put('service/category/trash/{id}', 'Api\Service\CategoryController@trash');
Route::put('service/category/restore/{id}', 'Api\Service\CategoryController@restore');
Route::post('service/category', 'Api\Service\CategoryController@save');
Route::post('service/category/upload', 'Api\Service\CategoryController@uploadFile');
Route::delete('service/category/{id}', 'Api\Service\CategoryController@destroy');
Route::delete('service/category/remove/icon', 'Api\Service\CategoryController@removeFile');

Route::get('services', 'Api\Service\ServiceController@index');
Route::put('service/{id}', 'Api\Service\ServiceController@save');
Route::put('service/trash/{id}', 'Api\Service\ServiceController@trash');
Route::put('service/restore/{id}', 'Api\Service\ServiceController@restore');
Route::post('service', 'Api\Service\ServiceController@save');
Route::post('service/upload/{file}', 'Api\Service\ServiceController@uploadFile');
Route::post('service/import', 'Api\Service\ServiceController@import');
Route::post('services/remove', 'Api\Service\ServiceController@bulkTrash');
Route::post('services/restore', 'Api\Service\ServiceController@bulkRestore');
Route::post('services/destroy', 'Api\Service\ServiceController@bulkDestroy');
Route::delete('service/{id}', 'Api\Service\ServiceController@destroy');
Route::delete('service/remove/{file}', 'Api\Service\ServiceController@removeFile');

Route::get('service/reviews', 'Api\Service\ReviewController@index');
Route::put('service/review/{id}/approved', 'Api\Service\ReviewController@approved');
Route::put('service/review/{id}', 'Api\Service\ReviewController@save');
Route::post('service/review', 'Api\Service\ReviewController@save');
Route::delete('service/review/{id}', 'Api\Service\ReviewController@destroy');

Route::get('service/{id}/business-hours', 'Api\Service\BusinessHourController@index');
Route::put('service/{id}/business-hours', 'Api\Service\BusinessHourController@save');

Route::get('events', 'Api\Event\EventController@index');
Route::get('event/{id}/media', 'Api\Event\MediaController@index');
Route::get('event/{id}/attendance', 'Api\Event\EventController@attendance');
Route::put('event/trash/{id}', 'Api\Event\EventController@trash');
Route::put('event/restore/{id}', 'Api\Event\EventController@restore');
Route::put('event/{id}', 'Api\Event\EventController@save');
Route::post('event', 'Api\Event\EventController@save');
Route::post('event/media', 'Api\Event\MediaController@save');
Route::post('event/upload/{type}', 'Api\Event\EventController@uploadFile');
Route::delete('event/{id}', 'Api\Event\EventController@destroy');
Route::delete('event/remove/{file}', 'Api\Event\EventController@removeFile');

Route::get('config/notification', 'Api\Configurations\NotificationController@getInterval');
Route::put('config/notification', 'Api\Configurations\NotificationController@setInterval');