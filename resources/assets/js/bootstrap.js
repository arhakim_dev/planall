
window._ = require('lodash');
window.Vue = require('vue');
window.$ = window.jQuery = require('jquery');
window.Popper = require('popper.js').default;
window.axios = require('axios');

window.baseURL = document.head.querySelector('base').href;

require('select2');
require('selectize');
require('bootstrap-datepicker');
require('timepicker');


// window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let csrfToken = document.head.querySelector('meta[name="csrf-token"]').content;
let ApiToken = document.head.querySelector('meta[name="api-token"]').content;

window.axios.defaults.headers.common = {
	'Access-Control-Allow-Origin': '*',
	'Access-Control-Allow-Headers': '*',
	'X-Requested-With': 'XMLHttpRequest',
	'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE',
	'X-CSRF-TOKEN': csrfToken,
	'Authorization': 'Bearer '+ApiToken
}

$.ajaxSetup({ 
	dataType:'json', 
	headers: {
		'Access-Control-Allow-Origin': '*',
		'Access-Control-Allow-Headers': '*',
		'X-Requested-With': 'XMLHttpRequest',
		'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE',
		'X-CSRF-TOKEN': csrfToken, 
		'Authorization': 'Bearer '+ApiToken
	} 
});

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });
