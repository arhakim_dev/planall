require('./bootstrap');

import router from './router.js';
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue);
Vue.component('select2', require('./components/select2.vue'));
Vue.component('selectize', require('./components/selectize.vue'));
Vue.component('datepicker', require('./components/datepicker.vue'));
Vue.component('timepicker', require('./components/timepicker.vue'));

Vue.config.devtools = true
const app = new Vue({
	el: '#app',
	router,
});



/* $('select.select2').css('width', '100%').select2();
$(document).on('select2:select', 'select', function(e){
	let data = e.params.data;
	let $el = $(e.target);
	$el.val(data.id);

}); */

$(document).on('keyup input', 'textarea', function(e) { 
	$(e.target)
		.css('height', 'auto')
		.css('height', this.scrollHeight + (this.offsetHeight - this.clientHeight));
});

$("textarea").each(function () {
	this.style.height = (this.value)? (this.scrollHeight+10)+'px' : '120px';
});


/* $('.year').datepicker({
	format: " yyyy",
	startView: "decade",
	minViewMode: "years"
})
$('.timepicker').timepicker({
	timeFormat: 'H:i:s',
	disableTextInput: true
})
$(document).on('changeTime', '.timepicker', function(e){
	console.log(e)
}) */