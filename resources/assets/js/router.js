import Vue from 'vue'
import Router from 'vue-router'

import dashboardIndex from './pages/dashboard/index.vue'
import dashboardUsers from './pages/dashboard/users/index.vue'
import dashboardUserRoles from './pages/dashboard/users/role.vue'

import dashboardPosts from './pages/dashboard/posts/index.vue'
import dashboardPostCategories from './pages/dashboard/posts/category.vue'
import dashboardPostComments from './pages/dashboard/posts/comment.vue'

import dashboardUniversities from './pages/dashboard/universities/index.vue'
import dashboardUniversityPrograms from './pages/dashboard/universities/program.vue'
import dashboardUniversityReviews from './pages/dashboard/universities/review.vue'
import dashboardUniversitySubjects from './pages/dashboard/universities/subject.vue'

import dashboardServices from './pages/dashboard/services/index.vue'
import dashboardServiceCategories from './pages/dashboard/services/category.vue'
import dashboardServiceReviews from './pages/dashboard/services/review.vue'

import dashboardEvents from './pages/dashboard/events/index.vue'

import ConfigurationNotification from './pages/dashboard/configurations/notification.vue'

Vue.use(Router)


export default new Router({
  routes: [
    {
      path: '/',
      name: 'dashboard-index',
      component: dashboardIndex
    },
    {
      path: '/users',
      name: 'dashboard-users',
      component: dashboardUsers
    },
    {
      path: '/user/roles',
      name: 'dashboard-user-roles',
      component: dashboardUserRoles
    },
    {
      path: '/:type/posts',
      name: 'dashboard-blogs',
      component: dashboardPosts
    },
    {
      path: '/:type/post/categories',
      name: 'dashboard-blog-categories',
      component: dashboardPostCategories
    },
    {
      path: '/:type/post/comments',
      name: 'dashboard-blog-comments',
      component: dashboardPostComments
    },
    {
      path: '/universities',
      name: 'dashboard-universities',
      component: dashboardUniversities
    },
    {
      path: '/university/programs',
      name: 'dashboard-universities-programs',
      component: dashboardUniversityPrograms
    },
    {
      path: '/university/reviews',
      name: 'dashboard-universities-reviews',
      component: dashboardUniversityReviews
    },
    {
      path: '/university/subjects',
      name: 'dashboard-universities-subjects',
      component: dashboardUniversitySubjects
    },
    {
      path: '/services',
      name: 'dashboard-services',
      component: dashboardServices
    },
    {
      path: '/service/categories',
      name: 'dashboard-service-categories',
      component: dashboardServiceCategories
    },
    {
      path: '/service/reviews',
      name: 'dashboard-service-reviews',
      component: dashboardServiceReviews
    },
    {
      path: '/events',
      name: 'dashboard-service-events',
      component: dashboardEvents
    },
    {
      path: '/notification',
      name: 'configuration-notification',
      component: ConfigurationNotification
    },
    
  ]
  
})