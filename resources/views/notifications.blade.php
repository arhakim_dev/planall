@extends('page', ['body_class' => '', 'footerStyle' => ''])

@section('content')

<div class="container py-5">
	<h4 class="section-title mt-5">All Notifications</h4>
	<div class="list-group">
	@forelse($notifications as $notification)
		<a href="{{$notification['url']}}" class="list-group-item list-group-item-action d-flex align-items-center justify-content-between">
			<div>
				@if($notification['thumbnail'])
				<img src="{{ $notification['thumbnail'] }}" class="img-thumbnail rounded-circle mx-2" width="50">
				@endif
				<span class="text-secondary">{{$notification['title']}}</span> ~ 
				{{$notification['message']}}
			</div>
			<div>
				<span class="text-muted">{{$notification['created_at']}}</span>&nbsp;
			<span class="badge badge-pill badge-{{($notification['is_read'])? 'info' : 'secondary'}}">&nbsp;</span>
			</div>
		</a>
	@empty
		<div class="list-group-item">All is good!</div>
	@endforelse
	</div>
</div>
@stop