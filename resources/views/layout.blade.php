<!DOCTYPE html>
<html>
<head>
		<base href="{{ config('app.url') }}/" title="{{ config('app.name') }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{config('app.name')}}</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
		<script>window.$laravel = { csrfToken: '{{ csrf_token() }}' }</script>
		<link href="{{ asset('img/favico2.png') }}" rel="icon">
		<link rel="stylesheet" href="{{ asset('css/app.css?_='.time()) }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}">
		@stack('styles')
</head>
<body class="hold-transition @yield('body_class')">

	<div id="app" class="h-100">
	@yield('body')
	</div>
	
  {{-- <script src="{{ asset('vendor/popper.js/dist/umd/popper.min.js') }}"></script> --}}
  <script src="{{ asset('js/app.js?_='.time()) }}"></script>
  <script src="{{ asset('js/fn.js?_='.time()) }}"></script>
  <script src="{{ asset('js/script.js?_='.time()) }}"></script>  
  @stack('scripts')

  @if($_NOTIFICATION_INTERVAL_ > 0)
    @auth
    <script>
      function getNotifCounter(){
        $.get('user-notif-counter')
        .done(function(response){
          callback(response);
        })
      }
      getNotifCounter();
      setInterval(function(){
        getNotifCounter();
      }, 1000*60*{{$_NOTIFICATION_INTERVAL_}})
    </script>
    @endauth
  @endif
</body>
</html>