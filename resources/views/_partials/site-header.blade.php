
<header id="site-header" class="position-fixed bg-primary">
	@if(session('flash_message') !== '')
	<div class="alert-warning text-center text-secondary">
		{{session('flash_message')}}
	</div>
	@endif
	<div class="d-none d-md-block d-lg-none text-center pt-1">
		<a href="{{url('/')}}">
			<img src="img/logo-white.png" height="50" alt="Logo"> 
		</a>
	</div>
	<nav class="navbar navbar-expand-md py-md-0 py-lg-2">
		<div class="container-fluid">
			<a class="navbar-brand mr-0 d-md-none d-lg-inline-block" href="{{url('/')}}">
				<img src="{{ asset('img/logo-white.png') }}" height="50" class="d-inline-block align-top site-logo" alt="Logo"> </a>
			<button class="navbar-toggler navbar-toggler-right text-white " data-toggle="collapse" data-target="#top-menu" aria-controls="top-menu" aria-expanded="false" aria-label="Toggle navigation">
				<span class="fa fa-bars"></span>
			</button>
			<form class="form-inline w-100 w-lg-20 w-xl-30 d-md-none d-lg-block ml-lg-2 noajax" method="GET" action="search/universities">
				<input type="hidden" name="degree" value="undergraduate"/>
				<input type="hidden" name="tuition" value="1500,500000"/>
				<input type="hidden" name="sort" value="ranking"/>
				<input class="form-control w-100" type="search" placeholder="Search" name="q">
			</form>

			<div class="collapse navbar-collapse text-center justify-content-end" id="top-menu">
				<ul class="navbar-nav text-white mx-md-auto">
					<li class="nav-item">
						<a class="nav-link" href="{{url('/')}}"> Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="search/universities">Universities</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Service Providers <i class="fa fa-sort-down"></i></a>
						<div class="dropdown-menu">
							@forelse($_SERVICES_ as $service)
							<a class="dropdown-item" href="{{$service->url}}">{{$service->name}}</a>
							@empty
							<smal class="text-muted">Service Providers Not Available</smal>
							@endforelse
						</div>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="forum">Forum</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="blog">Blogs</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="events">Events</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Contact Us</a>
					</li>
					
					@auth
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="dropdownUserNotifications" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fa fa-bell fs-4"></i>
							<span class="badge badge-pill badge-danger ml-auto position-absolute" style="top:15px;display:none;" id="user-notif-counter"></span>
						</a>
						<div class="dropdown-menu dropdown-menu-right px-1" aria-labelledby="dropdownUserNotifications" style="width: max-content;">
							<a class="dropdown-item">Notification</a>
							<div class="dropdown-divider"></div>
							<div id="user-notifications-container" class="fs-1" style="max-height:400px;overflow:auto;"></div>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="notifications">All Notification </a>
						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding-top:5px;">
							<img class="img-fluid rounded-circle border" id="user-avatar" src="{{ asset(Auth::user()->avatar) }}" width="33" height="33"> </a>
						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
							<small class="ml-1">Hi, <span class="font-weight-bold">{{Auth::user()->first_name}}</span></small>
							<a class="dropdown-item" href="profile">Profile</a>
							<div class="dropdown-divider"></div>
							@if($_ACCESS_['universities']->count())
							<small class="ml-1">Universities</small>
								@foreach($_ACCESS_['universities']->get() as $university)
									<a class="dropdown-item" href="{{$university->url}}">{{$university->name}}</a>
								@endforeach
								<div class="dropdown-divider"></div>
							@endif
							@if($_ACCESS_['services']->count())
							<small class="ml-1">Services</small>
								@foreach($_ACCESS_['services']->get() as $service)
									<a class="dropdown-item" href="{{$service->url}}">{{$service->name}}</a>
								@endforeach
								<div class="dropdown-divider"></div>
							@endif
							<a class="dropdown-item" href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Sign out</a>
							<form id="logout-form" action="logout" method="POST" style="display: none;">{{ csrf_field() }}</form>
						</div>
					</li>
					@endauth
				</ul>
				@guest
				<a class="btn navbar-btn btn-light text-primary font-weight-bold rounded ml-1" data-toggle="modal" data-target="#modal-login"> LOGIN</a>
				@endguest
			</div>
		</div>
	</nav>
	<form class="form-inline w-75 d-none d-md-block d-lg-none mx-auto mb-1 noajax" method="GET" action="search/universities">
		<input type="hidden" name="degree" value="undergraduate"/>
		<input type="hidden" name="tuition" value="1500,500000"/>
		<input type="hidden" name="sort" value="ranking"/>
		<input class="form-control w-100" type="search" placeholder="Search" name="q">
	</form>
</header>
