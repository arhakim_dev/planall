<a class="dropdown-item d-flex align-items-center justify-content-start py-1 px-1" href="{{$url}}">
	@if($thumbnail)
	<img src="{{ $thumbnail }}" class="img-thumbnail rounded-circle mx-2" width="50">
	@endif
	<div> 
		<div>{{title_case($title)}}</div>
		<div class="text-muted">{{$message}}</div>
		<i class="text-muted">{{$time}}</i>
	</div>
</a>