
<footer id="site-footer" class="pt-5 bg-white text-muted" style="{{$footerStyle or 'margin-top:-145px;'}}">
	<div class="container">
		<div class="row">
			<div class="col-4 text-center">
				<h4>Information</h4>
				<p>
					<a href="https://www.fineprep.com/ctet/" target="_blank" rel="noopener">About CTET</a><br>
					<a href="https://www.fineprep.com/faq" target="_blank" rel="noopener">FAQ</a><br>
					<a href="https://www.fineprep.com/contact" target="_blank" rel="noopener">Contact</a>
				</p>
			</div>
			<div class="col-4 text-center">
				<h4>PlanAll Policies</h4>
				<p>
					<a href="https://www.fineprep.com/refund-policy/" target="_blank" rel="noopener">Refund Policy</a><br>
					<a href="https://www.fineprep.com/terms/" target="_blank" rel="noopener">Terms of Use</a><br>
					<a href="https://www.fineprep.com/privacy-policy/" target="_blank" rel="noopener">Privacy Policy</a>
				</p>
			</div>
			<div class="col-4 text-center">
				<h4>Connect with PlanAll</h4>
				<ul class="nav justify-content-center social-links">
					<li class="nav-item">
						<a class="nav-link mr-1" rel="nofollow" title="Facebook" href="https://facebook.com/fineprep">
							<i class="fa fa-facebook"></i>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link mr-1" rel="nofollow" title="LinkedIn" href="https://linkedin.com/company/fineprep">
							<i class="fa fa-linkedin"></i>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" rel="nofollow" title="Contact" href="mailto:hello@fineprep.com">
							<i class="fa fa-envelope"></i>
						</a>
					</li>
				</ul>

			</div>
			{{-- <div class="col-12">
				<h1 class="site-logo text-center" style="color: #0fadef;">
					<img src="{{ asset('img/logo-blue.png') }}" height="50" class="d-inline-block align-top" alt="Logo"></h1>
				<ul class="nav justify-content-center mb-3">
					<li class="nav-item">
						<a class="nav-link text-grey-light" href="#">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link text-grey-light" href="#">Blog</a>
					</li>
					<li class="nav-item">
						<a class="nav-link text-grey-light" href="#">About Us</a>
					</li>
					<li class="nav-item">
						<a class="nav-link text-grey-light" href="#">Contact Us</a>
					</li>
				</ul>
				<p class="text-center text-grey-light">Copyright 2018 Miki Grujic</p>
				<ul class="nav justify-content-center">
					<li class="nav-item">
						<a class="nav-link" href="#">
							<i class="fa fa-facebook" style="color: #3b5997;"></i>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">
							<i class="fa fa-twitter" style="color: #29c7f7;"></i>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">
							<i class="fa fa-linkedin" style="color: #036cbd;"></i>
						</a>
					</li>
				</ul>
			</div> --}}
		</div>
	</div>
	<footer class="text-center py-3">
		©2018 FinePrep Educational Technologies Pvt. Ltd.
	</footer>
</footer>

@if(Auth::check() == false)
<div class="modal fade" id="modal-login" tabindex="-1" data-backdrop="static">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body p-0">
				<button data-dismiss="modal" class="close position-absolute py-3 rounded-circle" style="right:20px; background-color: rgba(255, 255, 255, .5);z-index:1;">
					<img class="" src="{{asset('img/icon/delete.png')}}" height="30"> </button>
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-6 bg-gradient text-center text-white py-4 d-flex flex-column justify-content-center align-items-center">
							<img class="d-block img-fluid my-3 mx-auto w-25 w-lg-50" src="{{ asset('img/logo-white.png') }}">
							<div class="position-relative b-0-offset b-md-2-offset">
								<p class="font-weight-bold mb-0 fs-3"> Need any help?Let us know </p>
								<p>
									<a href="mailto:support@planall.com" class="text-white">
										<i class="fa fa-envelope-o"></i> support@planall.com</a>
								</p>
							</div>
						</div>
						<div class="col-md-6 py-4">
							<h2 class="mb-4 text-secondary">Welcome</h2>
							<form class="flat" method="post" action="login">
								<div class="form-group">
									<input type="email" name="email" class="form-control no-icon" placeholder="Email Address" 
										data-validation="email"> </div>
								<div class="form-group">
									<input type="password" name="password" class="form-control no-icon" placeholder="Password" 
										data-validation="required"> </div>
								<div class="form-group text-center">
									<button class="btn btn-secondary bg-gradient-vertical w-50 py-lg-2" type="submit">Login</button>
									<p>Do not have an account?
									<a href="#" data-toggle="modal" data-target="#modal-signup">Sign Up</a><br>
									or <a href="#" data-toggle="modal" data-target="#modal-forgotpwd">forgot password?</a>
									</p>
								</div>
							</form>
							<div class="text-center">
								<!-- <fb:login-button scope="public_profile,email" onlogin="checkLoginState();"></fb:login-button>-->
								<a href="{{url('auth/facebook') }}" class="btn bg-gradient-vertical text-white popup-window" title="Login with Facebook account">
									<i class="fa fa-facebook fa-2x px-2"></i>
								</a>
								<span> or </span>
								<a href="{{url('auth/google') }}" class="btn bg-white text-secondary btn-outline-secondary py-2 popup-window" title="Login with Google account">
									<img src="{{ asset('img/icon/google-plus.png') }}"> </a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-signup" tabindex="-1" data-backdrop="static">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-body p-0">
				<button data-dismiss="modal" class="close position-absolute p-2 rounded-circle" style="top:20px;right:20px; background-color: rgba(255, 255, 255, .5);z-index:1;">
					<img class="" src="{{ asset('img/icon/delete.png') }}"> </button>
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-6 bg-gradient text-center text-white py-4 d-flex flex-column justify-content-center align-items-center">
							<img class="d-block img-fluid my-3 mx-auto w-25 w-lg-50" src="{{ asset('img/logo-white.png')}}">
							<div class="position-relative b-0-offset b-4-md-offset">
								<p class="font-weight-bold mb-0"> Need any help?Let us know </p>
								<p>
									<a href="mailto:support@planall.com" class="text-white">
										<i class="fa fa-envelope-o"></i> support@planall.com</a>
								</p>
							</div>
						</div>
						<div class="col-md-6 py-4 pl-lg-3 pr-lg-5">
							<h2 class="mb-4">
								<span class="text-secondary">Sign Up |</span>
								<span class="text-muted"> Student</span>
							</h2>
							<div id="tab-signup">
								<ul class="nav nav-pills mb-3">
									<li class="nav-item mr-2 mr-lg-0 mb-3">
										<a class="active nav-link bg-white text-secondary rounded-circle shadow disabled" href="" data-toggle="pill" data-target="#formone">1</a>
									</li>
									<li class="nav-item">
										<a href="" class="nav-link bg-white text-secondary rounded-circle shadow disabled" data-toggle="pill" data-target="#formtwo" disabled>2</a>
									</li>
								</ul>
							</div>
							<div class="tab-content">
								<div class="tab-pane fade show active" id="formone" role="tabpanel">
									<form class="flat" method="post" action="register" id="form-signup1">
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<input type="text" class="form-control" name="first_name" placeholder="First Name" 
															data-validation="required"> </div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<input type="text" class="form-control" name="last_name" placeholder="Last Name" 
															data-validation="required"> </div>
												</div>
											</div>
										<div class="form-group">
											<input type="email" class="form-control" name="email" placeholder="Email" 
												data-validation="email"> </div>
										<div class="form-group position-relative">
											<input type="password" class="form-control" name="password_confirmation" placeholder="Password" 
												data-validation="strength" data-validation-strength="2"  data-validation-length="min8" data-validation-error-msg="The password should be at least 8 characters long and should have alphabets and numerical, special characters are allowed"> </div>
										<div class="form-group">
											<input type="password" class="form-control" name="password" placeholder="Confirm" 
												data-validation="confirmation"> </div>
										<div class="form-group">
											<input type="text" class="form-control datepicker" name="birth_date" placeholder="Date of birth" 
												data-validation="date"> </div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<select name="country" class="text-secondary box-shadow-none border-bottom-secondary"	data-validation="required">
														<option value="">Country</option>
													@if($_COUNTRIES_->count())
														@foreach($_COUNTRIES_ as $country)
														<option value="{{$country->code}}">{{$country->name}}</option>
														@endforeach
													@endif
													</select>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<input type="text" class="form-control" name="phone" placeholder="Phone"  data-validation="custom" data-validation-regexp="^[\+]?[(]?[0-9]{2,3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,11}$" data-validation-optional="true" data-validation-error-msg="Incorrect phone number format"> </div>
											</div>
										</div>
										<div class="form-group">
											<div data-toggle="buttons" class="btn-group d-flex justify-content-center align-items-center">
												<label class="btn btn-outline-secondary form-check-label gender-option">
													<input type="radio" class="form-check-input" name="gender" value="F">
													<i class="fa fa-venus fa-2x"></i>
												</label>
												<span class="mx-2">Gender</span>
												<label class="btn btn-outline-secondary  form-check-label gender-option active">
													<input type="radio" class="form-check-input" name="gender" value="M" checked>
													<i class="fa fa-mars fa-2x"></i>
												</label>
											</div>
										</div>
										<div class="form-group">
											<div class="custom-control custom-radio">
												<input type="radio" id="tcpp" name="terms" class="custom-control-input" value="on" 
													data-validation="required" data-validation-error-msg="You have to agree to our terms">
												<label class="custom-control-label text-muted" for="tcpp">
													I accept <a href="terms-conditions" target="_blank">terms and conditions</a> &amp; <a href="privacy-policy" target="_blank">privacy policy</a></label>
											</div>
										</div>
										<div class="form-group text-center">
											<button type="submit" class="btn btn-secondary bg-gradient-vertical w-50 py-2">Proceed</button>
											<p> Already a member?
												<a href="#" data-toggle="modal" data-target="#modal-login">Log in</a>
											</p>
										</div>
										<div class="form-group text-center">
											<a href="{{url('auth/facebook') }}" class="btn bg-gradient-vertical text-white popup-window" title="Login with Facebook account">
												<i class="fa fa-facebook fa-2x px-2"></i>
											</a>
											<span> or </span>
											<a href="{{url('auth/google') }}" class="btn bg-white text-secondary btn-outline-secondary py-2 popup-window" title="Login with Google account">
												<img src="{{ asset('img/icon/google-plus.png') }}"> </a>
										</div>
									</form>
								</div>
								<div class="tab-pane fade" id="formtwo" role="tabpanel">
									<form class="flat" method="post" action="register2" id="form-signup2">
										<input type="hidden" name="uid" class="uid">
										<div class="form-group text-center">
											<figure class="rounded-circle position-relative mx-auto file-container border border-secondary" style="height: 170px;width: 170px;">
												<img class="img-fluid rounded-circle" id="profile-photo">
												<div class="btn position-absolute text-secondary w-100 h-100 rounded-circle d-flex justify-content-center align-items-center flex-column" style="top: 0;left: 0;background-color: rgba(255, 255, 255, .5);" data-toggle="modal" data-target="#modal-cavatar">
													<i class="fa fa-camera"></i>
													{{-- <input type="file" name="file" data-action="profile/photo"> --}}
													<div> Upload your
														<br> profile photo </div>
												</div>
												<div class="upload-progress"></div>
												<input type="hidden" name="photo" id="photo">
											</figure>
										</div>
										<div class="form-group">
											<select name="interest_course" name="major" class="text-secondary box-shadow-none border-bottom-secondary">
												<option value="" disabled selected>Interested course</option>
											@if($_MAJORS_->count())
												@foreach($_MAJORS_ as $major)
												<option value="{{$major->id}}">{{$major->name}}</option>
												@endforeach
											@endif
												<option value="0">Others</option>
											</select>
										</div>
										<div class="form-group">
											<select name="interest_location" class="text-secondary box-shadow-none border-bottom-secondary">
												<option value="">Interested Location</option>
											@if($_COUNTRIES_->count())
												@foreach($_COUNTRIES_ as $country)
												<option value="{{$country->code}}">{{$country->name}}</option>
												@endforeach
											@endif
											</select>
										</div>
										<div class="form-group">
											<select name="dream_university" class="text-secondary box-shadow-none border-bottom-secondary">
												<option value="">Dream University</option>
											@if($_UNIVERSITIES_->count())
												@foreach($_UNIVERSITIES_ as $university)
												<option value="{{$university->id}}">{{$university->name}}</option>
												@endforeach
											@endif
											</select>
										</div>
										<div class="form-group text-center">
											<button type="submit" class="btn btn-secondary bg-gradient-vertical w-50 py-2">Finish</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-forgotpwd" tabindex="-1" data-backdrop="static">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body p-0">
					<button data-dismiss="modal" class="close position-absolute py-3 rounded-circle" style="right:20px; background-color: rgba(255, 255, 255, .5);z-index:1;">
						<img class="" src="{{asset('img/icon/delete.png')}}" height="30"> </button>
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-6 bg-gradient text-center text-white py-4 d-flex flex-column justify-content-center align-items-center">
								<img class="d-block img-fluid my-3 mx-auto w-25 w-lg-50" src="{{ asset('img/logo-white.png') }}">
								<div class="position-relative b-0-offset b-md-2-offset">
									<p class="font-weight-bold mb-0 fs-3"> Need any help?Let us know </p>
									<p>
										<a href="mailto:support@planall.com" class="text-white">
											<i class="fa fa-envelope-o"></i> support@planall.com</a>
									</p>
								</div>
							</div>
							<div class="col-md-6 py-4">
								<h2 class="mb-4 text-secondary">Forgot Password</h2>
								<p class="text-muted"><i>We will send you a link to reset your password to your email</i></p>
								<form class="flat" method="post" action="password/email">
									<div class="form-group">
										<input type="email" name="email" class="form-control no-icon" placeholder="Email Address" 
											data-validation="email"> </div>
									<div class="form-group text-center">
										<button class="btn btn-secondary bg-gradient-vertical w-50 py-lg-2" type="submit">Submit</button>
									</div>
									<p class="text-center">Remember your password? <a href="#modal-login" data-toggle="modal">Log in</a></p>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<div class="modal fade" id="modal-socials" tabindex="-1" role="dialog" aria-labelledby="modal-socials" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body" style="height: 580px;">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          
          <div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" frameborder="0"></iframe>
          </div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-cavatar" tabindex="-1" role="dialog" aria-labelledby="modal-avatar" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<button data-dismiss="modal" aria-label="Close" class="close position-absolute p-2 rounded-circle" style="top:0;right:0; background-color: rgba(255, 255, 255, .5);z-index:1;">
					<img class="" src="{{asset('img/icon/delete.png')}}" height="20"> </button>
				
					<h5 class="mb-4 text-secondary text-center font-weight-bold">Upload avatar</h5>
					<div class="image-editor text-center">
						<div class="cropit-preview mx-auto mt-5 mb-2"></div>
						<div class="row">
							<div class="col-md-6">
								<span class="rotate-ccw"><i class="fa fa-rotate-left"></i></span>
								<span class="rotate-cw"><i class="fa fa-rotate-right"></i></span>
							</div>
							<div class="col-md-6">
								<input type="range" class="cropit-image-zoom-input">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 text-right">
								<button class="btn btn-outline-secondary btn-sm position-relative w-75">
									Choose Image <i class="fa fa-image"></i>
									<input type="file" class="cropit-image-input">
								</button>
							</div>
							<div class="col-md-6">
								<form action="profile/photo" method="post" id="form-avatar">
									<input type="hidden" name="uid" class="uid">
									<input type="hidden" name="avatar" id="avatar">
									<button class="btn btn-secondary btn-sm export w-75" type="submit" disabled>Save</button>
								</form>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</div>

@endif

<div id="scroll2-top" class="shadow bg-secondary text-white px-2 py-1 cursor-pointer position-fixed rounded-circle" style="right:50px;bottom:50px;display:none;">
	<i class="fa fa-angle-up"></i>
</div>

@push('styles')
<style>
	#site-footer a{
		color: #666;
		text-decoration: none;
	}
	#site-footer a:hover{
		color: #1E72BD;
	}
	#site-footer .social-links a{
		color: white;
		background-color: #1E72BD;
	}
	#site-footer .social-links a:hover{
		color: whitesmoke;
		background-color: #777;
	}
</style>
	@guest
	
	<style>
		.cropit-preview {
			background-color: #f8f8f8;
			background-size: cover;
			border: 5px solid #ccc;
			border-radius: 3px;
			margin-top: 7px;
			width: 250px;
			height: 250px;
		}

		.cropit-preview-image-container {
			cursor: move;
		}

		.cropit-preview-background {
			opacity: .2;
			cursor: auto;
		}

		.image-size-label {
			margin-top: 10px;
		}

		input, .export {
			/* Use relative position to prevent from being covered by image background */
			position: relative;
			z-index: 10;
			display: block;
		}

		button {
			margin-top: 10px;
		}
		.selectize-input, .selectize-input input{
			font-size: 16px;
		}
	</style>
	
	@endguest
@endpush
@push('scripts')
	@guest
	<script src="{{ asset('vendor/cropit/dist/jquery.cropit.js') }}"></script>
	<script>
		let _date = new Date();
		let _windowSocialite = null;

		$(function(){
			
			$.validate({
				modules : 'security, date',
				onModulesLoaded : function() {
					var optionalConfig = {
						fontSize: '12pt',
						padding: '4px',
						bad : 'Very bad',
						weak : 'Weak',
						good : 'Good',
						strong : 'Strong'
					};

					$('input[name="password_confirmation"]').displayPasswordStrength(optionalConfig);
				}
			});

			$('.image-editor').cropit({
				exportZoom: 1.25,
				imageBackground: true,
				imageBackgroundBorderWidth: 50,
				imageState: {
					src: "{{asset('img/blank-image.jpg')}}",
				},
				onFileChange: function(e){
					let $el = $(e.target);
					let $container = $el.parents('.image-editor');
					$container.find('button[type="submit"]').prop('disabled', false);
				},
				onImageError: function(error) {
					alert(error.message)
				}
			});

			$('.rotate-cw').click(function() {
				$('.image-editor').cropit('rotateCW');
			});
			$('.rotate-ccw').click(function() {
				$('.image-editor').cropit('rotateCCW');
			});

			$('.export').click(function() {
				var imageData = $('.image-editor').cropit('export');
				$('#form-avatar #avatar').val(imageData);
				$('#form-avatar').submit();
			});

			$('input.datepicker').datepicker({
				format: 'yyyy-mm-dd',
				// startDate : (_date.getFullYear() - 50),
				endDate : (_date.getFullYear() - 10)+'-12-31'
			})

			$('#modal-signup').on('shown.bs.modal', function (e) {
				$('#modal-login').modal('hide');
				setTimeout(function(){
					$('body').addClass('modal-open')
				}, 1000);
				
			})

			$('#modal-login').on('shown.bs.modal', function (e) {
				$('#modal-signup').modal('hide')
				$('#modal-forgotpwd').modal('hide')
				setTimeout(function(){
					$('body').addClass('modal-open')
				}, 1000);
			})

			$('#modal-forgotpwd').on('shown.bs.modal', function (e) {
				$('#modal-login').modal('hide')
				setTimeout(function(){
					$('body').addClass('modal-open')
				}, 1000);
			})

			$(document).on('click', '.popup-window', function(e){
				e.preventDefault();
				let $this = $(this);
				let x = (screen.width / 2) - (600 / 2);
				let y = (screen.height / 2) - (700 / 2);

				_windowSocialite = window.open($this.attr('href'), $this.attr('title'), 'width=650,height=660,location=0,menubar=0,status=0,toolbar=0,left='+x+',top=0');
				
			});

		})

		function checkSocialLogin(id){
			$.post(baseURL+'auth/social/login', {id: id})
				.done(function(response){
					_windowSocialite.close();
					callback(response)
				})
				.fail(function(error){
					let response = ajax.getErrorMessage(error);
					$(_windowSocialite.document).find('#status-social-auth').text(response.message);
				})
		}

	</script>
	@endguest
@endpush