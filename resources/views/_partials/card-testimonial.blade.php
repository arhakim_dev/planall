
<div class="card rounded mb-4 shadow">
	<div class="card-body">
		<h5 class="card-title d-flex justify-content-between">
			<div class="font-weight-bold">{{$testimonial->title}}</div>
			<div class="rating">
				@for($r = 1; $r <= $testimonial->rate; $r++)
				<span class="fa fa-star checked"></span>
				@endfor
				@for($r = 1; $r <= (5 - $testimonial->rate); $r++)
				<span class="fa fa-star-o"></span>
				@endfor
			</div>
		</h5>
		<p class="card-text text-muted text-justify">{{$testimonial->description}}</p>
		<div class="commenter d-flex justify-content-start">
			<figure class="w-25 mr-2">
				<img class="img-fluid rounded-circle" src="{{$testimonial->user->avatar}}"> </figure>
			<div>
				<b>{{$testimonial->user->fullName}}</b>
				<br>
				<small class="text-muted">{{$testimonial->user->profession}}</small>
			</div>
		</div>
		<i class="fa fa-quote-right"></i>
	</div>
</div>