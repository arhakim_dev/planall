
<div class="row section-separator" style="{{$style or ''}}">
@if(!empty($url))
	<div class="col-12 text-center" id="{{$id}}">
		<div class="mr-3">
			<a class="btn rounded-circle shadow btn-{{$variant or 'secondary'}} btn-action" href="{{$url}}" id="{{$idURL}}" {{(@$loader)? 'data-loader='.$loader : ''}} title="see more">
				<i class="fa fa-angle-down fa-2x"></i>
			</a>
		</div>
		<div class="collapse-to" style="display:none;">
			<div class="btn rounded-circle cursor-pointer shadow btn-{{$variant or 'secondary'}}" data-collapse-to="{{@$collapseTo}}" data-items="{{@$collapseItems}}" title="Collapse">
				<i class="fa fa-angle-up fa-2x"></i>
			</div>
		</div>
	</div>
@endif
</div>