<div class="card shadow bg-gradient mt-1 {{ $post->category->slug }}" id="post-{{ $post->id }}">
	@if(@$removable)
		@if(Auth::check() && Auth::user()->id == $post->user_id)
	<form method="delete" action="profile/post/remove/{{ $post->id }}">
		<div class="btn btn-secondary btn-float-action rounded-circle position-absolute" onclick="confirm(this.title+'?')?$(this).parent('form').submit():''" title="Remove {{title_case($post->title)}}">
			<i class="fa fa-close"></i>
		</div>
	</form>
		@endif
	@endif
	{{-- <img class="card-img-top" src="{{$post->hero_image}}" alt="{{title_case($post->title)}}"> --}}
	<div class="card-body text-white text-center">
		<span class="post-meta">{{date('F d - H:i', strtotime($post->created_at))}}</span>
		<h4 class="card-title font-weight-bold"><a href="{{$post->url}}" class="text-white">{{title_case($post->title)}}</a></h4>
		<p class="text-center">
			<i class="fa fa-eye"> {{$post->views}}</i>
		</p>
	</div>
</div>