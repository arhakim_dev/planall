<div class="card rounded mb-5 shadow">
	<div class="card-body">
		<div class="d-flex justify-content-between mb-3">
			@include('_partials/star-rating', ['rating' => $review->rate, 'variant' => 'warning'])
			<small class="text-muted">{{$review->created_at->format('F d, Y - H:ia')}}</small>
		</div>
		<p class="card-text">{{$review->description}}</p>
		<div class="commenter font-weight-bold text-secondary">
			{{$review->name}}
		</div>
		<i class="fa fa-quote-right"></i>
	</div>
</div>