 <li class="list-group-item d-flex py-3 mb-3 shadow rounded align-items-center" id="friend-{{$friend->id}}">
	@if($friend->user != '')
	<div>
		<figure class="d-inline-block mr-1 w-25 w-md-15 w-xl-10 rounded-circle border">
			<img class="img-fluid rounded-circle" src="{{$friend->user->avatar }}"> </figure>
		<a href="{{$friend->user->url}}" class="fs-4"> {{$friend->user->fullName}} </a>
	</div>
	<div class="ml-auto cursor-pointer">
	@if(isset($removable) && $removable == true)
		@if(Auth::check() && Auth::user()->id == $friend->user_id)
		<form method="delete" action="profile/friend/remove/{{$friend->id}}">
			<img src="{{ asset('img/icon/minus-gradient.png') }}" onclick="confirm(this.title+'?')?$(this).parent('form').submit():''" title="Remove {{$friend->user->fullName}}">
		</form>
		@endif
	@endif
	</div>
	@else
	<div>
		<figure class="d-inline-block mr-1 w-25 w-md-15 w-xl-10 rounded-circle border">
			<img class="img-fluid rounded-circle" src="{{ asset('img/blank-image.jpg') }}"> </figure>
		<a href="javascript:void(0)" class="fs-1 text-muted"> User not exist or The account has been deactivated </a>
	</div>
	<div class="ml-auto cursor-pointer">
	@if(isset($removable) && $removable == true)
		@if(Auth::check() && Auth::user()->id == $friend->user_id)
		<form method="delete" action="profile/friend/remove/{{$friend->id}}">
			<img src="{{ asset('img/icon/minus-gradient.png') }}" onclick="confirm(this.title+'?')?$(this).parent('form').submit():''" title="Remove">
		</form>
		@endif
	@endif
	</div>
	@endif
</li>