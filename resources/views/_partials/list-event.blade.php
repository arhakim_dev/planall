
<div href="#" class="list-group-item text-white bg-gradient rounded" id="event-{{ $event->id }}">
	@if(isset($removable) && $removable == true)
		@if(Auth::check() && Auth::user()->id == $user->id)
	<form method="delete" action="profile/event/remove/{{ $event->id }}">
		<div class="btn btn-secondary btn-float-action rounded-circle position-absolute" onclick="confirm(this.title+'?')?$(this).parent('form').submit():''" title="Remove {{$event->title}}">
			<i class="fa fa-close"></i>
		</div>
	</form>
		@endif
	@endif
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2 fs-4 lh-5 font-weight-bold text-md-center" style="word-break: break-word;">{{date('d M y', strtotime($event->start_date))}}</div>
			<div class="col-md-9 pl-3">
				<div class="font-weight-bold fs-4">{{$event->title}}</div>
				<div class="fs-2">
					<i class="fa fa-clock-o"></i> {{date('H:ia', strtotime($event->start_time))}} - {{date('H:ia', strtotime($event->end_time))}}</div>
				<div class="fs-2">
					<i class="fa fa-map-marker"></i> {{$event->address}}</div>
			</div>
			<div class="col-md-1 d-flex flex-column justify-content-center align-items-center pt-2">
				<a href="{{$event->url}}" class="btn btn-light btn-outline-light rounded-circle">
					<i class="fa fa-angle-right"></i>
				</a>
			</div>
		</div>
	</div>
</div>