<div class="rating  text-{{$variant or 'secondary'}}">
	@for($i = 1; $i <= $rating; $i++)
	<span class="fa fa-star checked"></span>
	@endfor
	@for($i = 1; $i <= (5 - $rating); $i++)
	<span class="fa fa-star-o"></span>
	@endfor
</div>