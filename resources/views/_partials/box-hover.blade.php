<div class="option-box">
	<div class="box-img-popup shadow transition" style="background-image: url({{$image or 'img/microscope-385364_1280.png'}});"></div>
	<div class="box-inner rounded bg-gradient">
		<div class="box-title">
			<a href="{{$url or '#'}}" class="text-white" style="text-decoration:none;">
				<span class="">{{$title}}</span>
			</a>
	</div>
	</div>
</div>