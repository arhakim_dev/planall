<div class="col-md-6">
	<div class="card mb-3" style="height: 110px;">
		<div class="card-body d-flex justify-content-start align-items-center p-1">
			<figure class="d-inline-block mr-1 w-25 w-md-15 w-xl-10 ">
				<img class="img-fluid rounded-circle" src="{{$avatar}}"> </figure>
			<div style="max-width:88%;">
				<div>
					<strong class="text-secondary">{{$name}}</strong> {!!$message!!}. 
				</div>
				<i class="text-muted fs-1">{{$timeHuman}}</i>
			</div>
		</div>
	</div>
</div>