@extends('page', ['body_class' => '', 'footerStyle' => ''])

@section('content')

<div id="fb-root"></div>
<script>
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId={{env('FACEBOOK_ID')}}&autoLogAppEvents=1";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
<div class="hero-area h-50 h-md-75 position-relative overlay" style="top: 80px; background-image: url({{$university->hero_image}}); background-size: cover; background-repeat: no-repeat;background-position:center center;"> 
	@if($userHasAccess)
	<div class="position-absolute text-center w-100 file-container" style="top:35%;">
		<button type="button" class="btn btn-outline-light position-relative">Change Image <i class="fa fa-image"></i>
			<input type="file" name="file" accept="image/*" class="file-uploader" data-action="university/upload/hero"/>
		</button>
		<div class="upload-progress"></div>
		<form action="university/{{$university->id}}/update" method="put" id="form-hero-image" class="mt-2" style="display:none;">
			<p>
				<small class="text-white" id="hero-image-file">File</small>&nbsp;
				<i class="fa fa-remove text-danger remove-file cursor-pointer" onclick="_removeFileUploaded(this)" data-action="university/file/remove" data-file=""></i>&nbsp;
				<input type="hidden" name="hero_image">
			</p>
			<button type="submit" class="btn btn-sm btn-secondary">Save</button>
		</form>
	</div>
	@endif
</div>
<div class="position-relative" style="top:80px;">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<figure class="shadow rounded-circle p-1 bg-white mt-6-offset mx-auto position-relative" style="height:250px;width:250px;">
					<img src="{{$university->logo}}" class="img-fluid rounded-circle w-100" id="logo"> 
					@if($userHasAccess)
					<button type="button" class="btn btn-secondary rounded-circle position-absolute" style="top:50px;right:0;" data-toggle="modal" data-target="#modal-logo">
							<i class="fa fa-pencil"></i>
					</button>
					@endif
				</figure>
			</div>
		</div>
	</div>
</div>
<div class="py-5">
	<div class="container">
		<div class="row mb-3">
			<div class="col-12">
				<form action="university/{{$university->id}}/update" method="put">
				@if($userHasAccess)
					<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
				@endif
					<input type="text" name="name" class="cover-flat text-secondary mb-1 form-control fs-8 text-center" value="{{$university->name}}" disabled style="line-height:1;">
					<textarea name="address" class="cover-flat fs-4 font-weight-bold w-100 form-control text-center text-secondary" rows="1" disabled style="line-height:1;">{{$university->fullAddress}}</textarea>
				</form>
				<div class="text-center text-secondary">
					<div class="row w-md-50 mx-auto">
						<div class="col-md-4">
							<br>
							<span>{{$university->views}} views</span>
						</div>
						<div class="col-md-4">
								@include('_partials/star-rating', ['rating' => $rating])
						
								<span>{{$university->reviews->count()}} reviews</span>
						</div>
						<div class="col-md-4">
							<br>
							<span>{{$shortList}} shortlists</span>
						</div>
					</div>
					@if(!$isShortlisted && !$userHasAccess)
						@auth
					<form method="post" action="user/university/shortlist" id="form-university-shortlist" class="mb-2">
						<input type="hidden" name="university_id" value="{{$university->id}}">
						<button class="btn btn-lg bg-white shadow mt-2 text-secondary font-weight-bold" type="submit">Shortlist</button>
					</form>
					@else
					<a href="#modal-login" class="btn btn-lg bg-white shadow mt-2 text-secondary font-weight-bold" data-toggle="modal">Shortlist</a>
					@endauth
					@endif
				</div>
				<nav class="navbar navbar-expand-lg pt-1">
					<button class="navbar-toggler mx-auto text-secondary " data-toggle="collapse" data-target="#sub-menu">
						<span class="fa fa-bars"></span>
					</button>
					
					<div class="collapse navbar-collapse text-center" id="sub-menu">
						<ul class="navbar-nav mx-auto shadow">
							<li class="nav-item bg-white">
								<a class="nav-link scroll2 active" href="#basic">Basic</a>
							</li>
							<li class="nav-item bg-white">
								<a class="nav-link scroll2" href="#admission">Admissions</a>
							</li>
							<li class="nav-item bg-white">
								<a class="nav-link scroll2" href="#costs">Costs</a>
							</li>
							<li class="nav-item bg-white">
								<a class="nav-link scroll2" href="#rankings">Rankings</a>
							</li>
							<li class="nav-item bg-white">
								<a class="nav-link scroll2" href="#aftercollege">After College</a>
							</li>
							<li class="nav-item bg-white">
								<a class="nav-link scroll2" href="#facilities">Facilities</a>
							</li>
							<li class="nav-item bg-white">
								<a class="nav-link scroll2" href="#review">Review & Discussions</a>
							</li>
						
						</ul>
					</div>
				</nav>
			</div>
		</div>
		<div class="row mb-3" id="basic">
			<div class="col-12">
				@if($showDescription)
				<div class="card shadow mb-5">
					<div class="card-header bg-white d-flex justify-content-between align-items-center" data-toggle="collapse" data-target="#collapse-about" aria-expanded="false" aria-controls="collapse-about">
						<h4 class="title">About</h4>
						<span class="fa fa-angle-up"></span>
					</div>
					<div class="card-body collapse show" id="collapse-about">
						<form action="university/{{$university->id}}/update" method="put">
							@if($userHasAccess)
								<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
							@endif
							<div class="form-group">
								<textarea class="form-control cover-flat text-justify" rows="4" disabled name="description">{{$university->description or 'Data not available'}}</textarea>
							</div>
						</form>
					</div>
				</div>
				@endif
				<div class="row">
					<div class="col-md-4">
						<div class="card shadow mb-5">
							<div class="card-body">
								<form action="university/{{$university->id}}/update" method="put" class="mb-0">
								@if($userHasAccess)
									<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
								@endif
								<div class="row">
									<div class="col-md-2">
										<img class="mx-auto" src="img/icon/lock.png" height="40"> </div>
									<div class="col-md-10">
										<select class="cover-flat text-center w-100" disabled name="is_public" style="padding-left:46%;">
											<option value="0" {{($university->is_public)? '' : 'selected'}}>Private</option>
											<option value="1" {{($university->is_public)? 'selected' : ''}}>Public</option>
										</select>
									</div>
								</div>
								</form>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="card shadow mb-5">
							<div class="card-body">
								<form action="university/{{$university->id}}/update" method="put" class="mb-0">
								@if($userHasAccess)
									<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
								@endif
								<div class="row">
									<div class="col-md-2">
										<img class="mx-auto" src="img/icon/museum.png" height="40"> </div>
									<div class="col-md-10">
										<input type="number" min="1000" max="{{date('Y')}}" maxlength="4" name="founding_year" value="{{$university->founding_year}}" class="form-control cover-flat text-center no-arrows" disabled oninput="this.value=this.value.slice(0,this.maxLength)">
									</div>
								</div>
								</form>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="card shadow mb-5">
							<div class="card-body">
								<form action="university/{{$university->id}}/update" method="put" class="mb-0">
								@if($userHasAccess)
									<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
								@endif
									<div class="row">
										<div class="col-md-2">
											<img class="mx-auto" src="img/icon/internet.png" height="40"> </div>
										<div class="col-md-10">
											<input type="url" class="form-control cover-flat text-secondary clickable text-center" data-validation="url" data-validation-optional="true" data-validation-error-msg="Incorrect URL format. ex: https://website.com" readonly name="website" value="{{$university->website}}">
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<hr> </div>
		</div>
		<div class="row mb-3">
			<div class="col-12">
				<h2 class="section-title">Majors</h2>
				<div class="card shadow mb-5">
					<div class="card-header d-flex justify-content-between align-items-center bg-white" data-toggle="collapse" data-target="#subject-undergraduate" aria-expanded="false" aria-controls="subject-undergraduate">
						<h4 class="title">Undergraduate</h4>
						<span class="fa fa-angle-down"></span>
					</div>
					<div class="card-body collapse show" id="subject-undergraduate" {{-- style="max-height: 400px; overflow: auto; padding-bottom: 20px;" --}}>
						<form action="university/{{$university->id}}/update" method="put">
							@if($userHasAccess)
							<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
							@endif
							<select name="subjects[undergraduate][]" class="cover-flat" multiple disabled data-selectize-options='{"plugins": ["remove_button"]}'>
								@foreach($subjects['undergraduate'] as $_subject)
								@if($loop->first && empty($subject['undergraduate']) && !$userHasAccess)
								<option class="placeholder" value="" selected>Data Not Available</option>
								@endif
									<option value="{{$_subject->id}}" {{in_array($_subject->id, $subject['undergraduate'])? 'selected' : ''}}>{{$_subject->name}}</option>
								@endforeach
							</select>
						</form>
					</div>
				</div>
				<div class="card shadow mb-5">
					<div class="card-header d-flex justify-content-between align-items-center bg-white" data-toggle="collapse" data-target="#subject-ms" aria-expanded="false" aria-controls="subject-ms">
						<h4 class="title">MS</h4>
						<span class="fa fa-angle-down"></span>
					</div>
					<div class="card-body collapse show" id="subject-ms">
						<form action="university/{{$university->id}}/update" method="put">
							@if($userHasAccess)
							<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
							@endif
							<select name="subjects[ms][]" class="cover-flat" multiple disabled data-selectize-options='{"plugins": ["remove_button"]}'>
								@foreach($subjects['ms'] as $_subject)
								@if($loop->first && empty($subject['ms']) && !$userHasAccess)
								<option class="placeholder" value="" selected>Data Not Available</option>
								@endif
									<option value="{{$_subject->id}}" {{in_array($_subject->id, $subject['ms'])? 'selected' : ''}}>{{$_subject->name}}</option>
								@endforeach
							</select>
							</form>
					</div>
				</div>
				<div class="card shadow mb-5">
					<div class="card-header d-flex justify-content-between align-items-center bg-white" data-toggle="collapse" data-target="#subject-mba" aria-expanded="false" aria-controls="subject-mba">
						<h4 class="title">MBA</h4>
						<span class="fa fa-angle-up"></span>
					</div>
					<div class="card-body collapse show" id="subject-mba">
						<form action="university/{{$university->id}}/update" method="put">
							@if($userHasAccess)
							<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
							@endif
							<select name="subjects[mba][]" class="cover-flat" multiple disabled data-selectize-options='{"plugins": ["remove_button"]}'>
								@foreach($subjects['mba'] as $_subject)
									@if($loop->first && empty($subject['mba']) && !$userHasAccess)
									<option class="placeholder" value="" selected>Data Not Available</option>
									@endif
									<option value="{{$_subject->id}}" {{in_array($_subject->id, $subject['mba'])? 'selected' : ''}}>{{$_subject->name}}</option>
								@endforeach
							</select>
							</form>
					</div>
				</div>
				<hr> 
				
			</div>
		</div>
		<div class="row mb-3">
			<div class="col-12">
				<div class="card shadow mb-5" id="admission">
					<div class="card-header d-flex justify-content-between align-items-center bg-white" data-toggle="collapse" data-target="#collapse-admision" aria-expanded="false" aria-controls="collapse-admision">
						<h4 class="title">Admissions</h4>
						<span class="fa fa-angle-up"></span>
					</div>
					<div class="card-body collapse show container text-center" id="collapse-admision">
						<form action="university/{{$university->id}}/update" method="put">
							@if($userHasAccess)
							<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
							@endif
						@if($university->programAdmissions()->count())
						<table class="table table-bordered table-responsive">
							<thead>
								<tr>
									<th width="300"></th>
									<th width="300">Under Graduation</th>
									<th width="300">Graduation (Except MBA)</th>
									<th width="300">Graduation (MBA)</th>
								</tr>
							</thead>
							<tbody>
							@foreach($university->programAdmissions()->get() as $program)
								@if($program->id == 10)
								<tr>
									<td colspan="4" class="font-weight-bold text-center">English Proficiency Tests</td>
								</tr>
								@endif
								<tr>
									<th scope="row">{{$program->name}}</th>
									@if($userHasAccess)
										@if($program->id == 3)
										<td><textarea name="programs[{{$program->id}}][undergraduate]" class="form-control cover-flat" disabled>{{$program->pivot->undergraduate}}</textarea></td>
										<td><textarea name="programs[{{$program->id}}][ms]" class="form-control cover-flat" disabled>{{$program->pivot->ms}}</textarea></td>
										<td><textarea name="programs[{{$program->id}}][mba]" class="form-control cover-flat" disabled>{{$program->pivot->mba}}</textarea></td>
										@else
									<td><input type="text" name="programs[{{$program->id}}][undergraduate]" value="{{$program->pivot->undergraduate}}" class="form-control cover-flat" disabled></td>
									<td><input type="text" name="programs[{{$program->id}}][ms]" value="{{$program->pivot->ms}}" class="form-control cover-flat" disabled></td>
									<td><input type="text" name="programs[{{$program->id}}][mba]" value="{{$program->pivot->mba}}" class="form-control cover-flat" disabled></td>
										@endif
									@else
									<td>{{!empty($program->pivot->undergraduate)? $program->pivot->undergraduate : 'Data not available'}}</td>
									<td>{{!empty($program->pivot->ms)? $program->pivot->ms : 'Data not available'}}</td>
									<td>{{!empty($program->pivot->mba)? $program->pivot->mba : 'Data not available'}}</td>
									@endif
								</tr>
							@endforeach
							</tbody>
						</table>
						@else
						<div class="text-left">Data not available</div>
						@endif
						</form>
					</div>
				</div>				
				<div class="card shadow  mb-5" id="costs">
					<div class="card-header d-flex justify-content-between align-items-center bg-white" data-toggle="collapse" data-target="#collapse-attendance" aria-expanded="false" aria-controls="collapse-attendance">
						<h4 class="title">Cost of Attendance</h4>
						<span class="fa fa-angle-up"></span>
					</div>
					<div class="card-body collapse show container text-center" id="collapse-attendance">
						<form action="university/{{$university->id}}/update" method="put">
							@if($userHasAccess)
							<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
							@endif
						@if($university->programCosts()->count())
						<table class="table table-bordered table-responsive">
							<thead>
								<tr>
									<th width="300"></th>
									<th width="300">Under Graduation</th>
									<th width="300">Graduation (Except MBA)</th>
									<th width="300">Graduation (MBA)</th>
								</tr>
							</thead>
							<tbody>
								@foreach($university->programCosts()->get() as $program)
								<tr>
									<th scope="row">{{$program->name}}</th>
									@if($userHasAccess)
										@if($program->id == 14)
										<td><textarea name="programs[{{$program->id}}][undergraduate]" class="form-control cover-flat" disabled>{{$program->pivot->undergraduate}}</textarea></td>
										<td><textarea name="programs[{{$program->id}}][ms]" class="form-control cover-flat" disabled>{{$program->pivot->ms}}</textarea></td>
										<td><textarea name="programs[{{$program->id}}][mba]" class="form-control cover-flat" disabled>{{$program->pivot->mba}}</textarea></td>
										@else
										<td><input type="text" name="programs[{{$program->id}}][undergraduate]" value="{{$program->pivot->undergraduate}}" class="form-control cover-flat" disabled></td>
										<td><input type="text" name="programs[{{$program->id}}][ms]" value="{{$program->pivot->ms}}" class="form-control cover-flat" disabled></td>
										<td><input type="text" name="programs[{{$program->id}}][mba]" value="{{$program->pivot->mba}}" class="form-control cover-flat" disabled></td>
										@endif
									@else
									<td>{{!empty($program->pivot->undergraduate)? $program->pivot->undergraduate : 'Data not available'}}</td>
									<td>{{!empty($program->pivot->ms)? $program->pivot->ms : 'Data not available'}}</td>
									<td>{{!empty($program->pivot->mba)? $program->pivot->mba : 'Data not available'}}</td>
									@endif
								</tr>
								@endforeach
							</tbody>
						</table>
						@else
						<div class="text-left">Data not available</div>
						@endif
					</form>
					</div>
				</div>

				<div id="rankings" class="position-relative mb-5">
					<form action="university/{{$university->id}}/update" method="put">
						@if($userHasAccess)
						<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"  style="z-index:1;"><i class="fa fa-pencil"></i></div>
						@endif
						<div class="row">
						@if($university->programRankings()->count())
							@foreach($university->programRankings()->get() as $program)
							<div class="col-md-4">
								<div class="card shadow">
									<div class="card-header d-flex justify-content-between align-items-center bg-white">
										<h4 class="title fs-2">{{$program->name}}</h4>
									</div>
									<div class="card-body">
										<div class="form-group">
											<input type="text" name="programs[{{$program->id}}][undergraduate]" class="form-control cover-flat text-left" disabled value="{{!empty($program->pivot->undergraduate)?$program->pivot->undergraduate : 'Data not available'}}">
										</div>
									</div>
								</div>
							</div>
							@endforeach
						@endif
					</div>
					</form>
				</div>
				<hr> 
			</div>
		</div>
		<div class="row mb-3" id="aftercollege">
			<div class="col-12">
				<h2 class="section-title">After College</h2>
				<div class="row">
					<div class="col-md-6">
						<div class="card shadow">
							<div class="card-header d-flex justify-content-between align-items-center bg-white" data-toggle="collapse" data-target="#collapse-earning" aria-expanded="false" aria-controls="collapse-earning">
								<h4 class="title">Earnings after Graduation</h4>
								<span class="fa fa-angle-up"></span>
							</div>
							<div class="card-body collapse show" id="collapse-earning">
								<form action="university/{{$university->id}}/update" method="put">
									@if($userHasAccess)
									<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
									@endif
									<input type="text" name="after_college[earning]" class="form-control cover-flat text-secondary text-left" disabled value="{{($afterCollege)?$afterCollege->earning : 'Data not available'}}" style="line-height:1;">
								</form>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="card shadow mb-5">
							<div class="card-header d-flex justify-content-between align-items-center bg-white" data-toggle="collapse" data-target="#collapse-placement" aria-expanded="false" aria-controls="collapse-placement">
								<h4 class="title">Placement Rate</h4>
								<span class="fa fa-angle-up"></span>
							</div>
							<div class="card-body collapse show text-center" id="collapse-placement">
								<form action="university/{{$university->id}}/update" method="put">
									@if($userHasAccess)
									<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
									@endif
								<p class="mb-3 box-placeholder {{($afterCollege->placement_rate)? 'd-none' : 'd-block'}}" data-class-enabled="{{($afterCollege->placement_rate)? '' : 'd-none'}}">Data Not Available</p>
								<div class="pie-chart mx-auto position-relative py-5 {{($afterCollege->placement_rate)? 'd-block' : 'd-none'}}" data-percent="{{intval(@$afterCollege->placement_rate)}}" style="width:200px;height:200px;" data-class-enabled="{{($afterCollege->placement_rate)? '' : 'd-block'}}">
									<div class="d-flex justify-content-center align-items-center text-secondary font-weight-bold">
										<input type="number" name="after_college[placement_rate]" class="cover-flat text-secondary text-center no-arrows font-weight-bold" min="0" max="100" maxLength="3" disabled value="{{intval(@$afterCollege->placement_rate)}}" style="width:35px;z-index:1;" oninput="this.value=this.value.slice(0,this.maxLength)"><span>%</span>
									</div>
									<p> Placement rate </p>
								</div>
							</form>
							</div>
						</div>
					</div>
				</div>
				<div class="card shadow mb-5">
					<div class="card-header d-flex justify-content-between align-items-center bg-white" data-toggle="collapse" data-target="#collapse-employees" aria-expanded="false" aria-controls="collapse-employees">
						<h4 class="title">Top Employers</h4>
						<span class="fa fa-angle-up"></span>
					</div>
					<div class="card-body collapse show" id="collapse-employees">
						<form action="university/{{$university->id}}/update" method="put">
							@if($userHasAccess)
							<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
							
							{{-- <input type="text" name="after_college[top_employees]" class="form-control cover-flat text-secondary text-left" disabled value="{{isset($afterCollege)? $afterCollege->top_employees : ''}}" placeholder="Separated with comma" title="Separated with comma"> --}}
							@endif
							@if($afterCollege)
							<select name="after_college[top_employees][]" class="cover-flat" disabled multiple data-selectize-options='{"create": true, "plugins": ["remove_button"]}'>
								@foreach(explode(',', $afterCollege->top_employees) as $top)
								<option value="{{$top}}" selected>{{$top}}</option>
								@endforeach
							</select>
							<small class="text-muted text-helper" style="display:none;">* Type the top employers and separate them with comma</small>
							@else
							Data not available
							@endif
						</form>
					</div>
				</div>
				<hr> 
			</div>
		</div>
		<div class="row mb-3">
			<div class="col-12">
				<div class="card shadow mb-5" id="students">
					<div class="card-header d-flex justify-content-between align-items-center bg-white" data-toggle="collapse" data-target="#collapse-students" aria-expanded="false" aria-controls="collapse-students">
						<h4 class="title">Student Statistics</h4>
						<span class="fa fa-angle-up"></span>
					</div>
					<div class="card-body collapse show container text-center" id="collapse-students">
						<form action="university/{{$university->id}}/update" method="put">
							@if($userHasAccess)
							<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
							@endif
						@if($university->programStudents()->count())
						<div class="row mb-md-5">
							<div class="col-md-6 mb-3">
								<div class="rounded-circle py-5 bg-secondary w-75 w-sm-50 w-md-75 w-lg-50 mx-auto text-white">
									{{-- <p class="font-weight-bold fs-5 fs-xs-4">
										{{intval($university->programNumberStudent()->undergraduate)}}</p> --}}
									<input type="text" name="programs[{{$university->programNumberStudent()->id}}][undergraduate]" class="cover-flat text-white text-center font-weight-bold fs-5 fs-xs-4 w-100" disabled value="{{$university->programNumberStudent()->pivot->undergraduate}}" data-class-enabled="text-secondary">
									<div class="fs-3 mb-md-3">{{$university->programNumberStudent()->name}}</div>
								</div>
							</div>
							<div class="col-md-6 mb-3">
								<div class="rounded-circle py-5 bg-secondary w-75 w-sm-50 w-md-75 w-lg-50 mx-auto text-white">
									{{-- <p class="font-weight-bold fs-5 fs-xs-4">{{intval($university->programNumberInternationalStudent()->undergraduate)}}%</p> --}}
										<input type="text" name="programs[{{$university->programNumberInternationalStudent()->id}}][undergraduate]" class="cover-flat text-white text-center font-weight-bold fs-5 fs-xs-4 w-100" disabled value="{{$university->programNumberInternationalStudent()->pivot->undergraduate}}" data-class-enabled="text-secondary">
									<div class="fs-3 mb-sm-4 mb-md-3">{{$university->programNumberInternationalStudent()->name}}</div>
								</div>
							</div>
						</div>
						<div class="row text-secondary font-weight-bold">
							<div class="col-md-6" id="female">
									<input type="text" name="programs[{{$university->programFemaleRatio()->id}}][undergraduate]" class="cover-flat text-secondary text-center fs-7 w-25 font-weight-bold" disabled value="{{@$university->programFemaleRatio()->pivot->undergraduate}}">
							</div>
							<div class="col-md-6" id="male">
									<input type="text" name="programs[{{$university->programMaleRatio()->id}}][undergraduate]" class="cover-flat text-secondary text-center fs-7 w-25 font-weight-bold" disabled value="{{@$university->programMaleRatio()->pivot->undergraduate}}">
							</div>
						</div>
						@else
						<div class="text-left">Data not available</div>
						@endif
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row mb-3">
			<div class="col-12">
				<h2 class="section-title">Alumni Information</h2>
				<div class="card shadow mb-5">
					<div class="card-header d-flex justify-content-between align-items-center bg-white" data-toggle="collapse" data-target="#collapse-famous" aria-expanded="false" aria-controls="collapse-famous">
						<h4 class="title">Famous Alumnis</h4>
						<span class="fa fa-angle-up"></span>
					</div>
					<div class="card-body collapse show container text-center" id="collapse-famous">
						<form action="university/{{$university->id}}/update" method="put">
							@if($userHasAccess)
							<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
							@endif
						<div class="row">
							@for($i = 1; $i <= 3; $i++)
								
								<div class="col-12 col-md-4">
									<figure class="mx-auto position-relative file-container" style="height:250px;width:250px;">
										<img src="{{isset($fameAlumnis[$i])?$fameAlumnis[$i]['photo'] : 'img/blank-image.jpg' }}" class="img-fluid rounded-circle alumni-{{$i}} w-100 d-none {{isset($fameAlumnis[$i])? 'd-block' : '' }}" data-class-enabled="d-block"> 
										@if($userHasAccess)
										<button type="button" class="btn btn-secondary btn-upload rounded-circle position-absolute d-none" style="top:50px;right:0;" data-class-enabled="d-block">
											<input type="file" name="file" accept="image/*" class="file-uploader" data-action="university/upload/alumni-{{$i}}"/>
												<i class="fa fa-pencil"></i>
										</button>
										<button type="button" class="btn btn-danger btn-remove-alumni rounded-circle position-absolute alumni-{{$i}} d-none" style="top:85px;right:0;" data-class-enabled="{{isset($fameAlumnis[$i])? 'd-block' :'' }}" data-action="university/{{$university->id}}/alumni/{{$i}}">
												<i class="fa fa-remove"></i>
										</button>
										<div class="upload-progress"></div>
										@endif										
										<input type="hidden" name="alumni[{{$i}}][photo]" value="{{isset($fameAlumnis[$i])?$fameAlumnis[$i]['photo']:''}}" class="alumni-{{$i}}">
									</figure>
									<input type="text" class="form-control cover-flat font-weight-bold text-center alumni-{{$i}} d-none {{isset($fameAlumnis[$i])? 'd-block' :'' }}" disabled name="alumni[{{$i}}][name]" value="{{isset($fameAlumnis[$i])?$fameAlumnis[$i]['name']:''}}" placeholder="Name" data-class-enabled="d-block">
								</div>
							@endfor
						</div>
						</form>
					</div>
				</div>
				<div class="card shadow mb-5">
					<div class="card-header d-flex justify-content-between align-items-center bg-white" data-toggle="collapse" data-target="#collapse-group" aria-expanded="false" aria-controls="collapse-group">
						<h4 class="title">Alumni Group</h4>
						<span class="fa fa-angle-up"></span>
					</div>
					<div class="card-body collapse show" id="collapse-group">
						<form action="university/{{$university->id}}/update" method="put">
							@if($userHasAccess)
							<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
							@endif
							@isset($university->alumni)
							<input type="url" class="form-control cover-flat text-secondary clickable text-left" data-validation="url" data-validation-optional="true" data-validation-error-msg="Incorrect URL format. ex: https://website.com" readonly name="alumni[website]" value="{{$university->alumni->website}}">
							</p>
							@else
							Data not available
							@endisset
						</form>
					</div>
				</div>
				<hr> </div>
		</div>
		<div class="row mb-3" id="facilities">
			<div class="col-md-12">
				<h2 class="section-title">Facilities</h2>
				<div class="card shadow mb-5">
					<div class="card-header d-flex justify-content-between align-items-center bg-white" data-toggle="collapse" data-target="#collapse-facilities" aria-expanded="false" aria-controls="collapse-facilities">
						<h4 class="title">Infrastructure & General Facilities</h4>
						<span class="fa fa-angle-up"></span>
					</div>
					<div class="card-body collapse show" id="collapse-facilities">
						<form action="university/{{$university->id}}/update" method="put">
							@if($userHasAccess)
							<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
							@endif
							@isset($university->facility)
							<textarea class="form-control cover-flat text-justify card-text" name="facility[general]" disabled>{{$university->facility->general}}</textarea>
							@else
							Data not available
							@endisset
						</form>
					</div>
				</div>
				<hr> </div>
		</div>
		<div class="row mb-3">
			<div class="col-12">
				<h2 class="section-title">Accomodation</h2>
				<div class="card shadow mb-5">
					<div class="card-header d-flex justify-content-between align-items-center bg-white" data-toggle="collapse" data-target="#collapse-prov-facilities" aria-expanded="false" aria-controls="collapse-prov-facilities">
						<h4 class="title">Provided by the University</h4>
						<span class="fa fa-angle-down"></span>
					</div>
					<div class="card-body collapse show" id="collapse-prov-facilities">
						<form action="university/{{$university->id}}/update" method="put">
							@if($userHasAccess)
							<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
							@endif
							@isset($university->facility)
							<textarea class="form-control cover-flat text-justify card-text" name="facility[accomodation]" disabled>{{$university->facility->accomodation}}</textarea>
							@else
								Data not available
							@endisset
						</form>
					</div>
				</div>
				<div class="card shadow mb-5">
					<div class="card-header d-flex justify-content-between align-items-center bg-white" data-toggle="collapse" data-target="#collapse-near-univ" aria-expanded="false" aria-controls="collapse-near-univ">
						<h4 class="title">Near the University</h4>
						<span class="fa fa-angle-up"></span>
					</div>
					<div class="card-body collapse show" id="collapse-near-univ">
						<div id="google-map">
							<iframe frameborder="0" style="border:0; width:100%;height:500px;" src="https://www.google.com/maps/embed/v1/place?key={{env('GOOGLE_MAP_KEY')}}&q={{$university->address}}" allowfullscreen=""></iframe>
						</div>
						{{-- <hr>
						<div id="map"></div> --}}
					</div>
				</div>
				<hr> </div>
		</div>
		<div class="row mb-3">
			<div class="col-12">
				<h2 class="section-title">Social Media</h2>
				<div class="card shadow mb-5">
					<div class="card-body text-center">
						<form action="university/{{$university->id}}/update" method="put">
							@if($userHasAccess)
							<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
							@endif
							<div class="row">
								<div class="col-6 col-md-3">									
									@if($university->facebook)
									<a href="{{$university->facebook}}" target="_blank">
										<i class="fa fa-fw fa-facebook fa-3x mx-md-3" style="color: #3b5997;"></i>
									</a>
									@endif
									<input type="url" class="form-control cover-flat text-secondary clickable text-center d-none" data-validation="url" data-validation-optional="true" data-validation-error-msg="Incorrect URL format. ex: https://facebook.com" readonly name="facebook" value="{{$university->facebook}}" data-class-enabled="d-block">
								</div>
								<div class="col-6 col-md-3">
									@if($university->twitter)
									<a href="{{$university->twitter}}" target="_blank">
										<i class="fa fa-fw fa-twitter fa-3x mx-md-3" style="color: #29c7f7;"></i>
									</a>
									@endif
									<input type="url" class="form-control cover-flat text-secondary clickable text-center d-none" data-validation="url" data-validation-optional="true" data-validation-error-msg="Incorrect URL format. ex: https://twitter.com" readonly name="twitter" value="{{$university->twitter}}" data-class-enabled="d-block">
								</div>
								<div class="col-6 col-md-3">
									@if($university->linkedin)
									<a href="{{$university->linkedin}}" target="_blank">
										<i class="fa fa-fw fa-linkedin fa-3x mx-md-3" style="color: #036cbd;"></i>
									</a>
									@endif
									<input type="url" class="form-control cover-flat text-secondary clickable text-center d-none" data-validation="url" data-validation-optional="true" data-validation-error-msg="Incorrect URL format. ex: https://linkedin.com" readonly name="linkedin" value="{{$university->linkedin}}" data-class-enabled="d-block">
								</div>
								<div class="col-6 col-md-3">
									@if($university->youtube)
									<a href="{{$university->youtube}}" target="_blank">
										<i class="fa fa-fw fa-youtube-play fa-3x mx-md-3" style="color: red;"></i>
									</a>
									@endif
									<input type="url" class="form-control cover-flat text-secondary clickable text-center d-none" data-validation="url" data-validation-optional="true" data-validation-error-msg="Incorrect URL format. ex: https://youtube.com" readonly name="youtube" value="{{$university->youtube}}" data-class-enabled="d-block">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row mb-3">
			<div class="col-12">
				<div class="card shadow mb-5">
					<div class="card-header d-flex justify-content-between bg-white align-items-center" data-toggle="collapse" data-target="#collapse-weather" aria-expanded="false" aria-controls="collapse-weather">
						<h4 class="title">Weather</h4>
						<span class="fa fa-angle-up"></span>
					</div>
					<div class="card-body collapse show container" id="collapse-weather">
						<p class="card-text font-weight-bold">{{$university->fullAddress}}</p>
						<div>Weather Averages
							<br> Overview
							<span class="text-secondary ml-3">Graphs</span>
						</div>
						<div class="chart-wrapper w-100">
							<canvas id="myChart"></canvas>
						</div>
					</div>
				</div>
				<hr> </div>
		</div>
		<div class="row" id="review">
			<div class="col-12">
				<h2 class="section-title mb-2">Reviews</h2>
				<div>
					<span class="fs-4 text-secondary">Rate {{$university->name}}</span>
					<br>
					<div class="d-flex justify-content-between aligns-item-center">
						@include('_partials/star-rating', ['rating' => 0])
						@auth
						@if(!$userHasAccess)
						<a href="#modal-review" data-toggle="modal" class="btn btn-primary btn-sm shadow">Add Review</a>
						@endif
						@endauth
					</div>
				</div>
				<div id="review-container">
					@forelse($university->reviews()->approved()->latest()->take(3)->get() as $review)
						@include('_partials/card-review', ['review' => $review])
					@empty
					Data not available
					@endforelse
				</div>
			</div>
		</div>
		@if($university->reviews->count() > 3)
			@include('_partials/row-loader', [
				'id' => 'review-loader-container', 
				'idURL' => 'next-reviews', 
				'url' => 'university/'.$university->id.'/review/next?offset=3&limit=8'
			])
		@endif
		<div class="row">
			<div class="col-12">
				<h2 class="section-title"> Feed </h2>
				<div class="row">
					@if($university->facebook)
					<div class="col-md-6">
						<div class="card shadow float-md-right mb-3 w-100 w-md-75">
							<div class="card-body text-center">
								<div class="fb-page" data-href="{{$university->facebook}}" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false">
									<blockquote cite="{{$university->facebook}}" class="fb-xfbml-parse-ignore">
										<a href="{{$university->facebook}}">{{$university->name}}</a>
									</blockquote>
								</div>
							</div>
						</div>
					</div>
					@endif
					@if($university->twitter)
					<div class="col-md-6">
						<div class="card shadow w-100 w-md-75 text-center">
							<div class="card-header pb-0 bg-white border-bottom-0">
								<div class="d-flex justify-content-start p-1" style="border: 1px solid #e9ebee;margin-top: 6px;">
									<div class="p-1" style="border: 1px solid #666;padding: 7px 6px 0;">
										<img src="{{$university->logo}}" width="35" height="40"> </div>
									<div class="pl-2">
										<a href="#">
											<b>{{$university->name}}</b>
										</a>
										<br>
										<a class="twitter-follow-button" href="{{$university->twitter}}">Follow</a>
									</div>
								</div>
							</div>
							<div class="card-body">
								<a class="twitter-timeline" data-width="350" data-height="405" href="{{$university->twitter}}" data-chrome="noheader nofooter noborders"></a>
								<script async="" src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
							</div>
						</div>
					</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-review" tabindex="-1" role="dialog" aria-labelledby="modal-review" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="mb-4 text-secondary font-weight-bold">Review {{$university->name}}</h5>
				<form method="post" action="university/review" id="form-review">
					<input type="hidden" name="university_id" value="{{$university->id}}">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<input type="text" class="form-control" name="name" value="{{Auth::check()? Auth::user()->fullName : 'Annonymous' }}" placeholder="Name" required>
							</div>
						</div>
						<div class="col-md-6 text-right">
							<select name="rate" id="review-rate" class="no-selectize">
								<option value="1" selected>1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<textarea name="description" rows="3" class="form-control" placeholder="Write your review here" required></textarea>
					</div>
					<div class="form-group text-center">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop

<div class="modal fade" id="modal-logo" tabindex="-1" role="dialog" aria-labelledby="modal-logo" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<button data-dismiss="modal" aria-label="Close" class="close position-absolute p-2 rounded-circle" style="top:0;right:0; background-color: rgba(255, 255, 255, .5);z-index:1;">
					<img class="" src="{{asset('img/icon/delete.png')}}" height="20"> </button>
				
					<h5 class="mb-4 text-secondary text-center font-weight-bold">Change Logo</h5>
					<div class="image-editor text-center">
						<div class="cropit-preview mx-auto mt-5 mb-2"></div>
						<div class="row">
							<div class="col-md-6">
								<span class="rotate-ccw"><i class="fa fa-rotate-left"></i></span>
								<span class="rotate-cw"><i class="fa fa-rotate-right"></i></span>
							</div>
							<div class="col-md-6">
								<input type="range" class="cropit-image-zoom-input">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 text-right">
								<button class="btn btn-outline-secondary btn-sm position-relative w-75">
									Choose Image <i class="fa fa-image"></i>
									<input type="file" class="cropit-image-input">
								</button>
							</div>
							<div class="col-md-6">
								<form action="university/{{$university->id}}/update" method="put" id="form-logo">
									<input type="hidden" name="logo">
									<button class="btn btn-secondary btn-sm export w-75" type="submit">Save</button>
								</form>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</div>

@push('styles')
<link rel="stylesheet" href="{{asset('vendor/jquery-bar-rating/dist/themes/fontawesome-stars.css')}}">
<style>
div#sub-menu a {
    padding-left: 1rem;
    padding-right: 1rem;
}
div#sub-menu a.active {
    background-color: #667eea;
    color: white;
}
</style>
@endpush
@push('scripts')
<script src="vendor/easypiechart/jquery.easypiechart.min.js"></script>
<script src="vendor/chart.js/dist/Chart.min.js"></script>
<script src="vendor/jquery-bar-rating/jquery.barrating.js"></script>
<script src="{{ asset('vendor/cropit/dist/jquery.cropit.js') }}"></script>
<script>
	let $placementRateChart;

	$(function(){
		$.validate();

		$('.pie-chart').easyPieChart({
				easing: 'easeOutBounce',
				barColor: '#667eea',
				trackColor: '#ace',
				lineWidth: 15,
				trackWidth: 13,
				lineCap: 'butt',
				scaleColor: false,
				size: 200,
				onStep: function(from, to, percent) {
					$(this.el).find('.percent').text(Math.round(percent));
				},
				onStart: function(from, to){
					$(this.el).find('canvas').css({
						position: 'absolute',
						top: 0,
						left: 0,
					})
				}
			});
	
		
			$('select#review-rate').barrating({
			theme: 'fontawesome-stars'
		})
		$(document).on('click', '.btn-enable-form', function(){
			let $form = $(this).parent('form');
			
			$.each($form.find('input, select, textarea, .btn-upload'), function(i, el){
				let $el = $(el)

				$el.prop('disabled', false);
				$el.prop('readonly', false);
				$el.removeClass('cover-flat');
				
				if($el.is('select') && $el.hasClass('selectized')){
					$el.selectize()[0].selectize.enable();
				}
			});
			
			$.each($form.find('[data-class-enabled]'), function(i, el){
				let $el = $(el)
				$el.addClass($el.data('class-enabled'))
			})
			$(this).removeClass('btn-enable-form').addClass('btn-disable-form');
			$(this).find('i').removeClass('fa-pencil').addClass('fa-check');
			
			$form.find('.text-helper').show();
		});
		$(document).on('click', '.btn-disable-form', function(){
			let $form = $(this).parent('form');
			$form.submit();
			
			$.each($form.find('input, select, textarea, .btn-upload'), function(i, el){
				let $el = $(el)

				if($el.attr('type') == 'url'){
					$el.prop('readonly', true);
				}else{
					$el.prop('disabled', true);
				}
				
				if($el.is('select') && $el.hasClass('selectized')){
					$el.selectize()[0].selectize.disable();
				}
				$el.addClass('cover-flat');
			});
		
			$.each($form.find('[data-class-enabled]'), function(i, el){
				let $el = $(el)
				$el.removeClass($el.data('class-enabled'))
			})

			$(this).removeClass('btn-disable-form').addClass('btn-enable-form');
			$(this).find('i')
				.removeClass('fa-check').addClass('fa-pencil');
			$form.find('.text-helper').hide();
		});
	
		$(document).on('click', 'input[type="url"]', function(){
			if($(this).val() != '' && $(this).attr('readonly')){
				window.open($(this).val())
			}
		});

		$('.image-editor').cropit({
			exportZoom: 1.25,
			imageBackground: true,
			imageBackgroundBorderWidth: 50,
			imageState: {
				src: "{{asset('img/blank-image.jpg')}}",
			},
			onFileChange: (e)=>{
				let $el = $(e.target);
				let $container = $el.parents('.image-editor');
				$container.find('button[type="submit"]').prop('disabled', false);
			},
			onImageError: (error) => {
				alert(error.message)
			}
		});
		$('.rotate-cw').click(function() {
			$('.image-editor').cropit('rotateCW');
		});
		$('.rotate-ccw').click(function() {
			$('.image-editor').cropit('rotateCCW');
		});
		$('#form-logo button:submit').click(function() {
			var imageData = $('.image-editor').cropit('export');
			$('#form-logo input[name="logo"]').val(imageData);
			$('#form-logo').submit();
		});

		$(document).on('click', '.btn-remove-alumni', function(e){
			let $el = $(e.target);
			
			if($el.is('button') == false){
				$el = $el.parents('button')
			}
			if(confirm('Remove this top employer item?')){
				$.ajax({
					url: $el.data('action'),
					method: 'delete'
				})
				.done(function(response){
					callback(response)
				})
				.fail(function(error){
					alert(getErrorMessage(error))
				})
			}
		})
		
		$(document).on('click', 'a.scroll2', (e) => {
			e.preventDefault();
			let $a = $(e.target);
			let $ul = $a.parents('ul')

			$ul.find('a').removeClass('active');
			$a.addClass('active')
			$('html, body').animate({
				scrollTop: $(document).find($a.attr('href')).offset().top - 80
			}, 700)
		});

		_fileUpload('.file-uploader')
	})
</script>

@if($weathers)
<script>
	var ctx = document.getElementById("myChart");
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: @json($weathers['label']),
			datasets: [{
				label: 'Low',
				data: @json($weathers['tempMin']),
				backgroundColor: '#66669a',
				borderWidth: 1
			},{
				label: 'High',
				data: @json($weathers['tempMax']),
				backgroundColor: '#7288eb',
				borderWidth: 1
			}]
		},
		options: {
			scales: {xAxes: [{stacked: true}],yAxes: [{stacked: true}]},
			legend: {position: 'left'}
		}
	})
</script>
@endif
{{-- @if(!empty($lat) && !empty($lng))
<script>
	var map;
	function initMap() {
		let $map = $('#map');
		$map.width($map.parent().width())
		$map.height(450)

		let location = {lat: {{$lat}}, lng: {{$lng}} };
		let map = new google.maps.Map(document.getElementById('map'), {
			center: location,
			zoom: 15
		});
		let marker = new google.maps.Marker({position: location, map: map});
	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCX0BMDCN86PS6KOLeIQ1h8ea7Fi79TtOA&callback=initMap"
async defer></script>
@else
<script>
	$('#map').text('Map Not Available');
</script>
@endif --}}
@endpush