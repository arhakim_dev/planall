<table class="table text-center text-secondary">
	<tbody>
		<tr>
			<td></td>
			@foreach($universities as $university)
			<td>
				<figure class="border rounded-circle p-1 mx-auto" style="width:150px;">
					<img class="img-fluid rounded-circle" src="{{$university->logo}}"> </figure>
				<div> {{$university->name}} </div>
				<small class="text-muted">Degree: {{($degree != 'undergraduate')?strtoupper($degree) : ucfirst($degree)}}</small>
			</td>
			@endforeach
		</tr>
		<tr>
			<td>
				<img class="img-fluid d-block mx-auto" src="img/icon/prize.png">
				<div> Rank </div>
			</td>
			@foreach($universities as $university)
			<td>
					{{$university->programRankingsAvg($degree)}}
		{{-- 	@foreach($university->programRankingsAvg() as $name => $value)
			<div>{{$name}} {{$value}}</div>
			@endforeach --}}
			</td>
			@endforeach
		</tr>
		<tr>
			<td>
				<img class="img-fluid d-block mx-auto" src="img/icon/database.png">
				<div> Avereage Tuition </div>
			</td>
			@foreach($universities as $university)
			<td>
				{{
				!empty($university->programTuition())
					? $university->programTuition()->pivot->{$degree}
					:'Data Not Available'
				}}</td>
			@endforeach
		</tr>
		<tr>
			<td>
				<img class="img-fluid d-block mx-auto" src="img/icon/approval.png">
				<div> Aceptance Rate </div>
			</td>
			@foreach($universities as $university)
			<td>
				{{
				!empty($university->programAcceptanceRate())
					? $university->programAcceptanceRate()->pivot->{$degree}
					: 'Data Not Available'
				}}</td>
			@endforeach
		</tr>
		<tr>
			<td>
				<img class="img-fluid d-block mx-auto" src="img/icon/numbered-list.png">
				<div> Programs </div>
			</td>
			@foreach($universities as $university)
			<td>{{$university->subjects()->wherePivot('type', $degree)->count()}}</td>
			@endforeach
		</tr>
		<tr>
			<td>
				<img class="img-fluid d-block mx-auto" src="img/icon/page.png">
				<div> Testing Requirements </div>
			</td>
			@foreach($universities as $university)
			<td>
				@forelse($university->programAdminTest($degree) as $name => $value)
					<div>{{$name}} {{$value}}</div>
				@empty
				Data Not Available
				@endforelse
			</td>
			@endforeach
		</tr>
		<tr>
			<td>
				<img class="img-fluid d-block mx-auto" src="img/icon/planner.png">
				<div> Application Deadlines </div>
			</td>
			@foreach($universities as $university)
			<td>
				{{
					!empty($university->programDeadline)
					? $university->programDeadline()->pivot->{$degree}
					: 'Data Not Available'
				}}</td>
			@endforeach
		</tr>
		<tr>
			<td>
				<img class="img-fluid d-block mx-auto" src="img/icon/add-database.png">
				<div> Average Earnings </div>
			</td>
			@foreach($universities as $university)
			<td>{{($university->afterCollege)?$university->afterCollege->earning:'Data Not Available'}}</td>
			@endforeach
		</tr>
	</tbody>
</table>