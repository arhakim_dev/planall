<div class="card mb-5 shadow univ-list" id="university-{{ $university['id'] }}">
	@if(isset($removable) && $removable == true)
		@if(Auth::check() && Auth::user()->id == $user->id)
	<form method="delete" action="profile/university/remove/{{ $university['id'] }}">
	<div class="btn btn-secondary btn-float-action rounded-circle position-absolute" onclick="confirm(this.title+'?')?$(this).parent('form').submit():''" title="Remove {{ $university['name'] }}">
		<i class="fa fa-close"></i>
	</div>
	</form>
		@endif
	@endif
	<div class="card-body">
		<figure class="shadow rounded-circle p-1 bg-white w-md-5 mt-4-offset mx-auto ml-md-3 w-50">
			<img src="{{ $university['logo'] }}" class="img-fluid rounded-circle"> 
		</figure>
		<h5 class="card-title font-weight-bold d-flex justify-content-start align-items-center flex-column flex-md-row">
			<a href="{{$university['url']}}" class="pr-md-3">{{ $university['name'] }}</a>
			@include('_partials/star-rating', ['rating' => $university['rating']])
			
			@if(isset($compareable) && $compareable == true)
			<div class="ml-md-auto">
				<div class="custom-control custom-checkbox d-inline-block">
					<form method="post" action="university/compare/add">
						<input type="hidden" name="university_id" value="{{ $university['id'] }}">
						<input type="checkbox" class="custom-control-input university-compare" name="opt" value="on" onchange="$(this).parent('form').submit()" id="univ-compare-{{ $university['id'] }}" {{ ($university['isCompared'])? 'checked': '' }}>
						<label class="custom-control-label" for="univ-compare-{{ $university['id'] }}" style="top:-16px;"></label>
					</form>
				</div>
				<span data-container="#university-{{ $university['id'] }}" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." data-placement="bottom" data-toggle="popover" class="cursor-pointer">
					<img src="{{ asset('img/icon/info.png') }}"> </span>
			</div>
			@endif
		</h5>
		<p class="">
			<i class="fa fa-map-marker"></i> {{ $university['address'] }}</p>
		<div class="container mb-2">
			<div class="row text-center" style="">
				<div class="col-md-3 d-flex align-items-start flex-column py-1">
					<img src="{{ asset('img/icon/prize.png') }}" class="mb-auto mx-auto">
					<div class="text-secondary mx-auto"> Rank </div>
					<div class="mx-auto">
						{{$university['ranking'] or 'Data Not Available'}}
				{{-- 	@foreach($university['ranking'] as $name => $value)
					<div>{{$name}} {{$value}}</div>
					@endforeach --}}
					</div>
				</div>
				<div class="col-md-3 d-flex align-items-start flex-column py-1">
					<img src="{{ asset('img/icon/database.png') }}" class="mb-auto mx-auto">
					<div class="text-secondary mx-auto"> Average Tuition </div>
					<div class="mx-auto"> {{ $university['tuitionAVG'] or 'Data Not Available' }} </div>
				</div>
				<div class="col-md-3 d-flex align-items-start flex-column py-1">
					<img src="{{ asset('img/icon/approval.png')}}" class="mb-auto mx-auto">
					<div class="text-secondary mx-auto"> Acceptance Rate </div>
					<div class="mx-auto"> {{ $university['acceptanceRate'] or 'Data Not Available' }}  </div>
				</div>
				<div class="col-md-3 d-flex align-items-start flex-column py-1">
					<img src="{{ asset('img/icon/numbered-list.png')}}" class="mb-auto mx-auto">
					<div class="text-secondary mx-auto"> Programs </div>
					<div class="mx-auto"> {{ $university['programs'] }} </div>
				</div>
			</div>
			<div class="row text-center">
				<div class="col-md-3 d-flex align-items-start flex-column py-1">
					<img src="{{ asset('img/icon/page.png')}}" class="mb-auto mx-auto">
					<div class="text-secondary mx-auto"> Testing Requirements </div>
					<div class="mx-auto">
					@forelse($university['testingRequirements'] as $name => $value)
						<div>{{$name}} {{$value}}</div>
					@empty
						-
					@endforelse
					</div>
				</div>
				<div class="col-md-3 d-flex align-items-start flex-column py-1">
					<img src="{{ asset('img/icon/page.png')}}" class="mb-auto mx-auto">
					<div class="text-secondary mx-auto"> English Requirements </div>
					<div class="mx-auto">
					@forelse($university['englishRequirements'] as $name => $value)
						<div>{{$name}} {{$value}}</div>
					@empty
						-
					@endforelse
					</div>
				</div>
				<div class="col-md-3 d-flex align-items-start flex-column py-1">
					<img src="{{ asset('img/icon/planner.png')}}" class="mb-auto mx-auto">
					<div class="text-secondary mx-auto"> Application Deadlines </div>
					<div class="mx-auto"> {{ $university['applicationDeadline'] or 'Data Not Available' }} </div>
				</div>
				<div class="col-md-3 d-flex align-items-start flex-column py-1">
					<img src="{{ asset('img/icon/add-database.png')}}" class="mb-auto mx-auto">
					<div class="text-secondary mx-auto"> Average Earnings </div>
					<div class="mx-auto"> {{ $university['earningAVG'] }} </div>
				</div>
				{{-- <div class="col-md-3 d-flex align-items-start flex-column py-1">
					<img src="{{ asset('img/icon/sun.png')}}" class="mb-auto mx-auto">
					<div class="text-secondary mx-auto"> {{ $university['weathers'] }}° </div>
					<div class="mx-auto"> Current Weather </div>
				</div> --}}
			</div>
		</div>
		<div class="text-center text-secondary pt-2 position-relative">
			<i class="fa fa-eye"></i> {{ $university['viewed'] }}
			<a href="{{ $university['url'] }}" class="btn btn-light rounded-circle btn-outline-secondary position-absolute" style="top:0;right:0;">
				<i class="fa fa-angle-right"></i>
			</a>
		</div>
	</div>
</div>
<script>
setTimeout(function(){
$(function(){ 
	let cid = "{{ $university['id'] }}";
	$('#university-'+cid+' [data-toggle="popover"]').popover()
})
}, 1000)
</script>