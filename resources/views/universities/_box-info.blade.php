<div class="card" style="{{$style or '' }}">
	<div class="card-body">
		<figure class="shadow rounded-circle p-1 bg-white w-15 mt-5-offset">
			<img src="{{$university->logo}}" class="img-fluid rounded-circle"> </figure>
		<h5 class="card-title"><a href="{{$university->url}}">{{$university->name}}</a></h5>
		<p title="{{$university->fullAddress}}">
			<i class="fa fa-map-marker"></i> {{$university->city.', '.$university->country->name}}</p>
		<p class="card-text text-muted text-justify">{{str_limit($university->description, 300)}}</p>
		<div class="position-absolute text-right w-100 p-2" style="bottom:0;left:0;">
			<a href="{{$university->url}}" class="btn btn-outline-secondary rounded-circle">
				<i class="fa fa-angle-right"></i>
			</a>
		</div>
	</div>
</div>