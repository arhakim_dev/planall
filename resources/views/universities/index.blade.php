@extends('page', ['body_class' => ''])

@section('content')

<div class="py-5">
	<div class="container mt-5">
		<div class="row">
			<div class="col-12">
				<div class="progress-overlay">
						<div><i class="fa fa-spinner fa-spin fa-2x"></i></div>
				</div>
				<div class="card shadow mb-3">
					<div class="card-header bg-white d-flex justify-content-between align-items-center" data-toggle="collapse" data-target="#collapse-about" aria-expanded="false" aria-controls="collapse-about">
						<h4 class="title">Filter <small><i class="fa fa-spinner fa-spin" style="display:none;"></i></small></h4>
						<span class="fa fa-chevron-up"></span>
					</div>
					<div class="card-body collapse show" id="collapse-about">
						<form method="post" action="search/universities" id="form-search-universities">
							<div class="row mb-3">
								<div class="col-md-4">
									<div class="form-group">
										<label class="font-weight-bold text-secondary">Location </label>
										<select name="country" data-selectize-options='{"plugins": ["remove_button"]}'>
											<option value="">Choose Country</option>
											@if($countries->count())
												@foreach($countries->get() as $country)
													<option value="{{$country->code}}" {{(request('country') == $country->code)? 'selected' : ''}}>{{$country->name}}</option>
												@endforeach
											@endif
										</select>
									</div>
									<div class="form-group">
										<label class="font-weight-bold text-secondary">College type</label>
										<select name="is_public" data-selectize-options='{"plugins": ["remove_button"]}'>
											<option value="">All</option>
											<option value="1">Public</option>
											<option value="0">Private</option>
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="font-weight-bold text-secondary">Major</label>
										<select name="major" data-selectize-options='{"plugins": ["remove_button"]}'>
											<option value="">Choose Major</option>
											@if($majors->count())
												@foreach($majors->get() as $major)
												<option value="{{$major->id}}" {{(request('major') == $major->id)? 'selected' : ''}}>{{$major->name}}</option>
												@endforeach
											@endif
										</select>
									</div>
									<div class="form-group">
										<label class="font-weight-bold text-secondary">Tuition fee</label>
										<br>
										<div class="d-flex justify-content-between">
											<div> USD
												<span id="range-slider-val-0">{{$tuition_start}}</span>
											</div>
											<div> USD
												<span id="range-slider-val-1">{{$tuition_end}}</span>
											</div>
										</div>
										<input id="range-slider" class="mx-1" type="text" data-slider-min="1500" data-slider-max="500000" data-slider-step="500" data-slider-value="[{{$tuition_start.','.$tuition_end}}]" name="tuition"> </div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="font-weight-bold text-secondary">Degree</label>
										<select name="degree">
											<option value="undergraduate">Undergraduate</option>
											<option value="ms" selected>MS</option>
											<option value="mba">MBA</option>
										</select>
									</div>
								</div>
							</div>							
							@if($q!= '')
							<hr>
							<strong class="text-secondary">Keyword</strong><br>
							<label class="border rounded p-1">
								<input type="hidden" name="q" value="{{$q}}">{{$q}} 
								<button type="button" class="close ml-1 text-danger" aria-label="Close" onclick="$('input[name=q]').val('');search();$(this).parent().remove();">
									<span aria-hidden="true">&times;</span>
								</button>
							</label>
							@endif
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="pb-5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="progress-overlay">
						<div><i class="fa fa-spinner fa-spin fa-2x"></i></div>
				</div>
				<h2 class="section-title text-left mb-2">Sort By</h2>
				<div data-toggle="buttons" class="btn-group btn-secondary-light d-flex mb-5">
					<label class="btn btn-secondary btn-sort form-check-label flex-fill shadow {{(request('sort') =='ranking' || empty(request('sort')))? 'active' : ''}}">
						<input type="radio" name="sort" value="ranking" class="form-check-input radio-sort" {{(request('sort') =='ranking' || empty(request('sort')))? 'checked' : ''}}> Ranking </label>
					<label class="btn btn-secondary btn-sort form-check-label flex-fill shadow {{(request('sort') =='tuition')? 'active' : ''}}">
						<input type="radio" name="sort" value="tuition" class="form-check-input radio-sort" {{(request('sort') =='tuition')? 'checked' : ''}}> Tuition Fee </label>
					<label class="btn btn-secondary btn-sort form-check-label flex-fill shadow {{(request('sort') =='rate')? 'active' : ''}}">
						<input type="radio" name="sort" value="rate" class="form-check-input radio-sort" {{(request('sort') =='rate')? 'checked' : ''}}> Acceptance Rate </label>
				</div>
				<div id="university-container">
					<i class="fa fa-spinner fa-spin fa-2x" style="display:none;"></i>
				</div>
			</div>
		</div>
		
    @include('_partials/row-loader', [
      'id' => 'university-loader-container', 'idURL' => 'next-universities', 'url' => '#'
    ])
	</div>
</div>

<div id="ucompare-container" class="card shadow transition" style="z-index: 1;">
	<div class="card-body">
		<div class="card-title d-flex justify-content-between align-items-center cursor-pointer">
			<figure class="m-0 shadow rounded transition">
			<img src="img/icon/scales.png" style="width: 35px;">
		</figure><span>Compare University</span>
		</div>
		
		<div class="text-center bg-light rounded mb-1">
			<small>Please select two or three universities.</small>
		</div>
		
		<form method="POST" action="university/compare" id="form-compare">
			<input type="hidden" name="degree">
			<ul class="list-group">
			@if(session('ucompare') != '')
				@foreach(session('ucompare') as $i => $university)
					<li class=" d-flex list-group-item justify-content-between align-items-center" id="ucompare-list-{{$i}}">
						<div class="text-truncate-ellipsis-90">{{$university}}</div>
						<button type="button" class="btn-sm btn btn-outline-danger remove-compare float-right" value="{{$i}}"> x </button>
					</li>
				@endforeach
			@endif
			</ul>
			<div class="text-center mt-2">
				<button class="btn btn-secondary btn-sm" {{(count(session('ucompare')) < 2)?'disabled':''}} type="submit">Compare</button>
			</div>
		</form>
	</div>
</div>

<div class="modal fade" id="modal-ucompare" tabindex="-1" role="dialog" aria-labelledby="modal-ucompare" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<button data-dismiss="modal" aria-label="Close" class="close position-absolute p-2 rounded-circle" style="top:0;right:0; background-color: rgba(255, 255, 255, .5);z-index:1;">
					<img class="" src="{{asset('img/icon/delete.png')}}" height="20"> </button>
				<h5 class="mb-4 text-secondary text-center font-weight-bold">Compare Universities</h5>
				
				<div id="university-compared" class="table-responsive">
				</div>
			</div>
		</div>
	</div>
</div>

@stop

@push('styles')
<link rel="stylesheet" href="{{asset('vendor/bootstrap-slider/bootstrap-slider.min.css')}}" type="text/css">
@endpush

@push('scripts')
<script src="{{asset('vendor/bootstrap-slider/bootstrap-slider.min.js')}}"></script>
<script>
let isSearching = false;
function search(){
	if(isSearching) return;
	
	let data = $('#form-search-universities').serialize()
		+'&sort='+$('input.radio-sort:checked').val();
	
	$('#form-search-universities').find('input').prop('disabled', true)
	$('input.radio-sort, select').prop('disabled', true)
	$('.progress-overlay').css('display', 'flex')
	$('#university-container').empty()
	$('i.fa-spinner').show()
	isSearching = true;

	$('#form-compare').find('input[name="degree"]').val($('select[name="degree"]').val())

	$.get('search/universities', data)
		.done(function(response){
			callback(response);
		})
		.fail(function(error){
			alert(getErrorMessage(error))
		})
		.always(function(){
			$('#form-search-universities').find('input').prop('disabled', false)
			$('input.radio-sort, select').prop('disabled', false)
			$('i.fa-spinner, .progress-overlay').hide()
			isSearching = false;
		})
}
$(function(){
	$("#range-slider").slider().on("slide", function(e) {
		$("#range-slider-val-0").text(e.value[0]);
		$("#range-slider-val-1").text(e.value[1]);
	});
	$('.slider-track div').click(function(){
		var min = $('.tooltip-min .tooltip-inner').text();
		$("#range-slider-val-0").text(min);
		
		var max = $('.tooltip-max .tooltip-inner').text();
		$("#range-slider-val-1").text(max);
	});	
	$("#range-slider").slider().on("slideStop", function(e) {
		search()
	})
	$(document).on('change', 'select', function(e){
		let $el = $(e.target);
		
		$('#form-compare').find('input[name="degree"]').val($el.val())
		search()
	})
	$(document).on('change click','input[type="radio"]', function(e){
		$(e.target).prop('checked', true)

		setTimeout(function(){
			search()
		}, 500)
	})
	$(document).on('click', '#ucompare-container .card-title', function(){
		if($('#ucompare-container').hasClass('open')){
				$('#ucompare-container').removeClass('open')
			}else{
				$('#ucompare-container').addClass('open')
			}
	})
	$(document).on('click', '.remove-compare', function(e){
		let $el = $(e.target)
		$.post('university/compare/add', {university_id:$el.val()})
			.done(function(response){
				callback(response)
			})
	});
	
	setTimeout(function(){
		search();
	}, 1000)
});

</script>
@endpush

