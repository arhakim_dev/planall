@component('mail::message')

<p>Hi {{$admin->first_name}},</p>

<p>University you've manage (<a href="{{url($university->url)}}">{{$university->name}}</a>) is shortlisted by <a href="{{url($user->url)}}">{{$user->fullName}}</a></p>

~ {{ config('app.name') }}
@endcomponent