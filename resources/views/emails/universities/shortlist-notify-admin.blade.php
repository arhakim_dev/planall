@component('mail::message')

<p>Hi {{$admin->first_name}},</p>

<p>University <a href="{{url($university->url)}}">{{$university->name}}</a> is added to <a href="{{url($user->url)}}">{{$user->fullName}}'s</a> Shortlist</p>

~ {{ config('app.name') }}
@endcomponent