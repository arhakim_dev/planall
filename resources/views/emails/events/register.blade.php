@component('mail::message')

<p>Hi {{$user->first_name}},</p>

@if($userEvent->is_waiting_list)
<p>You are registered to event <strong>{{title_case($event->title)}}</strong> as waiting list</p>
@else
<p>You are registered to event <strong>{{title_case($event->title)}}</strong></p>
@endif

Thanks,<br>
{{ config('app.name') }}
@endcomponent