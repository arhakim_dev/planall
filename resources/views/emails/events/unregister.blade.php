@component('mail::message')

<p>Hi {{$user->first_name}},</p>

<p>You are about to unregister <strong>{{title_case($event->title)}}</strong> event</p>

Thanks,<br>
{{ config('app.name') }}
@endcomponent