@component('mail::message')

<p>Hi {{$user->first_name}},</p>

<p>You are receiving this email because we received a password reset request for your account.</p>

@component('mail::button', ['url' => url('password/reset/'. $token)])
Reset Password
@endcomponent

<hr>
<p>If you did not request a password reset, no further action is required.</p>

Best,<br>
{{ config('app.name') }}
@endcomponent