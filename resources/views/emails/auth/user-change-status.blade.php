@component('mail::message')

<p>Hi {{$user->first_name}},</p>

<p>Your account status in <strong>{{ config('app.name') }}</strong> now is <strong>{{$status}}</strong>.</p>

~ {{ config('app.name') }}
@endcomponent