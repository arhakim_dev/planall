@component('mail::message')

<p>Hi {{$admin->first_name}},</p>

<p>User <strong>{{$user->fullName}}</strong> at <strong>{{$user->email}}</strong> has just registered to <strong>{{config('app.name')}}</strong></p>

~ {{ config('app.name') }}
@endcomponent