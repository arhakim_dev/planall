@component('mail::message')

<p>Hi {{$user->first_name}},</p>

<p>Thank you for registering to {{ config('app.name') }}. 
To complete your registration process, please click link below to activate your account.</p>

@component('mail::button', ['url' => url('account/activate?code='. $user->activation_code)])
Activate My Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent