@component('mail::message')

<p>Hi {{$admin->first_name}},</p>

<p>Service Provider you've manage (<a href="{{url($service->url)}}">{{$service->name}}</a>) is shortlisted by <a href="{{url($user->url)}}">{{$user->fullName}}</a></p>

~ {{ config('app.name') }}
@endcomponent