@component('mail::message')

<p>Hi {{$admin->first_name}},</p>

<p>Service Provider <a href="{{url($service->url)}}">{{$service->name}}</a> is added to <a href="{{url($user->url)}}">{{$user->fullName}}'s</a> Shortlist</p>

~ {{ config('app.name') }}
@endcomponent