@component('mail::message')

<p>Hi {{$admin->first_name}},</p>

<p>New review created on <a href="{{url($service->url)}}">{{$service->name}}</a> by <a href="{{url($reviewer->url)}}">{{$reviewer->fullName}}</a></p>

<p>Please open the <strong>Dashboard - Universities</strong> to approve the review</p>
~ {{ config('app.name') }}
@endcomponent