@component('mail::message')

<p>Hi {{$admin->first_name}},</p>

<p>New post <a href="{{url($post->url)}}">{{$post->title}}</a> has just created in <strong>{{$post->type}}: {{$post->category->name}}</strong> by <a href="{{url($post->user->url)}}">{{$post->user->fullName}}</a></p>

<p>Please open the <strong>Dashboard - Posts</strong> to review and approve the post</p>
~ {{ config('app.name') }}
@endcomponent