@component('mail::message')

<p>Hi {{$admin->first_name}},</p>

<p>New comment created on <a href="{{url($post->url)}}">{{$post->title}}</a> by <a href="{{url($comment->user->url)}}">{{$post->user->fullName}}</a></p>

<p>Please open the <strong>Dashboard - {{ucfirst($post->type)}}</strong> to review and approve the comment</p>
~ {{ config('app.name') }}
@endcomponent