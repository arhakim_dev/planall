
<div class="list-group-item text-white pb-4 mb-4 bg-gradient rounded">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2 fs-9 lh-5 font-weight-bold text-md-center" style="word-break: break-word;">
				{{date('d F', strtotime($event->start_date))}}
			</div>
			<div class="col-md-9 pl-3">
				<div class="font-weight-bold fs-6">
					<a href="{{$event->url}}" class="text-white">{{$event->title}}</a>
				</div>
				<div class="">
					<i class="fa fa-clock-o"></i> 
					{{date('h:i a', strtotime($event->start_time)).' - '.date('h:i a', strtotime($event->end_time))}}</div>
				<div class="">
					<i class="fa fa-map-marker"></i> {{$event->address}}</div>
				
				@if($event->status != 'active')
					<small class="bg-{{($event->status == 'done')? 'info': 'danger'}} rounded p-1 float-md-right">
						{{ucfirst($event->status)}}</small>
				@endif
			</div>
			<div class="col-md-1 d-flex flex-column justify-content-center align-items-center">
				<a href="{{$event->url}}" class="btn btn-light btn-outline-light rounded-circle">
					<i class="fa fa-angle-right"></i>
				</a>
			</div>
		</div>
	</div>
</div>