<div class="card shadow mr-md-3 mb-3">
	<div class="card-body text-center">
		<figure class="mx-auto w-50">
			<img src="{{$user->avatar}}" class="img-fluid rounded-circle"> </figure>
		<a href="#" class="font-weight-bold fs-4">{{$user->fullName}}</a>
		<br>
		<span>{{$user->profession or '&nbsp;'}}</span>
		<hr class="w-50"> </div>
</div>