@extends('page', ['body_class' => 'event-listing', 'footerStyle' => ''])

@section('content')

<div class="hero-area h-100 position-relative" style="top: 80px; background-image: url(img/hero-image-event.png); background-size: cover; background-repeat: no-repeat;background-position:center center;">
	<div class="container h-100">
		<div class="row h-100">
			<div class="col-md-12 h-100 d-flex justify-content-center align-items-center">
				<h2 class="hero-title"> EVENTS </h2>
			</div>
		</div>
	</div>
</div>

<div class="py-5 mt-5">
	<div class="container position-relative">
		<div class="progress-overlay" style="display:none;">
				<div><i class="fa fa-spinner fa-spin fa-2x"></i></div>
		</div>
		<div class="row">
			<form id="form-list-events" class="w-100">
				<div data-toggle="buttons" class="btn-group btn-secondary-light mb-5 w-100 d-inline-block d-md-inline-flex">
					<div class="col-md-6 px-0 d-flex">
						@for($i = 1; $i <= 6; $i++)
						<label class="btn btn-secondary month-option {{($i == date('n'))? 'active' : ''}} form-check-label flex-fill" style="border-radius:0;">
						<input type="radio" class="form-check-input" {{($i == date('n'))? 'checked' : ''}} value="{{$i}}" name="month">{{date('M', mktime(0, 0, 0, $i, 10))}}</label>
						@endfor
					</div>
					<div class="col-md-6 px-0 d-flex border-left-md-secondary">
							@for($i = 7; $i <= 12; $i++)
							<label class="btn btn-secondary month-option {{($i == date('n'))? 'active' : ''}} form-check-label flex-fill border-left-0" style="border-radius:0;">
							<input type="radio" class="form-check-input" {{($i == date('n'))? 'checked' : ''}} value="{{$i}}" name="month"> {{date('M', mktime(0, 0, 0, $i, 10))}}</label>
							@endfor
					</div>
					</div>
			</form>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="list-group" id="events-container"></div>
			</div>
		</div>
		
		@if(!empty($nextURL))
			@include('_partials.row-loader', [
				'id' => 'event-loader-container', 'idURL' => 'next-latest-event', 'url' => $nextURL, 'style' => 'margin-bottom:-100px;', 'loader' => '.progress-overlay'
			])
		@endif

	</div>
</div>
@stop

@push('scripts')
<script>
	$(function(){
		
		$(document).on('change clickk','input[type="radio"]', function(){
			show()
		})
	})
	function show(){		
		$('#form-list-events').find('input').prop('disabled', true)
		$('.progress-overlay').css('display', 'flex')
		$('#service-container').empty()
		$('i.fa-spinner').show()

		$.get('events', {sort: 'month', month: $('input[type="radio"]:checked').val()})
			.done(function(response){
				callback(response);
			})
			.fail(function(error){
				alert(getErrorMessage(error))
			})
			.always(function(){
		$('#form-list-events').find('input').prop('disabled', false)
				$('i.fa-spinner, .progress-overlay').hide()
			})
	}
	setTimeout(function(){
		show();
	}, 1000)
</script>
@endpush