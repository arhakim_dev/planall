<div class="card shadow mr-md-3 mb-3">
	<div class="card-body text-center p-2">
		@if($media->type == 'url')
		<div class="position-relative cursor-pointer" data-toggle="modal" data-target="#modal-video" data-video="{{$media->media}}">
			<div class="position-absolute w-100 h-100" style="top:0;left:0;"></div>
			<iframe class="embed-responsive-item" src="{{$media->media}}" frameborder="0"></iframe>
		</div>
		@else
		<figure class="border p-2">
			<i class="fa fa-file-powerpoint-o fa-5x"></i>
		</figure>
		<a href="{{$media->media}}" target="_blank">{{str_replace('files/ppt', '', $media->media)}}</a>
		@endif
	</div>
</div>