@extends('page', ['body_class' => 'event-listing', 'footerStyle' => ''])

@section('content')
<div class="hero-area pb-1 d-flex align-items-end h-100" style="top: 80px; background-image: url('img/hero-image-event.png'); background-size: cover; background-repeat: no-repeat;background-position:center center;">
	<div class="container h-100">
		<div class="row h-75">
			<div class="col-12 text-center pt-3 h-100 d-flex justify-content-center align-items-center">
				<h2 class="hero-title">{{$event->title}}</h2>
			</div>
		</div>
		<div class="row text-white mb-3">
			<div class="col-12 col-sm-6">
				<div class="font-weight-bold">
					<p class="mb-1">
						<img src="img/icon/calendar.png"> {{$eventDate}}</p>
					<p class="mb-1">
						<img src="img/icon/clock-white.png"> {{date('h:i a', strtotime($event->start_time)).' - '.date('h:i a', strtotime($event->end_time))}}</p>
					<div class="row">
						<div class="col-md-1 pr-0">
							<img src="img/icon/contacts-white.png">
						</div>
						<div class="col-md-11 pl-0">
								<span id="seat-counter">{{($event->seat - $attendees)}}</span> of {{$event->seat}} seat available<br>
							<span id="attendees-counter">{{$attendees}}</span> people attending<br>
							<span id="waiting-list-counter">{{$waitingList}}</span> waiting list<br>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-6 text-right">
			@if($event->status == 'active')
				@if($isUserRegistered)
					<button disabled class="btn btn-light btn-lg text-secondary bg-white px-4 rounded"> Register </button>
					<div><small class="text-white">* You already registered</small></div>
					<form method="delete" action="event/cancel/registration" data-confirm="Cancel to attending this event?">
						<input type="hidden" name="event" value="{{$event->id}}">
						<button type="submit" class="btn btn-outline-light btn-sm rounded">Cancel Registration </button>
					</form>

				@else
					@if(!$isPastEvent)
					<form method="post" action="event/register">
						<input type="hidden" name="event" value="{{$event->id}}">
						<button type="submit" class="btn btn-light btn-lg text-secondary bg-white px-4 rounded"> Register </button>
					</form>
					@endif
				@endif
			@endif
			</div>
		</div>
	</div>
</div>
<div class="pt-5">
	<div class="container">
		<div class="row mb-5">
			<div class="col-md-12">
				<h2 class="section-title">Description</h2>
				<div class="text-justify">{!!$event->description!!}</div>
			</div>
		</div>
		@if($users->count())
		<div class="row">
			<div class="col-md-12">
				<h2 class="section-title">People who are attending</h2>
				<div class="row" id="event-user-container">
					@forelse($users->get() as $user)
					<div class="col-md-4">
						@include('events._card-user', ['user' => $user])
					</div>
					@empty
						<div class="border py-2 text-center">
								Data Not Available
						</div>
					@endforelse
				</div>
			</div>
		</div>
			@include('_partials.row-loader', [
				'id' => 'eventuser-loader-container', 'idURL' => 'next-latest-eventuser', 'url' => $nextUserURL, 'loader' => '.progress-overlay'
			])
		@endif
	</div>
</div>
<div class="pb-5">
	<div class="container">
		@if($media->count())
		<div class="row">
			<div class="col-md-12">
				<h1 class="section-title">Access to recordings</h1>
				<div class="row" id="event-media-container">
					@forelse($media->get() as $media)
					<div class="col-md-4">
						@include('events._card-media', ['media' => $media])
					</div>
					@empty
						<div class="border bg-white py-2 text-center">
							Data Not Available
						</div>
					@endif
				</div>
			</div>
		</div>		
		@include('_partials.row-loader', [
			'id' => 'eventmedia-loader-container', 'idURL' => 'next-latest-eventmedia', 'url' => $nextMediaURL, 'loader' => '.progress-overlay'
		])
		@endif
	</div>
</div>

<div class="modal fade" id="modal-video">
		<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="modal-body p-0" style="height:600px;">
					<button type="button" class="close position-absolute p-1 rounded-circle" data-dismiss="modal" aria-hidden="true" style="right: 100px;top: 10px;background: rgba(255, 255, 255, .5);">
						<i class="fa fa-times"></i></button>
						<i class="fa fa-spinner fa-spin fa-3x position-absolute" style="top: 40%; left: 48%; z-index: 0; display: none;" id="iframe-loader"></i>
					<iframe class="embed-responsive-item w-100 h-100" src="" allowfullscreen frameborder="0" onload="$('#iframe-loader').hide()"></iframe>
				</div>
			</div>
		</div>
	</div>
@stop

@push('scripts')
<script>
$(function(){
	$(document).on('show.bs.modal', '#modal-video', function(e){
		let $el = $(e.target)
		let $btn = $(e.relatedTarget)

		$('#modal-video').find('iframe').attr('src', $btn.data('video')+'?autoplay=1');
		$('#iframe-loader').show()
	})
	$(document).on('hidden.bs.modal', '#modal-video', function(e){
		$(e.target).find('iframe').attr('src', '');
	});
})
</script>
@endpush