@extends('page', ['body_class' => 'home', 'footerStyle' => 'margin-top:-145px;'])

@section('content')
<div class="hero-area py-5 position-relative" style="background-image: url({{asset('img/hero-image.png')}});background-size:cover;">
  <div class="container py-5 my-5 w-md-75">
    <div class="row mt-2">
      <div class="col-4 text-right">
        <a class="btn btn-lg w-md-50 rounded btn-light text-primary w-100" href="#">A </a>
      </div>
      <div class="col-4" id="line-ab"></div>
      <div class="col-4 text-left">
        <a class="btn btn-lg btn-light w-md-50 rounded text-primary w-100" href="#">B </a>
      </div>
    </div>
    <div class="row mt-5">
      <div class="col-3"></div>
      <div class="col-6 text-center">
        <a class="btn btn-lg btn-light rounded text-primary w-100 w-md-75 px-0" href="#">Question</a>
        <div class="mt-2 text-light">
          <i class="fa fa-undo"></i>
          <br>
          <p>Undo Answer</p>
        </div>
      </div>
      <div class="col-3"></div>
    </div>
    <div class="row">
      <div class="col-4 text-right">
        <a class="btn btn-lg btn-light w-md-50 w-100 rounded text-primary" href="#">C </a>
      </div>
      <div class="col-4" id="line-cd"></div>
      <div class="col-4 text-left ">
        <a class="btn btn-lg btn-light w-md-50 w-100 rounded text-primary" href="#">D </a>
      </div>
    </div>
  </div>
</div>

@if($services->count())
<div id="shortcut-wrapper">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <ul class="nav nav-pills d-flex justify-content-center">
        @foreach($services as $service)
          <li class="nav-item m-1 shadow rounded bg-white transition">
            <a class="nav-link text-center" href="{{$service->url}}">
              <span class="shortcut-icon" style="background-image: url({{$service->icon}});"></span>
              <span class="shortcut-text">{{$service->name}}</span>
            </a>
          </li>
        @endforeach
        </ul>
      </div>
    </div>
  </div>
</div>
@endif

<div class="pb-5">
  @if($majors->count())
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 class="section-title">Choose by Study Discipline</h2>
      </div>
    </div>
    <div class="row" id="major-container">
    @foreach($majors as $major)
      <div class="col-md-4">
        @include('_partials/box-hover', ['title' => $major->name, 'url' => $major->url, 'image' => $major->image])
      </div>
    @endforeach
    </div>
    @include('_partials/row-loader', [
      'id' => 'major-loader-container', 'idURL' => 'next-majors', 'url' => $nextMajorsURL, 'collapseTo' => $majors->count(), 'collapseItems' => '#major-container > div'
    ])
  </div>
  @endif

  @if($countries->count())
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 class="section-title">Choose by Study Country</h2>
      </div>
    </div>
    <div class="row" id="country-container">
    @foreach($countries as $country)
      <div class="col-md-4">
        @include('_partials/box-hover', ['title' => $country->name, 'url' => $country->url])
      </div>
      @endforeach
    </div>
    @include('_partials/row-loader', [
      'id' => 'country-loader-container', 'idURL' => 'next-countries', 'url' => $nextCountriesURL, 'collapseTo' => $countries->count(), 'collapseItems' => '#country-container > div'
    ])
  </div>
  @endif
</div>

<div class="py-2 bg-gradient" style="margin-top:-120px;">
  <div class="container">
    <div class="row mb-3">
      <div class="col-12">
        <h2 class="section-title text-light">Live User Activity</h2>
        <div class="row" id="user-activities-container" data-offset="{{$userActivities->count()}}" data-max-items="{{$userActivities->count()}}" data-latest="{{$userActivitiesLatestTime}}">

        @forelse($userActivities as $notification)
          @include('_partials.list-user-activity', [
            'avatar' => $notification->avatar,
            'name' => $notification->name,
            'message' => $notification->message,
            'timeHuman' => $notification->timeHuman,
          ])
        @empty
          <div class="d-flex justify-content-between align-items-center py-3 mb-3 shadow rounded">
            Good day!
          </div>
        @endforelse
       </div>
      </div>
    </div>

    @if($universities->count())
    <div class="row">
      <div class="col-12">
        <h2 class="section-title text-white mb-5">Universities in Spotlight</h2>
      </div>
    </div>
    <div class="row" id="university-container">
      @foreach($universities as $university)
      <div class="col-md-6 mb-5">
        @include('universities._box-info', ['university' => $university, 'style' => 'height:360px;'])
      </div>
       @endforeach
    </div>
      {{-- @include('_partials/row-loader', [
        'id' => 'university-loader-container', 'idURL' => 'next-universities', 'url' => $nextUniversitiesURL, 'variant' => 'light'
     ]) --}}
    @endif
  </div>
</div>

<div class="bg-white pt-2 pb-5">
  <div class="container">
    <div class="row">
      <div class="col-12">
          <h2 class="section-title">Upcoming Events</h2>
      </div>
    </div>
    @if($events->count())
    <div class="row">
      <div class="col-12">
        <div class="list-group bg-gradient rounded" id="event-container">
          @foreach($events as $event)
            @include('_partials.list-event', ['event' => $event])
          @endforeach
        </div>
      </div>
    </div>
      @include('_partials/row-loader', [
        'id' => 'event-loader-container', 'idURL' => 'next-events', 'url' => $nextEventsURL, 'variant' => 'light', 'collapseTo' => $events->count(), 'collapseItems' => '#event-container > div'
      ])
    @else
    <div class="row">
      <div class="col-12">
        <p class="bg-gradient text-white text-center p-3 rounded">No data available</p>
      </div>
    </div>
    @endif
  </div>
</div>

@if($forums->count())
<div class="py-5 bg-gradient " style="{{$events->count()? 'margin-top:-150px;' : ''}}">
  <div class="container">
    <div class="row">
      <div class="col-12 text-white">
        <h2 class="section-title mb-2 text-white">Forum</h2>
        <p class="lead text-center">Trending Q &amp; A </p>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="progress-overlay" style="display:none;">
            <div><i class="fa fa-spinner fa-spin fa-2x"></i></div>
        </div>
        <table class="table-responsive table w-100 table-valign-middle spacing-table sortable" data-action="{{$sortForumURL}}">
          <thead class="text-white">
            <tr>
              <th width="600" class="text-center border-top-0 sort" data-sort="asc">Topic</th>
              <th width="200" class="text-center border-top-0 sort sorted asc" data-sort="asc">Posted</th>
              <th width="200" class="text-center border-top-0 sort" data-sort="asc">Answered</th>
              <th width="75" class="text-center border-top-0 sort" data-sort="asc">Views</th>
              <th width="75" class="border-top-0">&nbsp;</th>
            </tr>
          </thead>
          <tbody class="text-center" id="latest-forum-container">
            @foreach($forums as $post)
              @continue(empty($post->user))
              @include('posts._forum-table-row', ['post' => $post])
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    @include('_partials/row-loader', [
      'id' => 'forum-loader-container', 'idURL' => 'next-latest-forum', 'url' => $nextForumURL, 'variant' => 'light', 'collapseTo' => $forums->count(), 'collapseItems' => '#latest-forum-container > tr'
    ])
    <div class="row">
      <div class="col-12 text-center">
        <a href="forum" class="btn btn-light rounded text-secondary px-4 py-2">JOIN OUR FORUM</a>
      </div>
    </div>
  </div>
</div>
@endif

@if($blogs->count())
<div class="pt-2 pb-5 bg-white">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 class="section-title">Blog</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="card-columns card-col-3" id="latest-blog-container">
        @foreach($blogs as $post)
          @include('_partials/card-post', ['post' => $post])
        @endforeach
        </div>
      </div>
    </div>
    @include('_partials/row-loader', [
      'id' => 'blog-loader-container', 'idURL' => 'next-latest-blog', 'url' => $nextBlogsURL, 'collapseTo' => $blogs->count(), 'collapseItems' => '#latest-blog-container > div'
    ])
  </div>
</div>
@endif

@if($testimonials->count())
<div class="pt-2 pb-5 bg-gradient" style="margin-top:-120px;">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="section-title text-white">What our users says</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="card-columns" id="testimonial-container">
          @forelse($testimonials as $testimonial)
            @include('_partials/card-testimonial', ['testimonial' => $testimonial])
          @empty
          <p class="bg-white text-center p-3 rounded">No data available</p>
          @endforelse
        </div>
      </div>
    </div>
    @include('_partials/row-loader', [
      'id' => 'testimonial-loader-container', 'idURL' => 'next-testimonials', 'url' => $nextTestimonialsURL, 'variant' => 'light', 'collapseTo' => $testimonials->count(), 'collapseItems' => '#testimonial-container > div'
    ])
  </div>
</div>
@endif
@stop

@if($_NOTIFICATION_INTERVAL_ > 0)
@push('scripts')
<script>
  function userActivities(){
    let $container = $('#user-activities-container');
    let o = $container.data('offset');

    $.get('user-activities?latest='+$container.data('latest'))
      .done(function(response){
        if(response.length == undefined){
          $container.data('offset', o + 1)
          if($container.find('div.col-md-6').length == $container.data('max-items')){
            $container.find('div.col-md-6:first-child').fadeOut('slow');
            setTimeout(function(){
              $container.find('div.col-md-6:first-child').remove();
              callback(response);
            }, 500)
          }else{
            callback(response);
          }
        }
      })
  }
  
  setInterval(function(){
    userActivities();
  }, 1000*60*{{$_NOTIFICATION_INTERVAL_}})
  </script>
@endpush
@endif