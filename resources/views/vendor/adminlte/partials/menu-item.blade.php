<!-- {{-- /*
@if (is_string($item))
    <li class="header">{{ $item }}</li>
@else
    <li class="{{ $item['class'] }}">
        <a href="{{ $item['href'] }}"
           @if (isset($item['target'])) target="{{ $item['target'] }}" @endif
        >
            <i class="fa fa-fw fa-{{ $item['icon'] or 'circle-o' }} {{ isset($item['icon_color']) ? 'text-' . $item['icon_color'] : '' }}"></i>
            <span>{{ $item['text'] }}</span>
            @if (isset($item['label']))
                <span class="pull-right-container">
                    <span class="label label-{{ $item['label_color'] or 'primary' }} pull-right">{{ $item['label'] }}</span>
                </span>
            @elseif (isset($item['submenu']))
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
            @endif
        </a>
        @if (isset($item['submenu']))
            <ul class="{{ $item['submenu_class'] }}">
                @each('adminlte::partials.menu-item', $item['submenu'], 'item')
            </ul>
        @endif
    </li>
@endif
*/ --}} -->

@php
$userRoles = Auth::user()->roles()->pluck('name')->toArray();
// dd($userRoles);
@endphp

<li><a href="#"><i class="fa fa-user"></i> {{Auth::user()->fullName}}</a></li>
<li class="header">Main Menu</li>
<li><router-link to="/">Dashboard</router-link>
@if(count(array_intersect(['super-admin'], $userRoles)) > 0)
<li class="treeview">
	<a href="#">
		<i class="fa fa-fw fa-users"></i> Manage Users
		<span class="pull-right-container">
		<i class="fa fa-angle-left pull-right"></i>
		</span>
	</a>
	<ul class="treeview-menu">
		<li class="treeview">
			<router-link to="/users">
				<i class="fa fa-fw fa-list"></i>
				<span>User List</span>
			</router-link>
		</li>
		<li class="treeview">
			<router-link to="/user/roles">
				<i class="fa fa-fw fa-circle-o"></i>
				<span>User Roles</span>
			</router-link>
		</li>
	</ul>
</li>
@endif
@if(count(array_intersect(['super-admin', 'sub-admin'], $userRoles)) > 0)
<li class="treeview">
	<a href="#">
		<i class="fa fa-fw fa-newspaper-o"></i> Manage Blogs
		<span class="pull-right-container">
		<i class="fa fa-angle-left pull-right"></i>
		</span>
	</a>
	<ul class="treeview-menu">
		<li class="treeview">
			<router-link to="/blog/posts">
				<i class="fa fa-fw fa-list"></i>
				<span>Post List</span>
			</router-link>
		</li>
		<li class="treeview">
			<router-link to="/blog/post/categories">
				<i class="fa fa-fw fa-circle-o"></i>
				<span>Post Categories</span>
			</router-link>
		</li>
		<li class="treeview">
			<router-link to="/blog/post/comments">
				<i class="fa fa-fw fa-comment"></i>
				<span>Comments</span>
			</router-link>
		</li>
	</ul>
</li>
@endif
@if(count(array_intersect(['super-admin', 'sub-admin'], $userRoles)) > 0)
<li class="treeview">
	<a href="#">
		<i class="fa fa-fw fa-comments"></i> Manage Forum
		<span class="pull-right-container">
		<i class="fa fa-angle-left pull-right"></i>
		</span>
	</a>
	<ul class="treeview-menu">
		<li class="treeview">
			<router-link to="/forum/posts">
				<i class="fa fa-fw fa-list"></i>
				<span>Forum List</span>
			</router-link>
		</li>
		<li class="treeview">
			<router-link to="/forum/post/categories">
				<i class="fa fa-fw fa-circle-o"></i>
				<span>Categories</span>
			</router-link>
		</li>
		<li class="treeview">
			<router-link to="/forum/post/comments">
				<i class="fa fa-fw fa-comment"></i>
				<span>Comments</span>
			</router-link>
		</li>
	</ul>
</li>
@endif
@if(count(array_intersect(['super-admin', 'sub-admin'], $userRoles)) > 0)
<li class="treeview">
	<a href="#">
		<i class="fa fa-fw fa-university"></i> Manage Universities
		<span class="pull-right-container">
		<i class="fa fa-angle-left pull-right"></i>
		</span>
	</a>
	<ul class="treeview-menu">
		<li class="treeview">
			<router-link to="/universities">
				<i class="fa fa-fw fa-list"></i>
				<span>University List</span>
			</router-link>
		</li>
		<li class="treeview">
			<router-link to="/university/subjects">
				<i class="fa fa-fw fa-check"></i>
				<span>Subjects</span>
			</router-link>
		</li>
		<li class="treeview">
			<router-link to="/university/programs">
				<i class="fa fa-fw fa-circle-o"></i>
				<span>Programs</span>
			</router-link>
		</li>
		<li class="treeview">
			<router-link to="/university/reviews">
				<i class="fa fa-fw fa-star-half-o"></i>
				<span>Reviews</span>
			</router-link>
		</li>
	</ul>
</li>
@endif
@if(count(array_intersect(['super-admin', 'sub-admin'], $userRoles)) > 0)
<li class="treeview">
	<a href="#">
		<i class="fa fa-fw fa-cog"></i> Manage Services
		<span class="pull-right-container">
		<i class="fa fa-angle-left pull-right"></i>
		</span>
	</a>
	<ul class="treeview-menu">
		<li class="treeview">
			<router-link to="/services">
				<i class="fa fa-fw fa-list"></i>
				<span>Service List</span>
			</router-link>
		</li>
		<li class="treeview">
			<router-link to="/service/categories">
				<i class="fa fa-fw fa-circle-o"></i>
				<span>Categories</span>
			</router-link>
		</li>
		<li class="treeview">
			<router-link to="/service/reviews">
				<i class="fa fa-fw fa-star-half-o"></i>
				<span>Reviews</span>
			</router-link>
		</li>
	</ul>
</li>
@endif
@if(count(array_intersect(['super-admin', 'sub-admin'], $userRoles)) > 0)
<li class="treeview">
	<a href="#">
		<i class="fa fa-fw fa-calendar"></i> Manage Events
		<span class="pull-right-container">
		<i class="fa fa-angle-left pull-right"></i>
		</span>
	</a>
	<ul class="treeview-menu">
		<li class="treeview">
			<router-link to="/events">
				<i class="fa fa-fw fa-list"></i>
				<span>Event List</span>
			</router-link>
		</li>
	</ul>
</li>
@endif
@if(count(array_intersect(['super-admin', 'sub-admin'], $userRoles)) > 0)
<li class="treeview">
	<a href="#">
		<i class="fa fa-fw fa-cog"></i> Configurations
		<span class="pull-right-container">
		<i class="fa fa-angle-left pull-right"></i>
		</span>
	</a>
	<ul class="treeview-menu">
		<li class="treeview">
			<router-link to="/notification">
				<i class="fa fa-fw fa-list"></i>
				<span>notification</span>
			</router-link>
		</li>
	</ul>
</li>
@endif
