<!DOCTYPE html>
<html>
<head>
    <base href="{{ config('app.url') }}/" title="{{ config('app.name') }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="{{ asset('img/favico-blue.png') }}" rel="icon">
    <title>
			@yield('title_prefix', config('adminlte.title_prefix', ''))
			@yield('title', config('adminlte.title', 'AdminLTE 2'))
			@yield('title_postfix', config('adminlte.title_postfix', ''))
		</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="api-token" content="{{Auth::check()? Auth::user()->api_token : '' }}">
    <link href="{{ asset('css/v/app.css?_='.time()) }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/jquery-bar-rating/dist/themes/fontawesome-stars.css') }}">

    @yield('adminlte_css')

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
    html, body{font-size:14px;}
    </style>
</head>
<body class="hold-transition @yield('body_class')">

<div id="app">
@yield('body')
</div>    
	<script src="{{ asset('js/v/manifest.js?_='.time()) }}"></script> 
	<script src="{{ asset('js/v/vendor.js?_='.time()) }}"></script> 
	<script src="{{ asset('js/v/app.js?_='.time()) }}"></script> 
@yield('adminlte_js')

</body>
</html>
