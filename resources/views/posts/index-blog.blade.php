@extends('page', ['body_class' => 'blogs', 'footerStyle' => ''])

@section('content')
<div class="hero-area pt-5 pt-md-0">
	<div id="hero-slider" class="carousel slide carousel-fade" data-ride="carousel" data-interval="3000" data-pause="false">
		<ol class="carousel-indicators circle">
			<li data-target="#hero-slider" data-slide-to="0" class="active"></li>
			<li data-target="#hero-slider" data-slide-to="1"></li>
		</ol>
		<div class="carousel-inner">
			<div class="carousel-item active">
				<img class="d-block w-100" src="{{asset('img/hero-slider-1.png')}}" alt=""> </div>
			<div class="carousel-item">
				<img class="d-block w-100" src="{{asset('img/hero-image-event.png')}}" alt=""> </div>
			<div class="carousel-caption">
				<h2 class="hero-title">Top 4 Reasons To Study In London</h2>
			</div>
		</div>
	</div>
</div>

<div class="py-5">
@if($popular->count())
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="section-title">Popular Blog Posts</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-12 mb-3 p-3">
				<div class="card-columns card-col-3">
				@foreach($popular->get() as $post)
					@include('_partials.card-post', $post)
				@endforeach
				</div>
			</div>
		</div>
	</div>
@endif
</div>

<div>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="section-title">Latest Blog</h1>
			</div>
		</div>
	</div>
	<div class="container-fluid bg-gradient mb-5">
		<div class="row">
			<div class="col-12 pt-2">
				<ul id="post-cateogories" class="text-center">
					<li><a href="blog/latest?clear=true" class="text-white btn-action" style="text-decoration:underline;font-weight:bold;">All</a></li>
				@foreach($categories as $category)
					<li><a href="blog/{{$category->slug}}/latest?clear=true" class="text-white btn-action" data-loader=".progress-overlay">{{$category->name}}</a></li>
				@endforeach
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="pb-5">
	<div class="container">
		@if($latest->count())
		<div class="row">
			<div class="col-12 mb-3 p-3">
				<div class="progress-overlay" style="display:none;">
						<div><i class="fa fa-spinner fa-spin fa-2x"></i></div>
				</div>
				<div class="card-columns card-col-3" id="latest-blog-container">
					@foreach($latest->get() as $post)
						@include('_partials.card-post', $post)
					@endforeach
				</div>
			</div>
		</div>
			@if(!empty($nextBlogURL))
				@include('_partials.row-loader', [
					'id' => 'blog-loader-container', 'idURL' => 'next-latest-blog', 'url' => $nextURL, 'style' => 'margin-bottom:-100px;', 'loader' => '.progress-overlay'
				])
			@endif
		@endif
	</div>
</div>

@stop

@push('styles')
<style>
	ul#post-cateogories{
		list-style: none;
	}
	ul#post-cateogories li{
		display: inline;
		padding: 10px 5px;
	}
</style>
@endpush

@push('scripts')
<script>
	$(function(){
		$('#post-cateogories').on('click', 'a', function(){
			$('#post-cateogories').find('a').css({
				textDecoration: 'none',
				fontWeight: 'normal'
			});

			$(this).css({
				textDecoration: 'underline',
				fontWeight: 'bold'
			});			
		})
	})
</script>
@endpush