@extends('page', ['body_class' => 'forum-listing', 'footerStyle' => 'margin-top:-150px'])

@section('content')

<div class="mt-5 pt-5 pb-4">
	<div class="container">
		<div class="progress-overlay">
				<div><i class="fa fa-spinner fa-spin fa-2x"></i></div>
		</div>
		<form id="form-search-forum">
		<div class="row">
			<div class="col-12 col-md-9 mx-auto">
				<h1 class="section-title text-center mb-2">Filter</h1>
				<div data-toggle="buttons" class="btn-group btn-secondary-light mb-5 shadow w-100">
					<div class="container-fluid px-0">
						<div class="row">
							<div class="col-12 col-md-6 pr-md-0 d-flex">
								<label class="btn btn-secondary active form-check-label flex-fill w-50 w-md-auto">
									<input name="sort" type="radio" value="popular" class="form-check-input" checked=""> Popular</label>
								<label class="btn btn-secondary form-check-label flex-fill  w-50 w-md-auto">
									<input name="sort" type="radio" value="recent" class="form-check-input"> Recent</label>
							</div>
							<div class="col-12 col-md-6 pl-md-0 d-flex border-left-md-secondary">
								<label class="btn btn-secondary form-check-label flex-fill w-50 w-md-auto">
									<input name="sort" type="radio" value="viewed" class="form-check-input"> Most Viewed</label>
								<label class="btn btn-secondary form-check-label flex-fill w-50 w-md-auto">
									<input name="sort" type="radio" value="new" class="form-check-input"> New Question</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<h1 class="section-title text-center mb-2">Category</h1>
				@if($categories->count())
				<div id="category-container" data-toggle="buttons" class="btn-group btn-secondary-light mb-5 w-100 d-none d-md-flex shadow">
					<label class="btn btn-secondary form-check-label flex-fill px-xs-1 active">
						<input name="category" type="radio" value="" class="form-check-input" checked> All
									</label>								

					@foreach($categories as $category)
					<label class="btn btn-secondary form-check-label flex-fill px-xs-1">
						<input name="category" type="radio" value="{{ $category->slug}}" class="form-check-input"> {{$category->name}}</label>
						@break($loop->index == 3)
					@endforeach

					@if($categories->count() > 3)
					<label class="btn btn-secondary dropdown-toggle flex-fill px-xs-1 mb-0" data-toggle="dropdown">Others</label>
					<div class="dropdown-menu">
						@foreach($categories as $category)
							@continue($loop->index < 4)
							<label class="btn btn-secondary form-check-label flex-fill px-xs-1 dropdown-item"><input name="category" type="radio" value="{{ $category->slug}}" class="form-check-input"> {{$category->name}}</label>
						@endforeach
								</div>
								@endif
							</div>
				@endif
				<div class="row d-md-none">
						<div class="col-md-6">
						<select name="category">
							<option value="">All</option>
							@if($categories->count())
								@foreach($categories as $category)
								<option value="{{ $category->slug}}">{{$category->name}}</option>
								@endforeach
							@endif
							</select>
						</div>
					</div>
			</div>
		</div>
		</form>
		@auth
		<div class="row">
			<div class="col-12 text-center">
				<hr>
				<a href="#modal-add-forum" data-toggle="modal" class="btn btn-light btn-lg bg-white shadow text-secondary font-weight-bold"> Add Questions </a>
			</div>
		</div>
		@endauth
	</div>
</div>
<div class="pb-5">
	<div class="container">
		<div class="row">
			<div class="progress-overlay">
					<div><i class="fa fa-spinner fa-spin fa-2x"></i></div>
			</div>
			<div class="col-md-12">
				<table class="table-responsive table w-100 table-valign-middle spacing-table">
					<thead class="text-secondary">
						<tr>
							<th width="600" class="text-center border-top-0">Topic</th>
							<th width="200" class="text-center border-top-0">Posted</th>
							<th width="200" class="text-center border-top-0">Answered</th>
							<th width="75" class="text-center border-top-0">Views</th>
							<th width="75" class="border-top-0">&nbsp;</th>
						</tr>
					</thead>
					<tbody class="text-center" id="post-forum-container">
					</tbody>
				</table>
			</div>
		</div>
		@include('_partials.row-loader', [
			'id' => 'post-loader-container', 'idURL' => 'next-post', 'url' => $nextURL
		])
	</div>
</div>


<div class="modal fade" id="modal-add-forum" tabindex="-1" role="dialog" aria-labelledby="modal-add-forum" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<button data-dismiss="modal" aria-label="Close" class="close position-absolute p-2 rounded-circle" style="top:0;right:0; background-color: rgba(255, 255, 255, .5);z-index:1;">
					<img class="" src="{{asset('img/icon/delete.png')}}" height="20"> </button>
					<div class="d-flex justify-content-start align-items-center">
						<figure class="w-10 mr-1 mb-0">
							<img src="{{(Auth::check())? Auth::user()->avatar : 'img/avatar.png'}}" class="rounded-circle border w-100">
						</figure>
						<span class="font-weight-bold">Ask a Question</span> 
					</div>
					<hr>
					<form method="post" action="forum/post/create" id="form-post">
						<div class="form-group">
							<textarea name="title" class="form-control" rows="3" placeholder="Ask Your Questions here..."></textarea>
							{{-- <input type="text" class="form-control" name="title" required placeholder="Ask Your Questions here..."> --}}
						</div>
						<div class="form-group">
							<select name="category" required style="width:100%;">
								@if($categories->count())
									@foreach($categories as $category)
									<option value="{{$category->id}}">{{(strtolower($category->name) == 'uncategorized')? 'Select Category' : $category->name}}</option>					
									@endforeach
								@endif
							</select>
						</div>
						{{-- <div class="form-group">
							<textarea name="description" class="form-control editor" rows="1"></textarea>
						</div>
						<div class="form-group">
							<button class="btn btn-outline-secondary position-relative btn-sm">
								Feature Image <i class="fa fa-camera"></i>
								<input type="file" name="file" onclick="_fileupload(this)" data-action="post/hero/upload">
							</button>
							<span class="btn btn-outline-danger btn-sm" id="btn-remove-hero-image" style="display:none;margin-top:10px;">
								<i class="fa fa-close"></i>
							</span>
							<figure class="file-container position-relative">
								<img class="img-fluid img-thumbnail" id="post-hero-image">
								<div class="upload-progress"></div>
								<input type="hidden" name="hero_image">
							</figure>
						</div> --}}
						<div class="form-group text-center">
							<hr class="mt-1">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					
					</form>
			</div>
		</div>
	</div>
</div>
@stop

@push('styles')
<style>
[data-toggle="buttons"] label{
	border-radius: 0 !important;
}
</style>
@endpush
@push('scripts')
<script>
	let isSearching = false;
	$(function(){
		$(document).on('change click','input[type="radio"]', function(){
			search()
		})
		$('#category-container label.form-check-label').click(function(e){
			let $el = $(this)
			let $parent = $el.parents('#category-container');
			
			$parent.find('label').removeClass('active');
			if($el.hasClass('dropdown-item')){
				$parent.find('label.dropdown-toggle').addClass('active').text($(this).text())
			}else{
				$parent.find('label.dropdown-toggle').removeClass('active').text('Others')
			}
		});
		
		$( window ).resize(function() {
			endisCategory();
		});

		@guest
		$('#modal-login').modal('show')
		@endguest
	})
	
	
	function endisCategory(){
		if($( window ).width() <= 736){
			$('#form-search-forum select').prop('disabled', false)
			$('#category-container input').prop('disabled', true)
		}else{
			$('#form-search-forum select').prop('disabled', true)
			$('#category-container input').prop('disabled', false)
		}
	}
	function search(){
		if(isSearching) return;
		
		let data = $('#form-search-forum').serialize()
				
		$('#form-search-forum').find('input, select').prop('disabled', true)
		$('.progress-overlay').css('display', 'flex')
		$('#latest-forum-container').empty()
		$('i.fa-spinner').show()
		isSearching = true;

		$.get('search/forum', data)
			.done(function(response){
				callback(response);
			})
			.fail(function(error){
				alert(getErrorMessage(error))
			})
			.always(function(){
				$('input, select').prop('disabled', false)
				$('i.fa-spinner, .progress-overlay').hide()
				isSearching = false;
				endisCategory();
			})
	}
	setTimeout(function(){
		_tinymce('textarea.editor');
		endisCategory()
		search();
	}, 3000)
</script>
@endpush