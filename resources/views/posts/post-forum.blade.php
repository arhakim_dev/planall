@extends('page', ['body_class' => 'forum', 'footerStyle' => 'margin-top:-145px'])

@section('content')

<div class="hero-area pb-1 pt-5 h-100 overlay" style="top: 80px; background-image: url({{$post->hero_image}}); background-size: cover; background-repeat: no-repeat;background-position:center center;">
	<div class="container h-100">
		<div class="row h-75">
			<div class="col-12 text-center pt-3 h-100">
				<div class="h-100 d-flex flex-column align-items-center justify-content-end">
					<figure class="p-1 bg-white rounded-circle w-50 w-sm-25">
						<img src="{{$post->user->avatar}}" class="img-fluid d-block rounded-circle w-100"> </figure>
					<span class="text-white font-weight-bold fs-7">{{$post->user->fullName}}</span>
				</div>
			</div>
		</div>
		<div class="row text-white hero-info">
			<div class="col-12 col-sm-6 text-center text-sm-left">
				<p class="mb-1">Category: <a href="{{$post->category->url}}" class="text-white">{{$post->category->name}}</a></p>
				<p class="mb-1">Posted on {{$post->created_at->format('F d, Y')}}</p>
				<div class="d-flex d-md-block justify-content-around mb-3 mb-md-0">
					<p class="mb-1">
						<img src="img/icon/eye.png"> {{$post->views}}</p>
					<p class="mb-1">
						<img src="img/icon/thumbs-up.png"> <span class="post-{{$post->id}}-total-likes">{{$post->likes}}</span></p>
					<p class="mb-1">
						<img src="img/icon/thumbs-down.png"> <span class="post-{{$post->id}}-total-dislikes">{{$post->dislikes}}</span></p>
				</div>
			</div>
			<div class="col-12 col-sm-6 d-flex align-items-center align-items-md-end justify-content-end">
				<div class="w-100 w-md-75 justify-content-around d-flex">
					<i class="fa fa-twitter fa-2x"></i>
					<i class="fa fa-facebook fa-2x"></i>
					<i class="fa fa-linkedin fa-2x"></i>
					<i class="fa fa-google-plus fa-2x"></i>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="py-5">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="section-title fs-4 fs-md-9">{{$post->title}}
					<hr class="mb-0"> </h2>
				<div class="d-flex justify-content-between align-items-center mb-5">
					<span class="fs-4 fs-md-9 font-weight-bold text-secondary align-items-start">Replies</span>
					<div>
					@auth
						<a href="#modal-comment" data-toggle="modal" data-title="{{$post->title}}" class="btn btn-lg text-white px-4 bg-gradient-vertical ml-auto"> Answer </a>
					@endauth
					</div>
				</div>
				@if(!empty($post->description))
					@include('posts._forum-comment', ['comment' =>$post])
				@endif
				<div id="forumcomment-container">
						{!!$comments!!}
				{{-- @forelse($comments as $comment)
					@include('posts._forum-comment', ['comment' =>$comment, 'isComment' => true])
				@empty

				@endforelse --}}
				</div>
			</div>
		</div>
		@include('_partials.row-loader', [
			'id' => 'forumcmt-loader-container', 'idURL' => 'next-forum-comment', 'url' => $nextCommentsURL
		])
		@auth
		<div class="row mb-5">
			<div class="col-12">
				<h2 class="section-title fs-4 fs-md-9">Was this Helpful? </h2>
				<div class="d-flex px-lg-5 mx-lg-5 justify-content-around">
					<form method="post" action="post/{{$post->id}}/likes" class="w-50 w-md-20">
						<button type="submit" class="btn btn-light shadow bg-white w-100 py-3">
							<img src="img/icon/thumbs-up-blue.png" class="" width="80"> </button>
					</form>
					<form method="post" action="post/{{$post->id}}/dislikes" class="w-50 w-md-20">
						<button type="submit" class="btn btn-light shadow bg-white w-100 py-3">
							<img src="img/icon/thumbs-down-blue.png" width="80"> </button>
					</form>
				</div>
			</div>
		</div>
		@endauth
		<div class="row">
			<div class="col-md-12">
				<h2 class="section-title fs-4 fs-md-9">Related questions</h2>
				<table class="text-center table-responsive table w-100 table-valign-middle spacing-table sortable">
					<thead class="text-secondary">
						<tr>
							<th width="500" class="text-left pl-3 border-top-0">Topic</th>
							<th width="150" class="border-top-0">Posted</th>
							<th width="150" class="border-top-0">Answered</th>
							<th width="150" class="border-top-0">Views</th>
							<th width="150" class="border-top-0">&nbsp;</th>
						</tr>
					</thead>
					<tbody id="related-forum-container">
					@if($related->count())
						@foreach($related->get() as $_post)							
							@include('posts._forum-table-row', ['post' => $_post])
						@endforeach
					@endif
					</tbody>
				</table>
			</div>
		</div>
		@include('_partials.row-loader', [
			'id' => 'related-loader-container', 'idURL' => 'next-related', 'url' => $nextRelatedsURL
		])
	</div>
</div>

<div class="modal fade" id="modal-comment" tabindex="-1" role="dialog" aria-labelledby="modal-comment" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					
				<h5 class="modal-title">Reply to: <span id="title"></span></h5>
				<hr>
				<form class="w-100" method="post" action="post/comment" id="form-comment">
					<input type="hidden" name="post_id" value="{{$post->id}}">
					<input type="hidden" name="replied_to" id="replied_to">
					<figure class="mx-auto w-15 rounded-circle bg-white p-1 d-none d-md-block" style="margin-bottom:-100px;">
						<img src="{{ (Auth::check())? Auth::user()->avatar : asset('img/blank-image.jpg')}}" class="img-fluid rounded-circle"> </figure>
					<div class="d-md-flex justify-content-between py-2">
						<div class="w-100 w-md-50 form-group">
							<input type="text" class="form-control w-100 w-md-75" name="name" placeholder="Name" required value="{{ (Auth::check())? Auth::user()->fullName : ''}}" {{ (Auth::check())? 'readonly' : ''}}> </div>
						<div class="w-100 w-md-50 form-group">
							<input type="email" class="form-control w-100 w-md-75 float-right" name="email" placeholder="Email" required value="{{ (Auth::check())? Auth::user()->email : ''}}" {{ (Auth::check())? 'readonly' : ''}}> </div>
					</div>
					<div class="pt-4 form-group">
						<textarea class="form-control" rows="5" name="description"  required></textarea>
					</div>
					<div class="form-group">
						<button class="btn btn-primary px-4 float-right" type="submit">Post</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop

@push('scripts')
<script>
$(function(){
  $(document).on('click', '.scroll2-comment', (e) => {
    e.preventDefault();
    let $btn = $(e.target);
 
    $('html, body').animate({
      scrollTop: $('#comment-'+$btn.data('replied-id')).offset().top - 80
    }, 700)
  });
	$('#modal-comment').on('show.bs.modal', function(e){
		let $btn = $(e.relatedTarget)
		let $modal = $(this);
		let modalTitle = $btn.data('title');
	
		$modal.find('.modal-title span#title').text(modalTitle);
    $('#form-comment #replied_to').val($btn.data('comment-id'))
	})

	
	@guest
		$('#modal-login').modal('show')
	@endguest
})
</script>
@endpush