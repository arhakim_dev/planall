@extends('page', ['body_class' => 'blogs', 'footerStyle' => ''])

@section('content')
  <div class="hero-area h-100 mb-5 overlay" style="top: 80px; background-image: url({{asset('img/hero-slider-1.png')}}); background-size: cover; background-repeat: no-repeat;background-position:center center;">
    <div class="text-center d-flex justify-content-center align-items-center h-75 mt-5 pt-3">
      <h2 class="hero-title w-75">{{$post->title}}</h2>
    </div>
    <div class="container mt-lg-4 pt-lg-3 hero-info">
      <div class="row"> </div>
      <div class="row text-white text-center text-sm-left font-weight-bold">
        <div class="col-sm-6 col-md-4">
          <div class="">
          <p class="mb-1"> Category: 
            @if($post->category)
            <a href="{{$post->category->url}}" class="text-white">{{$post->category->name}}</a>
            @endif
          </p>
          <p class="mb-1"> Posted on {{date('F d, Y', strtotime($post->created_at))}}</p>
          <p class="mb-1">
            <img src="{{asset('img/icon/eye.png')}}"> {{$post->views}} </p>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 d-none d-md-block">
          <figure class="shadow rounded-circle p-1 bg-white w-50 mx-auto" style="margin-bottom: -100px">
            <img src="{{asset($post->user->avatar)}}" class="img-fluid rounded-circle"> </figure>
        </div>
        <div class="col-sm-6 col-md-4 d-flex align-items-center align-items-lg-end justify-content-center">
          @if(!empty($post->user->twitter))
          <a href="{{$post->user->twitter}}" class="text-white mr-4 mb-1" target="_blank"><img src="{{asset('img/icon/twitter-white.png')}}"></a>
          @endif
          @if(!empty($post->user->facebook))
          <a href="{{$post->user->facebook}}" class="text-white mr-4 mb-1" target="_blank"><img src="{{asset('img/icon/fb-white.png')}}"></a>
          @endif
          @if(!empty($post->user->linkedin))
          <a href="{{$post->user->linkedin}}" class="text-white mr-4 mb-1" target="_blank"><img src="{{asset('img/icon/linkedin-white.png')}}"></a>
          @endif
          @if(!empty($post->user->google))
          <a href="{{$post->user->google}}" class="text-white mb-1" target="_blank"><img src="{{asset('img/icon/g-plus-white.png')}}"></a>
          @endif
        </div>
      </div>
    </div>
  </div>
  <div class="pt-5 text-justify mb-5">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          {!! $post->description !!}
          <hr>
          <div class="card shadow">
            <div class="card-body">
              <div class="row">
                <div class="col-12 col-md-3">
                  <img class="img-thumbnail rounded-circle img-fluid" src="{{asset($post->user->avatar)}}"> </div>
                <div class="col-12 col-md-9 d-flex align-items-end flex-column">
                  <div class="card-title text-secondary font-weight-bold w-100">{{$post->user->fullName}}
                    <br>
                    <small class="text-muted">{{$post->user->profession}}</small>
                  </div>
                  <p class="text-muted">{{$post->user->about}}</p>
                  <div class="text-right mt-auto">
                    <a href="{{$post->user->twitter}}" target="_blan">
                        <img src="{{asset('img/icon/twitter-color.png')}}">
                    </a>&nbsp;
                    <a href="{{$post->user->facebook}}" target="_blan">
                        <img src="{{asset('img/icon/fb-color.png')}}">
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="pt-2 bg-gradient text-white font-weight-bold mb-5">
    <div class="container">
      <div class="row text-center">
        <div class="col-md-6">
          <p class="fs-4 mb-2">STAY UP TO DATE</p>
          <p>Sign-up to receive our posts</p>
        </div>
        <div class="col-md-6 d-md-flex align-items-center">
          <form class="form-inline" method="post" action="mailing-list" id="form-subscriber">
            <div class="form-group w-100 w-md-75">
              <input type="email" class="form-control mr-2 w-100" placeholder="Email" name="email" required> </div>
            <div class="form-group w-100 w-md-25 my-3">
              <button class="btn btn-outline-light mx-auto" type="submit">Sign Up</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="mb-5">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="section-title">Related blog posts</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-12 mb-3 p-3">
          @if($related->count())					
          <div class="card-columns card-col-3">
            @foreach($related->get() as $_post)
              @include('_partials.card-post', ['post' => $_post])
            @endforeach
          </div>
          @endif
        </div>
      </div>
    </div>
  </div>
  <div class="mb-5">
    <div class="container">
      <div class="row">
        <div class="col-12" id="comment-container">
          <h2 class="section-title">Comments</h2>
					<div id="blogcomment-container">
					{!!$comments!!}
					</div>
        </div>
      </div>
			@include('_partials.row-loader', [
				'id' => 'blogcmt-loader-container', 'idURL' => 'next-blogcomment', 'url' => $nextCommentsURL, 'style' => 'margin-bottom:-100px;', 'variant' => 'light'
			])
    </div>
  </div>
  <div class="py-5 bg-gradient" id="form-comment-container">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="section-title text-white text-left fs-4 fs-md-9">Leave a comment</h2>
          <form class="w-100" method="post" action="post/comment" id="form-comment">
						<input type="hidden" name="post_id" value="{{$post->id}}">
						<input type="hidden" name="replied_to" id="replied_to">
            <figure class="mx-auto w-15 rounded-circle bg-white p-1 d-none d-md-block" style="margin-bottom:-100px;">
						  <img src="{{ (Auth::check())? Auth::user()->avatar : asset('img/blank-image.jpg')}}" class="img-fluid rounded-circle"> </figure>
            <div class="d-md-flex justify-content-between py-2">
              <div class="w-100 w-md-50 form-group">
                <input type="text" class="form-control w-100 w-md-75" name="name" placeholder="Name" required value="{{ (Auth::check())? Auth::user()->fullName : ''}}" {{ (Auth::check())? 'readonly' : ''}}> </div>
              <div class="w-100 w-md-50 form-group">
                <input type="email" class="form-control w-100 w-md-75 float-right" name="email" placeholder="Email" required value="{{ (Auth::check())? Auth::user()->email : ''}}" {{ (Auth::check())? 'readonly' : ''}}> </div>
            </div>
            <div class="pt-4 form-group">
              <textarea class="form-control" rows="5" name="description"  required id="description"></textarea>
            </div>
            <div class="form-group">
              <button class="btn btn-light px-4 float-right text-secondary" type="submit">Post</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
 
@stop

@push('scripts')
<script>
$(function(){
  $(document).on('click', '.scroll2-comment', (e) => {
    e.preventDefault();
    let $btn = $(e.target);
 
    $('html, body').animate({
      scrollTop: $(document).find('#comment-'+$btn.data('replied-id')).offset().top - 80
    }, 700)
  });
  
  $(document).on('click', '.btn-comment', (e) => {
    e.preventDefault();
    let $btn = $(e.target);
    $('#form-comment #replied_to').val($btn.data('comment-id'))
 
    $('html, body').animate({
      scrollTop: $('#form-comment-container').offset().top
    }, 700)
  });

	@if(request()->query('comment'))
	setTimeout(function(){
		addComment();
	}, 1000);
	@endif

});
</script>

@endpush