<div class="card mb-3 shadow" id="comment-{{ $comment->id }}">
	@if(isset($removable) && $removable == true)
		@if(Auth::check() && Auth::user()->id == $comment->user_id)
	<form method="delete" action="profile/comment/remove/{{ $comment->id }}">
		<div class="btn btn-secondary btn-float-action rounded-circle position-absolute" onclick="confirm(this.title+'?')?$(this).parent('form').submit():''" title="Remove comment">
			<i class="fa fa-close"></i>
		</div>
	</form>
		@endif
	@endif
	<div class="card-body">
		<div class="row">
			<div class="col-md-6 d-flex align-items-center">
				<figure class="rounded-circle w-25 w-md-15 mr-2 mx-auto mx-md-0">
					<img src="{{($comment->user)? $comment->user->avatar : asset('img/avatar.png')}}" class="img-fluid rounded-circle">
				</figure>
			</div>
			<div class="col-md-6 text-center d-md-flex align-items-center text-secondary justify-content-start justify-content-md-end"> {{date('F d, Y - H:ia', strtotime($comment->created_at))}} </div>
		</div>
		<div>
		@if($comment->replied_to && !empty($comment->repliedTo))
			<p class="fs-1">
				<a href="#replied-to-{{$comment->replied_to}}" class="text-muted scroll2-comment" data-replied-id="{{$comment->replied_to}}"><i class="fa fa-comment-o"></i>	Replied to {{@$comment->repliedTo()->name}}</a></p>
		@endif
			<p class="card-text"> {{$comment->description}} </p>
			<i class="fa fa-quote-right"></i>
		</div>
		<div>
			@if(@$disableReply == false)
			<a href="{{$commentURL or '#comment-to-'.$comment->id }}" class="btn-comment" data-comment-id="{{$comment->id}}">Reply
				<i class="fa fa-angle-right"></i>
			</a>
			@endif
		</div>
	</div>
</div>

{{-- <div class="pl-4" id="repies-{{ $comment->id }}-container"></div>
@if($comment->replies()->count())
<div class="py-2 border-top border-bottom text-center">
	<a href="blog/{{ $comment->id }}/comment/replies/next?offset=1&limit3" class="btn-action" id="next-{{ $comment->id }}-replies" data-loader="#next-{{ $comment->id }}-replies .fa-spinner">See More <i class="fa fa-spinner fa-spin" style="display:none;"></i></a>
</div>
@endif --}}