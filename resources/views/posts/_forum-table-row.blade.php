
<tr class="shadow bg-white" id="post-{{ $post->id }}">
	<td class="text-left py-md-4 pl-md-3 font-weight-bold fs-md-4 rounded-left"><a href="{{$post->url}}"> {{$post->title}}</a> </td>
	<td>
		<div>
			<a href="{{$post->user->url}}" target="_blank">
				<small>{{$post->user->fullName}}</small>
			</a>
		</div>
		<div>
			<small>{{$post->created_at->diffForHumans()}}</small>
		</div>
	</td>
	<td>
	@php $lastComment = $post->comments()->latest()->first(); @endphp
	@if($lastComment)
		<div>
			<small>{{$lastComment->name}}</small>
		</div>
		<div>
			<small>{{$lastComment->created_at->diffForHumans()}}</small>
		</div>
	@else
		-
	@endif
	</td>
	<td>{{$post->views}}</td>
	<td class="position-relative rounded-right">
		@if(isset($removable) && $removable == true)
			@if(Auth::check() && Auth::user()->id == $post->user_id)
		<form method="delete" action="profile/post/remove/{{ $post->id }}">
			<div class="btn btn-secondary btn-float-action rounded-circle position-absolute" onclick="confirm(this.title+'?')?$(this).parent('form').submit():''" title="Remove {{$post->title}}" style="right:-5px;">
				<i class="fa fa-close"></i>
			</div>
		</form>
			@endif
		@endif
		<a href="{{$post->url}}" class="btn btn-outline-secondary rounded-circle">
			<i class="fa fa-angle-right"></i>
		</a>
	</td>
</tr>
{{-- <tr class="spacer">
	<td colspan="5">&nbsp;</td>
</tr> --}}