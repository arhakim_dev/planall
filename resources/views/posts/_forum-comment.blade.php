<div class="card mb-5 shadow" id="comment-{{ $comment->id }}">
		<div class="card-body">
			<div class="row mb-3">
				<div class="col-md-6 d-flex align-items-center">
					<figure class="rounded-circle w-25 w-md-15 mr-2">
						<img src="{{($comment->user)? $comment->user->avatar : asset('img/avatar.png')}}" class="img-fluid rounded-circle">
					</figure>
					<span class="text-secondary font-weight-bold"> {{$comment->name}} </span>
				</div>
				<div class="col-md-6 d-flex align-items-center text-secondary justify-content-start justify-content-md-end">{{$comment->created_at->format('F d, Y').' at '.$comment->created_at->format('H:ia')}}</div>
			</div>
			@if($comment->replied_to)
				<p class="fs-1">
					<a href="#replied-to-{{$comment->replied_to}}" class="text-muted scroll2-comment" data-replied-id="{{$comment->replied_to}}"><i class="fa fa-comment-o"></i>	Replied to {{$comment->replied_to_user}}</a></p>
			@endif
			<div class="card-text">
				{!!$comment->description!!}
				<i class="fa fa-quote-right"></i>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-4 d-flex justify-content-center justify-content-md-center py-4 py-md-0">
					@auth
					<a href="#modal-comment" data-toggle="modal" data-title="{{$comment->name}}'s comment" data-comment-id="{{$comment->id}}">Reply
						<i class="fa fa-angle-right"></i>
					</a>
					@endauth
				</div>
				<div class="col-md-4 d-flex justify-content-start justify-content-md-around">
				<form method="post" action="post/{{$comment->id}}/likes{{isset($isComment)?'?comment=true':''}}" class="mr-3 mr-md-0">
						<button type="submit" class="btn bg-white" {{Auth::check()?'':'disabled'}}>
							<img src="img/icon/thumbs-up-blue.png"> <span class="{{(isset($isComment)?'comment':'post').'-'.$comment->id}}-total-likes">{{$comment->likes}}</span>
						</button>
					</form>
					<form method="post" action="post/{{$comment->id}}/dislikes{{isset($isComment)?'?comment=true':''}}" class="mr-3 mr-md-0">
						<button type="submit" class="btn bg-white" {{Auth::check()?'':'disabled'}}>
							<img src="img/icon/thumbs-down-blue.png"> <span class="{{(isset($isComment)?'comment':'post').'-'.$comment->id}}-total-dislikes">{{$comment->dislikes}}</span>
						</button>
					</form>
				</div>
				<div class="col-md-4"> </div>
			</div>
		</div>
	</div>