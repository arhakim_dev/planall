@extends('page', ['body_class' => 'profile'])

@section('content')
	<div class="hero-area pb-1 h-100" style="background-image: url( {{$user->hero_image}} );background-repeat: no-repeat;background-size: cover;">
    <div class="container h-100 pt-md-5">
      <div class="row h-75 mb-5 mb-sm-0 mb-md-1-landscape mb-xl-5-landscape">
        <div class="col-sm-3 col-lg-12"></div>
        <div class="h-100 d-flex flex-column align-items-center justify-content-end col-sm-6 col-lg-12">
          <figure class="bg-white rounded-circle p-1 position-relative file-container" style="height:250px;width:250px;">
            <img class="img-fluid rounded-circle" src="{{$user->avatar}}" id="profile-photo">
						@if(Auth::check() && Auth::user()->id == $user->id)
            {{-- <button class="btn btn-secondary btn-float-action rounded-circle position-absolute" style="top:50px;">
              <i class="fa fa-pencil"></i>
							<input type="file" name="file" data-action="profile/photo">
						</button> --}}
						<button type="button" class="btn btn-secondary rounded-circle position-absolute" style="top:50px;right:0;" data-toggle="modal" data-target="#modal-cavatar">
								<i class="fa fa-pencil"></i>
						</button>
						@endif
						<div class="upload-progress"></div>
          </figure>
          <div class="text-white text-center">
            <h2 class="font-weight-bold"> {{$user->fullName}} </h2>
            <div>
              <i class="fa fa-map-marker"></i> <span id="user-country">{{ empty($user->country)?'': $user->country->name }}</span> </div>
          </div>
        </div>
        <div class="col-sm-3 col-lg-12"></div>
      </div>
      <div class="row text-white px-lg-5">
        <div class="col-12">
          <ul class="nav nav-pills nav-fill tab">
            <li class="nav-item w-md-auto">
              <a href="" class="active nav-link" data-toggle="pill" data-target="#tababout">Info</a>
            </li>
            {{-- <li class="nav-item w-md-auto">
              <a href="" class="nav-link" data-toggle="pill" data-target="#tabfriends">Friends</a>
            </li> --}}
            <li class="nav-item w-md-auto">
              <a href="" class="nav-link" data-toggle="pill" data-target="#tabuniv">Universities</a>
            </li>
            </li>
            <li class="nav-item w-md-auto">
              <a href="" class="nav-link" data-toggle="pill" data-target="#tabservice">Services</a>
            </li>
            <li class="nav-item w-md-auto">
              <a href="" class="nav-link" data-toggle="pill" data-target="#tabblog">Blog</a>
            </li>
            <li class="nav-item w-md-auto">
              <a href="" class="nav-link" data-toggle="pill" data-target="#tabforum">Forum</a>
            </li>
            <li class="nav-item w-md-auto">
              <a class="nav-link" data-toggle="pill" data-target="#tabevent" href="">Regsitered Event</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="py-5 mb-5">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="tab-content">
            <div class="tab-pane active" id="tababout" role="tabpanel">
								<div class="card shadow mb-3">
									<div class="card-header bg-white d-flex justify-content-between align-items-center" data-toggle="collapse" data-target="#collapse-about" aria-expanded="false" aria-controls="collapse-about">
										<h4 class="title font-weight-normal">About</h4>
										<span class="fa fa-angle-up"></span>
									</div>
									<div class="card-body collapse show" id="collapse-about">
										<form class="" method="put" action="profile/info">
											@if(Auth::check() && Auth::user()->id == $user->id)
											<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
											@endif
											<div class="form-group">
												<textarea class="form-control cover-flat text-justify" rows="4" disabled name="about">{{$user->about}}</textarea>
											</div>
										</form>
									</div>
								</div>
								<div class="card shadow mb-3">
									<div class="card-body text-secondary text-center">
										<form class="" method="put" action="profile/info">
											@if(Auth::check() && Auth::user()->id == $user->id)
											<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
											@endif
											<div class="form-group d-md-flex justify-content-between align-items-center mb-0">
												<label class="fs-4">Profession</label>
												<input type="text" class="form-control cover-flat w-50 text-secondary" name="profession" value="{{$user->profession}}" disabled>
											</div>
										</form>
									</div>
								</div>
              <div class="card shadow mb-3">
                <div class="card-body text-secondary text-center">
									<form class="" method="put" action="profile/info">
										@if(Auth::check() && Auth::user()->id == $user->id)
										<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
										@endif
										<div class="form-group d-md-flex justify-content-between align-items-center mb-0">
											<label class="fs-4">Date of birth</label>
											<input type="text" class="form-control datepicker cover-flat w-50 text-secondary" name="birth_date" value="{{$user->birth_date}}" disabled>
										</div>
									</form>
                </div>
              </div>
              <div class="card shadow mb-3">
                <div class="card-body text-secondary text-center">
									<form method="put" action="profile/info">
										@if(Auth::check() && Auth::user()->id == $user->id)
										<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
										@endif
										<div class="form-group d-md-flex justify-content-between align-items-center mb-0">
											<label class="fs-4">Country</label>
											<select name="country_code" class="cover-flat w-100 text-secondary text-align-last-right" disabled>
											@if($countries->count())
												<option value=""></option>
												@foreach($countries as $country)
												<option value="{{$country->code}}" {{($user->country_code == $country->code)? "selected": ""}}>{{$country->name}}</option>
												@endforeach
											@endif
											</select>
										</div>
									</form>
                </div>
              </div>
              <div class="card shadow mb-3">
                <div class="card-body text-secondary text-center">
									<form method="put" action="profile/info">
										@if(Auth::check() && Auth::user()->id == $user->id)
										<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
										@endif
										<div class="form-group d-md-flex justify-content-between align-items-center mb-0">
											<label class="fs-4">Contact Number</label>
											<input type="text" class="form-control cover-flat w-50 text-secondary" disabled name="phone" value="{{$user->phone}}">
										</div>
									</form>
                </div>
              </div>
              <div class="card shadow mb-3">
                <div class="card-body text-secondary text-center">
									<form method="put" action="profile/info">
										@if(Auth::check() && Auth::user()->id == $user->id)
										<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
										@endif
										<div class="form-group d-md-flex justify-content-between align-items-center mb-0">
											<label class="fs-4">Interested Course</label>
											<select name="interest_course" class="cover-flat w-100 text-secondary text-align-last-right" disabled>
											@if($majors->count())
												<option value=""></option>
												@foreach($majors as $major)
												<option value="{{$major->id}}" {{($user->interest_course == $major->id)? "selected": ""}}>{{$major->name}}</option>
												@endforeach
											@endif
											</select>
										</div>
									</form>
                </div>
              </div>
              <div class="card shadow mb-3">
                <div class="card-body text-secondary text-center">
									<form method="put" action="profile/info">
										@if(Auth::check() && Auth::user()->id == $user->id)
										<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
										@endif
										<div class="form-group d-md-flex justify-content-between align-items-center mb-0">
											<label class="fs-4">Interested Location</label>
											<select name="interest_location" class="cover-flat w-100 text-secondary text-align-last-right" disabled>
													@if($countries->count())
													<option value=""></option>
														@foreach($countries as $country)
														<option value="{{$country->code}}" {{($user->interest_location == $country->code)? "selected": ""}}>{{$country->name}}</option>
														@endforeach
													@endif
											</select>
										</div>
									</form>
                </div>
              </div>
              <div class="card shadow mb-3">
                <div class="card-body text-secondary text-center">
									<form method="put" action="profile/info">
										@if(Auth::check() && Auth::user()->id == $user->id)
										<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
										@endif
										<div class="form-group d-md-flex justify-content-between align-items-center mb-0">
											<label class="fs-4">Dream University</label>
											<select name="dream_university" class="cover-flat w-100 text-secondary text-align-last-right" disabled>
											@if($allUniversity->count())
												<option value=""></option>
												@foreach($allUniversity as $university)
												<option value="{{$university->id}}" {{($user->dream_university == $university->id)? "selected": ""}}>{{$university->name}}</option>
												@endforeach
											@endif
											</select>
										</div>
									</form>
                </div>
              </div>
              <div class="card shadow mb-3">
                <div class="card-header bg-white d-flex justify-content-between align-items-center" data-toggle="collapse" data-target="#collapse-socmed" aria-expanded="false" aria-controls="collapse-socmed">
                  <h4 class="title font-weight-normal">Social Media</h4>
                  <span class="fa fa-angle-up"></span>
                </div>
                <div class="card-body collapse show" id="collapse-socmed">
									<form class="" method="put" action="profile/info">
										@if(Auth::check() && Auth::user()->id == $user->id)
										<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
										@endif
										<div class="form-group">
											<div class="row">
												<div class="col-md-7">
													<label>Facebook</label>
												</div>
												<div class="col-md-5">
													<input type="url" class="form-control cover-flat text-secondary clickable" data-validation="url" data-validation-optional="true" data-validation-error-msg="Incorrect URL format. ex: https://facebook.com/your-name" readonly name="facebook" value="{{$user->facebook}}">
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-md-7">
													<label>Twitter</label>
												</div>
												<div class="col-md-5">
													<input type="url" class="form-control cover-flat text-secondary clickable" data-validation="url" data-validation-optional="true" data-validation-error-msg="Incorrect URL format. ex: https://twitter.com/your-name"readonly name="twitter" value="{{$user->twitter}}">
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-md-7">
													<label>Instagram</label>
												</div>
												<div class="col-md-5">
													<input type="url" class="form-control cover-flat text-secondary clickable" data-validation="url" data-validation-optional="true" data-validation-error-msg="Incorrect URL format. ex: https://instagram.com/your-name"readonly name="instagram" value="{{$user->instagram}}">
												</div>
											</div>
										</div>
									</form>
                </div>
              </div>
              <hr>
							@if(Auth::check() && Auth::user()->id == $user->id)
              <div class="py-3 text-center">
                <button class="btn btn-secondary mx-auto py-2" style="background-image: url({{$user->hero_image}}); background-size: cover; background-repeat: no-repeat;background-position:center center;" data-toggle="modal" data-target="#modal-cpassword"> Change Password </button>
              </div>
							@endif
            </div>
            {{-- <div class="tab-pane" id="tabfriends" role="tabpanel">
              <h2 class="section-title"> {{$friends->count()}} friends </h2>
              <ul class="list-group" id="friends-container">
								@if($friends->count())
									@foreach($friends->get() as $friend)
										@include('_partials.list-friend', ['friend' => $friend, 'removable' => true])
									@endforeach
								@else
								@endif
              </ul>
							@include('_partials.row-loader', [
								'id' => 'friend-loader-container', 'idURL' => 'next-friends', 'url' => $nextFriendsURL, 'style' => 'margin-bottom:-30px;'
							])
            </div> --}}
            <div class="tab-pane" id="tabuniv" role="tabpanel">
							<div id="university-container">
							@if(count($universities))
								@foreach($universities as $university)
									@include('universities._box-statistic', ['university' => $university, 'removable' => true, 'user' => $user])
								@endforeach
							@endif
							</div>
							@include('_partials.row-loader', [
								'id' => 'university-loader-container', 'idURL' => 'next-university', 'url' => $nextUniversitiesURL, 'style' => 'margin-bottom:-30px;'
							])
						</div>
						<div class="tab-pane" id="tabservice" role="tabpanel">
							<div id="service-container">
							@forelse($services as $service)
								@include('services/_box-info', ['service' => $service, 'removable' => true, 'user' => $user])
							@empty
								<p class="text-center">No Data Available</p>
							@endforelse
							
							</div>
							@include('_partials.row-loader', [
								'id' => 'service-loader-container', 'idURL' => 'next-service', 'url' => $nextServicesURL, 'style' => 'margin-bottom:-30px;'
							])
						</div>
            <div class="tab-pane" id="tabblog" role="tabpanel">
              <h2 class="section-title"> Your Posts </h2>
							
							<div class="text-center">
								<button class="btn btn-light btn-lg shadow rounded bg-white text-secondary" data-toggle="modal" data-target="#modal-add-blog">Write a Blog</button>
							</div>
							
              <div class="row">
								<div class="col-12 mb-3 p-3">
									<div class="card-columns card-col-3" id="latest-blog-container">
									@if($blogs->count())
										@foreach($blogs as $post)
											@include('_partials.card-post', ['post' => $post, 'removable' => true])
										@endforeach
									@endif
               	 </div>
								</div>
							</div>
							@include('_partials.row-loader', [
								'id' => 'blog-loader-container', 'idURL' => 'next-latest-blog', 'url' => $nextBlogsURL
							])
              <div class="row">
                <div class="col-12">
                  <h2 class="section-title">Your Comments</h2>
									<div id="blogcomment-container">
									@if($blogComments->count())
										@foreach($blogComments as $comment)
											@continue(empty($comment->post))
											@include('posts._blog-comment', [
												'comment' => $comment, 
												'removable' => true, 
												'commentURL' => $comment->post->url.'?comment=true'
											])
										@endforeach
									@endif
									</div>
                </div>
              </div>
							@include('_partials.row-loader', [
								'id' => 'blogcmt-loader-container', 'idURL' => 'next-blogcomment', 'url' => $nextBologCommentsURL, 'style' => 'margin-bottom:-30px;'
							])
            </div>
            <div class="tab-pane" id="tabforum" role="tabpanel">
              <h2 class="section-title"> Your Questions </h2>
              <table class="table-responsive table w-100 table-valign-middle spacing-table">
                <thead class="text-secondary">
                  <tr>
                    <th width="600" class="text-left pl-3 border-top-0">Topic</th>
                    <th width="200" class="border-top-0">Posted</th>
                    <th width="200" class="border-top-0">Answered</th>
                    <th width="75" class="border-top-0">Views</th>
                    <th width="75" class="border-top-0">&nbsp;</th>
                  </tr>
                </thead>
                <tbody id="latest-forum-container">
								@if($forums->count())
									@foreach($forums as $post)
										{{-- @if ($loop->first)
                  <tr class="spacer">
                    <td colspan="5">&nbsp;</td>
                  </tr>
										@endif --}}
										
										@include('posts._forum-table-row', ['post' => $post, 'removable' => true])
									@endforeach
								@endif
								</tbody>
              </table>
							@include('_partials.row-loader', [
								'id' => 'forum-loader-container', 'idURL' => 'next-latest-forum', 'url' => $nextForumURL
							])
              <div class="row">
                <div class="col-12">
                  <h2 class="section-title">Your Comments</h2>
									<div id="forumcomment-container">
									@if($forumComments->count())
										@foreach($forumComments as $comment)
											@continue(empty($comment->post))
											@include('posts._blog-comment', [
												'comment' => $comment, 
												'removable' => true, 
												'commentURL' => $comment->post->url.'?comment=true'
											])
										@endforeach
									@endif
									</div>
                </div>
              </div>
							@include('_partials.row-loader', [
								'id' => 'forumcmt-loader-container', 'idURL' => 'next-forumcomment', 'url' => $nextForumCommentsURL, 'style' => 'margin-bottom:-30px;'
							])
            </div>
            <div class="tab-pane" id="tabevent" role="tabpanel">
              <div class="list-group" id="events-container">
								@if($events->count())
									@foreach($events as $event)
										@include('_partials.list-event', ['event' => $event, 'removable' => true, 'user' => $user])
									@endforeach
								@else
								<p class="text-center">No Data Available</p>
								@endif
              </div>
							@include('_partials.row-loader', [
								'id' => 'event-loader-container', 'idURL' => 'next-events', 'url' => $nextEventsURL, 'style' => 'margin-bottom:-30px;'
							])
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
	
	
  <div class="modal fade" id="modal-cpassword" tabindex="-1" role="dialog" aria-labelledby="modal-password" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <button data-dismiss="modal" aria-label="Close" class="close position-absolute p-2 rounded-circle" style="top:0;right:0; background-color: rgba(255, 255, 255, .5);z-index:1;">
            <img class="" src="{{asset('img/icon/delete.png')}}" height="20"> </button>
          
						<h5 class="mb-4 text-secondary text-center font-weight-bold">Change Password</h5>
						<form method="post" action="password/change" id="form-cpassword">
							{!! csrf_field() !!}
							<div class="form-group">
								<input type="password" class="form-control" name="old_password" placeholder="Password"> </div>
							<div class="form-group">
								<input type="password" class="form-control" name="password" placeholder="New Password"> </div>
							<div class="form-group">
								<input type="password" class="form-control" name="password_confirmation" placeholder="Re-enter New Password"> </div>
							<div class="form-group text-center">
								<button class="btn btn-secondary" type="submit">Finish</button>
							</div>
						</form>
        </div>
      </div>
    </div>
  </div>
  
	
	<div class="modal fade" id="modal-add-blog" tabindex="-1" role="dialog" aria-labelledby="modal-add-blog" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content">
				<div class="modal-body">
          <button data-dismiss="modal" aria-label="Close" class="close position-absolute p-2 rounded-circle" style="top:0;right:0; background-color: rgba(255, 255, 255, .5);z-index:1;">
            <img class="" src="{{asset('img/icon/delete.png')}}" height="20"> </button>
						<h5 class="mb-4 text-secondary text-center font-weight-bold">Write a Blog</h5>
						<hr>
						<form method="post" action="blog/post/create" id="form-blog">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label>Title</label>
										<input type="text" class="form-control" name="title" required>
									</div>
									<div class="form-group">
										<label>Category</label>
										<select name="category" required class="w-100">
											@if($blogCategories->count())
												@foreach($blogCategories->get() as $bcategory)
												<option value="{{$bcategory->id}}">{{$bcategory->name}}</option>									
												@endforeach
											@endif
										</select>
									</div>
									<div class="form-group">
										<button class="btn btn-outline-secondary position-relative btn-sm">
											Feature Image <i class="fa fa-camera"></i>
											<input type="file" name="file" onchange="_fileUpload(this)" data-action="post/hero/upload">
										</button>
										<span class="btn btn-outline-danger btn-sm" id="btn-remove-hero-image" style="display:none;margin-top:10px;">
											<i class="fa fa-close"></i>
										</span>
										<figure class="file-container position-relative">
											<img class="img-fluid img-thumbnail" id="post-hero-image">
											<div class="upload-progress"></div>
											<input type="hidden" name="hero_image">
										</figure>
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group">
										<label>Description</label>
										<textarea name="description" class="form-control editor"></textarea>
									</div>
								</div>
							</div>
							<div class="form-group text-center">
								<hr class="mt-1">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						
						</form>
				</div>
			</div>
		</div>
	</div>
	
  <div class="modal fade" id="modal-cavatar" tabindex="-1" role="dialog" aria-labelledby="modal-avatar" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <button data-dismiss="modal" aria-label="Close" class="close position-absolute p-2 rounded-circle" style="top:0;right:0; background-color: rgba(255, 255, 255, .5);z-index:1;">
            <img class="" src="{{asset('img/icon/delete.png')}}" height="20"> </button>
          
						<h5 class="mb-4 text-secondary text-center font-weight-bold">Change avatar</h5>
						<div class="image-editor text-center">
							<div class="cropit-preview mx-auto mt-5 mb-2"></div>
							<div class="row">
								<div class="col-md-6">
									<span class="rotate-ccw"><i class="fa fa-rotate-left"></i></span>
									<span class="rotate-cw"><i class="fa fa-rotate-right"></i></span>
								</div>
								<div class="col-md-6">
									<input type="range" class="cropit-image-zoom-input">
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 text-right">
									<button class="btn btn-outline-secondary btn-sm position-relative w-75">
										Choose Image <i class="fa fa-image"></i>
										<input type="file" class="cropit-image-input">
									</button>
								</div>
								<div class="col-md-6">
									<form action="profile/photo" method="post" id="form-avatar">
										<input type="hidden" name="uid" value="{{ Auth::user()->id }}" class="uid">
										<input type="hidden" name="avatar" id="avatar">
										<button class="btn btn-secondary btn-sm export w-75" type="submit">Save</button>
									</form>
								</div>
							</div>
						</div>
        </div>
      </div>
    </div>
  </div>
  
@stop

@push('scripts')
	<script src="vendor/cropit/dist/jquery.cropit.js"></script>
	<script>
	$(function(){
		
		let _date = new Date();
		$('input.datepicker').datepicker({
				format: 'yyyy-mm-dd',
				endDate : (_date.getFullYear() - 10)+'-12-31'
		});

		$.validate({
			modules: 'html5'
		})
		$(document).on('click', '.btn-enable-form', function(){
			let $form = $(this).parent('form');
			
			$.each($form.find('input, select, textarea'), function(i, el){
				$(el).prop('disabled', false);
				$(el).prop('readonly', false);
				$(el).removeClass('cover-flat');
				if($(el).is('select') && $(el).hasClass('selectized')){
					$(el).selectize()[0].selectize.enable();
				}
			});
			
			$(this).removeClass('btn-enable-form').addClass('btn-disable-form');
			$(this).find('i')
				.removeClass('fa-pencil').addClass('fa-check');
			
		});
		$(document).on('click', '.btn-disable-form', function(){
			let $form = $(this).parent('form');
			$form.submit();
			
			$.each($form.find('input, select, textarea'), function(i, el){
				if($(el).attr('type') == 'url'){
					$(el).prop('readonly', true);
				}else{
					$(el).prop('disabled', true);
				}
				if($(el).is('select') && $(el).hasClass('selectized')){
					$(el).selectize()[0].selectize.disable();
				}
				$(el).addClass('cover-flat');
			});
		
			$(this).removeClass('btn-disable-form').addClass('btn-enable-form');
			$(this).find('i')
				.removeClass('fa-check').addClass('fa-pencil');
				
		});
		
		$(document).on('click', '#btn-remove-hero-image', function(e){
			let $btn = $(this);
			
			$('#post-hero-image').removeAttr('src');
			$('#form-blog input[name="hero_image"]').val('');
			
			setTimeout(function(){
				$btn.slideUp();
			}, 500);
		});
			
		$(document).on('click', 'input[type="url"]', function(){
			if($(this).val() != '' && $(this).attr('readonly')){
				window.open($(this).val())
			}
		});

		$('.image-editor').cropit({
			exportZoom: 1.25,
			imageBackground: true,
			imageBackgroundBorderWidth: 50,
			imageState: {
				src: "{{asset('img/blank-image.jpg')}}",
			},
			onFileChange: (e)=>{
				let $el = $(e.target);
				let $container = $el.parents('.image-editor');
				$container.find('button[type="submit"]').prop('disabled', false);
			},
			onImageError: (error) => {
				alert(error.message)
			}
		});
		$('.rotate-cw').click(function() {
			$('.image-editor').cropit('rotateCW');
		});
		$('.rotate-ccw').click(function() {
			$('.image-editor').cropit('rotateCCW');
		});

		$('.export').click(function() {
			var imageData = $('.image-editor').cropit('export');
			$('#form-avatar #avatar').val(imageData);
			$('#form-avatar').submit();
		});

		setTimeout(function(){
			_tinymce('textarea.editor');
		}, 700);
	});

	</script>
@endpush