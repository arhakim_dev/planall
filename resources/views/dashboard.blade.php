@extends('adminlte::page')

@section('title', 'Plan All')

@section('content_header')
    <!-- <h1>Dashboard</h1>-->
@stop

@section('content')
	<transition name="view" mode="out-in" appear>
		<router-view :key="$route.fullPath"></router-view>
	</transition>
@stop