@extends('page', ['body_class' => '', 'footerStyle' => ''])

@section('content')
	
<div class="py-5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="card shadow mt-3">
					<div class="card-header bg-white"><h4 class="title">{{$service->name}} - Map</h4></div>
					<div class="card-body">
						<p class="card-text fs-2">{{$service->address}}</p>
						@if(empty($service->longitude) && empty($service->latitude))
							<div class="jumbotron w-100 text-center">Map Is Not Available</div>
						@endif
						<div id="map"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop

@push('scripts')
@if(!empty($service->longitude) && !empty($service->latitude))
<script>
	var map;
	function initMap() {
		let $map = $('#map');
		$map.width($map.parent().width())
		$map.height(450)

		let location = {lat: {{$service->latitude}}, lng: {{$service->longitude}} };
		let map = new google.maps.Map(document.getElementById('map'), {
			center: location,
			zoom: 15
		});
		let marker = new google.maps.Marker({position: location, map: map});
	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&callback=initMap"
async defer></script>
@endif
@endpush