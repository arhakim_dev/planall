@extends('page', ['body_class' => 'service-listing', 'footerStyle' => 'margin-top:-145px;'])

@section('content')

<div style="background-image: url('img/hero-image-service.png');background-size:cover;background-repeat:no-repeat;" class="hero-area py-5 h-100 overlay">
	<div class="container h-100">
		<div class="row h-100">
			<div class="col-12 text-white text-center h-100 d-flex justify-content-center align-items-center flex-column
pt-5">
				<h2 class="font-weight-bold mt-5 pt-5 fs-10"> {{$category->name}} </h2>
				<p class="font-weight-bold mt-auto"> Choose Other Service
					<br>
					<i class="fa fa-angle-down fa-2x"></i>
				</p>
			</div>
		</div>
	</div>
</div>
@if($diffCategories->count())
<div id="shortcut-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<ul class="nav nav-pills d-flex justify-content-center">
					@foreach($diffCategories->get() as $diffCategory)
					<li class="nav-item m-1 shadow rounded bg-white transition">
						<a class="nav-link text-center" href="{{$diffCategory->url}}">
							<span class="shortcut-icon" style="background-image: url({{$diffCategory->icon}});"></span>
							<span class="shortcut-text">{{$diffCategory->name}}</span>
						</a>
					</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
</div>
@endif
<div class="my-5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="progress-overlay">
						<div><i class="fa fa-spinner fa-spin fa-2x"></i></div>
				</div>
				<div class="card shadow mb-3">
					<div class="card-header bg-white d-flex justify-content-between align-items-center" data-toggle="collapse" data-target="#collapse-about" aria-expanded="false" aria-controls="collapse-about">
						<h4 class="title">Filter</h4>
						<span class="fa fa-chevron-up"></span>
					</div>
					<div class="card-body collapse show" id="collapse-about">
						<form id="form-search-services">
							<div class="row mb-3">
								<div class="col-12 col-md-4">
									<div class="form-group">
										<label class="font-weight-bold text-secondary">Country </label>
										<select name="country" id="countries" placeholder="Select Country" data-selectize-options='{"plugins": ["remove_button"]}'>
											<option value="">All</option>
											@foreach($countries as $country)
											<option value="{{$country->code}}" {{($country->code == 'US')? 'selected' : ''}}>{{$country->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-12 col-md-4">
									<div class="form-group">
										<label class="font-weight-bold text-secondary">City</label>
										<select name="city" id="cities" placeholder="Select City" data-selectize-options='{"plugins": ["remove_button"]}'></select>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				
				<h4 class="section-title text-left mb-2">Sort By</h4>
				<div data-toggle="buttons" class="btn-group btn-secondary-light d-flex mb-5">
					<label class="btn btn-secondary form-check-label flex-fill shadow {{(request('sort') == '' || request('sort') =='alphabetically')? 'active' : ''}}">
						<input type="radio" name="sort" value="alphabetically" class="form-check-input radio-sort" {{(request('sort') == '' || request('sort') =='alphabetically')? 'checked' : ''}}> Alphabetically </label>
					<label class="btn btn-secondary form-check-label flex-fill shadow {{(request('sort') =='ratings')? 'active' : ''}}">
						<input type="radio" name="sort" value="ratings" class="form-check-input radio-sort" {{(request('sort') =='ratings')? 'checked' : ''}}> Ratings </label>
					<label class="btn btn-secondary form-check-label flex-fill shadow {{(request('sort') =='viewed')? 'active' : ''}}">
						<input type="radio" name="sort" value="viewed" class="form-check-input radio-sort" {{(request('sort') =='viewed')? 'checked' : ''}}> Most Viewed </label>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="pb-5">
	<div class="container">
		<div class="row position-relative">
			<div class="progress-overlay">
					<div><i class="fa fa-spinner fa-spin fa-2x"></i></div>
			</div>
			<div class="col-12" id="service-container">
				<i class="fa fa-spinner fa-spin fa-2x" style="display:none;"></i>
			</div>
		</div>
		
    @include('_partials/row-loader', [
      'id' => 'service-loader-container', 'idURL' => 'next-services', 'url' => '#'
    ])
	</div>
</div>
@stop

@push('scripts')
<script>
		let isSearching = false;
	$(function(){
		$('select#countries').on('change', function(e){
			let $el = $(e.target);

			let $city = $('select#cities').selectize()[0].selectize;
			$city.disable();
			$city.clear();
			$city.clearOptions();
			$city.refreshOptions(true);
			$city.load(function(cb) {
				$.get('service/cities?country=' + $el.val())
					.done(function(results) {
							$city.enable();
							cb(results);
					})
					.fail(function(error) {
							getErrorMessage(error);
					})
			});
			search()
		})
		$(document).on('change', 'select#cities', function(e){
			search()
		})

		$(document).on('change clickk','input[type="radio"]', function(){
			search()
		})

	});
	
	function search(){
		if(isSearching) return;
		
		let data = $('#form-search-services').serialize()+'&category={{$category->slug}}'
			+'&sort='+$('input.radio-sort:checked').val()
				
		$('#form-search-services').find('input').prop('disabled', true)
		$('input.radio-sort, select').prop('disabled', true)
		$('.progress-overlay').css('display', 'flex')
		$('#service-container').empty()
		$('i.fa-spinner').show()
		isSearching = true;

		$.get('search/services', data)
			.done(function(response){
				callback(response);
			})
			.fail(function(error){
				alert(getErrorMessage(error))
			})
			.always(function(){
				$('input.radio-sort, select').prop('disabled', false)
				$('i.fa-spinner, .progress-overlay').hide()
				isSearching = false;
			})
	}
	setTimeout(function(){
		search();
	}, 1000)
</script>
@endpush