<div class="card mb-5 shadow" id="service-{{$service->id}}">
	<div class="card-body">
			@if(isset($removable) && $removable == true)
				@if(Auth::check() && Auth::user()->id == $user->id)
			<form method="delete" action="profile/service/remove/{{ $service->id }}">
			<div class="btn btn-secondary btn-float-action rounded-circle position-absolute" onclick="confirm(this.title+'?')?$(this).parent('form').submit():''" title="Remove {{ $service->name }}">
				<i class="fa fa-close"></i>
			</div>
			</form>
				@endif
			@endif
		@if($service->is_featured)
		<img src="img/icon/service-featured.png" class="position-absolute rounded-circle" style="top:10px;right:30px;" title="Featured">
		@endif
		<figure class="shadow rounded-circle p-1 bg-white w-50 w-md-25 w-lg-15 mt-6-offset mx-auto ml-md-3">
			<img src="{{$service->logo}}" class="img-fluid rounded-circle"> </figure>
		<h5 class="card-title font-weight-bold d-flex justify-content-start align-items-center flex-column flex-md-row">
			<a href="{{$service->url}}" class="pr-md-3 pr-0" target="_blank">{{$service->name}}</a>
			@include('_partials/star-rating', ['rating' => $service->rate])
			<div class="ml-md-auto" style="font-weight:normal;">
				<small class="text-secondary">
					<img src="img/icon/eye-blue.png">&nbsp;{{intval($service->views)}} </small>
			</div>
		</h5>
		<div class="row">
			<div class="mb-2 col-md-1 text-center">
				<img src="img/icon/map-marker.png" width="18">
			</div>
			<div class="mb-2 col-12 col-md-11"> {{title_case($service->address)}} </div>
		</div>
		<div class="row">
			<div class="mb-2 col-md-1 text-center">
				<img class="mx-auto" src="img/icon/clock-50.png" width="22"> </div>
			<div class="mb-2 col-12 col-md-11">
				<div class="row">
					@forelse($service->businessHours as $works)
					<div class="col-6 col-md-3 mb-2">
						<div class="font-weight-bold">{{$works->day.': '}}</div>
						@if($works->open == '00:00:00' && $works->close == '00:00:00')
						<small><i>Closed</i></small>
						@elseif($works->open == '00:00:00' && $works->close == '23:59:59')
						<small><i>Open 24 Hours</i></small>
						@else
						<small><i>Open: {{$works->open}}</i></small><br>
						<small><i>Close: {{$works->close}}</i></small><br>
						@endif
					</div>
					@empty
						<p>-</p>
					@endforelse
				</div>
			</div>
		</div>
		<div class="row">
			<div class="mb-2 col-md-1 text-center">
				<img src="img/icon/internet.png" width="18">
			</div>
			<div class="mb-2 col-12 col-md-11">
				<a href="{{$service->website}}" target="_blank">{{$service->website}}</a>
			</div>
		</div>
		<div class="text-center text-secondary pt-2 position-relative">
			<a href="{{$service->url}}" class="btn btn-light rounded-circle btn-outline-secondary px-2 float-right"  target="_blank">
				<i class="fa fa-angle-right"></i>
			</a>
		</div>
	</div>
</div>