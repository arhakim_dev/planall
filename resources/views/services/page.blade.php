@extends('page', ['body_class' => '', 'footerStyle' => ''])

@section('content')

<div class="hero-area h-50 h-md-75 position-relative overlay" style="top: 80px; background-image: url({{$service->hero_image}}); background-size: cover; background-repeat: no-repeat;background-position:center center;"> 

	@if($userHasAccess)
	<div class="position-absolute text-center w-100 file-container" style="top:35%;">
		<button type="button" class="btn btn-outline-light position-relative">Change Image <i class="fa fa-image"></i>
			<input type="file" name="file" accept="image/*" class="file-uploader" data-action="service/upload/hero"/>
		</button>
		<div class="upload-progress"></div>
		<form action="service/{{$service->id}}/update" method="put" id="form-hero-image" class="mt-2" style="display:none;">
			<p>
				<small class="text-white" id="hero-image-file">File</small>&nbsp;
				<i class="fa fa-remove text-danger remove-file cursor-pointer" onclick="_removeFileUploaded(this)" data-action="service/file/remove" data-file=""></i>&nbsp;
				<input type="hidden" name="hero_image">
			</p>
			<button type="submit" class="btn btn-sm btn-secondary">Save</button>
		</form>
	</div>
	@endif
</div>
<div class="position-relative" style="top:80px;">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="card mb-5 shadow mt-6-offset">
					<div class="card-body">
						@if($service->is_featured)
						<img src="img/icon/service-featured.png" class="position-absolute rounded-circle" style="top:10px;right:30px;" title="Featured">
						@endif
						<form action="service/{{$service->id}}/update" method="put">
						@if($userHasAccess)
							<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
						@endif
						<div class="d-flex justify-content-start align-items-center flex-column flex-md-row text-secondary border-bottom border-secondary mb-2">
							<h2 class="card-title font-weight-bold w-75">
								<input type="text" name="name" class="cover-flat text-secondary mb-2 form-control fs-8 text-left" value="{{$service->name}}" disabled placeholder="Service Name" required>
							</h2>
							<span class="ml-md-auto">
								<img src="img/icon/eye-blue.png">&nbsp;{{intval($service->views)}} </span>
						</div>
						<div class="row">
							<div class="col-md-3"> Posted on:
								<br>
								<span class="text-secondary">{{$service->created_at->format('F d, Y')}}</span>
							</div>
							<div class="col-md-3"> Category:
								<br>
								<select class="cover-flat w-100 cursor-pointer text-secondary" disabled name="category_id">
									@foreach($categories as $category)
									<option value="{{$category->id}}" {{($service->category_id == $category->id)? 'selected' : ''}} data-url="{{$category->url}}">{{$category->name}}</option>
									@endforeach
								</select>
							</div>
							<div class="col-md-3"> Rating:
								<br>
								@include('_partials/star-rating', ['rating' => $service->rate])
							</div>
							<div class="col-md-3 text-right">
									@if($service->twitter)
									<a href="{{$service->twitter}}" target="_blank">
										<i class="fa fa-twitter" style="color: #29c7f7;"></i>
									</a>&nbsp;
									@endif
								@if($service->facebook)
								<a href="{{$service->facebook}}" target="_blank">
									<i class="fa fa-facebook" style="color: #3b5997;"></i>
								</a>
								@endif
								<input type="url" class="form-control form-control-sm cover-flat text-secondary clickable text-center d-none mb-1" data-validation="url" data-validation-optional="true" data-validation-error-msg="Incorrect URL format. ex: https://twitter.com" readonly name="twitter" value="{{$service->twitter}}" data-class-enabled="d-block" placeholder="Twitter URL">
								<input type="url" class="form-control form-control-sm cover-flat text-secondary clickable text-center d-none" data-validation="url" data-validation-optional="true" data-validation-error-msg="Incorrect URL format. ex: https://facebook.com" readonly name="facebook" value="{{$service->facebook}}" data-class-enabled="d-block" placeholder="Facebook URL">
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@if(!$isShortlisted && !$userHasAccess)
<div class="row">
	<div class="col-12 text-center">
		@auth
		<form method="post" action="user/service/shortlist" id="form-service-shortlist">
			<input type="hidden" name="service_id" value="{{$service->id}}">
			<button class="btn btn-lg bg-white shadow mt-3 text-secondary font-weight-bold" type="submit">Shortlist Service</button>
		</form>
		@else
		<a href="#modal-login" class="btn btn-lg bg-white shadow mt-3 text-secondary font-weight-bold" data-toggle="modal">Shortlist Service</a>
		@endauth
	</div>
</div>
@endif

<div class="py-5">
	<div class="container">
		<div class="row mb-3">
			<div class="col-12">
				<div class="card shadow mb-3">
					<div class="card-body">
						<form action="service/{{$service->id}}/update" method="put">
						@if($userHasAccess)
							<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
						@endif
							<div class="row">
								<div class="mb-2 col-12 col-md-1 text-center">
									<img src="img/icon/map-marker.png"> </div>
								{{-- <div class="mb-2 col-12 col-md-9 d-flex align-items-center justify-content-center"> --}}
								<div class="mb-2 col-12 col-md-11">
									<textarea name="address" class="cover-flat form-control text-center mb-1 w-100" rows="1" disabled placeholder="Address" required>{{title_case($service->address)}}</textarea>
									<div class="row d-none" data-class-enabled="d-flex">
										<div class="col-md-6">
											<select name="country_code">
												@foreach($countries as $country)
												<option value="{{$country->code}}" {{($country->code == $service->country_code)? 'selected' : ''}}>{{$country->name}}</option>
												@endforeach
											</select>
										</div>
										<div class="col-md-6">
											<select name="city" data-selectize-options='{"create" : true}' placeholder="Select City">
											</select>
										</div>
									</div>
								</div>
								{{-- <div class="mb-2 col-12 d-flex justify-content-center align-items-center col-md-2">
									<p class="card-text">
										<a href="service/{{$service->country_code}}/{{$service->slug}}/map" target="_blank">see map</a>
									</p>
								</div> --}}
							</div>
						</div>
					</form>
				</div>
				<div class="card shadow mb-3">
					<div class="card-body text-center">
						<form action="service/{{$service->id}}/update" method="put">
						@if($userHasAccess)
							<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
						@endif
							<div class="row">
								<div class="mb-2 col-md-1">
									<img class="mx-auto" src="img/icon/ringer.png"> </div>
								<div class="mb-2 col-12 col-md-11 text-center">
									<input type="text" name="contact_phone" class="form-control text-secondary text-center w-100 cursor-pointer cover-flat" value="{{$service->contact_phone}}" placeholder="Data not available" disabled data-validation="custom" data-validation-regexp="^[\+]?[(]?[0-9]{2,3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,11}$" data-validation-optional="true" data-validation-error-msg="Incorrect phone number format" placeholder="Contact Phone" required>
									<small class="text-helper" style="display:none;">* e.g: +00 000 00000000</small>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="card shadow mb-3">
					<div class="card-body text-center">
						<form action="service/{{$service->id}}/update" method="put">
						@if($userHasAccess)
							<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
						@endif
							<div class="row">
								<div class="mb-2 col-md-1">
									<img class="mx-auto" src="img/icon/contacts.png"> </div>
								<div class="mb-2 col-12 col-md-11">
									<label style="display:none;">Contact Name</label>
									<input type="text" name="contact_name" placeholder="Data Not Available" class="cover-flat text-center  form-control" value="{{$service->contact_name}}" disabled >
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="card shadow mb-3">
					<div class="card-body text-center">
						<form action="service/{{$service->id}}/update" method="put">
						@if($userHasAccess)
							<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
						@endif
							<div class="row">
								<div class="mb-2 col-md-1">
									<img class="mx-auto" src="img/icon/museum.png"> </div>
								<div class="mb-2 text-center col-12 col-md-11">
									<label style="display:none;">Founding Year</label>
									<input type="number" min="1000" max="{{date('Y')}}" maxlength="4" name="founding_year" value="{{$service->founding_year}}" class="form-control cover-flat text-center no-arrows" disabled oninput="this.value=this.value.slice(0,this.maxLength)" placeholder="Data Not Available">
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="card shadow mb-3">
					<div class="card-body text-center">
						<form action="service/{{$service->id}}/update" method="put">
						@if($userHasAccess)
							<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
						@endif
							<div class="row">
								<div class="mb-2 col-md-1">
									<img class="mx-auto" src="img/icon/internet.png"> </div>
								<div class="mb-2 text-center col-12 col-md-11">
									<label style="display:none;">Website</label>
									<input type="url" class="form-control cover-flat text-secondary clickable text-center" data-validation="url" data-validation-optional="true" data-validation-error-msg="Incorrect URL format. ex: https://website.com" readonly name="website" value="{{$service->website}}" placeholder="Data Not Available">
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="card shadow mb-3">
					<div class="card-header bg-white d-flex justify-content-between align-items-center" data-toggle="collapse" data-target="#collapse-about" aria-expanded="false" aria-controls="collapse-about">
						<h4 class="title">About</h4>
						<span class="fa fa-angle-up"></span>
					</div>
					<div class="card-body collapse show" id="collapse-about">
						<form action="service/{{$service->id}}/update" method="put">
						@if($userHasAccess)
							<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
						@endif
						<textarea name="description" class="cover-flat form-control text-justify" rows="3" disabled placeholder="Data Not Available">{{$service->description}}</textarea>
						</form>
					</div>
				</div>
				<div class="card shadow mb-3">
					<div class="card-body">
						@if($userHasAccess)
							<a href="#modal-hours" data-toggle="modal" class="btn btn-secondary btn-float-action rounded-circle position-absolute"><i class="fa fa-pencil"></i></a>
						@endif
							<div class="row">
								<div class="mb-2 col-md-1">
									<img class="mx-auto" src="img/icon/clock-50.png"> </div>
								<div class="mb-2 col-md-11">
									<div class="row" id="business-hours-container">
									@forelse($service->businessHours as $works)
										<div class="col-6 col-md-3 mb-2">
											<div class="font-weight-bold">{{$works->day.': '}}</div>
											@if($works->open == '00:00:00' && $works->close == '00:00:00')
											<small><i>Closed</i></small>
											@elseif($works->open == '00:00:00' && $works->close == '23:59:59')
											<small><i>Open 24 Hours</i></small>
											@else
											<small><i>Open: {{$works->open}}</i></small><br>
											<small><i>Close: {{$works->close}}</i></small><br>
											@endif
										</div>
									@empty
										<p>Data not available</p>
									@endforelse
									</div>
								</div>
							</div>
					</div>
				</div>
				<div class="card shadow mb-3">
					<div class="card-body text-center">
						<form action="service/{{$service->id}}/update" method="put">
						@if($userHasAccess)
							<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
						@endif
							<div class="row">
								<div class="mb-2 col-md-3 text-left">
									<span class="font-weight-bold text-secondary">Home Delivery</span>
								</div>
								<div class="mb-2 text-center col-12 col-md-7">
									<select class="cover-flat text-center w-100" disabled name="is_delivery">
										<option value="0" {{($service->is_delivery)? '' : 'selected'}}>No</option>
										<option value="1" {{($service->is_delivery)? 'selected' : ''}}>Yes</option>
									</select>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="card shadow mt-3">
					<div class="card-body">
						<form action="service/{{$service->id}}/update" method="put">
						@if($userHasAccess)
							<div class="btn btn-secondary btn-float-action rounded-circle position-absolute btn-enable-form"><i class="fa fa-pencil"></i></div>
						@endif
							<div class="form-group d-none" data-class-enabled="d-block">
								<span>Longitude : </span>
								<input type="text" class="form-control cover-flat text-left w-25 mb-2 mr-2" name="longitude" value="{{$service->longitude}}" required placeholder="Longitude" disabled>
							</div>
							<div class="form-group d-none" data-class-enabled="d-block">
								<span>Latitude : </span>
								<input type="text" class="form-control cover-flat text-left w-25 mb-2" name="latitude" value="{{$service->latitude}}" required placeholder="Latitude" disabled>
							</div>
						</form>
						@if(empty($service->longitude) && empty($service->latitude))
							<div class="jumbotron w-100 text-center">Map Is Not Available</div>
						@endif
						<div id="map"></div>
					</div>
				</div>

				<hr> </div>
		</div>
		<div class="row">
			<div class="col-12">
				<h2 class="section-title mb-2">Reviews &amp; Ratings</h2>
				<div class="fs-4 text-secondary">Rate Us</div>
				<br>
				<div class="d-flex justify-content-between aligns-item-center mb-3">
					@include('_partials/star-rating', ['rating' => 0])
					@auth
					@if(!$userHasAccess)
					<a href="#modal-review" data-toggle="modal" class="btn btn-primary btn-sm shadow">Add Review</a>
					@endif
					@endauth
				</div>
				<div id="review-container">
					@forelse($service->reviews()->approved()->latest()->take(3)->get() as $review)
					<div class="card rounded mb-5 shadow">
						<div class="card-body">
							<div class="d-flex justify-content-between mb-3">
								@include('_partials/star-rating', ['rating' => $review->rate])
								<small class="text-muted">{{$review->created_at->format('F d, Y - H:ia')}}</small>
							</div>
							<p class="card-text">{{$review->description}}</p>
							@if($review->name)
							<div class="commenter font-weight-bold">
								<a href="#">{{$review->name}}</a>
							</div>
							@endif
							<i class="fa fa-quote-right"></i>
						</div>
					</div>
					@empty
					<span class="text-muted">Data not available</span>
					@endforelse
				</div>
			</div>
		</div>
		@if($service->reviews->count() > 3)
			@include('_partials/row-loader', [
				'id' => 'review-loader-container', 'idURL' => 'next-reviews', 'url' => 'service/reviews/next?offset=3&limit=8'
			])
		@endif
	</div>
</div>

<div class="modal fade" id="modal-review" tabindex="-1" role="dialog" aria-labelledby="modal-review" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="mb-4 text-secondary font-weight-bold">Review {{$service->name}}</h5>
				<form method="post" action="service/review" id="form-review">
					<input type="hidden" name="service_id" value="{{$service->id}}">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<input type="text" class="form-control" name="name" value="{{Auth::check()? Auth::user()->fullName : 'Annonymous' }}" placeholder="Name" required>
							</div>
						</div>
						<div class="col-md-6 text-right">
							<select name="rate" id="review-rate" class="no-selectize">
								<option value="1" selected>1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<textarea name="description" rows="3" class="form-control" placeholder="Write your review here" required></textarea>
					</div>
					<div class="form-group text-center">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="modal-hours" tabindex="-1" role="dialog" aria-labelledby="modal-hours" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="mb-4 text-secondary font-weight-bold">Business Hours</h5>
				<form action="service/{{$service->id}}/update" method="put">
					<p class="alert alert-info fs-1">
						* Off-day / Close = Set open hours to "00:00:00" and close hours to "00:00:00"<br>
						* Open 24 hours = Set open hours to "00:00:00" and close hours to "23:59:59"<br>
					</p>
					@forelse($service->businessHours as $works)
					<div class="row border-bottom mx-1 mb-2" data-day="{{$works->day}}">
						<div class="col-md-2" class="text-md-right">{{$works->day}}</div>
						<div class="col-md-3">
							<div class="form-group">
								<input type="text" name="hours[{{$works->day}}][open]" class="form-control timepicker w-100 open-hours" value="{{$works->open}}" placeholder="Open at" title="Open at">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<input type="text" name="hours[{{$works->day}}][close]" class="form-control timepicker w-100 close-hours" value="{{$works->close}}" placeholder="Closed at" title="Closed at">
							</div>
						</div>
						<div class="col-md-4">
							<div class="custom-control custom-radio">
									<input type="radio" id="closeHours-{{$works->day}}" class="custom-control-input setHours" value="close" name="sethours_{{$works->day}}">
									<label class="custom-control-label" for="closeHours-{{$works->day}}">Close</label>
								</div>
								<div class="custom-control custom-radio">
									<input type="radio" id="24Hours-{{$works->day}}" class="custom-control-input setHours" value="24Hours" name="sethours_{{$works->day}}">
									<label class="custom-control-label" for="24Hours-{{$works->day}}">24 Hours</label>
								</div>
						</div>
					</div>
					@endforeach
					<div class="form-group text-center">
						<button class="btn btn-primary" type="submit">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop

@push('styles')
<link rel="stylesheet" href="vendor/jquery-bar-rating/dist/themes/fontawesome-stars.css">
<link rel="stylesheet" href="vendor/timepicker/jquery.timepicker.css">
@endpush

@push('scripts')
<script src="vendor/jquery-bar-rating/jquery.barrating.js"></script>
<script src="vendor/timepicker/jquery.timepicker.min.js"></script>
<script>
	$(function(){
		$.validate();
		$('.timepicker').timepicker({
				timeFormat: 'H:i:s',
				disableTextInput: true
			})
		$('select#review-rate').barrating({
			theme: 'fontawesome-stars'
		})

		$(document).on('click', '.btn-enable-form', function(){
			let $form = $(this).parent('form');
			
			$.each($form.find('input, select, textarea, .btn-upload'), function(i, el){
				let $el = $(el)
				
				if(i == 0) $el.focus();

				$el.prop('disabled', false);
				$el.prop('readonly', false);
				$el.removeClass('cover-flat');
				
				if($el.is('select') && $el.hasClass('selectized')){
					$el.selectize()[0].selectize.enable();
				}
			});
			
			$.each($form.find('[data-class-enabled]'), function(i, el){
				let $el = $(el)
				$el.addClass($el.data('class-enabled'))
			})
			
			$(this).removeClass('btn-enable-form').addClass('btn-disable-form');
			$(this).find('i')
				.removeClass('fa-pencil').addClass('fa-check');
			
			$form.find('.text-helper, label').show();
		});
		$(document).on('click', '.btn-disable-form', function(){
			let $form = $(this).parent('form');
			$form.submit();
			
			$.each($form.find('input, select, textarea, .btn-upload'), function(i, el){
				let $el = $(el)

				if($el.attr('type') == 'url'){
					$el.prop('readonly', true);
				}else{
					$el.prop('disabled', true);
				}
				
				if($el.is('select') && $el.hasClass('selectized')){
					$el.selectize()[0].selectize.disable();
				}
				$el.addClass('cover-flat');
			});
		
			$.each($form.find('[data-class-enabled]'), function(i, el){
				let $el = $(el)
				$el.removeClass($el.data('class-enabled'))
			})

			$(this).removeClass('btn-disable-form').addClass('btn-enable-form');
			$(this).find('i')
				.removeClass('fa-check').addClass('fa-pencil');
				$form.find('.text-helper, label').hide();
		});

		$('.setHours').on('change', function(e){
			let $el = $(e.target)
			let day = $el.parents('.row').data('day')
			
			if($el.val() == 'close'){
				$('input[name="hours['+day+'][open]"]').val('00:00:00')
				$('input[name="hours['+day+'][close]"]').val('00:00:00')
			}else{
				$('input[name="hours['+day+'][open]"]').val('00:00:00')
				$('input[name="hours['+day+'][close]"]').val('23:59:59')
			}
		})
		$('.open-hours, .close-hours').on('change', function(e){
			let $el = $(e.target)
			let day = $el.parents('.row').data('day')
			let openHours = $('input[name="hours['+day+'][open]"]').val()
			let closeHours = $('input[name="hours['+day+'][close]"]').val()

			if(openHours == '00:00:00' && closeHours == '00:00:00'){
				$('input#closeHours-'+day).prop('checked', true)
			}else{
				$el.parents('.row').find('.setHours').prop('checked', false)
			}
		})

		$('select[name="country_code"]').on('change', function(e){
				let $el = $(e.target);

				let $city = $('select[name="city"]').selectize()[0].selectize;
        $city.disable();
        $city.clear();
        $city.clearOptions();
        $city.refreshOptions(true);
        $city.load(function(cb) {
					$.get('service/cities?country=' + $el.val())
						.done(function(results) {
								$city.enable();
								cb(results);
								console.log(results);
						})
						.fail(function(error) {
								getErrorMessage(error);
						})
        });
		})
		_fileUpload('.file-uploader')
	})

</script>
@if(!empty($service->longitude) && !empty($service->latitude))
<script>
	var map;
	function initMap() {
		let $map = $('#map');
		$map.width($map.parent().width())
		$map.height(450)

		let location = {lat: {{$service->latitude}}, lng: {{$service->longitude}} };
		let map = new google.maps.Map(document.getElementById('map'), {
			center: location,
			zoom: 15
		});
		let marker = new google.maps.Marker({position: location, map: map});
	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&callback=initMap"
async defer></script>
@endif
@endpush