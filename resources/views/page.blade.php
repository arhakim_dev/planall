@extends('layout')
@section('body_class', $body_class)

@section('body')

	@include('_partials.site-header')
	
	@yield('content')
	
	@include('_partials.site-footer')
@stop