let mix = require('laravel-mix');

mix.js('resources/assets/js/app.js', 'public/js/v')
	.extract(['jquery', 'vue', 'axios', 'popper.js'])
	.sourceMaps();
mix.sass('resources/assets/sass/app.scss', 'public/css/v');

mix.scripts([
	'node_modules/jquery/dist/jquery.min.js',
	'node_modules/popper.js/dist/umd/popper.min.js',
	'node_modules/bootstrap/dist/js/bootstrap.min.js',
	'node_modules/select2/dist/js/select2.min.js',
	'node_modules/selectize/dist/js/standalone/selectize.js',
	'node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
	'node_modules/jquery-form-validator/form-validator/jquery.form-validator.min.js',
	'node_modules/jquery-form-validator/form-validator/security.js',
], 'public/js/app.js')
	.sourceMaps();

mix.copyDirectory('node_modules/blueimp-file-upload', 'public/vendor/blueimp-file-upload');
mix.copyDirectory('node_modules/tinymce', 'public/vendor/tinymce');
mix.copyDirectory('node_modules/cropit', 'public/vendor/cropit');
mix.copyDirectory('node_modules/jquery-bar-rating', 'public/vendor/jquery-bar-rating');
mix.copyDirectory('node_modules/font-awesome', 'public/vendor/font-awesome');
mix.copyDirectory('node_modules/chart.js', 'public/vendor/chart.js');
mix.copyDirectory('node_modules/timepicker', 'public/vendor/timepicker');

mix.sass('public/css/theme.scss', 'public/css/app.css').options({processCssUrls: false});
